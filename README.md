# Mibase Members.

This repository is part of the **Mibase Software for Toy Libraries.**

Mibase Members allows members to:

* View and search toys
* Add toys to their favorites list
* Check what they have on loan
* Renew their membership
* Hold & reserver toys
* Make appoints
* Put themselves on the duty roster
* Change their password

## License and Warranty
This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more details <http://www.gnu.org/licenses/>.

### Development with Docker and batect (Experimental)
This setup is for running PHP and Apache within a container.
It uses [batect](https://batect.dev/) to facilitate local development

#### Pre-requisites:

* [JVM](https://java.com/en/download/) (version 8 or newer)
* [Docker](https://docs.docker.com/get-docker/) (Version 18.03.1 or newer)
* [Postgres](https://www.postgresql.org/download/) (Currently tested on version 12.3, The login screen shows a warning but the app works fine) - On Mac: `brew install postgres`
* [Mibase Open System's](https://bitbucket.org/mibase/mibase/src/master/) database and toy images. Mibase members relies on [Mibase Open System's](https://bitbucket.org/mibase/mibase/src/master/) DB and toy images.You need to follow the Mibase Open System setup instructions first.

Once the Mibase's DB is created, follow these steps:

1. Create `html/config.php` using `html/config_sample.php` as the reference. Edit the settings in `config.php` to match your setup.

1. Set these settings in `html/config.php` to:

    * `$app_root = '';`
        (_This is because batect will mount the `html` folder, not the root, as a volume, change the settings in `batect.yml` if you want to configure differently_)

    * `$shared_domain = 'localhost:3001';`
    (_This is because batect will map the container's port `80` to `3001` in the host, change the settings in `batect.yml` if you want to configure differently_)

1. Run `./batect app`

1. Go to [http://localhost:3001/login.php]()