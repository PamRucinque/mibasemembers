<!DOCTYPE html>
<?php include('../mibase_check_login.php'); ?>
<head>
    <?php
    //include('get_library.php');
    $paypal_join = 'Yes';
    $type = '';
    include('../header/head.php');
    ?>
    <title>Payments</title>

    <link href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" rel="stylesheet" />

    <title><?php echo $libraryname; ?> - Paypal Payments Page</title>

</head>

<body onload="on_load();"  >
    <?php include('../header/menu.php'); ?>


    <div class="container-fluid">
        <div class="container">
            <div class="row">
                <div class="col-sm-4 hidden-xs">
                    <?php
                    include('../connect.php');
                    //<img src="https://demo.mibase.com.au/login_image/demo.jpg" alt="Library Logo" width="250px">
                    $logo = '<img class="logo-toy" src="https://' . $subdomain . '.mibase.com.au/login_image/' . $subdomain . '.jpg" alt="Toy Library Logo" width="200px;">';

                    echo '<a href="index.php" title="Registration Form logo">' . $logo . '</a>';
                    ?>
                </div>
                <div class="col-sm-8 col-xs-12" style="text-align: center;">                            
                    <h1><?php echo 'Payments'; ?></h1>


                </div>
            </div>
        </div>
        <?php
        include('../connect.php');
        $model_str = '';
        $borid = $_SESSION['borid'];
        include('functions.php');
        include('get_settings.php');
        include('../home/get_index_info.php');
        include('members.php');
        ?>

        <div  class="container"  style="background-color: whitesmoke;">
            <div class="row">
                <div class="col-sm-4">
                    <?php 
                    if ($_SESSION['settings']['mem_payments'] == 'Yes'){
                        include('paypal.php');
                    }
                    ?>
                </div>
                <div class="col-sm-8">
                    <?php
                    echo '<h4>Payment by EFT</h4><br>';
                    echo $payment_info;
                    ?>
                </div>
            </div>

    </div>
         <div style="height:100px;"></div>

</body>
</html>


