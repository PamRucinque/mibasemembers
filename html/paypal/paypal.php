<?php

$amount = 0;
$timezone = 'Australia/Brisbane';
if ($mem_payments == 'Yes') {
    //$borid = 0;
    if (isset($_POST['a'])) {
        $amount = $_POST['a'];
    } else {
        if (isset($_GET['a'])) {
            $amount = $_GET['a'];
        }
    }

    if ($borid > 0) {
        $timezone = 'Australia/Brisbane';
        $mem = new member($borid, $timezone);


        $ref = $mem->longname . ': ' . $borid;
    } else {
        if (isset($_POST['ref'])) {
            $ref = $_POST['ref'];
        }
    }

    if (isset($_POST['d'])) {
        $type = $_POST['d'];
        if ($type == 'Membership Fees') {
            //$amount = $mem->renewal_fee;
            //$amount = 0;
        }
        $ref .= ': ' . $type;
    } else {
        if (isset($_GET['m'])) {

            $type = $_GET['m'];
            if ($type == 'Gift_Card') {
                include('get_member.php');
                $ref = $p_name . ': ' . $borid . ': ' . $type;
            } else {
                $ref .= ': ' . $type;
            }
        }
    }


    if (floatval($amount) > 0) {
        echo '<h4>Paypal and Credit Card Payments</h4><br>';
        echo '<h4>Amount To Pay: <font color="blue">$' . number_format($amount, 2) . '</font></h4>';
    } else {
        echo '<h4><font color="black">Payment by PayPal or Credit Card</font></h4><br>';
    }
    if ($ref != '') {
        echo '<h4>Reference: <font color="blue">' . $ref . '</font></h4>';
    } else {
        echo '<h4><font color="red">Enter your Member Number in the member# box below, or your surname in the ref box.</font></h4><br>';
    }

    if (($paypal_join == 'Yes') && (floatval($amount) > 0) && ($ref != '')) {
        if ($paypal_email !== '') {
            $paypal_str = paypal($amount, $paypal_email, $ref);
        } else {
            $paypal_str = paypal($amount, $email_from, $ref);
        }

        echo '';
        echo $paypal_str;
    }
    include('form.php');
}
?>
