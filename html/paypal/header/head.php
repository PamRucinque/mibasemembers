<meta charset="utf-8"/>
<meta http-equiv="X-UA-Compatible" content="IE=EDGE,chrome=1"/>
<meta name="viewport" content="width=device-width">
<link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.0.13/css/all.css" integrity="sha384-DNOHZ68U8hZfKXOrtjWvjxusGo9WQnrNx2sqG0tfsghAvtVlRW3tvkXWZh58N9jp" crossorigin="anonymous">
<link rel="shortcut icon" type="image/x-icon" href="https://demo.mibase.com.au/home/favicon.ico" />
<link type="text/css" rel="stylesheet" href="https://fonts.googleapis.com/css?family=Open+Sans:400,600,700|Fjalla+One|Roboto+Slab:300,400,500,700" />
<link rel="stylesheet" href="css/menu.css" />
<link rel="stylesheet" href="css/style.css" />
<link rel="stylesheet" href="css/jquery-ui.min.css" />
<script src='js/jquery-3.0.0.js' type='text/javascript'></script> 
<script src='https://www.google.com/recaptcha/api.js'></script>

