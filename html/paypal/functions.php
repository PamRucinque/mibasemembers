<?php

function paypal($amount, $email_from, $itemname) {
    $paypal_button = '<p>To pay by paypal or by credit card, please press button below. <br /><br /></p>';
    $paypal_button .= '<form name="_xclick" action="https://www.paypal.com/cgi-bin/webscr" method="post">';
    $paypal_button .= '<input type="hidden" name="cmd" value="_xclick">';
    $paypal_button .= '<input type="hidden" name="currency_code" value="AUD">';
    $paypal_button .= '<input type="hidden" name="business" value="' . $email_from . '">';
    $paypal_button .= '<input type="hidden" name="amount" value="' . $amount . '">';
    $paypal_button .= '<input type="hidden" name="item_name" value="' . $itemname . '">';
    $paypal_button .= '<input type="image" src="https://www.paypalobjects.com/en_AU/i/btn/btn_buynowCC_LG.gif" border="0" name="submit" alt="PayPal � The safer, easier way to pay online!">';
    $paypal_button .= '<img alt="" border="0" src="https://www.paypalobjects.com/en_AU/i/scr/pixel.gif" width="1" height="1">';
    $paypal_button .= '</form>';
    return $paypal_button;
}
