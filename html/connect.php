<?php
if (!session_id()) {
    session_start();
}
//These two settings have to be set even though they're not used to create the connection (?)
//include('config.php');
$dbuser = $_SESSION['dbuser'];
$dbpwd = $_SESSION['dbpasswd'];

$connect_str = $_SESSION['connect_str'];
$connect_pdo = $_SESSION['connect_pdo'];
$conn = pg_connect($connect_str);


