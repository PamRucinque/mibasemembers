<?php
include('../mibase_check_login.php');

$connect_str = $_SESSION['connect_str'];
$conn = pg_connect($connect_str);

$memberid = $_SESSION['borid'];
$membername = $_SESSION['firstname'] . ' ' . $_SESSION['surname'];
$library_code = $_SESSION['library_code'];
$reserve_fee = $_SESSION['settings']['reserve_fee'];


$query_trans = "SELECT reserve_toy.*, toys.toyname as toyname, toys.rent, toys.toyname as toyname, reserve_toy.idcat as idcat 
    from reserve_toy 
    left join toys on toys.idcat = reserve_toy.idcat
    WHERE member_id = " . $memberid . " and reserve_toy.status='ACTIVE' ORDER BY date_start;";

//echo $query_trans;
//$numrows = pg_numrows($result);
$count = 1;
$fee = 0;
$result_trans = pg_exec($conn, $query_trans);
$numrows = pg_numrows($result_trans);


if ($numrows > 0) {
    echo '<h2><font color="darkorange"> Reserved Toys </font></h2>';
    echo '<table id="reserves" class="table table-striped table-bordered table-sm table-hover display responsive compact" style="overflow: hidden;" cellspacing="0" width="100%" role="grid">';
    echo '<thead>';
    echo '<tr>';
    echo '<th  data-priority="3">start</th>';
    echo '<th class=""  data-priority="4">end</th>';
    echo '<th class="" data-priority="1">Toy</th>';
    echo '<th data-priority="5" class="wrap">Picture</th>';
    echo '<th class="" data-priority="6">fee</th>';
    echo '<th class="" data-priority="7">paid</th>';
    echo '<th class="" data-priority="2"></th>';
    echo '</thead>';
}
?>


<?php
$total = 0;
$total_fee = 0;


for ($ri = 0; $ri < $numrows; $ri++) {

    $row = pg_fetch_array($result_trans, $ri);
    $reserve_id = $row['id'];
    $img = '';
    if ($row['paid'] == '') {
        $paid = 'No';
    }
    $reserve_borid = $row['member_id'];
    $reserve_idcat = $row['idcat'];
    $reserve_toyname = $row['toyname'];
    $approved = $row['approved'];
    if ($approved == Null) {
        $approved = 'No';
    }


    $file_pic = '../../toy_images/' . $library_code . '/' . strtolower($reserve_idcat) . '.jpg';

    if (file_exists($file_pic)) {
        $img = '<img height="70px" src="../../toy_images/' . $subdomain . '/' . strtolower($reserve_idcat) . '.jpg" alt="toy image">';
    }
    $format_datestart = substr($row['date_start'], 8, 2) . '-' . substr($row['date_start'], 5, 2) . '-' . substr($row['date_start'], 0, 4);
    $format_dateend = substr($row['date_end'], 8, 2) . '-' . substr($row['date_end'], 5, 2) . '-' . substr($row['date_end'], 0, 4);
    $format_created = substr($row['date_created'], 8, 2) . '-' . substr($row['date_created'], 5, 2) . '-' . substr($row['date_created'], 0, 4);


    $toy_link = '<a id="button" href="../toy/toy.php?v=lib&idcat=' . $row['idcat'] . '">' . $row['idcat'] . '</a>';
    $del_link = "<a id='button' class='btn btn-danger' href='delete_reservation.php?id=" . $row['id'] . "'>Delete</a>";
    $link = '../toys/toy.php?idcat=' . $row['idcat'];
    $onclick = "javascript:location.href='" . $link . "'";
    $total_fee = $total_fee + $row['rent'];
    ?>
    <tr>
        <td><?php echo $format_datestart; ?></td>
        <td><?php echo $format_dateend; ?></td>
        <td style="min-width: 150px;"><?php echo $toy_link . ': ' . $reserve_toyname; ?></td>
        <td><?php echo $img; ?></td>
        <td><?php echo $row['rent']; ?></td>
        <td><?php echo $paid; ?></td>
        <td><?php echo $del_link; ?></td>

    </tr>
    <?php
}
echo '</table>';
if ($numrows > 0) {
    echo '<h4 align="right" style="padding-right: 20px;"><font color="red">Total Fee for Reservation Toys: $' . $total_fee . '</font></h4>';
}
?>


