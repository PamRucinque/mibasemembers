<?php
$branch = substr(getcwd(), 22, strpos(getcwd() . '/', '/', 22 + 1) - 22);
include(get_include_path() . '/mibaselive/html/' . $branch . '/mibase_check_login.php');
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8"></meta>
        <title>Mibase Loans</title>
        <link rel="stylesheet" type="text/css" href="view.css" media="all"></link>
        <link type="text/css" href="js/themes/base/jquery.ui.all.css" rel="stylesheet" />
        <script type="text/javascript" src="js/jquery-1.9.0.js"></script>
        <script type="text/javascript" src="js/ui/jquery.ui.core.js"></script>
        <script type="text/javascript" src="js/ui/jquery.ui.datepicker.js"></script>

        <script type="text/javascript">
            $(function () {
                var pickerOpts = {
                    dateFormat: "d MM yy",
                    showOtherMonths: true

                };
                $("#date_roster").datepicker(pickerOpts);
            });
        </script>
        <script>
            function overlay() {
                if ($("#overlay").is(":visible")) {
                    $("#overlay").hide();
                }
            }
        </script>
    </head>




    <?php
    include('../home/header.php');
    include('functions/functions.php');
    if (isset($_SESSION['alert'])) {
        if (trim($_SESSION['alert']) != '') {
            //include('../alerts/overlay.php');
        }
    }
    $_SESSION['alert'] = '';
    //$_SESSION['alert'] = '';
    $memberid = $_SESSION['memberid'];
    $membername = $_SESSION['membername'];
    $subdomain = $_SESSION['subdomain'];
    ?>
    <body id="main_body">
        <div style="padding-left: 20px;">
            <?php
            $str_heading = '<br><h2>My Loans: <font color="blue">' . $memberid . '</font> : ' . $membername . '</h2>';
            echo $str_heading;
            include('../roster/get_member.php');
            echo '<font color="blue">' . $_SESSION['alert'] . '</font>';


            include('loans.php');
            //include('partypack.php');
            include('reserves.php');
            ?>

            <?php
            include('roster.php');
            //include('missing_parts.php');
            ?>
        </div>
    </body>
</html>
