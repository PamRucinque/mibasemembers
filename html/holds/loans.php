<script>
    $(document).ready(function () {
        $('#loans').DataTable({
            "paging": false,
            "lengthChange": false,
            "bFilter": false,

            language: {
                searchPlaceholder: "Search Toys",
                searchClass: "form-control",
                search: "",
            },
            responsive: {
                details: {
                    renderer: function (api, rowIdx, columns) {
                        var data = $.map(columns, function (col, i) {
                            return col.hidden ?
                                    '<tr data-dt-row="' + col.rowIndex + '" data-dt-column="' + col.columnIndex + '">' +
                                    '<td>' + col.title + ':' + '</td> ' +
                                    '<td>' + col.data + '</td>' +
                                    '</tr>' :
                                    '';
                        }).join('');
                        return data ?
                                $('<table/>').append(data) :
                                false;
                    }
                }
            }
        });

    });
</script>
<?php
include('../mibase_check_login.php');

$connect_str = $_SESSION['connect_str'];
$conn = pg_connect($connect_str);

$memberid = $_SESSION['borid'];
$membername = $_SESSION['firstname'] . ' ' . $_SESSION['surname'];
$trans_due = '';

$toy_holds = $_SESSION['settings']['toy_holds'];
$loanperiod = $_SESSION['settings']['loanperiod'];
$mem_times_renew = $_SESSION['settings']['mem_times_renew'];
$mem_renew_overdue = $_SESSION['settings']['mem_renew_overdue'];
$overdue_renew = $_SESSION['settings']['overdue_renew'];
$subdomain = $_SESSION['library_code'];
$missing_str = '';

$trans = "SELECT transaction.* , transaction.idcat as idcat, 
 renew as mem_renew, coalesce(t.reserved,0) as reserve_toy,
array_to_string(
 array(  SELECT (p.type || ': '  || p.description || '<br>')
  FROM parts p
  WHERE p.itemno = transaction.idcat and (p.alertuser)) ,
 ' '
 ) AS missing,
 
(select loan_type from toys where transaction.idcat = toys.idcat) as loan_type,
(select count(id) as onhold from toy_holds where status = 'ACTIVE' and toy_holds.idcat = transaction.idcat) as holds

FROM transaction 
left join (select count(id) as reserved,idcat from reserve_toy where date_start > current_date group by idcat) t on t.idcat = transaction.idcat
WHERE borid = " . $memberid . " 
and return Is Null ORDER BY transaction.created DESC;";

//echo $trans;

$count = 1;
//echo $connection_str;
$trans = pg_exec($conn, $trans);
$x = pg_numrows($trans);
//echo 'number rows' . $x;
$due = strtotime($trans_due);



if ($x > 0) {
    echo '<h2><font color="darkorange"> Toys On Loan </font></h2>';
    echo '<br><font color="red">' . $_SESSION['renew'] . '</font>';
    echo '<table id="loans" class="table table-striped table-bordered table-sm table-hover display responsive compact" style="overflow: hidden;" cellspacing="0" width="100%" role="grid">';
    echo '<thead>';
    echo '<tr>';
    echo '<th data-priority="6">id</th>';
    echo '<th data-priority="5">Loaned</th>';
    echo '<th data-priority="2">Due</th>';
    echo '<th class="text-wrap" data-priority="1">Toy</th>';
    echo '<th data-priority="4" class="wrap">Picture</th>';
    echo '<th data-priority="3">Renew</th>';

    echo '</tr></thead>';
} else {
    if (isset($_SESSION['memberid'])) {
        //echo '<br>This member has no Toys on loan';
    }
}
?>

<?php
$total = 0;

for ($ri = 0; $ri < $x; $ri++) {

    $row = pg_fetch_array($trans, $ri);
    $total = $total + 1;
    if (is_numeric($total)) {
        if ($total % 2 == 0) {
            $bg_color = 'background-color: whitesmoke;';
        } else {
            $bg_color = 'background-color: white;';
        }
    }
    $ts1 = strtotime($row['due']);
    $ts2 = strtotime($row['date_loan']);
    $missing = $row['missing'];
    $days_diff = round(-1 * round(($ts2 - $ts1) / (60 * 60 * 24), 0));
    if ($days_diff < 0) {
        $days_diff = 0;
    }
    //$max = $loanperiod * $mem_times_renew + $loanperiod;
    $max = $loanperiod * $mem_times_renew;
    if ($subdomain == 'stonnington') {
        $max = ($loanperiod + 5) * $mem_times_renew;
    }
    //$weekday = date('l', strtotime($row['date_roster']));
    $img = '';
    $trans_id = $row['id'];

    $missing = '<font color="red">' . $row['missing'] . '</font>';

    $trans_borid = $row['borid'];
    $trans_item = $row['item'] . $found;
    $trans_idcat = $row['idcat'];
    $trans_bornmame = $row['borname'];
    $trans_due = $row['due'];
    $loan_type = $row['loan_type'];
    $format_loan = substr($row['date_loan'], 8, 2) . '-' . substr($row['date_loan'], 5, 2) . '-' . substr($row['date_loan'], 0, 4);
    $format_due = substr($row['due'], 8, 2) . '-' . substr($row['due'], 5, 2) . '-' . substr($row['due'], 0, 4);
    $now = date('Y-m-d');
    $ref_renew = 'renew_toy.php?id=' . $row['id'] . '&&renew=' . $row['times_renew'];

    if (strtotime($now) > strtotime($trans_due) && ($mem_renew_overdue == 'No')) {
        $due_str = '<font color="red" font="strong"> OVERDUE  ' . $format_due . '</font>';
        if ($overdue_renew !== 'Yes') {
            $renew_str = 'Overdue toy cannot renew';
        } else {
            $renew_str = '<a id="button" class="btn btn-primary" href="' . $ref_renew . '">Renew</a><br>' . ' max: ' . $max . ' days: ' . $days_diff;
        }
    } else {
        $due_str = $format_due;
        if ($mem_times_renew > 0) {
            if ((strtoupper($loan_type) == 'GOLD STAR') || ($loan_type == 'JC')) {
                $renew_str = '<font color="red">GOLD STAR AND JUMPING CASTLE <br>TOYS CANNOT BE RENEWED</font>';
            } else {
                if ($days_diff > $max) {
                    $renew_str = '<font color="blue">Cannot Renew, can only renew ' . $mem_times_renew . ' time(s).</font>';
                } else {
                    if ($row['holds'] > 0) {
                        $renew_str = '<font color="blue">Cannot Renew, this toy is on hold.</font>';
                    } else {
                        $renew_str = '<a id="button" class="btn btn-primary" href="' . $ref_renew . '">Renew</a><br>' . ' max: ' . $max . ' days: ' . $days_diff;
                    }
                    if (strtotime($now) > strtotime($trans_due)) {
                        $due_str = '<font color="red" font="strong"> OVERDUE  ' . $format_due . '</font>';
                    }
                }
            }
        } else {
            $renew_str = 'Renews not activated';
        }
    }
    if (($row['reserve_toy'] > 0)) {
        $renew_str = '<font color="red">A RESERVATION TOY <br>CANNOT BE RENEWED</font>';
    }


    $file_pic = '../../toy_images/' . $subdomain . '/' . strtolower($trans_idcat) . '.jpg';

    if (file_exists($file_pic)) {
        $img = '<img style="max-width: 100px;" height="70" src="../../toy_images/' . $subdomain . '/' . strtolower($trans_idcat) . '.jpg" alt="toy image">';
    }
    $loan_str = $format_loan;
    $hold_str = $row['holds'];
    $link_toy = '<a id="button" href="../toy/toy.php?v=lib&idcat=' . $trans_idcat . '">' . $trans_idcat . '</a>';

    if ($missing != '') {
        $mising_str = '<br><font color="red">' . $missing . '</font>';
    }


    $str_missing = get_missing($trans_idcat);
    if ($str_missing != '') {
        //echo '<tr><td colspan="6"><font color="red">' . $str_missing . '</font></td></tr>';
    }
    $link = '../toys/toy.php?idcat=' . $row['idcat'];
    $onclick = "javascript:location.href='" . $link . "'";
    ?>
    <tr>
        <td><?php echo $row['id']; ?></td>
        <td style="max-width: 80px;" ><?php echo $loan_str; ?></td>
        <td style="max-width: 80px;" ><?php echo $due_str; ?></td>
        <td style="max-width: 250px;" class="wrap"><?php echo $link_toy . ': ' . $row['item'] . '<br>' . $str_missing; ?></td>
        <td style="max-width: 100px;" class="text-hide"><?php echo $img; ?></td>
        <td><?php echo $renew_str; ?></td>

    </tr>
    <?php
}
echo '</table>';

function get_missing($idcat) {
    //include ('../connect.php');
    $connect_str = $_SESSION['connect_str'];
    $conn = pg_connect($connect_str);
    $idcat = strtoupper($idcat);
    $query1 = "select * from parts where upper(itemno) = '" . $idcat . "' and (type = 'Missing' or type = 'Found');";
    $result1 = pg_Exec($conn, $query1);
    $numrows = pg_numrows($result1);

    $str = '';
    for ($ri = 0; $ri < $numrows; $ri++) {
        $row = pg_fetch_array($result1, $ri);
        $str .= '<font color="red">' . $row['type'] . ': ' . $row['description'] . '<br>';
    }
    $str = rtrim($str, ",");
    if ($numrows == 0) {
        $str = '</font>';
    }
    //$str = 'Number of rows:' . $numrows;
    return $str;
}

//pg_close($link);
?>


