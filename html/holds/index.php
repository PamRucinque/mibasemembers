<!DOCTYPE html>
<!--[if lt IE 8 ]> <html class="no-js ie7 oldie" lang="en"> <![endif]-->
<!--[if IE 8]> <html class="no-js ie8 oldie" lang="en"> <![endif]-->
<!--[if IE 9]> <html class="no-js ie9" lang="en"> <![endif]-->
<!--[if (gt IE 9)|!(IE)]><!--> <html class="no-jQuery" lang="en"> <!--<![endif]-->
    <head>
        <?php
        include('../header/head.php');
        ?>
        <script>
            function setFocus()
            {
                var msg = document.getElementById("msg").innerText;
                //alert(msg);
                if (msg !== '') {
                    $('#myModal').modal('show');
                }

            }
        </script>

    </head>

    <body  onload="setFocus()">
        <?php include('../header/header.php'); ?>
        <div class="container-fluid">
            <?php
            $button_str = 'OK';
            $blinking = '';
            $java_str = 'Close';
            $str_alert = $_SESSION['renew'];
            $mem_times_renew = $_SESSION['settings']['mem_times_renew'];
            echo '<title>' . $libraryname . '</title>';
            echo '<h3>My Holds: ' . $_SESSION['borid'] . ': ' . $_SESSION['firstname'] . ' ' . $_SESSION['surname'] . '</h3>';
            echo '<font color="blue">' . $_SESSION['alert'] . '</font>';

            include('../roster/get_member.php');

            include('../home/get_index_info.php');

            //include('roster.php');
            ?>

            <div class = "row">
                <div class="col-sm-1"></div>
                <div class = "col-sm-8 col-xs-12">
                    <?php
                    if ($subdomain != 'stonnington') {
                        include('../appointments/rosters/list_holds.php');
                    }
                    ?>
                </div>
                <div class="col-sm-3"></div>
            </div>
            <div class = "row">
                <div class="col-sm-1"></div>
                <div class = "col-sm-8">

                    <?php
                    $toy_holds = $_SESSION['settings']['toy_holds'];
                    //include('loans.php');
                    if ($toy_holds == 'Yes') {
                        include('holds.php');
                    }
                    ?>
                </div>
                <div class="col-sm-3"></div>
            </div>
            <div style="height:100px;"></div>
        </div>
        <div class="container fluid">
            <div id='msg' style='display: block;'><?php echo $str_alert; ?></div>
            <!-- Trigger the modal with a button -->
            <!-- Modal -->
            <div class="modal fade" id="myModal" role="dialog"  data-backdrop="static" data-keyboard="false">
                <div class="modal-dialog">

                    <!-- Modal content-->
                    <div class="modal-content">
                        <div class="modal-header">

                            <h4 class="modal-title">Mibase Alert</h4>
                        </div>
                        <div class="modal-body">
                            <p><?php echo $str_alert; ?></p>
                        </div>
                        <div class="modal-footer">
<?php echo $blinking; ?>
                            <button type="button" class="btn btn-danger" data-dismiss="modal" id="update_terms" name="update_terms" onclick="<?php echo $java_str; ?>"><?php echo $button_str; ?></button>
                        </div>
                    </div>

                </div>
            </div>

        </div>


        <script type="text/javascript" src="../js/menu.js"></script>
        <script type="text/javascript" src="../js/jquery.ui.datepicker.js"></script>
        <script type="text/javascript" src="../js/jquery-ui.js"></script>
        <script type="text/javascript" src="../js/bootstrap.min.js"></script>

    </body>
</html>
<?php
$str_alert = '';
$_SESSION['renew'] = '';


