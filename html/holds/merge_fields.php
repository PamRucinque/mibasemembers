<?php

/* 
 * Copyright (C) 2017 michelle
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

$mem_homepage = str_replace("[longname]", $longname, $mem_homepage);
$mem_homepage = str_replace("[membertype]", $membertype, $mem_homepage);
$mem_homepage = str_replace("[joined]", $format_joined, $mem_homepage);
$mem_homepage = str_replace("[expired]", $format_expired, $mem_homepage);
//$total_value
$mem_homepage = str_replace("[total_value]", $total_value, $mem_homepage);
$mem_homepage = str_replace("[children]", $children, $mem_homepage);
$mem_homepage = str_replace("[next_open]", $next_open, $mem_homepage);
$mem_homepage = str_replace("[balance]", $balance, $mem_homepage);

