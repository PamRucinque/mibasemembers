<script type="text/javascript">
    function edit_log(str) {

        $("#logs").show();
        document.forms['log_form']['id'].value = str;
        var notes = document.getElementById("notes_" + str).innerHTML;
        //alert(notes);
        document.forms['log_form']['notes'].value = notes;
        document.getElementById("notes").focus();

    }
    function new_log() {
        $("#logs").show();
        $("#new_log").hide();
        document.forms['log_form']['id'].value = 0;
        document.getElementById("notes").focus();
    }
    function cancel() {
        $("#logs").hide();
        $("#new_log").show();
    }
    function del_log(str) {
        //alert(str);
        document.getElementById("id_delete").value = str;
        document.getElementById("form_delete").submit();
    }
</script>

<div id="logs" style="display: none;background-color: whitesmoke;padding:10px;">
    <form id="log_form" name="log_form" method="post" action="index.php">
        <div class="row">
            <div class="col-sm-8">
                <label for="notes"><b>Notes:</b></label>
                <input type="text" class="form-control" id="notes" placeholder="Add notes" name="notes" value="">            
            </div>

            <div class="col-sm-1"  style="padding-top: 30px">
                <input type=submit id="submit_new_log" name="submit_new_log" class="btn btn-success" value="Save">
            </div>
            <div class="col-sm-1" style="padding-top: 30px">
                <a type=button id="cancel" name="cancel" style="color: white;" class="btn btn-danger" onclick="cancel();">Cancel</a>
            </div>

        </div>
        <input type="hidden" class="form-control" id="id" name="id" value="">
    </form>
</div>

<form id="form_delete" action="detail.php" method="post">
    <input type="hidden" class="form-control" id="id_delete" placeholder="" name="id_delete" value="">
</form>


