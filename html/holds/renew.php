<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<?php
$branch = substr(getcwd(), 22, strpos(getcwd() . '/', '/', 22 + 1) - 22);
include(get_include_path() . '/mibaselive/html/' . $branch . '/mibase_check_login.php');
include('../home/get_settings.php');

//include('../roster/get_member.php');
?>
<html xmlns="http://www.w3.org/1999/xhtml">
    <head>
        <title>Mibase Members</title>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
        <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0"/> 
        <link rel="apple-touch-icon" href="favicon-114.png" />
        <meta name="apple-mobile-web-app-capable" content="yes" /><!-- hide top bar in mobile safari-->
        <!--<meta name="apple-mobile-web-app-status-bar-style" content="black-translucent" /> translucent top bar -->
        <!--<link rel="stylesheet" type="text/css" media="screen" href="style.css" />-->
        <link rel="shortcut icon" href="/favicon.ico">
            <script type="text/javascript" src="../js/tooltip.js"></script>
            <script>
                function menu() {
                    var sidebar = document.getElementById('sidebar');
                    var main = document.getElementById('main');
                    var top = document.getElementById('heading');
                    if (sidebar.style.visibility === 'visible') {
                        sidebar.style.visibility = 'hidden';
                        //main.style.visibility = 'visible';
                        main.style.padding = '80px 20px 20px 10px';
                        top.style.padding = '0 0 0 10px';


                    } else {
                        sidebar.style.visibility = 'visible';
                        //main.style.visibility = 'hidden';
                        //top.style.visibility = 'visible';
                        main.style.padding = '80px 20px 20px 210px';
                        top.style.padding = '0 0 0 210px';
                        document.getElementById('menu').value = 'Hide Menu';
                    }
                }

            </script>
    </head>
    <?php
    if (!session_id()) {
        session_start();
    }
    if (iphone()) {
        echo '<link rel="stylesheet" type="text/css" href="../toys/css/iphone.css" />';
    } else {
        echo '<link rel="stylesheet" type="text/css" href="../toys/css/ipad.css" />';
    }

    function iphone() {
        if (!isset($_SERVER['HTTP_USER_AGENT'])) {
            return false;
        }
        if (strstr($_SERVER['HTTP_USER_AGENT'], 'iPod') || strstr($_SERVER['HTTP_USER_AGENT'], 'iPhone') || strstr($_SERVER['HTTP_USER_AGENT'], 'iPad') || strstr($_SERVER['HTTP_USER_AGENT'], 'Android')) {
            return true;
        }
        //return strstr($_SERVER['HTTP_USER_AGENT'], 'iPhone') !== false;
    }
    
    $memberid = $_SESSION['memberid'];
    $membername = $_SESSION['membername'];
    $subdomain = $_SESSION['subdomain'];
    $url_css = 'https://mla.mibase.com.au/home/';

    //if (isset($_POST['button_red_login'])) {
    if (isset($_GET['button_red_login'])) {
    
        $hash = update_hash($memberid, $membername, $_SESSION['mypassword']);
        //$alert = $hash;
        include('send_email.php');
        echo 'Your member details have been sent for checking.<br>';
    }

    $view = 1;
    if ($_GET['v'] != '') {
        $view = trim($_GET['v']);
        $_SESSION['v'] = $view;
    }

    //include('../home/header.php');

    //include('../home/get_settings.php');

    $mem_reserves = 'Yes';
    $img_home = '<img src="../css/img/home-icon.png" width="23px" style="padding-top: 2px; padding-left: 2px;">';
    $img_star = '<img src="../css/img/star-icon.png" width="23px" style="padding-top: 2px; padding-left: 2px;">';
    $img_select = '<img src="../css/img/stary-icon.png" width="23px" style="padding-top: 2px; padding-left: 2px;">';


    $str_menu_home = '<li><a href="index.php" class="ico msg"><span class="ico msg">' . $img_home . '</span>My Loans</a></li>';
    $str_menu_renew = '<li><a href="renew.php" class="active"><span class="ico msg" height="40px">' . $img_star . '</span>Renewals</a></li>';

    $view = 2;
    if ($_GET['v'] != '') {
        $view = trim($_GET['v']);
        $_SESSION['v'] = $view;
    }
    ?>
    <body>
        <div id="wrap">
            <div id = "main">
                <div class = "top" id="heading">
                    <h2 class = "title"><?php
                        if (iphone()) {
                            $heading = '<font color="blue">' . $memberid . '</font> : ' . $membername . '';
                        } else {
                            $heading = '<h2>Membership Renewals <font color="blue">' . $memberid . '</font> : ' . $membername . '</h2>';
                        }
                        echo $heading;
                        ?></h2>

                </div>

                <div class="header">

                    <a class="left" href="../password/index.php">Reset Password</a>
                    <a class="middle"  id="menu" onclick="menu()">Menu</a>
                    <a class="right" href="../Logout.php">Logout</a>

                </div><!--header-->

                <div class="content">

                    <tbody>

                        <?php
                        if ($view == 2) {
                            $str_menu_details = '<li><a href="renew.phpv=2"  class="active"><span class="ico msg">' . $img_select . '</span>Renew Membership</a></li>';
                        }
                        echo '<font color="blue">' . $_SESSION['alert'] . '</font>';


                        include('../roster/get_member.php');
                        echo '<strong>Member Type:<font color="blue"> ' . $membertype . '</strong></font><br>';
                        //'Start Membership Period: 18-06-2015 End Membership Period: 18-06-2016.' If membership has expired, show 'Membership Expired [date]. Renew via EFT or cash at a session. Details on our website.'
                        include('get_index_info.php');
                        //echo $payment_info;
                        $now = time(); // or your date as well
                        $exp = strtotime($expired);
                        $diff = ($exp - $now) / (60 * 60 * 24);
                        if ($diff < 0) {
                            echo '<h3><strong><font color="red">Your Membership Expired On: ' . $format_expired . '</font></strong></h3><br>';
                            echo '<br><br><u><h2 style="font-size:24px;color:darkslateblue;">Payment</h2></u><br>';
                            echo $payment_info . '<br>';
                        } else {
                            if ($diff >= 0 && $diff <= 30) {
                                echo '<h3><strong><font color="red">Your Membership is Due to Expire On: ' . $format_expired . '</font></strong></h3>';
                                echo '<u><h2 style="font-size:24px;color:darkslateblue;">Payment</h2></u><br>';
                                echo $payment_info . '<br>';
                            } else {
                                echo '<font color="blue">Start Membership Period: </font>' . $format_start . '<font color="blue"> End Membership Period: </font>' . $format_expired . '<br>';
                            }
                        }
                        if ($member_update == 'Yes') {
                            echo '<u><h2 style="font-size:24px;color:darkslateblue;">Update Member Information</h2></u><br>';
                            include('../payments/member_update.php');
                        }
                        ?> 


                    </tbody>


                </div><!--content-->

            </div><!--main-->


            <div id="sidebar">
                <div class="content">
                    <ul class="nav">
                        <?php
                        echo $str_menu_home;
                        echo $str_menu_renew;


                        $img = '<li style="background-color: white;"><img width="190px" style="padding-left: 10px;" src="../../toy_images/' . $subdomain . '/' . strtolower($idcat) . '.jpg" alt="toy image"></li>';
//echo $img;
                        echo $str_heading;

                        if ($alert != '') {
                            echo '<script language="javascript">';
                            echo 'alert("' . $alert . '")';
                            echo '</script>';
                            $alert = '';
                        }

//echo 'Alert: ' . $_SESSION['xx'];
                        ?>

                    </ul>
                </div>
                <!--content-->


            </div><!--sidebar-->
        </div><!--wrap-->


<!--<script type="text/javascript" src="script.js">-->
    </body>
</html>
<?php
function update_hash($memberid, $username,$password){
     include('../connect.php');
    //$conn = pg_connect($_SESSION['conn']);
     $now = DATE('Y-m-d');
    $hash = $memberid . $username . $password . '  ' . $now;
    $key = md5($hash);
    $sql = "update borwrs set key = '" . $key . "' where id = " . $memberid . ";";
    $result = pg_Exec($conn, $sql);
    return $key;  
}
?>