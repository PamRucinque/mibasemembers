<?php

$branch = substr(getcwd(), 22, strpos(getcwd() . '/', '/', 22 + 1) - 22);
include(get_include_path() . '/mibaselive/html/' . $branch . '/mibase_check_login.php');

$memberid = $_SESSION['memberid'];
include('../connect.php');
include('functions/functions.php');
include('get_template.php');
include('../home/get_settings.php');

$str_message = $template_message;
$strSubject = $email_subject;
if ($email_from == '') {
    $email_from = 'noreply@mibase.com.au';
}
if ($template_email != '') {
    $email_from = $template_email;
}
//echo $str_message;
$query = "SELECT 
surname, firstname,  id, emailaddress,
  partnerssurname,partnersname, pwd, rostertype5,expired, address, suburb, postcode,    
   phone2 as mobile, membertype, key,
 
array_to_string(
 array(  SELECT c.child_name
  FROM children c
  WHERE c.id = p.id) ,
 ','
 ) AS children
FROM borwrs p 
where member_status= 'ACTIVE' and id = " . $memberid . ";";

//echo $query;
$result_list = pg_exec($conn, $query);
$row = pg_fetch_array($result_list);
$strTo = $row['emailaddress'];
$email = $row['emailaddress'];
$firstname = $row['firstname'];
$membertype = $row['membertype'];
$key = $row['key'];
$surname = $row['surname'];
$partnerssurname = $row['partnerssurname'];
$partnersname = $row['partnersname'];
$pwd = $row['pwd'];
$username = $row['rostertype5'];
$expired = $row['expired'];
$borid = $row['id'];
$mobile = $row['mobile'];
$address = $row['address'] . ' ' . $row['suburb'] . ' ' . $row['postcode'];
$children = $row['children'];

if ($surname == $partnerssurname) {
    if ($partnersname == '') {
        $longname = $firstname . ' ' . $surname;
    } else {
        $longname = $firstname . ' and ' . $partnersname . ' ' . $surname;
    }
} else {
    if (($partnersname == '') || ($partnerssurname == '')) {
        $longname = $firstname . ' ' . $surname;
    } else {
        $longname = $firstname . ' ' . $surname . ' and ' . $partnersname . ' ' . $partnerssurname;
    }
}

$correct_link = 'http://' . $subdomain . '.mibase.com.au/php/public/members/update.php?borid=' . $borid . '&&key=' . $key;
$correct_link = '<a href="' . $correct_link . '">Yes, my details are correct</a>';
$agree = 'http://' . $subdomain . '.mibase.com.au/php/public/members/update.php?borid=' . $borid . '&&username=' . $username . '&&pwd=' . $pwd . '&&agree=Yes';
$agree = '<a href="' . $agree . '">Yes, I agree to Terms and Conditions</a>';
$photo = 'http://' . $subdomain . '.mibase.com.au/php/public/members/update.php?borid=' . $borid . '&&username=' . $username . '&&pwd=' . $pwd . '&&photos=Yes';
$photo = '<a href="' . $photo . '">I give permission to take photos.</a>';
$terms = 'http://' . $subdomain . '.mibase.com.au/php/website/index.php?v=10';
$terms = '<a href="' . $terms . '">Terms and Conditions</a>';

$str_message = str_replace("[username]", $username, $str_message);
$str_message = str_replace("[pwd]", $pwd, $str_message);
$str_message = str_replace("[firstname]", $firstname, $str_message);
$str_message = str_replace("[subdomain]", $subdomain, $str_message);
$str_message = str_replace("[longname]", $longname, $str_message);
$str_message = str_replace("[duties_owing]", $duties_owing, $str_message);
$str_message = str_replace("[children]", $children, $str_message);
$str_message = str_replace("[mobile]", $mobile, $str_message);
$str_message = str_replace("[address]", $address, $str_message);
$str_message = str_replace("[email]", $email, $str_message);
$str_message = str_replace("[membertype]", $membertype, $str_message);
$str_message = str_replace("[borid]", $borid, $str_message);
$str_message = str_replace("[correct_link]", $correct_link, $str_message);
$str_message = str_replace("[link_tcs]", $terms, $str_message);
$str_message = str_replace("[agree]", $agree, $str_message);
$str_message = str_replace("[photos]", $photo, $str_message);

if ($template_email != '') {
    $email_from = $template_email;
}
$mibase_email = 'noreply@mibase.com.au';

$headers = "From: " . $libraryname . " <" . $mibase_email . ">" . "\r\n";
$headers .= "Reply-To: <" . $email_from . ">\r\n";
$headers .= "Return-Path: <" . 'info@mibase.com.au' . ">\r\n";
$headers .= "MIME-Version: 1.0" . "\r\n";
$headers .= "Organization: " . $libraryname . "\r\n";
$headers .= "X-Priority: 3\r\n";
$headers .= "X-Mailer: PHP" . phpversion() . "\r\n";
$headers .= "Content-type:text/html;charset=UTF-8" . "\r\n";
$headers .= "Content-Transfer-Encoding: 7bit\n\n";
//$headers .= "--" . $strSid . "\n";
//echo $headers;
$output = mail($strTo, $strSubject, $str_message, $headers);


//$flgSend = send_email($strTo, $strSubject, $str_message, $strHeader, $param);
$subject = 'RENEWALS: ' . $libraryname;
$admin_message = '<h3>' . $memberid . ': ' . $membername . ' has Renewed and checked their Details</h3>';
$admin_message .= '<h3>Below is the message sent:</h3><br><br>';
$admin_message .= $str_message;
//$flagSend = send_email($email_from, $subject, $admin_message, $strHeader, $param);
$flagSend = mail($email_from, $subject, $admin_message, $headers);
//echo $flgSend . $str_message;
$alert = 'Message Sent';

if ($output) {

    $str_summary = '<font color="blue"><strong><br> Thank you, Your Message has been sent.</strong></font> <br><br>';
}



