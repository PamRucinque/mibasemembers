<?php
$branch = substr(getcwd(),22,strpos(getcwd() . '/','/', 22+1) - 22);
include(get_include_path() . '/mibaselive/html/' . $branch . '/mibase_check_login.php');

include('../connect.php');

$pdo = new PDO($connect_pdo, $dbuser, $dbpasswd);
$template = 'member_update';
$sql = "SELECT * from template WHERE type_template = ?";

$sth = $pdo->prepare($sql);
$array = array($template);
$sth->execute($array);

$result = $sth->fetchAll();
$stherr = $sth->errorInfo();
$numrows = $sth->rowCount();

for ($ri = 0; $ri < $numrows; $ri++) {
    $row = $result[$ri];
    $id = $row['id'];
    $email_subject = $row['subject'];
    $template_message = $row['message'];
    $type_template = $row['type_template'];
    $template_email = $row['template_email'];
}

if ($stherr[0] != '00000') {
    $_SESSION['login_status'] .= "An  error occurred.\n";
    $_SESSION['login_status'] .= 'Error' . $stherr[0] . '<br>';
    $_SESSION['login_status'] .= 'Error' . $stherr[1] . '<br>';
    $_SESSION['login_status'] .= 'Error' . $stherr[2] . '<br>';
}


