<div class="modal fade" id="history_p6" role="dialog"  data-backdrop="static" data-keyboard="false">
    <div class="modal-dialog">

        <!-- Modal content-->
        <div class="modal-content"  style="width: 660px;">
            <div class="modal-header">
                <div class="col-sm-9">
                    <div id="history_title"></div>
                </div>
                <div class="col-sm-3">
                    <a class="btn btn-danger" style="color:white;" onclick="cancel_history();">Close</a>
                </div>
            </div>


            <div class="modal-body">

                <div class="row">
                    <div class="col-sm-12">

                    </div>

                    <div class="col-sm-12" style="padding-bottom: 10px;">
                        <div id="loan_hist" style="max-width: 620px;height:500px; padding-right:20px;"></div>                   
                    </div>
                </div>

            </div>
            <div class="modal-footer">

            </div>

        </div>
    </div>
</div>
