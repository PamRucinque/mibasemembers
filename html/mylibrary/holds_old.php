<script>
    $(document).ready(function () {
        $('#holds').DataTable({
            "lengthChange": true,
            "pageLength": 50,
            "pagingType": "full_numbers",
            "lengthChange": false,
            "bFilter": false,
            language: {
                searchPlaceholder: "Search Toys",
                searchClass: "form-control",
                search: "",
            },
            responsive: {
                details: {
                    renderer: function (api, rowIdx, columns) {
                        var data = $.map(columns, function (col, i) {
                            return col.hidden ?
                                    '<tr data-dt-row="' + col.rowIndex + '" data-dt-column="' + col.columnIndex + '">' +
                                    '<td>' + col.title + ':' + '</td> ' +
                                    '<td>' + col.data + '</td>' +
                                    '</tr>' :
                                    '';
                        }).join('');
                        return data ?
                                $('<table/>').append(data) :
                                false;
                    }
                }
            }
        });

    });
</script>


<?php
include('../mibase_check_login.php');
if (isset($_POST['submit_new_log'])) {
    include('edit_hold.php');
}
$connect_str = $_SESSION['connect_str'];
$conn = pg_connect($connect_str);
$hold_str_save = '';

//include('../connect.php');
//include('../home/get_settings.php');
//date_end > (current_date - interval '7 days')
$memberid = $_SESSION['borid'];
$toy_hold_period = $_SESSION['settings']['toy_hold_period'];
$total = 0;

$trans = "SELECT toy_holds.*, toys.toyname, toys.idcat as idcat, COALESCE(journal.amount,0) as fee 
from toy_holds
left join toys on toy_holds.idcat = toys.idcat
left join journal on journal.icode = toy_holds.idcat and toy_holds.borid = journal.bcode 
and journal.category = 'Hold Toy'
and toy_holds.date_start = journal.datepaid
where borid = " . $memberid .
        " AND (toy_holds.status != 'DELETED' and toy_holds.status != 'LOANED' and toy_holds.status != 'EXPIRED') " .
        " order by date_start;";
//echo $trans;
$x = 0;
$count = 1;
$fee = 0;
$trans = pg_exec($conn, $trans);
$x = pg_numrows($trans);
//echo 'number rows' . $x;
$due = strtotime($trans_due);
$str_holds = '';
$str_delete = '';

$missing = '';
if ($x > 0) {
    echo '<h2><font color="darkorange">Toy Holds</font></h2>';

    echo $hold_str_save;
    include('log_form.php');
    echo '<table id="holds" class="table table-striped table-bordered table-sm table-hover display responsive compact text-nowrap" style="overflow: hidden;" cellspacing="0" width="100%" role="grid">';
    echo '<thead>';
    echo '<tr>';
    //echo '<td></td>';
    echo '<th data-priority="3">date/expires</th>';
    echo '<th data-priority="1" class="wrap ">toy</th>';
    echo '<th data-priority="6" class="wrap">picture</th>';
    echo '<th data-priority="7">fee/paid</th>';
    echo '<th data-priority="8">notes</th>';
    echo '<th data-priority="2"></th>';
    echo '<th data-priority="3">status</th>';
    echo '</thead>';
}
?>

<?php
for ($ri = 0; $ri < $x; $ri++) {
    //echo "<tr>\n";
    $row = pg_fetch_array($trans, $ri);
    $total = $total + 1;
    if (is_numeric($total)) {
        if ($total % 2 == 0) {
            $bg_color = 'background-color: whitesmoke;';
        } else {
            $bg_color = 'background-color: white;';
        }
    }

    $hold_id = $row['id'];
    $hold_item = $row['toyname'];
    $hold_idcat = $row['idcat'];
    $hold_start = $row['date_start'];
    $hold_end = $row['date_end'];

    $format_start = substr($row['date_start'], 8, 2) . '-' . substr($row['date_start'], 5, 2) . '-' . substr($row['date_start'], 0, 4);
    $format_end = substr($row['date_end'], 8, 2) . '/' . substr($row['date_end'], 5, 2) . '/' . substr($row['date_end'], 0, 4);
    $now = date('Y-m-d');

    $ref_hold1 = 'delete_hold.php?th=' . $row['id'] . '&idcat=' . $row['idcat'];

    if ($hold_end != null) {
        if (strtotime($now) > strtotime($hold_end)) {
            $due_str = '<font color="red" font="strong">EXPIRED</font>';
        } else {
            $due_str = $format_end;
        }
    } else {
        $due_str = '';
    }
    if ($row['status'] == 'READY') {
        $status = '<strong><font color="red">' . $row['status'] . '</font></strong>';
    } else {
        $status = $row['status'];
    }
    $file_pic = '../../toy_images/' . $subdomain . '/' . strtolower($row['idcat']) . '.jpg';

    if (file_exists($file_pic)) {
        $img = '<img height="70px" src="../../toy_images/' . $subdomain . '/' . strtolower($row['idcat']) . '.jpg" alt="toy image">';
    }
    $link_toy = '<a id="button" href="../toy/toy.php?v=lib&idcat=' . $row['idcat'] . '">' . $row['idcat'] . '</a>';

    $str_delete = "<a id ='button' class='btn btn-danger' href='" . $ref_hold1 . "'>Delete</a>";
    $total = $total + $row['fee'];
    $link = '../toys/toy.php?idcat=' . $row['idcat'];
    $onclick = "javascript:location.href='" . $link . "'";
    $fee = $fee + $row['fee'];
    $edit_btn = ' <a class="btn btn-primary" style="color:white;" onclick="edit_log(' . $row['id'] . ');">Edit Notes</a>';
    ?>
    <tr>

        <td><?php echo $format_start . '<br>' . $due_str; ?></td>
        <td><?php echo $link_toy . ': ' . $hold_item; ?></td>
        <td><?php echo $img; ?></td> 
        <td><?php echo $row['fee'] . '<br>' . $row['paid']; ?></td>
        <td id="notes_<?php echo $row['id']; ?>"><?php echo $row['notes']; ?></td>
        <td><?php echo $str_delete; ?></td>
        <td><?php echo $edit_btn . '<br>' . $status; ?></td>

    </tr>
    <?php
}
echo '</table>';

if ($x > 0) {
    echo '<h4 align="right" style="padding-right: 20px;"><font color="red">Total Fee for Hold Toys: $' . $fee . '</font></h4>';
}






