<script>
    $(document).ready(function () {
        $('#reserves').DataTable({
            "paging": false,
            "lengthChange": false,
            "bFilter": false,
            "bInfo": false,
            language: {
                searchPlaceholder: "Search Toys",
                searchClass: "form-control",
                search: "",
            },
            responsive: {
                details: {
                    renderer: function (api, rowIdx, columns) {
                        var data = $.map(columns, function (col, i) {
                            return col.hidden ?
                                    '<tr data-dt-row="' + col.rowIndex + '" data-dt-column="' + col.columnIndex + '">' +
                                    '<td>' + col.title + ':' + '</td> ' +
                                    '<td>' + col.data + '</td>' +
                                    '</tr>' :
                                    '';
                        }).join('');
                        return data ?
                                $('<table/>').append(data) :
                                false;
                    }
                }
            }
        });

    });
</script>


<?php
include('../mibase_check_login.php');

$connect_str = $_SESSION['connect_str'];
$conn = pg_connect($connect_str);
$connect_pdo = $_SESSION['connect_pdo'];

$str_parts = '';
$alert_parts = '';
$alert_parts_txt = '';
$table_parts = '';
$idcat = $_SESSION['idcat'];
$subdomain = $_SESSION['library_code'];

$pdo = new PDO($connect_pdo, $dbuser, $dbpasswd);

$sql = "select parts.*,
toys.toyname as toyname,
toys.idcat as idcat, parts.type as type,
typepart.picture as warning
 from parts 
 LEFT JOIN typepart on (parts.type = typepart.typepart) 
 LEFT JOIN toys on (toys.idcat = parts.itemno) 
    where borcode = ? ORDER by typepart.picture desc, parts.type, parts.datepart;";

$sth = $pdo->prepare($sql);
$array = array($_SESSION['borid']);
$sth->execute($array);

$result = $sth->fetchAll();
$stherr = $sth->errorInfo();
$numrows = $sth->rowCount();

if ($stherr[0] != '00000') {
    $error_msg .= "An  error occurred.\n";
    $error_msg .= 'Error' . $stherr[0] . '<br>';
    $error_msg .= 'Error' . $stherr[1] . '<br>';
    $error_msg .= 'Error' . $stherr[2] . '<br>';
}

$status_txt = Null;





if ($numrows > 0) {
    echo '<div id="missing_bm"></div><br><h2><font color="darkorange"> Missing and Found Pieces </font></h2>';
    echo '<table id="reserves" class="table table-striped table-bordered table-sm table-hover display responsive compact" style="overflow: hidden;" cellspacing="0" width="100%" role="grid">';
    echo '<thead>';
    echo '<tr>';
    echo '<th class="th-sm all" data-priority="3">date lost</th>';
    echo '<th class=""  data-priority="4">status</th>';
    echo '<th class="" data-priority="2">Toy</th>';
    echo '<th data-priority="5" class="wrap ">Picture</th>';
    echo '<th class="" data-priority="1">piece</th>';
    echo '</thead>';
}
$page_break = 'No';

for ($ri = 0; $ri < $numrows; $ri++) {
//echo "<tr>\n";
    $row = $result[$ri];
    $total = $total + 1;
    if (is_numeric($total)) {
        if ($total % 2 == 0) {
            $bg_color = 'background-color: whitesmoke;';
        } else {
            $bg_color = 'background-color: white;';
        }
    }
    //$subdomain = $_SESSION['subdomain'];
    $format_datepart = substr($row['datepart'], 8, 2) . '-' . substr($row['datepart'], 5, 2) . '-' . substr($row['datepart'], 0, 4);
    $alert_txt = '';

    $format_datepart = substr($row['datepart'], 8, 2) . '-' . substr($row['datepart'], 5, 2) . '-' . substr($row['datepart'], 0, 4);
    $alert_txt = null;
    if ($row['alertuser'] == 't') {
        $alert_txt .= 'Yes';
    } else {
        $alert_txt .= 'No';
    }

    if (($row['warning'] != '') && ($page_break == 'No')) {
        $table_parts .= '<tr><td colspan="4"><font color="#330066"><strong>Warnings</font></strong></td></tr>';
        $page_break = 'Yes';
    }


    if ($row['description'] != '') {
        if ($row['warning'] != '') {
            //$table_parts .= '<tr><td colspan="3"><font color="red">  ' . $row['description'] . '</font></td></tr>';
        } else {

            $link_toy = '<a id="button" href="../toy/toy.php?v=lib&idcat=' . $row['idcat'] . '">' . $row['idcat'] . '</a>';
            $file_pic = '../../toy_images/' . $subdomain . '/' . strtolower($row['idcat']) . '.jpg';

            if (file_exists($file_pic)) {
                $img = '<img height="70px" src="../../toy_images/' . $subdomain . '/' . strtolower($row['idcat']) . '.jpg" alt="toy image">';
            } else {
                $img = '';
            }
            $link = '../toys/toy.php?idcat=' . $row['idcat'];
            $onclick = "javascript:location.href='" . $link . "'";
            ?>
            <tr>
                <td><?php echo $format_datepart; ?></td>
                <td><font color="red"><?php echo $row['type']; ?></font></td>
                <td><?php echo $link_toy . ': ' . $row['toyname']; ?></td>
                <td><?php echo $img; ?></td>
                <td><?php echo $row['description']; ?></td>

            </tr>
            <?php
        }
    }
}
if ($numrows > 0) {
    echo '</table>';
}
?>

