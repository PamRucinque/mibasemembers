<script>
            $(document).ready(function () {
            $('#files').DataTable({
                "lengthChange": true,
                "pageLength": 50,
                "pagingType": "full_numbers",
                "lengthChange": false,
                "bFilter": false,
                language: {
                    searchPlaceholder: "Search Toys",
                    searchClass: "form-control",
                    search: "",
                },
                responsive: {
                    details: {
                        renderer: function (api, rowIdx, columns) {
                            var data = $.map(columns, function (col, i) {
                                return col.hidden ?
                                        '<tr data-dt-row="' + col.rowIndex + '" data-dt-column="' + col.columnIndex + '">' +
                                        '<td>' + col.title + ':' + '</td> ' +
                                        '<td>' + col.data + '</td>' +
                                        '</tr>' :
                                        '';
                            }).join('');
                            return data ?
                                    $('<table/>').append(data) :
                                    false;
                        }
                    }
                }
            });

        });
</script>
<?php
include('../mibase_check_login.php');

$connect_str = $_SESSION['connect_str'];
$conn = pg_connect($connect_str);
$result_txt = '';


$sql = "select * from files where access = 'member' order by type_file , lastupdate desc;";
$result_list = pg_exec($conn, $sql);
$numrows = pg_numrows($result_list);
if ($numrows > 0) {
    $result_txt = '<h2><font color="darkorange">Uploaded Files</font></h2>';
}
$result_txt .= '<table id="files" border="1" class="table table-striped table-bordered table-sm table-hover display responsive compact">';
for ($ri = 0; $ri < $numrows; $ri++) {
    $filename = pg_fetch_array($result_list, $ri);
    $filename['filename'] = trim($filename['filename']);

    $result_txt .= '<tr>';
    $result_txt .= '<td width = "5%">' . $filename['id'] . '</td>';
    //$result_txt .= '<td width = "8%">' . $filename['filename'] . '</td>';
    $result_txt .= '<td width = "70%">' . $filename['description'] . '</td>';
    //$result_txt .= '<td width = "7%">' . $filename['type_file'] . '</td>';
    //$result_txt .= '<td width = "10%">' . $filename['access'] . '</td>';
    //$pic = "'../../../toy_images/" . $subdomain . "/news/" . $filename['filename'] . "'";
    //$file_path = '<td width="10%"><img height="50px" width="50px" src="../../../toy_images/' . $subdomain . '/news/' . $filename['filename'] . '" onmouseover="showtrail(175,220, ' . $pic . ');" onmouseout="hidetrail();"></td>';

    $ref2 = 'delete.php?id=' . $filename['filename'];
    $result_txt .= '<td width="10%"><a id="button" target="_blank" href="../../toy_images/' . $subdomain . '/news/' . $filename['filename'] . '">Open</a></td>';
    //$result_txt .= '<td align="center"><a class ="button_small_red" href="' . $ref2 . '">Delete</a></td>';
    $result_txt .= '</tr>';
}

$result_txt .= '</table>';
echo $result_txt;

