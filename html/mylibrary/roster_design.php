
<div class="col-md-3">
    <div class="container-fluid roster_design">
        <div class="row">
            <div class="col-md-12">
                <h4 class="text-uppercase" style="color: darkorange;"><strong><i class="fas fa-calendar-alt"></i> <?php echo $day_str; ?></strong></h4>
                <ul class="list-inline">
                    <li class="list-inline-item" style="color: darkblue;"><strong><i class="far fa-calendar-alt"></i> <?php echo $week_str; ?></strong></li>
                </ul>
                <?php echo $str_detail; ?>
            </div>
        </div>
    </div>
</div>


<?php

