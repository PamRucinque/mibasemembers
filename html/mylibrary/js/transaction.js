function loan_history_popup(mid) {
    $('#history_p6').modal('show');
    var url = "data/get_loan_history.php"; // the script where you handle the form input.
    $.ajax({
        type: "POST",
        url: url,
        data: {
            mid: mid
        }, // serializes the form's elements.
        success: function (data)
        {
            if (data.result === 'ok') {
                
                get_loan_history_popup(data);
            } else {
                alert('no History Records');
            }


        }
    });
}



function cancel_history() {
    $("#loan_hist").html('');
    $('#history_p6').modal('hide');
}
function get_loan_history_popup(data) {
    if ('data' in data) {
        len = data.data.length;

        //var max_toys = document.getElementById("max_toys").value;
        //var total_toys = '<h4 style="color:darkblue;">Total Toys on Loan: ' + len + ' of ' + max_toys + '</h4>';
        //$("#total_toys").html(total_toys);
        var table_loans = '<h4>History of Toys Borrowed</h4>';
        table_loans += "<table id='tbl_loan_history' class='table table-striped table-bordered table-sm table-hover table-responsive-md dataTable compact order-column' cellspacing='0' width='100%' role='grid'>";
        table_loans += "<thead><tr>";
        table_loans += "<th class='th-sm'>Toy#</th>";
        table_loans += "<th class='th-sm'>Toyname</th>";
        table_loans += "<th class='th-sm'>days</th>";
        table_loans += "<th class='th-sm'>Loaned</th>";
        table_loans += "<th class='th-sm'>Due</th>";
        table_loans += "<th class='th-sm'>Returned</th>";
        table_loans += "<th class='th-sm'>id</th>";

        table_loans += "</tr></thead>";
        for (index = 0, len; index < len; ++index) {
            var row = data.data[index]
            table_loans += "<tr  class=\"member\">";
            table_loans += "<td ><b>" + row[1] + "</b>" + "</td>";
            table_loans += "<td  style=\"min-width:150px;\">" + row[3] + "</td>";
            if (parseInt(row[7], 10) < 0) {
                table_loans += "<td style=\"color:red;min-width:50px;\">" + -1 * (parseInt(row[7], 10)) + "</td>";
            } else {
                table_loans += "<td style=\"color:blue;min-width:50px;\">" + row[7] + "</td>";
            }
            table_loans += "<td   style=\"min-width:70px;\" date-sort=\"" + row[10] + "\" >" + row[4] + "</td>";
            table_loans += "<td style=\"min-width:70px;\" date-sort=\"" + row[6] + "\" >" + row[5] + "</td>";
            if (row[7] === null) {
                row[7] = '';
            }

            table_loans += "<td date-sort=\"" + row[8] + "\">" + row[9] + "</td>";



            table_loans += "<td>" + row[0] + "</td>";


            table_loans += "</tr>";
        }
        // table_loans += table_btns;
        table_loans += "</tbody></table>";

        $("#loan_hist").html(table_loans);

        $('#tbl_loan_history').DataTable({
            "lengthChange": false,
            "bPaginate": false,
            "pageLength": 10,
            "info": true,
            "pagingType": "simple_numbers",
            "bFilter": true,
            "order": [[6, "desc"]],

            language: {
                searchPlaceholder: "Search History",
                searchClass: "form-control",
                search: "",
            },
            responsive: true,
            fixedHeader: {
                header: true,
                footer: true
            }
        });



    }
}




