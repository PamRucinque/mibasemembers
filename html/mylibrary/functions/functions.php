<?php

$branch = substr(getcwd(), 22, strpos(getcwd() . '/', '/', 22 + 1) - 22);


//include('../mibase_check_login.php');

function send_email($to, $subject, $message, $header, $param) {
    if ($to == '') {
        $output = '<br>Email not sent, email is blank.<br>';
    } else {
        $script_start = '<html>
   <body>
        <style>
            p {
                font-size: 14px;
                margin: 0;
                padding: 0;
                border: 0;
            }
        </style>';
        $script_end = '</body></html>';
        $message = $script_start . $message . $script_end;
        $output = @mail($to, $subject, $message, $header, $param);
        //@mail('michelle@mibase.com.au',$template['subject'], $template['message'], $email['header'], $email['param']);
    }
    return $output;
}

function calculate_fine($due, $sun, $mon, $tues, $wed, $thurs, $fri, $sat, $weekly, $fine, $grace) {
    $days = array();
    //$weekly = 'Yes';
    //$fine = 2;
    for ($i = 0; $i <= 6; $i++) {
        $days[$i] = 0;
    }
    // $days 0 to 6 are the days of the week starting sunday, $days[7] = total days
    // days[8] = total sessions, days[9] = total fine
    $now = date('Y-m-d');
    $total = 0;
    $session = 0;
    $date_start = date_create_from_format('Y-m-d', $due);
    $date_end = date_create_from_format('Y-m-d', $now);

    $interval = $date_start->diff($date_end);
    $no_days = $interval->format('%a');

    for ($xi = 0; $xi <= $no_days; $xi++) {
        $str = '+' . $xi . 'day';
        $curr = strtotime($str, strtotime($due));
        $str_array = date("Y-m-d", $curr);
        $dw = date("w", $curr);
        $days[$dw] = $days[$dw] + 1;
        //echo $str_array . ' ' . $days[$dw] . ' ' . date( "l", $curr). '<br>';
        $total = $total + 1;
        //echo $days[$dw] . '<br>';
        array_push($overdue, $str_array);
    }
    $days[7] = $total;

    if ($sun > 0) {
        $session = $session + $days[0] * $sun;
    }
    if ($mon > 0) {
        $session = $session + $days[1] * $mon;
    }
    if ($tues > 0) {
        $session = $session + $days[2] * $tues;
    }
    if ($wed > 0) {
        $session = $session + $days[3] * $wed;
    }
    if ($thurs > 0) {
        $session = $session + $days[4] * $thurs;
    }
    if ($fri > 0) {
        $session = $session + $days[5] * $fri;
    }
    if ($sat > 0) {
        $session = $session + $days[6] * $sat;
    }
    if ($grace > 0) {
        $days[8] = $session - $grace;
        if ($days[8] < 0) {
            $days[8] = 0;
        }
    }

    //echo 'Sessions Overdue: ' . $days[8] . '<br>'; 
    //echo 'Total Days: ' . $days[7] . '<br>'; 

    if ($weekly == 'Yes') {
        $weeks = round(($total - $grace) / 7);
        if ($weeks < 0) {
            $weeks = 0;
        }
        $total_fine = round($weeks * $fine, 2);
        //$total_fine = round($fine*round($days[7]/7),2);
    } else {
        $total_fine = round($fine * $session, 2);
    }

    //echo 'Weekly Fine: ' . $weekly . ' ' . $total_fine . '<br>';
    // echo 'Fine Value: ' . $fine . ' <br>';
    $days[9] = $total_fine;
    $days[10] = $weeks;
    //if ($total_fine > 0) {
    if ($weekly == 'Yes') {
        $output .= '<font color="red">This Toy is overdue by ' . $weeks . ' weeks, your fine is $' . $total_fine . ' .</font><br>';
        if ($grace > 0) {
            $output .= '<font color="red">' . $grace . ' days grace has been granted. </font><br>';
        }
    } else {
        $output .= '<font color="red">This Toy is overdue by ' . $session . ' sessions, your fine is $' . $total_fine . '</font><br>';
        if ($grace > 0) {
            $output .= '<font color="red">' . $grace . ' days grace has been granted. </font><br>';
        }
    }


    return array('fine_status' => $output, 'amount' => $total_fine);
}

function fill_loan_array($idcat, $loanperiod, $date_start) {
    $c = array();

    for ($xi = 0; $xi <= $loanperiod; $xi++) {
        $str = '+' . $xi . 'day';
        $curr = strtotime($str, strtotime($date_start));
        $str_array = date("Y-m-d", $curr);
        //echo $str_array;
        array_push($c, $str_array);
    }
    return $c;
}

function fill_reservations_array($idcat) {
    include($url_button . 'connect.php');
    $conn = pg_connect($_SESSION['conn']);

    $query = "SELECT reserve_toy.*, borwrs.Firstname as firstname, Borwrs.surname as surname, 
    toys.idcat as idcat, toys.toyname as toyname
FROM (reserve_toy LEFT JOIN borwrs ON reserve_toy.member_id = Borwrs.id) LEFT JOIN toys ON reserve_toy.idcat = toys.idcat
 WHERE reserve_toy.idcat = '" . $idcat . "' AND status = 'ACTIVE' ORDER BY reserve_toy.date_start;";

    $a1 = array();

    $result1 = pg_Exec($conn, $query);
    $numrows = pg_numrows($result1);
    if ($numrows > 0) {

        for ($ri = 0; $ri < $numrows; $ri++) {
            $row = pg_fetch_array($result1, $ri);
            $date_end = date_create_from_format('Y-m-d', $row['date_end']);
            $date_start = date_create_from_format('Y-m-d', $row['date_start']);
            $interval = $date_start->diff($date_end);
            $no_days = $interval->format('%a');

            for ($xi = 0; $xi <= $no_days; $xi++) {
                $str = '+' . $xi . 'day';
                $curr = strtotime($str, strtotime($row['date_start']));
                $str_array = date("Y-m-d", $curr);
                array_push($a1, $str_array);
            }
        }
    }
    return $a1;

    pg_FreeResult($result1);

    pg_Close($conn);
}

function return_today($idcat, $borid) {
    include('../connect.php');

    $conn = pg_connect($_SESSION['conn']);
    $sql = "SELECT Count(id) AS countid FROM transaction 
            WHERE (date_loan::date)=(now()::date) AND ((idcat)='" . $idcat . "') AND ((borid)=" . $borid . ')';
    $nextval = pg_Exec($conn, $sql);
//echo $connection_str;
    $row = pg_fetch_array($nextval, 0);
    $return = $row['countid'];
    return $return;
}

function max_toys($membertype) {
    include('../connect.php');
    $conn = pg_connect($_SESSION['conn']);
    $sql = "SELECT maxnoitems AS max FROM membertype 
          WHERE (membertype)='" . $membertype . "'";
    $nextval = pg_Exec($conn, $sql);
    $row = pg_fetch_array($nextval, 0);
    $max_toys = $row['max'];
    return $max_toys;
}

function max_jounal_id() {
    include('../connect.php');
    $conn = pg_connect($_SESSION['conn']);
    $sql = "SELECT MAX(id) as newid FROM journal;";
    $nextval = pg_Exec($conn, $sql);
    $row = pg_fetch_array($nextval, 0);
    $max = $row['newid'] + 1;
    return $max;
}

function count_loans($borid) {
    include('../connect.php');
    $conn = pg_connect($_SESSION['conn']);

    $sql = "SELECT Count(id) AS noloans FROM transaction
    WHERE borid=" . $borid . " AND return Is Null;";
    $nextval = pg_Exec($conn, $sql);
    $row = pg_fetch_array($nextval, 0);
    $no_loans = $row['noloans'];
    return $no_loans;
}

function check_trans($idcat) {
    include('../connect.php');
    $conn = pg_connect($_SESSION['conn']);
    $sql = "select Count(id) AS countid FROM transaction where idcat='" . $idcat . "' AND return Is Null;";
    $nextval = pg_Exec($conn, $sql);
//echo $connection_str;
    $row = pg_fetch_array($nextval, 0);
    $trans = $row['countid'];
    //$trans = 4;
    return $trans;
}

function get_borid($idcat) {
    include('../connect.php');
    $conn = pg_connect($_SESSION['conn']);
    $sql = "select borid AS borid FROM transaction where idcat='" . $idcat . "' AND return Is Null;";
    $nextval = pg_Exec($conn, $sql);
//echo $connection_str;
    $row = pg_fetch_array($nextval, 0);
    $trans_borid = $row['borid'];
    $sql = "SELECT * From Borwrs WHERE id =" . $trans_borid . ";";
    $nextval = pg_Exec($conn, $sql);
    $row = pg_fetch_array($nextval, 0);
    $link = '<a class="button_small_red" href="loan.php?b=' . $row['id'] . '"> ' . $row['id'] . ' </a>';
    $longname = $row['firstname'] . ' ' . $row['surname'] . ' (' . $link . ')';
    //$trans = 4;
    return $longname;
}

function check_member($id) {
    include('../connect.php');
    $conn = pg_connect($_SESSION['conn']);
    $sql = "SELECT * From Borwrs WHERE id =" . $id . ";";
    $result = pg_Exec($conn, $sql);
    $numrows = pg_numrows($result);
    if ($numrows > 0) {
        $output = 'Yes';
    } else {
        $output = 'No';
    }
    return $output;
}

function get_toy($id) {
    $connect_str = $_SESSION['connection_str'];
    $conn = pg_connect($connect_str);

    $sql_trans = "SELECT transaction.*, toys.alert as alert, toys.rent as rent FROM transaction 
    left join toys on toys.idcat = transaction.idcat WHERE transaction.id=" . $id . ";";

    $nextval = pg_Exec($conn, $sql_trans);
    $trans = pg_fetch_array($nextval, 0);
    $toyname = clean($trans['item']);
    $due = $trans['due'];
    $borid = $trans['borid'];
    $longname = clean($trans['borname']);
    $idcat = $trans['idcat'];
    $alert = clean($trans['alert']);
    $date_loan = $trans['date_loan'];
    $rent = $trans['rent'];

    return array('toyname' => $toyname, 'due' => $due, 'borid' => $borid, 'idcat' => $idcat,
        'longname' => $longname, 'alert' => $alert, 'date_loan' => $date_loan, 'rent' => $rent);
}

function delete_rent($idcat, $borid) {
    include('../connect.php');
    $conn = pg_connect($_SESSION['conn']);
    $sql = "DELETE from journal WHERE 
    (datepaid::date)=(now()::date) AND ((icode)='" . $idcat . "') AND ((bcode)=" . $borid . ") AND ((category)= 'Rent');";
    $result_delete = pg_Exec($conn, $sql);
    if (!$result_delete) {
        $outcome = 'cannot delete rent:' . $query;
    }
    return $outcome;
}

function charge_rent($global_rent, $loanperiod, $id, $rentasfine, $rentfactor) {
    $toy = get_toy($id);

    //include('../connect.php');
    $connect_str = $_SESSION['connection_str'];
    $conn = pg_connect($connect_str);
    $status = '';

    if ($rentasfine == 'Yes') {
        if ($toy['rent'] > 0) {
            $totalrent = round(($toy['rent']), 2);
            $query_rent = "insert into journal( datepaid, bcode, icode,name, description, category, amount, type, typepayment)
            VALUES (now(),
            '{$toy['borid']}',
            '{$toy['idcat']}',
            '{$toy['longname']}',
            '{$toy['toyname']}',
            'Rent',
            {$totalrent},
            'DR', 'Debit'
        );";
        }
    } else {
        $totalrent = round(($global_rent), 2);
        $query_rent .= "insert into journal( datepaid, bcode, icode, name, description, category, amount, type, typepayment)
            VALUES (now(),
            '{$toy['borid']}',
            '{$toy['idcat']}',
            '{$toy['longname']}',
            '{$toy['toyname']}',
            'Rent',
            {$totalrent},
            'DR', 'Debit'
        );";
    }
    if ($totalrent > 0) {
        $status .= ' Rent of $' . $totalrent . ' has been charged.<br>';
        $query_renew .= $query_rent;
    }
    return array("status" => $status, "result" => $success, "idcat" => $idcat, "alert" => $alert, 'sql_str' => $query_renew);
}

function renew_toy($id, $loanperiod, $times_renew, $chargerent, $global_rent, $borid, $rentasfine, $rentfactor,$renew) {
    $connect_str = $_SESSION['connection_str'];
    $conn = pg_connect($connect_str);
    $query_renew = '';
    $success = 'No';
    $alert_member = 'No';
    $status = '';
    $renew = $renew + 1;
    $toy = get_toy($id);
    $idcat = $toy['idcat'];
    $alert = $toy['alert'];
    $ts1 = strtotime($toy['due']);
    $ts2 = strtotime($toy['date_loan']);
    $days_diff = -1 * round(($ts2 - $ts1) / (60 * 60 * 24), 0);
    $max = $loanperiod * $times_renew + $loanperiod + 6;
    $rent = array();
    //echo $days_diff;

    $str_due = strtotime("+" . $loanperiod . " days", strtotime($toy['due']));
    //$format_str_due = substr($str_due, 8, 2) . '-' . substr($str_due, 5, 2) . '-' . substr($str_due, 0, 4);

    $format_str_due = date('d M Y', $str_due);

    if ($days_diff < $max) {
        $query_renew = "UPDATE transaction SET due = (due + interval '" . $loanperiod . " days'), "
                . " timetrans = now()::time, renew = " . $borid . ", times_renew = " .$renew . " where id= " . $id . ";";
        echo $query_renew;
        if ($chargerent == 'Yes') {
            $rent = charge_rent($global_rent, $loanperiod, $id, $rentasfine, $rentfactor);
            //echo 'charge rent = Yes';
            $query_renew .= $rent['sql_str'];
            //$status .= $rent['status'];
        }else{
            $rent['status'] = '';
        }
    } else {
        $status .= 'Cannot renew ' . $idcat . ': ' . $toy['toyname'] . ', it has already been renewed ' . $times_renew . ' time(s).';
        //$status .= ' ' . $days_diff . ' max ' . $max . ' times renew: ' . $times_renew . ' Loan period: ' . $loanperiod;
        $alert_member = 'Yes';
    }


    $result = pg_exec($conn, $query_renew);

    if (!$result) {
        $message = 'Invalid query: ' . "\n";
        $message = $query_renew;
        //die($message);
        echo $message;
        $success = 'No';
    } else {
        $status .= $toy['idcat'] . ': ' . $toy['toyname'] . ' has been renewed. Due: ' . $format_str_due . '.  <font color="red">' . $toy['alert'] . '</font><br>';
        $status .= $rent['status'];
        $success = 'Yes';
    }
    //$status .= 'hello';
    return array("status" => $status, "result" => $success, "idcat" => $idcat, "alert" => $alert, "alert_member" => $alert_member);
}

function clean($input) {
    $output = stripslashes($input);
    $output = str_replace("'", "`", $output);
    $output = str_replace('/', '-', $output);
    //$output = str_replace("\", " ", $output);
    return $output;
}

Function check_alert($alert) {
    if ($alert != '') {
        echo '<script>
        $(document).ready(function(){
		alert("' . $alert . '");});
        </script>';
    }
}

?> 