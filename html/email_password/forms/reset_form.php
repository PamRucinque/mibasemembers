<form name="reset_password" method="post" id="reset_password" class="form-signin" onsubmit="return false" autocomplete="off">
    <div id="reauth-email" class="reauth-email"></div>
    <input autocomplete="false" name="hidden" type="text" style="display:none;">
    <input required name="key" type="hidden" id="key" class="form-control" value='<?php echo $key; ?>' >
    <input name="pwd1" type="password" id="pwd1" class="form-control" placeholder="Enter your new password" value='' autocomplete="new-password">
    <input name="pwd2" type="password" id="pwd2" class="form-control" placeholder="Enter your new password again, and press enter!" value='' autocomplete="new-password">
    <div class="row" id='btns' style='display: none;'>
        <div class="col-sm-6">
            <a href="<?php echo $login; ?>" class="btn btn-lg btn-info btn-block btn-signin">Login</a>
        </div>
        <div class="col-sm-6">
            <input type="submit" name="submit" value="Reset" class="btn btn-lg btn-danger btn-block btn-signin" onclick="reset_pwd();">  
        </div>  
    </div>

</form>
