<?php

include('../../config.php');
$r = array();
$msg = '';
//echo $shared_server;
//$email = 'michelle@mibase.com.au';
$librarycode = '';
$m = array();
$cp = array();
$update_password = 'Not Updated<br>';

if (isset($_POST['email'])) {
    $email = $_POST['email'];
    $email = str_replace('%40', '@', $email);
}


if ($shared_server) {

//get the server that the client requested
//note we will check it for hacker payload below
    $fqn = $_SERVER["SERVER_NAME"];

//do some checks on this variable
    if (check_server_fqn($fqn)) {
        if (ends_with($fqn, $shared_domain)) {
            $librarycode = get_host_name($fqn, $shared_domain);
        } else {
            exit_script('requested host is not on this server: ' . $host . ':' . $shared_domain);
        }
    } else {
        exit_script('incorrect server name');
    }
    $dbpwd = getLibraryPostgresPassword($librarycode, $mibase_server);
}
$validate = mibase_validate_email($email);
if ($validate === 'OK') {
    $m = get_member($email, $librarycode, $dbpwd);
    if ($m['status'] === 'OK') {
        $cp = check_password($m);
        if ($cp['update']) {
            $update_password = update_password($m['id'], $librarycode, $dbpwd, $cp['pwd'], $cp['username']);
        }
        $msg = email_password($m, $librarycode);
    } else {
        $msg = 'Your email address is Incorrect or Not Registered, please contact your Library Administrator.';
    }
} else {
    $msg = $validate;
}


$r["result"] = $msg;   //fix
header('Content-Type: application/json');
echo json_encode($r);

function email_password($m, $librarycode) {
    $out = false;
    $subject = 'Username and password for ' . $m['libraryname'];
    $message = 'Dear ' . $m['firstname'] . ',<br><br>';
    $message .= 'Your Username and password has been reset.<br><br>';
    $message .= 'To Login go to <a class="button_menu_member" href="https://' . $librarycode . '.mibase.com.au/mem/login.php">Member Login</a>';
    $message .= ' with the following username and password.<br><br>';
    $message .= 'Username: <font color="blue">' . $m['username'] . '</font><br>';
    $message .= 'Password: <font color="blue">' . $m['pwd'] . '</font><br><br>';
    $message .= '<font color="red"> This is an automated message, please do not reply</font>';
    $message .= '<br><br><br><font color="blue"> Sent from mibase recover password page. :)</font>';
    $email_from = 'noreply@mibase.com.au';
    $email_to = $m['emailaddress'];
    $headers .= 'From: ' . $m['libraryname'] . '<' . $email_from . '>' . "\r\n";
    $headers .= "MIME-Version: 1.0\r\n";
    $headers .= "Content-Type: text/html; charset=ISO-8859-1\r\n";
    $out = @mail($email_to, $subject, $message, $headers);
    if ($out) {
        $out_str = 'Your username and password has been emailed to your registered email address.';
    } else {
        $out_str = 'There was an error Emailing your username and password, please contact your library administrator.';
    }
    return $out_str;
}

function check_password($m) {

    $cp = array();
    $cp['update'] = false;
    $cp['pwd'] = $m['pwd'];
    $cp['username'] = $m['username'];

    $surname = $m['surname'];
    $username = $m['username'];
    if (($m['pwd'] == '') || ($m['pwd'] == 'mibase')) {
        $cp['pwd'] = generateStrongPassword();
        $cp['update'] = true;
    }
    if ($username == '' || $username == null) {
        $cp['update'] = true;
        $length_surname = strlen($surname);
        if ($length_surname < 10) {
            $surname = pad_surname($surname);
        } else {
            $surname = substr($surname, 0, 10);
            $length_surname = 10;
        }
        $surname = strtolower($surname);
        $username = rtrim($surname, "%") . $m['id'];
        $username = str_replace(" ", "", $username);
        $username = str_replace("`", "", $username);
        $cp['username'] = $username;
    }
    return $cp;
}

function update_password($memberid, $dbname, $dbpwd, $password, $username) {

    $connect_pdo = "pgsql:host=localhost;port=5432;dbname=" . $dbname;
    $dbuser = $dbname;
    $pdo = new PDO($connect_pdo, $dbuser, $dbpwd);
    $sql = "update borwrs set pwd = ?, rostertype5 = ? where id = ?;";
    $sth = $pdo->prepare($sql);
    $array = array($password, $username, $memberid);
    $sth->execute($array);
    $stherr = $sth->errorInfo();
    if ($stherr[0] != '00000') {
        $status = "An  error occurred.\n";
        $status .= 'Error' . $stherr[0] . '<br>';
        $status .= 'Error' . $stherr[1] . '<br>';
        $status .= 'Error' . $stherr[2] . '<br>';
    } else {
        $status = 'OK';
    }

    return $status;
}

function get_member($email, $dbname, $dbpwd) {
    $dbuser = $dbname;
    $member = array();
    $status = 'OK';
    $connect_pdo = "pgsql:host=localhost;port=5432;dbname=" . $dbname;
    $pdo = new PDO($connect_pdo, $dbuser, $dbpwd);
    $sql = "SELECT emailaddress, firstname,surname,id, rostertype5 as username, pwd,
	(select setting_value from settings where setting_name = 'libraryname') as libraryname
            from borwrs 
            where trim(emailaddress) = ? and member_status = 'ACTIVE';";
    $sth = $pdo->prepare($sql);
    $array = array($email);
    $sth->execute($array);

    $result = $sth->fetchAll();
    $stherr = $sth->errorInfo();
    $numrows = $sth->rowCount();
    //$email = 'not working';
    for ($ri = 0; $ri < $numrows; $ri++) {
        $member = $result[$ri];
    }
    if ($numrows != 1) {
        $status = 'error';
    }

    if ($stherr[0] != '00000') {
        $error_msg .= "An  error occurred.\n";
        $error_msg .= 'Error' . $stherr[0] . '<br>';
        $error_msg .= 'Error' . $stherr[1] . '<br>';
        $error_msg .= 'Error' . $stherr[2] . '<br>';
        $status = 'error';
    }
    $member['status'] = $status;
    return $member;
}

function getLibraryPostgresPassword($library_code, $mibaseserver) {
    global $dbhost, $toybasedbname, $toybasedbuser, $toybasedbpasswd, $dbport;

//now get the database password for the library database
    $toybase_connect_pdo = "pgsql:host=" . $dbhost . ";port=" . $dbport . ";dbname=" . $toybasedbname;
    $toybase_pdo = new PDO($toybase_connect_pdo, $toybasedbuser, $toybasedbpasswd);
    if ($mibaseserver == 'Yes') {
        $toybase_query_login = "select postgres as dbpassword
                    FROM libraries
                    WHERE subdomain = ? ;";
    } else {
        $toybase_query_login = "select dbpassword
                    FROM libraries
                    WHERE library_code = ? ;";
    }


    $toybase_sth = $toybase_pdo->prepare($toybase_query_login);
    $toybase_array = array($library_code);
    $toybase_sth->execute($toybase_array);

    $toybase_result = $toybase_sth->fetchAll();
    $toybase_numrows = $toybase_sth->rowCount();
    $toybase_stherr = $toybase_sth->errorInfo();
    if ($toybase_stherr[0] != '00000') {
        return "An  error occurred checking the login: " . $toybase_stherr[0] . " " . $toybase_stherr[1] . "" . $toybase_stherr[2];
    }

    if ($toybase_numrows > 0) {
        $row = $toybase_result[0];
        return $row['dbpassword'];
    } else {
        return 'Login Error: This library not on this server: ' . $library_code;
    }
}

function get_host_name($fqn, $shared_domain) {
    return substr($fqn, 0, strlen($fqn) - strlen($shared_domain) - 1);
}

function check_server_fqn($fqn) {  //@todo  we should also check that this fqn only contains letters numbers . - _
    if (strlen($fqn) > 100) {
        return false;
    } else {
        return true;
    }
}

/**
 * 
 * @param type $fqn
 * @param type $shared_domain
 * @return type
 */
function ends_with($fqn, $shared_domain) {
    return ( substr($fqn, strlen($fqn) - strlen($shared_domain)) == $shared_domain );
}

function generateStrongPassword($length = 10, $add_dashes = false, $available_sets = 'luds') {
    $sets = array();
    if (strpos($available_sets, 'l') !== false)
        $sets[] = 'abcdefghjkmnpqrstuvwxyz';
    if (strpos($available_sets, 'u') !== false)
        $sets[] = 'ABCDEFGHJKMNPQRSTUVWXYZ';
    if (strpos($available_sets, 'd') !== false)
        $sets[] = '23456789';
    if (strpos($available_sets, 's') == false)
        $sets[] = '!@#$%&*?';
    $all = '';
    $password = '';
    foreach ($sets as $set) {
        $password .= $set[array_rand(str_split($set))];
        $all .= $set;
    }
    $all = str_split($all);
    for ($i = 0; $i < $length - count($sets); $i++)
        $password .= $all[array_rand($all)];
    $password = str_shuffle($password);
    if (!$add_dashes)
        return $password;
    $dash_len = floor(sqrt($length));
    $dash_str = '';
    while (strlen($password) > $dash_len) {
        $dash_str .= substr($password, 0, $dash_len) . '-';
        $password = substr($password, $dash_len);
    }
    $dash_str .= $password;
    return $dash_str;
}

function pad_surname($string) {
    $length = 10;
    $out_string = '';
    for ($x = 0; $x < $length; $x++) {
        $char = substr($string, $x, 1);
        if ($char != '') {
            $out_string .= $char;
        } else {
            $out_string .= '%';
        }
    }

    return $out_string;
}

function mibase_validate_email($email) {
    $pattern = '/^[a-zA-Z0-9\.@_\-]+$/';
    $return = 'OK';

    //$pattern = '/\d+/';
    if (preg_match($pattern, $email) == 0) {
        $return = 'Login error: Email fails the regular expression test: ' . $email . ': ';
    }
    if ((strlen($email)) > 30) {
        $length = strlen($email);
        $return = 'Login error: Username exceeds max length of 30: ' . $length . ': ';
    }
    return $return;
}
