<?php

require('../mibase_check_login.php');
include('../connect.php');

$total = 0;
$x = 0;

$pdo = new PDO($_SESSION['connect_pdo'], $_SESSION['dbuser'], $_SESSION['dbpasswd']);

$query_toys = "SELECT toys.idcat as idcat, toyname, category,no_pieces, user1, toys.id as id, age, rent,
 manufacturer, toy_status, twitter, sub_category, l.description as location, t.due as due, 
 to_char(t.due,'dd-mm-yyyy') as due_str, l.description as location_description, t.total as onloan, coalesce(h.total,0) as holds  
 FROM toys 
 left join location l on l.location = toys.location
 left join (select count(id) as total,idcat, due from transaction where return is null group by idcat,due) t on t.idcat = toys.idcat 
 left join (select count(id) as att, attribute, idcat from toy_attributes where attribute = ? group by idcat,attribute) a on a.idcat = toys.idcat 
 left join (select count(id) as total, idcat from toy_holds where (status = 'ACTIVE'or status = 'READY' or status = 'PENDING' or status = 'LOCKED') group by idcat) h on h.idcat = toys.idcat 
WHERE 
 (upper(toyname) LIKE ?
        OR upper(manufacturer) LIKE ?
        OR upper(storage) LIKE ?
        OR upper(user1) LIKE ?
        OR upper(toys.idcat) LIKE ?
        OR upper(twitter) LIKE ?) 
 AND (toys.category LIKE ?) 
 AND ((toys.location LIKE ?) OR (toys.location IS NULL)) 
 AND (toy_status = 'ACTIVE') ";
$query_toys .= $age_q;
$query_toys .= $attribute_str;
$query_toys .= $str_status;
$query_toys .= $order_by;
//$query_toys .= ";";
//echo $query_toys . ' status:' . $str_status;
//

$sth = $pdo->prepare($query_toys);
$array = array($attribute, $search_str, $search_str, $search_str, $search_str, $search_str, $search_str, $category_str, $location_str, $age_str);
$sth->execute($array);

$result = $sth->fetchAll();


$stherr = $sth->errorInfo();

if ($stherr[0] != '00000') {
    echo "An INSERT query error occurred.\n";
    echo $query_edit;
    echo $connect_pdo;
    echo 'Error' . $stherr[0] . '<br>';
    echo 'Error' . $stherr[1] . '<br>';
    echo 'Error' . $stherr[2] . '<br>';
}

$XX = "No Record Found";

$result_txt = '';
$today = date("Y-m-d");

$total = sizeof($result);

$search_str = '';
if ($str_category != '') {
    $search_str .= ' Filtered on Category = ' . $str_category . ', ';
}
if ($age_str != '%%') {
    $search_str .= ' Filtered on Age = ' . $age . ', ';
}
if ($search != '') {
    $search_str .= ' Filtered on String = ' . $search . ', ';
}
if ($attribute != '') {
    $search_str .= ' Filtered on Attribute = ' . $attribute . ', ';
}
$toprint = sizeof($result);

include('total_row.php');
if ($_SESSION['view'] == 'list') {
    include('header.php');
} else {
    echo '<div class="row responsive" style="padding-bottom: 10px; max-height="200px;">';
}

for ($ri = 0; $ri < $toprint; $ri++) {
    $toy = $result[$ri];
    $bgcolor = 'white';
    $status = '';
    if ($toy['onloan'] > 0) {
        $bgcolor = 'lightgreen';
        $status = $toy['due_str'];
    }
    if ($toy['holds'] > 0){
        $bgcolor = '#FFCCFF';
        $status = 'On Hold';
    }
    if ($multi_location == 'Yes') {
        $location_description = $toy['location_description'];
    } else {
        $location_description = '';
    }
    $subdomain = $_SESSION['library_code'];
    $file_pic = '/home/mibaselive/html/toy_images/' . $subdomain . '/' . strtolower($toy['idcat']) . '.jpg';
    if ((file_exists($file_pic) && ($row['block_image'] != 'Yes'))) {
        $pic = "'https://" . $subdomain . ".mibase.com.au/toy_images/" . $subdomain . "/" . strtolower($toy['idcat']) . ".jpg" . "'";
        $pic_grid = "https://" . $subdomain . ".mibase.com.au/toy_images/" . $subdomain . "/" . strtolower($toy['idcat']) . ".jpg";
    } else {
        $pic = "'https://" . $subdomain . ".mibase.com.au/toy_images/" . $subdomain . "/" . 'blank' . ".jpg" . "'";
        $pic_grid = "https://" . $subdomain . ".mibase.com.au/toy_images/demo/" . 'blank' . ".jpg";
    }
    $link = '../toy/toy.php?id=' . $toy['idcat'];
    $img_grid = '<img src="' . $pic_grid . '" alt="' . strtolower($toy['idcat']) . '" width="180px" >';
    //$link = '../toy/toy.php';
    $view_str = '<a style="color:white;" class="btn btn-primary btn-sm" id="button" '
            . 'onmouseover="showtrail(175,220, ' . $pic . ');" onmouseout="hidetrail();" '
            . ' >View Toy</a>';
    if ($_SESSION['view'] == 'list') {
        include('row.php');
    } else {
        include('grid.php');
    }
}
if ($_SESSION['view'] == 'list') {
    echo '</tbody></table>';
} else {
    echo '</div>';
}




//print $result_txt;
?> 
