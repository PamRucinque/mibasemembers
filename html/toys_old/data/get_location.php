<?php
include('../mibase_check_login.php');

//connect to MySQL
//include( dirname(__FILE__) . '/../connect.php');
$query = "SELECT location, description  FROM location ORDER by location ASC;";
$result = pg_Exec($conn, $query);


echo '<select name="loc_filter" id="loc_filter" style="max-width: 150px;" onchange="this.form.submit();" class="form-control" placeholder="Select Location">\n';

$numrows = pg_numrows($result);

if ($_SESSION['loc_filter'] != '') {
    echo '<option value="' . $_SESSION['loc_filter'] . '" selected="selected">' . $_SESSION['loc_filter'] . '</option>';
} else {
    echo '<option value="" selected="selected">Select Location</option>';
}


for ($ri = 0; $ri < $numrows; $ri++) {
    $row = pg_fetch_array($result, $ri);
    echo "<option value='{$row['location']}'>{$row['description']}
    </option>\n";
}

echo "</select>\n";
?>

