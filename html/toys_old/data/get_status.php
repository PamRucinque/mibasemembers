<?php

include('../mibase_check_login.php');

echo '<select name="status" id="status" onchange="this.form.submit();" class="form-control" placeholder="Select Status" style="max-width: 200px;">\n';

if ($_SESSION['status'] != '') {
    echo '<option value="" selected="selected">' . $_SESSION['status'] . '</option>';
} else {
    echo '<option value="" selected="selected">Select Status</option>';
}
echo '<option value="Available">Available</option>';
echo '<option value="On Loan">On Loan</option>';
echo '<option value="On Hold">On Hold</option>';
echo '<option value=""></option>';
echo "</select>\n";

?>

