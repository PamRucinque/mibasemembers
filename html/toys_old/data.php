<?php

$overdue_txt = 'OVERDUE';
$overdue_text = '';
$catsort = $_SESSION['settings']['catsort'];
$subdomain = $_SESSION['library_code'];

//$overdue_txt = $_SESSION['settings']['overdue_txt'];

$str_category = '';
$str_status = '';
$category_str = '';
$location_str = '';
$attribute_str = '';
$attribute = '';
$search = '';
$search_str = '';
$age_q = '';
$age = '';


if (isset($_POST['reset'])) {
    $_SESSION['search'] = '';
    $_SESSION['category'] = '';
    $_SESSION['status'] = '';
    $_SESSION['age'] = '';
    $_SESSION['loc_filter'] = '';
    $_SESSION['attribute'] = '';
    $_SESSION['limit'] = 100;
    $_SESSION['view'] = 'list';
}
if (isset($_POST['pictures'])) {
    if ($_SESSION['view'] == 'list') {
        $_SESSION['view'] = 'pictures';
    } else {
        $_SESSION['view'] = 'list';
    }
} else {
    $_SESSION['view'] = 'list';
}

if (isset($_POST['show_all'])) {
    $_SESSION['limit'] = 'All';
}
if (isset($_POST['age'])) {
    $_SESSION['age'] = $_POST['age'];
    $_SESSION['limit'] = 'All';
}
if (isset($_POST['status'])) {
    $_SESSION['status'] = $_POST['status'];
    $_SESSION['limit'] = 'All';
}
if (isset($_POST['category'])) {
    $_SESSION['category'] = trim($_POST['category']);
    $_SESSION['limit'] = 'All';
}
if (isset($_POST['attribute'])) {
    $_SESSION['attribute'] = trim($_POST['attribute']);
    $_SESSION['limit'] = 'All';
}
if (isset($_POST['loc_filter'])) {
    $_SESSION['loc_filter'] = trim($_POST['loc_filter']);
    $_SESSION['limit'] = 'All';
}


if (isset($_SESSION['category'])) {
    $str_category = $_SESSION['category'];
}
if ($str_category == 'onloan') {
    $onloan_str = ' AND t.due is not null   ';
}
if ($str_category == '') {
    $category_str = $str_category . '%';
} else {
    $category_str = $str_category;
}

if (isset($_SESSION['age'])) {
    $age_str = $_SESSION['age'];
}

if (isset($_SESSION['attribute'])) {
    $attribute = $_SESSION['attribute'];
}

if (isset($_POST['loc_filter'])) {
    $_SESSION['loc_filter'] = trim($_POST['loc_filter']);
} else {
    $_SESSION['loc_filter'] = '';
}
if ($_SESSION['loc_filter'] != '') {
    $location_str = '%' . $_SESSION['loc_filter'] . '%';
}


if (isset($_SESSION['catsort'])) {
    $catsort = $_SESSION['catsort'];
}

$search = '';

if (isset($_POST['search'])) {
    if ($_POST['search'] != '') {
        $_SESSION['search'] = $_POST['search'];
        $_SESSION['limit'] = 'All';
    }
}

if (isset($_SESSION['search'])) {
    $search = $_SESSION['search'];
}


$search = strtoupper($search);
$search_str = '%' . $search . '%';


if ($_SESSION['age'] != '') {
    $age_q .= " AND (age LIKE ?) ";
    $age = $_SESSION['age'];
} else {
    $age_q .= " AND ((age LIKE ?) or (age is null)) ";
}
if ($_SESSION['status'] == 'Available') {
    $str_status = ' AND (t.due is null) AND h.total is null  ';
} else {
    if ($_SESSION['status'] == 'On Loan') {
        $str_status = ' AND t.due is not null   ';
    }
    if ($_SESSION['status'] == 'On Hold') {
        $str_status = ' AND h.total > 0   ';
    }
}
if ($location_str == '') {
    $location_str = '%' . $location_str . '%';
}
if ($_SESSION['limit'] == '') {
    $_SESSION['limit'] = 100;
}
if ($_SESSION['limit'] == 'All') {
    $limit_str = ';';
} else {
    $limit_str = ' LIMIT ' . $_SESSION['limit'] . ';';
}

$age_str = '%' . $age . '%';
//error message (not found message)
if ($catsort == 'No') {
    $order_by = 'ORDER BY id ASC';
} else {
    $order_by = 'ORDER BY category, id ASC';
}

if (isset($_SESSION['attribute'])) {
    if ($_SESSION['attribute'] != '') {
        $attribute_str = "AND a.att > 0   ";
        $attribute = $_SESSION['attribute'];
        $limit_str = '';
    } else {
        $attribute_str = "";
    }
}

$order_by .= $limit_str;


