<table id="members" class="table table-striped table-bordered table-sm table-hover display responsive compact" style="overflow: hidden;" cellspacing="0" width="100%" role="grid">
    <thead>
        <tr>
            <th class="th-sm all" data-priority="1">No</th>
            <th class=""  data-priority="6">id</th>
            <th class="" data-priority="5">Cat</th>
            <th data-priority="2" class="wrap ">Toy name</th>
            <th class="" data-priority="10" class="wrap"></th>
            <th class="" data-priority="7">Manufacturer</th>
            <th class=""  data-priority="9">Age</th>
            <?php
            if ($multi_location == 'Yes'){
                echo '<th class=""  data-priority="10">Location</th>';
            }
            
            ?>
            <th class="hidden-xs nowrap"  style="min-width: 30px;" data-priority="8">Pieces</th>
            <th class="hidden-xs" data-priority="3">Status/Due</th>

        </tr>
    </thead>
    <tbody>

