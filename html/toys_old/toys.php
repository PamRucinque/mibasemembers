<!DOCTYPE html>
<?php include('../mibase_check_login.php'); ?>
<html>
    <head>
        <title>Mibase Search Toys</title>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <?php include('../header/head.php'); ?>
        <script type="text/javascript" src="<?php echo $url; ?>/js/tooltip.js"></script>

    </head>
    <script>
        $(document).ready(function () {
            $('#members').DataTable({
                "lengthChange": true,
                "pageLength": 50,
                "pagingType": "full_numbers",
                "order": [[2, "asc"], [1, "asc"]],
                "lengthChange": false,
                "bFilter": false,
                language: {
                    searchPlaceholder: "Search Toys",
                    searchClass: "form-control",
                    search: "",
                },
                responsive: {
                    details: {
                        renderer: function (api, rowIdx, columns) {
                            var data = $.map(columns, function (col, i) {
                                return col.hidden ?
                                        '<tr data-dt-row="' + col.rowIndex + '" data-dt-column="' + col.columnIndex + '">' +
                                        '<td>' + col.title + ':' + '</td> ' +
                                        '<td>' + col.data + '</td>' +
                                        '</tr>' :
                                        '';
                            }).join('');
                            return data ?
                                    $('<table/>').append(data) :
                                    false;
                        }
                    }
                }
            });
            $('div.dataTables_filter input').addClass('form-control');
            $('div.dataTables_length select').addClass('recs');
        });
        function set_toyid(str) {
            document.getElementById('get_toy').value = str;
            document.getElementById('submit_toy').submit();
        }


    </script>


    <body>
        <?php include('../header/header.php'); ?>
        <div class="container-fluid">
            <div class="row">
                <div class="col-sm-12" style="background-color: white;padding-left: 20px;">
                    <?php
                    include('data.php');
                    include('search_form.php');
                    ?>
                </div>
            </div> 
            
            <div class="row">

                <div class="col-sm-12" style="background-color: white;padding-left: 20px;">
                    <?php
 
                    include('list.php');

                    ?>

                    <?php include('form.php'); ?>
                </div>
            </div>

        </div>
        <script type="text/javascript" src="js/menu.js"></script>
    </body>
</html>