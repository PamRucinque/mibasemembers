<form id="submit_toy" action="../toy/toy.php" method="POST" style="align-content: center;">
    <input type="hidden" name="get_toy" id="get_toy" value="" maxlength="4" class="display" readonly="readonly" />
</form>

<?php
$multi_location = $_SESSION['settings']['multi_location'];
?>
<div class="row">

    <div class="col-sm-2"  style="padding-top: 10px;">
        <form name="search_form" method="post" action="toys.php"> 
            <?php include('data/get_category.php'); ?>
        </form>
    </div>
    <div class="col-sm-2"  style="padding-top: 10px;">
        <form name="search_form" method="post" action="toys.php"> 
            <?php include('data/get_attribute.php'); ?>
        </form>
    </div>
    <div class="col-sm-2"  style="padding-top: 10px;">
        <form name="search_form" method="post" action="toys.php"> 
            <?php include('data/get_age.php'); ?>
        </form>
    </div>
    <div class="col-sm-2" style="padding-top: 10px;">
        <form name="search_form" method="post" action="toys.php"> 
            <?php
            if ($multi_location == 'Yes') {
                include('data/get_location.php');
            }else{
                include('data/get_status.php');
            }
            ?> 
        </form>
    </div>
    <div class="col-md-2" style="padding-top: 10px;">
        <form name="search_form" id="search_form" method="post" action="toys.php"> 
            <input id="search" name="search" type="text" placeholder='Search Toys' class='form-control' style='max-width: 200px;'/>

        </form>
    </div>

    <div class="col-sm-1"   style="padding-top: 10px;">
        <script>
            function reset() {
                //alert('hello');
                document.getElementById('category').value = '';
            }
        </script>
        <form name="search_form" method="post" action="toys.php"  onsubmit="reset();"> 
            <input type="submit" id="reset" name="reset" class="btn btn-danger" value="Reset"> 
        </form>
    </div>
    <div class="col-sm-1" style="padding-top: 10px;">
        <?php include('form_pictures.php'); ?>
    </div>
</div>
