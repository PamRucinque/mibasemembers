<?php
$holds_txt = '';
if (!session_id()) {
    session_start();
}
$idcat = $_SESSION['idcat'];
$connect_pdo = $_SESSION['connect_pdo'];
$dbuser = $_SESSION['dbuser'];
$dbpasswd = $_SESSION['dbpasswd'];
$library_code = $_SESSION['library_code'];

$count_rows = 0;

$pdo = new PDO($connect_pdo, $dbuser, $dbpasswd);


$query_toys = "Select toy_attributes.*, attributes.description as description 
from toy_attributes 
left join attributes on attributes.attribute = toy_attributes.attribute
where idcat= ? order by description;";

$sth = $pdo->prepare($query_toys);
$array = array($idcat);
$sth->execute($array);
$result = $sth->fetchAll();
$stherr = $sth->errorInfo();

if ($stherr[0] != '00000') {
    echo "An INSERT query error occurred.\n";
    echo $query_edit;
    echo $connect_pdo;
    echo 'Error' . $stherr[0] . '<br>';
    echo 'Error' . $stherr[1] . '<br>';
    echo 'Error' . $stherr[2] . '<br>';
}


$row_txt = '<table>';
$row_txt .= '<tr><td><h4><font color="blue">Attributes</font><h4></td></tr>';
$today = date("Y-m-d");

$toprint = sizeof($result);

for ($ri = 0; $ri < $toprint; $ri++) {
    $att = $result[$ri];
    $row_txt .= '<tr><td style="padding-right: 10px">' . $att['description'] . '</td>';


    $row_txt .= '</tr>';
    $count_rows = $count_rows + 1;
}
$row_txt .= '</table>';
if ($toprint > 0){
    echo $row_txt;
}



?>