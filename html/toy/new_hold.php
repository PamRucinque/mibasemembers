<?php

$branch = substr(getcwd(), 22, strpos(getcwd() . '/', '/', 22 + 1) - 22);
include('../mibase_check_login.php');
$check = array();
$ok = 'Yes';
$subdomain = $_SESSION['library_code'];
//include('get_settings.php');

if (!session_id()) {
    session_start();
}
$toy_hold_fee = $_SESSION['settings']['toy_hold_fee'];
$msg_str = '';

$idcat = strtoupper($_POST['idcat']);
$borid = $_POST['borid'];
$borname = $_POST['borname'];

//echo $toy_hold_fee;

include('get_status.php');
//echo $date_end;
$date_start = date('Y-m-d');

$query_hold = "INSERT INTO toy_holds "
        . "(borid, idcat, date_start, status,created, paid, id, type_hold) VALUES (?,?,?,'PENDING',now(),'No',?,'M');";

$redirect = 'Location: toy.php?&idcat=' . $idcat;

//$dup_hold = check_dup_hold($idcat, $borid);
delete_hold($idcat, $borid);

$check = check_max_holds($borid);
if ($subdomain === 'bendigo' && ($check['holds'] + 1 === $check['max'])) {
    $msg_str .= 'Reminder: the extra toy must include at least one puzzle!' . "\n" . "\n";
}
if ($check['holds'] >= $check['max']) {
    $ok = 'No';
    $msg_str .= 'Too many holds, max of ' . $check['max'] . ' is allowed for this membership.<br>';
} else {
    
}
//$ok = $check['holds'] . ' ' . $check['max'];
//echo $dup_hold;
//echo $connect_pdo;
if ($ok == 'Yes') {


    $holdid = new_holdid();

    $pdo = new PDO($connect_pdo, $dbuser, $dbpasswd);
    $sth = $pdo->prepare($query_hold);
    $array = array($borid, $idcat, $date_start, $holdid);
    $sth->execute($array);
//$holdid = $sth->fetchColumn();
    $stherr = $sth->errorInfo();
    if ($stherr[0] != '00000') {
        $return = "An INSERT query error occurred.\n";
        $return .= 'Error ' . $stherr[0] . '<br>';
        $return .= 'Error ' . $stherr[1] . '<br>';
        $return .= 'Error ' . $stherr[2] . '<br>';
//echo $return;
//echo $date_end;
    } else {
        if ($toy_hold_fee > 0) {
            include('data/get_member.php');
            $toy_hold_desc = 'Fee for Toy hold ' . $holdid;
            $pdo = new PDO($connect_pdo, $dbuser, $dbpasswd);
            $query_hold = "INSERT INTO journal (datepaid, bcode, icode, name, description, category, amount, type, typepayment, holdid)
                 VALUES (?,?,?,?,?,?,?,?,?,?);";
            $array = array($date_start, $borid, $idcat, $borname, 'Fee for Toy Hold, Toy # ' . $idcat, 'Hold Toy', $toy_hold_fee, 'DR', 'Debit', $holdid);
            $sth = $pdo->prepare($query_hold);
            $sth->execute($array);

            $stherr = $sth->errorInfo();
            if ($stherr[0] != '00000') {
                $return = "An INSERT query error occurred.\n";
                $return .= 'Error ' . $stherr[0] . '<br>';
                $return .= 'Error ' . $stherr[1] . '<br>';
                $return .= 'Error ' . $stherr[2] . '<br>';
                $msg_str .= $return;
            } else {
                $msg_str .= '<font color="blue">' . $holdid . ' :Hold on ' . $_SESSION['idcat'] . ' has been added for ' . $borname . ' (' . $_SESSION['borid'] . ').</font>';
                $msg_str .= '<br>To secure your hold please pay $' . $toy_hold_fee . ' per toy. Total amount due is shown on the My Library page and payment methods are detailed on the Home page. We will email you once your toy is ready for collection. ';
            }
        } else {
            $msg_str .= $holdid . ':Hold on ' . $_SESSION['idcat'] . ' has been added for ' . $borname . ' (' . $_SESSION['borid'] . ').';
            $msg_str .= "\n" . 'Please ensure you book an Appointment to collect your toys.';
        }
        //$msg_str .= $ok;
    }
}

//$msg_str = 'Cannot hold ' . $_SESSION['idcat'] . ' by  ' . $borname . ' (' . $_SESSION['borid'] . '). It already has an active hold.';

$_SESSION['alert_hold'] = $msg_str;
header($redirect);

function addDayswithdate($date, $days) {

    $date = strtotime("+" . $days . " days", strtotime($date));
    return date("Y-m-d", $date);
}

function check_dup_hold($idcat, $borid) {
    $output = 'No';
    $connect_pdo = $_SESSION['connect_pdo'];
    $dbuser = $_SESSION['dbuser'];
    $dbpasswd = $_SESSION['dbpasswd'];
    $query = "select id from toy_holds where idcat = ?";
    $query .= " and borid = ? and status != 'EXPIRED' and status != 'LOANED';";
    $pdo = new PDO($connect_pdo, $dbuser, $dbpasswd);
    $sth = $pdo->prepare($query);
    $array = array($idcat, $borid);
    $sth->execute($array);
    $numrows = $sth->rowCount();
    if ($numrows > 0) {
        $output = 'Yes';
    }

    return $output;
}

function check_max_holds($borid) {
    if (!session_id()) {
        session_start();
    }
    $subdomain = $_SESSION['library_code'];
    $connect_pdo = $_SESSION['connect_pdo'];
    $dbuser = $_SESSION['dbuser'];
    $dbpasswd = $_SESSION['dbpasswd'];
    $query = "select borwrs.id as borid, m.maxnoitems as max, coalesce(holds,0) as holds
                from borwrs 
                left join  membertype m on m.membertype = borwrs.membertype
                left join (select count(id) as holds,borid from toy_holds 
                where trim(UPPER(status)) = 'READY' or trim(upper(status)) = 'ACTIVE' or trim(upper(status)) = 'PENDING'
                group by borid) h on h.borid = borwrs.id
                where borwrs.id = ?;";

    $pdo = new PDO($connect_pdo, $dbuser, $dbpasswd);
    $sth = $pdo->prepare($query);
    $array = array($borid);
    $sth->execute($array);
    $result = $sth->fetchAll();
    $stherr = $sth->errorInfo();
    $numrows = $sth->rowCount();

    $row = $result[0];
    if ($subdomain == 'bendigo') {
        $row['max'] = $row['max'] + 1;
    }

    return $row;
}

function delete_hold($idcat, $borid) {
    $output = 'No';
    $connect_pdo = $_SESSION['connect_pdo'];
    $dbuser = $_SESSION['dbuser'];
    $dbpasswd = $_SESSION['dbpasswd'];
    $query = "delete from toy_holds where idcat = ?";
    $query .= " and borid = ?;";
    $pdo = new PDO($connect_pdo, $dbuser, $dbpasswd);
    $sth = $pdo->prepare($query);
    $array = array($idcat, $borid);
    $sth->execute($array);
}

function new_holdid() {
    $holdid = 0;
    $connect_str = $_SESSION['connect_str'];
    $conn = pg_connect($connect_str);
    $sql = "SELECT MAX(id) as holdid FROM toy_holds;";
//echo $query_new;

    $nextval = pg_Exec($conn, $sql);
//echo $connection_str;
    $row = pg_fetch_array($nextval, 0);
    $holdid = $row['holdid'];
    $holdid = $holdid + 1;
    return $holdid;
}

?>
