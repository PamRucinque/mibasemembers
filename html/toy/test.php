<?php

$borid = 758;
$check = check_max_holds($borid);
print_r($check);

function check_max_holds($borid) {
    if (!session_id()) {
        session_start();
    }

    $connect_pdo = $_SESSION['connect_pdo'];
    $dbuser = $_SESSION['dbuser'];
    $dbpasswd = $_SESSION['dbpasswd'];
    $query = "select borwrs.id as borid, m.maxnoitems as max, coalesce(holds,0) as holds
                from borwrs 
                left join  membertype m on m.membertype = borwrs.membertype
                left join (select count(id) as holds,borid from toy_holds 
                where trim(UPPER(status)) = 'READY' or trim(upper(status)) = 'ACTIVE' or trim(upper(status)) = 'PENDING'
                group by borid) h on h.borid = borwrs.id
                where borwrs.id = ?;";

    $pdo = new PDO($connect_pdo, $dbuser, $dbpasswd);
    $sth = $pdo->prepare($query);
    $array = array($borid);
    $sth->execute($array);
    $result = $sth->fetchAll();
    $stherr = $sth->errorInfo();
    $numrows = $sth->rowCount();

    $row = $result[0];

    return $row;
}
