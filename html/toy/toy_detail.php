<?php
include('get_toy.php');
$mem_hide_duedate = $_SESSION['settings']['mem_hide_duedate'];
?>



<?php
if (isset($_GET['v'])) {
    $v = $_GET['v'];
} else {
    $v = 'list';
}
$str_nav = '<a style="font-size: larger;"  class="btn btn-success" href="../toys/toys.php">Back to Toys List</a>';
if ($v == 'hire') {
    $str_nav = '<a style="font-size: larger;"  class="btn btn-success" href="../toys_hire/toys.php">Back to Toy Hire List</a>';
}
if ($v == 'new') {
    $str_nav = '<a style="font-size: larger;"  class="btn btn-success" href="../toys_new/toys.php">Back to New Toys</a>';
}
if ($v == 'p') {
    $str_nav = '<a style="font-size: larger;"  class="btn btn-success" href="../toys_popular/toys.php">Back to Popular Toys</a>';
}
if ($v == 'lib') {
    $str_nav = '<a style="font-size: larger;"  class="btn btn-success" href="../mylibrary/index.php">Back to My Library</a>';
}
if ($v == 'fav') {
    $str_nav = '<a style="font-size: larger;"  class="btn btn-success" href="../toys_favs/toys.php">Back to My Favourites</a>';
}



$str_title = '<br><h4 align="left"><font color="#900C3F">  ' . $idcat . ': ' . $toyname . '</font></h4>';
?>

<div class="row">
    <div  class="col-sm-6" style="padding-top: 10px;">
        <?php echo $str_nav; ?>
    </div>
    <div  class="col-sm-6"   style="float:right">
        <?php echo $str_title; ?>
    </div>
</div>
<div class="row">
    <div  class="col-sm-6">
        <?php
        echo '<br>' . $str_status . '<br>';
        echo $str_print;
        //echo $str_rent;
        if ($toy_holds == 'Yes') {
            //include('holds.php');
            //echo $holds_txt;
        }
        include('attributes.php');
        ?>
    </div>
    <div  class="col-sm-6"  style="float:right">
        <?php
        if ($row['block_image'] != 'Yes') {
            echo $img;
        }
        ?>
        <div class="row">
            <div  class="col-sm-12">
                <?php echo $str_gold_star; ?>
            </div>
            <div  class="col-sm-6">
                <?php echo $str_reserves; ?>
            </div>
            <div  class="col-sm-6">
                <?php
                //echo $row['favs'];
                if ($row['favs'] == 0) {
                    include('form_fav.php');
                } else {
                    include('form_fav_undo.php');
                }
                ?>
            </div>


        </div>
        <br><div class="row">
            <div  class="col-sm-12">
                <?php
                $castle = '';
                if ($loan_type == 'JC') {
                    $castle = $row['castle'];
                }

                if ($subdomain == 'maroondah') {
                    if ($category == 'JC') {
                        $castle = $row['castle'];
                    } else {
                        $castle = '';
                    }
                }
                echo $castle;
                if (($mem_hire_conditions == 'Yes') && ($reservecode != '' && $reservecode != null)) {
                    echo $row['castle'];
                }
                ?>
            </div>
            <div  class="col-sm-12">
                <?php include('missing_parts.php'); ?>
            </div>
        </div>



        <?php
        $file = '../../toy_images/' . $subdomain . '/' . strtolower($idcat) . '/' . strtolower($idcat) . '.pdf';
        $doc = ' <br><br><a href="' . $file . '" class="btn btn-primary">Open Instructions</a>';

        if (file_exists($file)) {
            echo $doc;
        }
        ?>

    </div>

</div>



