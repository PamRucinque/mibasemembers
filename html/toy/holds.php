<?php
$holds_txt = '';
if (!session_id()) {
    session_start();
}
$connect_pdo = $_SESSION['connect_pdo'];
$dbuser = $_SESSION['dbuser'];
$dbpasswd = $_SESSION['dbpasswd'];
$library_code = $_SESSION['library_code'];

if ($catsort == 'No') {
    $order_by = 'ORDER BY toys.id ASC ';
} else {
    $order_by = 'ORDER BY toys.category, toys.id ASC ';
}

$count_rows = 0;

$pdo = new PDO($connect_pdo, $dbuser, $dbpasswd);


$query_toys = "Select * from toy_holds where idcat=? and (status = 'ACTIVE' or status = 'PENDING' or status = 'READY') order by id desc;";

$sth = $pdo->prepare($query_toys);
$array = array($idcat);
$sth->execute($array);
$result = $sth->fetchAll();
$stherr = $sth->errorInfo();

if ($stherr[0] != '00000') {
    echo "An INSERT query error occurred.\n";
    echo $query_edit;
    echo $connect_pdo;
    echo 'Error' . $stherr[0] . '<br>';
    echo 'Error' . $stherr[1] . '<br>';
    echo 'Error' . $stherr[2] . '<br>';
}

//$query = "SELECT * FROM toys ORDER by id ASC;";


$XX = "No Record Found";

$row_txt = '<table>';
$row_txt .= '<tr><td>Start</td><td>End</td><td>borid</td></tr>';
$today = date("Y-m-d");

$toprint = sizeof($result);

for ($ri = 0; $ri < $toprint; $ri++) {
    $hold = $result[$ri];
    if ($hold['borid'] == $_SESSION['borid']) {
        $row_txt .= "<tr style='background-color: yellow;'>";
    } else {
        $row_txt .= "<tr>";
    }

    $row_txt .= '<td style="padding-right: 10px">' . $hold['date_start'] . '</td>';
    $row_txt .= '<td style="padding-right: 10px">' . $hold['date_end'] . '</td>';
    $row_txt .= '<td style="padding-right: 10px">' . $hold['borid'] . '</td>';

    $row_txt .= '</tr>';
    $count_rows = $count_rows + 1;
}
$row_txt .= '</table>';




if ($count_rows > 0) {
    $holds_txt = '<br><font color="blue"><strong> ' . $count_rows . ' </font> <font color="red"> Toys Holds in the queue</font></strong>';
    $holds_txt .= $row_txt;

}
?>