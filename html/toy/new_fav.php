<?php

$branch = substr(getcwd(), 22, strpos(getcwd() . '/', '/', 22 + 1) - 22);
include('../mibase_check_login.php');
include('../connect.php');

if (!session_id()) {
    session_start();
}

if (isset($_POST['fav_idcat'])) {
    $idcat = $_POST['fav_idcat'];
    $borid = $_SESSION['borid'];
}
$query_hold = "INSERT INTO favs "
        . "(borid, idcat, date_added) VALUES (?,?,now())"
        . " RETURNING id;";
$redirect = 'Location: toy.php?&idcat=' . $idcat;
//DELETE FROM favs where borid = ? and idcat = ?;

try {
    $connect_pdo = $_SESSION['connect_pdo'];
    $dbuser = $_SESSION['dbuser'];
    $dbpasswd = $_SESSION['dbpasswd'];
    $pdo = new PDO($connect_pdo, $dbuser, $dbpasswd);
} catch (PDOException $e) {
    $error = "Error! toy detail: " . $e->getMessage() . "<br/>";
    echo $error;
}


$sth = $pdo->prepare($query_hold);
$array = array($borid, $idcat);
$sth->execute($array);
$holdid = $sth->fetchColumn();
$stherr = $sth->errorInfo();
if ($stherr[0] != '00000') {
    $return = "An INSERT query error occurred.\n";
    $return .= 'Error ' . $stherr[0] . '<br>';
    $return .= 'Error ' . $stherr[1] . '<br>';
    $return .= 'Error ' . $stherr[2] . '<br>';
} else {
    header($redirect);
}
//echo $return;


