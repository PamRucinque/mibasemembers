<?php

if (!session_id()) {
    session_start();
}
$subdomain = $_SESSION['library_code'];
$connect_pdo = $_SESSION['connect_pdo'];
$dbuser = $_SESSION['dbuser'];
$dbpasswd = $_SESSION['dbpasswd'];
$library_code = $_SESSION['library_code'];
$str_reserves = '';
$mem_hide_duedate = $_SESSION['settings']['mem_hide_duedate'];
$storage_label = $_SESSION['settings']['storage_label'];
$mem_reserves = $_SESSION['settings']['mem_reserves'];
$hiretoys_label_button = $_SESSION['settings']['hiretoys_label_button'];
$toy_images = $_SESSION['toy_images'];
$reserve_period = $_SESSION['settings']['reserve_period'];
$toy_holds = $_SESSION['settings']['toy_holds'];
$mem_toy_holds_off = $_SESSION['settings']['mem_toy_holds_off'];
$toy_hold_fee = $_SESSION['settings']['toy_hold_fee'];
$online_hire = $_SESSION['settings']['online_hire'];
$mem_hire_conditions = $_SESSION['settings']['mem_hire_conditions'];
$mem_hold_on_loan = $_SESSION['settings']['mem_hold_on_loan'];

if (isset($_POST['get_toy'])) {
    $_SESSION['idcat'] = $_POST['get_toy'];
}
if (isset($_GET['idcat'])) {
    $_SESSION['idcat'] = $_GET['idcat'];
}
$idcat = $_SESSION['idcat'];

try {
    $pdo = new PDO($connect_pdo, $dbuser, $dbpasswd);
} catch (PDOException $e) {
    $error = "Error! toy detail: " . $e->getMessage() . "<br/>";
    die();
}


$sql = "SELECT toys.*, t.due, l.description as loc_description,
(select count(id) from toy_holds where toys.idcat = toy_holds.idcat limit 1) as toy_holds,
(select castle_link from homepage where id = 1 limit 1) as castle_link,
(select castle from homepage where id = 1 limit 1) as castle, l.description as location_description, toys.location as location,  
coalesce(f.c1,0) as favs 
from toys 
left join (select idcat, return, due from transaction where return is null) t on t.idcat = toys.idcat
left join location l on l.location = toys.location
left join (select count(id) as c1,idcat from favs where borid = ? group by idcat) f on f.idcat = toys.idcat
WHERE upper(toys.idcat) = ?;";
//echo $idcat;
$sth = $pdo->prepare($sql);
$array = array($_SESSION['borid'], $idcat);
$sth->execute($array);

$result = $sth->fetchAll();
$stherr = $sth->errorInfo();
$numrows = $sth->rowCount();

for ($ri = 0; $ri < $numrows; $ri++) {
    $row = $result[$ri];
    $id = $row['idcat'];
    $toy_id = $row['id'];
    $count_toy_holds = $row['toy_holds'];
    //$idcat = $row['toys.idcat'];
    $desc1 = str_replace("\n", "<br>", $row['desc1']);
    $desc2 = str_replace("\n", "<br>", $row['desc2']);
    //$desc1 = $row['desc1'];
    // $desc2 = $row['desc2'];
    $loc_desc = $row['location_description'];
    $location = $row['location'];
    $loan_type = trim($row['loan_type']);
    $category = $row['category'];
    $reservecode = $row['reservecode'];
    $toyname = $row['toyname'];
    $rent = $row['rent'];
    $age = $row['age'];
    $datestocktake = $row['datestocktake'];
    $manufacturer = $row['manufacturer'];
    $discountcost = $row['discountcost'];
    $status = $row['status'];
    $cost = $row['cost'];
    $supplier = $row['supplier'];
    $comments = $row['comments'];
    $returndateperiod = $row['returndateperiod'];
    $returndate = $row['returndate'];
    $no_pieces = $row['no_pieces'];
    $date_purchase = $row['date_purchase'];
    $sponsorname = $row['sponsorname'];
    $warnings = $row['warnings'];
    $user1 = $row['user1'];
    //$date_entered = date_format($row['date_entered'], 'd/m/y');
    $alert = $row['alert'];
    $toy_status = $row['toy_status'];
    $stype = $row['stype'];
    $units = $row['units'];
    $borrower = $row['borrower'];
    $lockdate = $row['datestocktake'];
    $alerts = $row['alert'];
    $lockreason = $row['lockreason'];
    $created = $row['created'];
    $modified = $row['modified'];
    $condition = $row['reservedfor'];
    $storage = $row['storage'];
    $trans_due = $row['due'];
    $product_link = $row['link'];
    $format_due = substr($row['due'], 8, 2) . '-' . substr($row['due'], 5, 2) . '-' . substr($row['due'], 0, 4);

    $format_purchase = substr($row['date_purchase'], 8, 2) . '-' . substr($row['date_purchase'], 5, 2) . '-' . substr($row['date_purchase'], 0, 4);
    $format_lock = substr($row['date_purchase'], 8, 2) . '-' . substr($row['date_purchase'], 5, 2) . '-' . substr($row['date_purchase'], 0, 4);
    if ($cost == Null) {
        $cost = 0;
    }
    if ($discountcost == Null) {
        $discountcost = 0;
    }
    if ($rent == Null) {
        $rent = 0;
    }
    if ($rent > 0) {
        $str_rent = '<strong><font="green">Hire Charge: </font>$</strong>' . $rent . ' per ' . $reserve_period . ' days.</font>';
        //$str_rent .= ' Hire Toys are for ' . $reserve_period . ' days </h2>';
    }
    if (($trans_due == null) || ($trans_due == '')) {
        if (($location == '') || ($location == null)) {
            $str_status = '<a style="background-color: lightgreen;"><font color="#330066">IN LIBRARY</font></a>';
        } else {
            $str_status = '<a style="background-color: lightgreen;"><font color="#330066">IN LIBRARY: ' . strtoupper($loc_desc) . '</font></a>';
        }
    } else {

        $str_status = '<a style="background-color: yellow;font-size: 20px;"><font color="#330066"></font>';
        if ($mem_hide_duedate == 'Yes') {
            $str_status = '</a>';
        } else {
            $str_status .= '<font color="#330066"> Due: ' . $format_due . '</font></a> ';
        }
    }




    $_SESSION['idcat'] = $idcat;
}

if ($stherr[0] != '00000') {
    $_SESSION['login_status'] .= "An  error occurred.\n";
    $_SESSION['login_status'] .= 'Error' . $stherr[0] . '<br>';
    $_SESSION['login_status'] .= 'Error' . $stherr[1] . '<br>';
    $_SESSION['login_status'] .= 'Error' . $stherr[2] . '<br>';
}
$desc1 = str_replace("\r\n", "</br>", $desc1);
$desc2 = str_replace("\r\n", "</br>", $desc2);
//$_SESSION['reserve_status'] = '';
//$toy_images = '/toy_images';
$file_pic = '../..' . $toy_images . '/' . $library_code . '/' . strtolower($idcat) . '.jpg';

if (file_exists($file_pic)) {
    $img = '<img width="230px" src="../..' . $toy_images . '/' . $library_code . '/' . strtolower($idcat) . '.jpg" alt="toy image">';
} else {
    //$img = '<img width="230px" src="../toy_images/demo/' . 'blank' . '.jpg" alt="toy image">';
    $img = '<img width="230px" src="../..' . $toy_images . '/demo/blank.jpg" alt="">';
}
$onloan = 'No';
if (($trans_due == null) || ($trans_due == '')) {
    if (($location == '') || ($location == null)) {
        $str_status = '<a style="background-color: lightgreen;font-size: 20px;"><font color="#330066">IN LIBRARY</font></a>';
    } else {
        $str_status = '<a style="background-color: lightgreen;font-size: 20px;"><font color="#330066">IN LIBRARY: ' . strtoupper($loc_desc) . '</font></a>';
    }
} else {

    $str_status = '<a style="background-color: yellow;font-size: 20px;"><font color="#330066"></font>';
    $onloan = 'Yes';
    if ($mem_hide_duedate == 'Yes') {
        $str_status = '</a>';
    } else {
        $str_status .= '<font color="#330066"> Due: ' . $format_due . '</font></a> ';
    }
}


//echo $img;

$str_print = '';
$str_print_reserves = '<div id="detail" style="padding-left: 10px">' . $str_status;
if ($toy_status == 'LOCKED' || $toy_status == 'WITHDRAWN') {
    $str_print .= '<strong><font color="blue">Lock Date:</font></strong>' . $lockdate . ': Reason: ' . $lockreason . '<br>';
}
//$str_print .= '<strong><font color="blue">Toy Number: </font></strong>' . $idcat . '<br>';
$str_print .= '<strong><font color="blue">Category: </font></strong>' . $category . '<br>';
if ($row['sub_category'] != '') {
    $str_print .= '<strong><font color="blue">Sub Category: </font></strong>' . $row['sub_category'] . '<br>';
}
$str_print_reserves .= '<strong><font color="blue">Category: </font></strong>' . $category . '<br>';
$str_print .= '<strong><font color="blue">No Pieces: </font></strong>' . $no_pieces . '<br>';
if ($row['stocktake_status'] == 'LOCKED') {
    $str_print .= '<strong><font color="red">This Toy is Locked for Stock take.</font></strong></font><br>';
}
$str_print_reserves .= '<strong><font color="blue">No Pieces: </font></strong>' . $no_pieces . '<br>';

$str_print .= '<strong><font color="blue">Description: </font></strong><br>' . $desc1 . '<br>' . $desc2 . '<br>';

if ($age != '') {
    $str_print .= '<strong><font color="blue">Age: </font></strong>' . $age . '<br>';
    $str_print_reserves .= '<strong><font color="blue">Age: </font></strong>' . $age . '<br>';
}

if ($product_link != '') {
    $str_print .= 'Product Link to this Toy: <a  target="_blank"  href="' . $product_link . '">More...</a><br>';
}

if ($loan_type != '') {
    //$str_print .= '<strong><font color="blue">Loan Type: </font></strong>' . $loan_type . '<br>';
}

if (($rent > 0)) {

    $str_print .= '<strong><font="green">Hire Charge: </font>$</strong>' . $rent . ' per ' . $reserve_period . ' days.</font><br>';
    //if ($row['loan_type'] == 'JC') {
    $str_hire_conditions = $row['castle_link'];
    //}
}

if ($storage_label == '') {
    $storage_label = 'Storage: ';
}
if ($storage != '') {
    $str_print .= '<strong><font color="blue"> ' . $storage_label . '</font>  </strong>' . $storage . '<br>';
}

$str_print .= '<strong><font color="red">' . $warnings . '</font></strong>';

if (($mem_reserves == 'Yes') && ($reservecode != '')) {


    $str_reserves = '<br><a id="button" class="btn btn-warning" style="font-size: 14px;" href="../reserves/index.php?idcat=' . $_SESSION['idcat'] . '">Reserve</a>';
    //echo "<h1><font color='red'>Please don't use Reservations work in progress!!</font></h1>";
    //include('reserve_page.php');

    if ($loan_type == 'GOLD STAR') {
        $str_gold_star = '<font color="red">This Toy is a Party Pack Toy or a Gold Star Toy and cannot be renewed.</font><br>';
    }
}
$str_print_reserves .= '</div>';
//$str_print .= $str_reserves;

$hold_btn = '';

$hold_btn .= '<br><div><form action="new_hold.php" method="post">';
$hold_btn .= '<input type="hidden" id="idcat" name = "idcat" required  value="' . $_SESSION['idcat'] . '">';
$hold_btn .= '<input type="hidden" id="borid" name = "borid" value="' . $_SESSION['borid'] . '" >';
$hold_btn .= '<input type="hidden" id="borname" name = "borname" value="' . $_SESSION['membername'] . '">';
$hold_btn .= '<input type="submit" id="hold" class="btn btn-primary" name="hold" value="Hold Toy" style="background-color: #F660AB;border-color:#F660AB;"/>';
$hold_btn .= '</form></div><br>';
//$hold_btn .= $count_toy_holds;
$multi_location = $_SESSION['settings']['multi_location'];
if ($toy_holds == 'Yes' && $mem_toy_holds_off != 'Yes' && $loan_type != 'GOLD STAR') {
    if ($multi_location === 'Yes' && $_SESSION['location'] === '' && $subdomain !== 'melbourne') {
        $str_reserves .= '<div><font color="red">Please book an appointment, to hold this toy.</font></div>';
        if ($subdomain !== 'ballarat') {
            $hold_btn = '';
        }
    }
    if ($multi_location === 'Yes' && $location !== $_SESSION['location'] && $subdomain === 'moreland') {
        $str_reserves .= '<div><font color="red">This Toy is located at ' . $loc_desc . '.</font></div>';
        $hold_btn = '';
    }
    if (($reservecode == '') || ($reservecode == null)) {
        if ($count_toy_holds == 0) {
            if ($trans_due == null) {

                if ($row['stocktake_status'] !== 'LOCKED' && $toy_status === 'ACTIVE') {

                    $str_reserves .= $hold_btn;
                }
            } else {
                $str_reserves .= '<div><br>This toy is on loan, cannot hold.</div>';
            }
        } else {

            $str_reserves .= '<div><br>This toy on hold.</div>';
        }
    }
}



