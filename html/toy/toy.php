<!DOCTYPE html>
<!--[if lt IE 8 ]> <html class="no-js ie7 oldie" lang="en"> <![endif]-->
<!--[if IE 8]> <html class="no-js ie8 oldie" lang="en"> <![endif]-->
<!--[if IE 9]> <html class="no-js ie9" lang="en"> <![endif]-->
<!--[if (gt IE 9)|!(IE)]><!--> <html class="no-jQuery" lang="en"> <!--<![endif]-->
    <head>
        <?php include('../header/head.php'); ?>
        <script>
            function setFocus()
            {

                var msg = document.getElementById("msg").innerHTML;
                if (msg !== '') {
                    $('#myModal').modal('show');
                }

            }

        </script>

    </head>
    <?php
    $str_alert = '';
    if (!session_id()) {
        session_start();
    }
    if (isset($_SESSION['alert_hold'])) {
        $str_alert = $_SESSION['alert_hold'];
    }

    $blinking = '';
    $str_gold_star = '';
    $mem_hold_on_loan = $_SESSION['settings']['mem_hold_on_loan'];
    include('../home/get_index_info.php');
    ?>

    <body  onload="setFocus()">
        <?php include('../header/header.php'); ?>
        <div class="container-fluid">
            <?php
            if (!session_id()) {
                session_start();
            }
            $catsort = $_SESSION['settings']['catsort'];
            $java_str = "$(location).attr('href', 'toy.php?idcat=" . $_SESSION['idcat'] . "')";
            $button_str = 'OK';

            include('toy_detail.php');
//echo $_SESSION['idcat'];
            ?>
        </section>
        <section class="container fluid">
            <div id='msg' style='display: block;'><?php echo $str_alert; ?></div>
            <!-- Trigger the modal with a button -->
            <!-- Modal -->
            <div class="modal fade" id="myModal" role="dialog"  data-backdrop="static" data-keyboard="false">
                <div class="modal-dialog">

                    <!-- Modal content-->
                    <div class="modal-content">
                        <div class="modal-header">

                            <h4 class="modal-title">Mibase Alert</h4>
                        </div>
                        <div class="modal-body">
                            <p><?php echo $str_alert; ?></p>
                        </div>
                        <div class="modal-footer">
                            <?php echo $blinking; ?>
                            <button type="button" class="btn btn-danger" data-dismiss="modal" id="update_terms" name="update_terms" onclick="<?php echo $java_str; ?>"><?php echo $button_str; ?></button>
                        </div>
                    </div>

                </div>
            </div>

    </div>
    <div style="height:100px"></div>
    <script type="text/javascript" src="../js/menu.js"></script>
    <script type="text/javascript" src="../js/jquery.ui.datepicker.js"></script>
    <script type="text/javascript" src="../js/jquery-ui.js"></script>
    <script type="text/javascript" src="../js/bootstrap.min.js"></script>

</body>
</html>
<?php
$str_alert = '';
$_SESSION['alert_hold'] = '';


