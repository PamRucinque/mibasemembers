<?php

$branch = substr(getcwd(), 22, strpos(getcwd() . '/', '/', 22 + 1) - 22);
include(dirname(__FILE__) . '/../mibase_check_login.php');

function get_roster_msg($mem_levy, $duties, $completed, $expired, $roster_expired) {
    $format_expired = substr($expired, 8, 2) . '/' . substr($expired, 5, 2) . '/' . substr($expired, 0, 4);
    $duties = round($duties,0);

    $expire_soon = 'No';
    if (($expired <= date('Y-m-d', strtotime('+6 months'))) && ($expired >= date('Y-m-d'))) {
        $expire_soon = 'Yes';
    }
    $levy = round($mem_levy / 4, 0);
    $out = '';

    $num = $duties - $completed;
    if (($num > 1) && ($expire_soon == 'Yes')) {
        $out .= ' <br>Please complete ' . $num . ' duties by ' . $format_expired . ' or pay $' . $levy * $num . ' non-duty levy.';
    } else {
        $out = '';
    }
    if (($roster_expired == 'Yes')&&($duties > 0)){
        $out .= '<br> Completed ' . $completed . ' dutie(s) out of ' . $duties . '.';
    }

    return array('out' => $out, 'duties_required' => $required);
}

function mibase_validate($input, $username, $pwd, $var) {
    $pattern = '/^\w{0,3}-?\w{1,6}$/';
    //$pattern = '/\d+/';
    if (preg_match($pattern, $input) != 0) {
        //echo 'Doesnt matchAAAA' . $getid;
        $return_value = $input;
    } else {
        $ip = $_SERVER['REMOTE_ADDR'];
        $location = $_SERVER['SCRIPT_FILENAME'];
        $subdomain = $_SERVER['HTTP_HOST'];
        $connect_pdo = "pgsql:port=5432;dbname=toybase;host=localhost";
        $dbuser = "toybase";
        $dbpasswd = "xyz";
        $pdo = new PDO($connect_pdo, $dbuser, $dbpasswd);
        if ($username == '') {
            $username = 'Not logged in';
        }
        if ($login_type == '') {
            $login_type = 'NA';
        }
        if ($pwd == '') {
            $pwd = 'NA';
        }


        $query = "INSERT INTO blocked (ip, injection, 
                 username, pwd, type_block, subdomain, login_type, page ,var)
                 VALUES (?,?,?,?,?,?,?,?,?);";
        //and get the statment object back
        $sth = $pdo->prepare($query);

        $array = array($ip, $input, $username, $pwd, 'SQL Injection', $subdomain, $login_type, $location, $var);


        //execute the preparedstament
        //$sth->execute($array);
        $stherr = $sth->errorInfo();

        if ($stherr[0] != '00000') {
            //echo "An INSERT query error occurred.\n";
            //echo $connect_pdo;
            //$return_value .= 'Error ' . $stherr[0] . '<br>';
            //$return_value .= 'Error ' . $stherr[1] . '<br>';
            //$return_value .= 'Error ' . $stherr[2] . '<br>';
        }

        $return_value = '';
    }
    return $return_value;
}

function mibase_url($branch, $subdomain) {
    $url = 'https://' . $subdomain . $domain . $branch . '/';
    return $url;
}

function mibase_validate_id($idcat, $id, $type) {
    $pattern = '/^[a-zA-Z]+$/';
    $pattern_integer = '/^\d+$/';
    $return = 'OK';

    //$pattern = '/\d+/';
    if ($type == 'id') {
        if (preg_match($pattern_integer, $id) == 0) {
            $return .= 'Login error: Memberid  fails the regular expression test: ' . $id . ': ';
        }
        if ((strlen($id)) > 10) {
            $length = strlen($memberid);
            $return .= 'id exceeds max length of 10: ' . $length . ': ';
        }
    }
    if ($type == 'idcat') {
        if (preg_match($pattern, $idcat) == 0) {
            $return .= 'IDCat fails the regular expression test: ' . $idcat . ': ';
        }
    }


    return $return;
}
