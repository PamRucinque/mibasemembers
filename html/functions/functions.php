<?php
$branch = substr(getcwd(), 22, strpos(getcwd() . '/', '/', 22 + 1) - 22);
include(get_include_path() . '/mibaselive/html/' . $branch . '/mibase_check_login.php');

function clean($string){
    $length = strlen($string);
    $clean_string = '';
    $replacement = '?';
    $pattern = '/^[\w_@\., \$]$/i';
    for ($x = 0; $x < $length; $x++) {
        $char = substr($string, $x, 1);
        //echo $x . ': ' . $char;
        if (preg_match($pattern, $char)){
            $clean_string .= $char; 
        }else{
            $clean_string .= $replacement;
        }
    }
    
    return $clean_string;
}
function clean_search($string){
    $length = strlen($string);
    $clean_string = '';
    $replacement = '?';
    $pattern = '/^[\w_@\., \$]$/i';
    for ($x = 0; $x < $length; $x++) {
        $char = substr($string, $x, 1);
        //echo $x . ': ' . $char;
        if (preg_match($pattern, $char)){
            $clean_string .= $char; 
        }else{
            $clean_string .= $replacement;
        }
    }
    
    return $clean_string;
}


function clean_id($string){
    $length = strlen($string);
    $clean_string = '';
    $replacement = '?';
    $pattern = '/^[\w\-]$/i';
    for ($x = 0; $x < $length; $x++) {
        $char = substr($string, $x, 1);
        //echo $x . ': ' . $char;
        if (preg_match($pattern, $char)){
            $clean_string .= $char; 
        }else{
            $clean_string .= '';
        }
    }
    if ($length > 15){
        $clean_string = 'error';
    }
    
    return $clean_string;
}


