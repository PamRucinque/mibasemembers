<?php

echo '<br><h2>Library Settings</h2>';
echo check_in_settings('libraryname');
echo check_in_settings('library_address');
echo check_in_settings('open_hours');
echo check_in_settings('multi_location');
echo check_in_settings('email_from');
echo check_in_settings('receipt_printer');
echo check_in_settings('receipt_printer_vol');
echo check_in_settings('loan_receipt');
echo check_in_settings('toy_barcode_notrim');
echo check_in_settings('idcat_noedit');
echo check_in_settings('format_toyid');
echo check_in_settings('delete_password');

echo '<br><h2>Display Settings</h2><br>';
echo check_in_settings('show_desc');
echo check_in_settings('hide_value');
echo check_in_settings('suburb_title');
echo check_in_settings('vol_new_member');
echo check_in_settings('vol_notes');
echo check_in_settings('vol_membertype');
echo check_in_settings('vol_disable_duty_reminder');
echo check_in_settings('vol_no_fine');
echo check_in_settings('vol_expired_off');
echo check_in_settings('vol_missing_off');
echo check_in_settings('vol_renew_member_off');
echo check_in_settings('vol_roster_off');
echo check_in_settings('vol_edittoy_off');
//echo check_in_settings('hide_balance');
////echo check_in_settings('disable_strrent');
//echo check_in_settings('display_overdues');
//echo check_in_settings('admin_notes');


echo '<br><h2>Extra Functions</h2><br>';
echo check_in_settings('set_due_date');
echo check_in_settings('set_loan_date');
echo check_in_settings('free_rent_btn');
echo check_in_settings('returns_off');
echo check_in_settings('unlock_returns');
echo check_in_settings('stocktake_override');
//echo check_in_settings('disable_returnall');
//echo check_in_settings('renew_member_off');
//echo check_in_settings('auto_debit_missing');

echo '<br><h2>New for Mobile Mibase</h2><br>';

echo check_in_settings('mobile_payments');
echo check_in_settings('mobile_settings');


echo '<br><h2>Alerts</h2><br>';

echo check_in_settings('system_alert');
echo check_in_settings('new_member_alert');
echo check_in_settings('duty_alert');
echo check_in_settings('roster_alerts');
echo check_in_settings('reservation_alert');
echo check_in_settings('member_alerts');
echo check_in_settings('term_dates');
echo check_in_settings('next_term_alert');


echo '<br><h2>Fine Calculations</h2><br>';
echo check_in_settings('recordfine');
echo check_in_settings('overdue_fine');
echo check_in_settings('weekly_fine');
echo check_in_settings('fine_value');
echo check_in_settings('grace');
echo check_in_settings('max_daily_fine');
echo check_in_settings('catsort');
echo check_in_settings('roundup_week');
echo check_in_settings('rounddown_week');
//echo check_in_settings('override_date_loan');
//echo check_in_settings('fine_factor');
//echo check_in_settings('upgrade_rent_factor');

echo '<br><h2>Loans</h2><br>';

echo check_in_settings('loan_special');
echo check_in_settings('loanperiod');
echo check_in_settings('renew_loan_period');
echo check_in_settings('loan_restrictions');
echo check_in_settings('holiday_override');
echo check_in_settings('times_renew');
echo check_in_settings('checked');
echo check_in_settings('overdue_txt');
echo check_in_settings('holiday_due');
echo check_in_settings('automatic_debit_off');
echo check_in_settings('stocktake_override');
//new
echo check_in_settings('loanperiod_mem');
echo check_in_settings('lp_custom');
echo check_in_settings('loan_special');
echo check_in_settings('loan_weighting');



echo '<br><h2>Reservations</h2><br>';
echo check_in_settings('reserve_period');
echo check_in_settings('reservations');
echo check_in_settings('mem_reserves');

echo '<br><h2>Holds</h2><br>';
echo check_in_settings('toy_holds');
echo check_in_settings('toy_hold_fee');


echo '<br><h2>Rent</h2>';
echo check_in_settings('chargerent');
echo check_in_settings('rentasfine');
echo check_in_settings('global_rent');

echo '<br><h2>Default Settings</h2>';
echo check_in_settings('loan_default_missing_paid');
echo check_in_settings('loan_default_missing_alert');
echo check_in_settings('state');
echo check_in_settings('automatic_debit_off');
echo check_in_settings('default_paymenttype');

echo '<br><h2>Roster Settings</h2>';
echo check_in_settings('rosters_annual');
echo check_in_settings('mem_private');
echo check_in_settings('multi_location');
echo check_in_settings('mem_roster_lock');
echo check_in_settings('mem_coord_off');
echo check_in_settings('mem_cancel_roster');
echo check_in_settings('mem_roster_show_all');



/** 

settings for mibase mobile

functions and display
8c4618838e83cc5306e4ea384a9f7114

mobile_payments
mobile_settings

vol_new_member
free_rent_btn
set_due_date
set_loan_date
toy_holds
toy_hold_fee
sub_category_loans
show_desc
hide_value
reservations
disable_returnall
hide_balance
disable_strrent
renew_member_off
display_overdues
vol_expired_off
vol_notes
vol_balance
vol_membertype
admin_notes
auto_debit_missing
vol_disable_duty_reminder
vol_no_fine
vol_renew_member_off
vol_missing_off
vol_roster_off
vol_edittoy_off
system_alert
returns_off


recordfine
overdue_fine
chargerent
weekly_fine
fine_value
grace
rentasfine
max_daily_fine
unlock_returns
catsort
roundup_week
rounddown_week
override_date_loan
fine_factor
upgrade_rent_factor



special

max_visits
special
special_return
special_loan
fortnight
fine_per_member
daily_min_fine
global_rent
reserve_fee
loan_weighting
max_toys_pa

Loans

loan_special
stocktake_override
loanperiod
renew_loan_period
reserve_period
loan_restrictions
rent_factor
extra_rent
reservation_alert
duty_alert
roster_alerts
times_renew
new_member_alert
checked
member_alerts



Default Settings

loan_default_missing_paid
loan_default_missing_alert

Library Settings

email_from
receipt_email
receipt_printer
receipt_printer_vol
term_dates
term_alert
next_term_alert
libraryname
format_toyid
loan_receipt
holiday_override
toy_barcode_notrim
holiday_due
disable_cc_email
multi_location
library_address
idcat_noedit
overdue_txt
open_hours
delete_password
suburb_title
mem_reserves
automatic_debit_off
 */









