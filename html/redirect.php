<?php

$redirect = '';
if ($library_code == 'caulfield') {
    $redirect = 'Location: https://carnegie.mibase.com.au/mem/login.php';
}
if ($library_code == 'kensington') {
    $redirect = 'Location: https://melbourne.mibase.com.au/mem/login.php';
}
if ($library_code == 'carlton') {
    $redirect = 'Location: https://melbourne.mibase.com.au/mem/login.php';
}
if ($library_code == 'wanganui') {
    $redirect = 'Location: https://whanganuitl.mibase.co.nz/members/login.php';
}

header($redirect);

