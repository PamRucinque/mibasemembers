<?php include('../mibase_check_login.php'); ?>

<form action="save.php" method="post" >

    <div class="row">
        <div class="col-sm-2"></div>

        <div class="col-sm-4" style="background-color:lavender;">
            <h2>Edit My Details</h2>
            <div class="form-group">
                <h4>First Contact</h4>



                <label for="firstname">First name:</label>
                <input type="text" class="form-control" id="firstname" placeholder="Enter Firstname" name="firstname" value="<?php echo $member['firstname']; ?>">
                <label for="surname">Surname:</label>
                <input type="text" class="form-control" id="surname" placeholder="Enter Surname" name="surname" value="<?php echo $member['surname']; ?>">
                <label for="mobile">Mobile Number: </label>
                <input type="text" class="form-control" id="mobile" placeholder="Enter Mobile Number" name="mobile" value="<?php echo $member['phone2']; ?>">
                <label for="work">Email:</label>
                <input type="text" class="form-control" id="email" placeholder="Email" name="email" value="<?php echo $member['emailaddress']; ?>">
                <label for="state">Address:</label>
                <input type="text" class="form-control" id="address" placeholder="Enter Address" name="address" value="<?php echo $member['address']; ?>">

                <label for="suburb">Suburb:</label>
                <input type="text" class="form-control" id="suburb" placeholder="Enter Suburb" name="suburb" value="<?php echo $member['suburb']; ?>">
                <label for="suburb">Postcode:</label>
                <input type="text" class="form-control" id="postcode" placeholder="Enter Postcode" name="postcode" value="<?php echo $member['postcode']; ?>">
                <label for="suburb">City:</label>
                <input type="text" class="form-control" id="city" placeholder="City" name="city" value="<?php echo $member['city']; ?>">
                <label for="state">State:</label>
                <input type="text" class="form-control" id="state" placeholder="Enter State" name="state" value="<?php echo $member['state']; ?>">


            </div>
        </div>
        <div class="col-sm-4" style="background-color:lavender;">
            <div class="form-group">
                <h4>Second Contact</h4>
                <label for="firstname">First name:</label>
                <input type="text" class="form-control" id="firstname1" placeholder="Enter Firstname" name="firstname1" value="<?php echo $member['partnersname']; ?>">
                <label for="surname">Surname:</label>
                <input type="text" class="form-control" id="surname1" placeholder="Enter Surname" name="surname1" value="<?php echo $member['partnerssurname']; ?>">
                <label for="mobile">Mobile Number: </label>
                <input type="text" class="form-control" id="mobile1" placeholder="Enter Mobile Number" name="mobile1" value="<?php echo $member['mobile1']; ?>">
                <label for="work">Email:</label>
                <input type="text" class="form-control" id="email2" placeholder="Email" name="email2" value="<?php echo $member['email2']; ?>">
                <br><button type="submit" class="btn btn-primary">Save My Details</button>
                <input type="hidden" id="id" name="id" value="<?php echo $_SESSION['borid']; ?>">
            </div>

        </div>
        <div class="col-sm-2" style="background-color:lavender;">

        </div>
    </div>
</form> 
