<!DOCTYPE html>
<!--[if lt IE 8 ]> <html class="no-js ie7 oldie" lang="en"> <![endif]-->
<!--[if IE 8]> <html class="no-js ie8 oldie" lang="en"> <![endif]-->
<!--[if IE 9]> <html class="no-js ie9" lang="en"> <![endif]-->
<!--[if (gt IE 9)|!(IE)]><!--> <html class="no-jQuery" lang="en"> <!--<![endif]-->
    <head>
        <?php
        include('../mibase_check_login.php');
        include('../header/head.php');
        ?>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
        <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>

    </head>

    <body width="300px">
        <section class="container fluid">

            <div class="col-sm-12">
                <?php
                if (!session_id()) {
                    session_start();
                }

                include('../header/menu.php');
                ?>
            </div>
        </section>
        <section class="container" style="width: 100%;">
            <?php
            if (isset($_POST['submit'])) {
                include('save.php');
            } else {
                include('get_member.php');
                echo '<h1>' . $member['firstname'] . ' ' . $member['surname'] . '</h1>'; 
                include ('edit_form.php');
            }
            ?>

        </section>

        <script type="text/javascript" src="../js/menu.js"></script>

    </body>
</html>


