<?php

/*
 * Copyright (C) 2018 Michelle Baird
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

//the database host and port
$dbhost = 'localhost';
$dbport = '5432';

// Path to application root folder
$app_root = '/mibasemembers';

// Path to toy images root folder
$toy_images = '/toy_images';
$login_image_folder = 'login_image/';

// Path to reports root folder
$report_location = '/mibaseopen/reports';

// Path to news
$news_location = '/news';

//the protocol for the web server either http or https
$web_server_protocol = 'http';

//the filing system path of the web server root folder
//this setting is only needed for file operations ( toy images and news files )
$web_root_folder = 'C:/Apache24/htdocs';

#Shared or Single System
//if this is a shared server that manages multiple toy libraries then set this value true
//for a non shared environment, ie one library on one server set this to false
$shared_server = false;

//links on menu to other mibase isntallations

$home_location = '../mibasewebsite';


#--------------------
#for a single server 
#--------------------
// these settings point at the toy libries database

$dbname = 'mylibrary';
$dbuser = 'mylibrary';
$dbpasswd = 'admin';

#--------------------
#for a shared server 
#--------------------
//specify the domain 
if ($shared_server) {
    $shared_domain = 'mibase.com.au';
} else {
    $shared_domain = 'localhost';
}


//for a shared system these variables point at the toybase database
$toybasedbname = 'toybase_open';
$toybasedbuser = 'toybase_open';
$toybasedbpasswd = 'password';

// specify the path to the login image files
//the images are library_code.jpg
$login_image_folder = '../login_image';

#--------------------
#Mibase Development Settings 
#--------------------
//development mode of mibase feautures that are not ready yet, please set to No
$mibase_server = 'No';

