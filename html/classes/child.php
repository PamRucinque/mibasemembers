<?php

class Child {

    public $id, $dob, $child_name, $surname, $gender, $borid, $alert,
            $notes, $sql_error, $result;

    public function data_from_table($id) {
        if (!session_id()) {
            session_start();
        }
        $connect_pdo = $_SESSION['connect_pdo'];
        $dbuser = $_SESSION['dbuser'];
        $dbpasswd = $_SESSION['dbpasswd'];
        $dbpwd = $_SESSION['dbpasswd'];
        $this->result = false;

        $sql = "select * from children where childid = ?;";

        $pdo = new PDO($connect_pdo, $dbuser, $dbpasswd);
        $sth = $pdo->prepare($sql);
        $array = array($id);
        $sth->execute($array);

        $result = $sth->fetchAll();
        $numrows = $sth->rowCount();
        $stherr = $sth->errorInfo();
        if ($stherr[0] != '00000') {
            $status .= "An  error occurred " . $stherr[0] . " " . $stherr[1] . "" . $stherr[2];
        }
        for ($ri = 0; $ri < $numrows; $ri++) {
            $r = $result[$ri];
            $this->id = $r['childid'];
            $this->borid = $r['id'];
            $this->dob = $r['d_o_b'];
            $this->gender = $r['sex'];
            $this->alert = $r['alert'];
            $this->surname = $r['surname'];
            $this->child_name = $r['child_name'];
            $this->notes = $r['notes'];
            $this->result = true;
        }
        $this->numrows = $numrows;
    }

    public function save() {
        if (!session_id()) {
            session_start();
        }
        $connect_pdo = $_SESSION['connect_pdo'];
        $dbuser = $_SESSION['dbuser'];
        $dbpasswd = $_SESSION['dbpasswd'];
        $dbpwd = $_SESSION['dbpasswd'];
        $this->result = false;

        $sql = "update children "
                . "set child_name=?,  "
                . "  surname=?, d_o_b=?, sex=?, id=?  "
                . "WHERE childid = ?;";

        if ($this->id == 0) {
            $sql = "INSERT INTO children (child_name,surname, d_o_b, sex, id, childid) "
                    . " VALUES (?,?,?,?,?,?);";
            $id = $this->get_newid();
            //echo $id;
        } else {
            $id = $this->id;
        }

        $pdo = new PDO($connect_pdo, $dbuser, $dbpasswd);
        $sth = $pdo->prepare($sql);

        $u = array();
        $u[] = $this->child_name;
        $u[] = $this->surname;
        $u[] = $this->dob;
        $u[] = $this->gender;
        $u[] = $this->borid;
        $u[] = $this->notes;
        $u[] = $id;
        $sth->execute($u);

        $stherr = $sth->errorInfo();
        if ($stherr[0] != '00000') {
            //echo "An  error occurred " . $stherr[0] . " " . $stherr[1] . "" . $stherr[2];
            $this->sql_error = "An  error occurred " . $stherr[0] . " " . $stherr[1] . "" . $stherr[2];
            $this->result = false;
        } else {
            $this->result = true;
        }
    }

    public function delete($id) {
        include('../connect/connect.php');
        $pdo = new PDO($connect_pdo, $dbuser, $dbpasswd);
        $sql = "delete from repairers where id = ?;";
        $sth = $pdo->prepare($sql);

        $array = array($id);

        $sth->execute($array);
        $stherr = $sth->errorInfo();
        if ($stherr[0] != '00000') {
            //echo "An  error occurred " . $stherr[0] . " " . $stherr[1] . "" . $stherr[2];
            $this->sql_error = "A delete error occurred " . $stherr[0] . " " . $stherr[1] . "" . $stherr[2];
            $this->result = false;
        } else {
            $this->result = true;
        }
    }

    public static function get_all_r($state) {
        include('../connect/connect.php');

        $sql = "select * from repairers where state LIKE ? order by company_name;";

        $pdo = new PDO($connect_pdo, $dbuser, $dbpasswd);
        $sth = $pdo->prepare($sql);
        $str = '%' . $state . '%';
        $array = array($str);
        $sth->execute($array);


        $result = $sth->fetchAll();
        $numrows = $sth->rowCount();
        $stherr = $sth->errorInfo();
        if ($stherr[0] != '00000') {
            $status .= "An  error occurred " . $stherr[0] . " " . $stherr[1] . "" . $stherr[2];
        }
        $rs = array();
        for ($ri = 0; $ri < $numrows; $ri++) {
            $r = new Repairer();
            $r->fill_from_array($result[$ri]);
            $rs[] = $r;
        }

        return $rs;
    }

    public static function get_selected_r($selected) {
        include('../connect/connect.php');

        $sql = "select * from repairers where id IN (" . $selected . ") order by company_name;";



        $pdo = new PDO($connect_pdo, $dbuser, $dbpasswd);
        $sth = $pdo->prepare($sql);
        $array = array();
        $sth->execute($array);
        //$sth->execute();
        $result = $sth->fetchAll();
        $numrows = $sth->rowCount();
        $stherr = $sth->errorInfo();

        if ($stherr[0] != '00000') {
            return "An  error occurred " . $stherr[0] . " " . $stherr[1] . "" . $stherr[2];
        }
        $rs = array();
        for ($ri = 0; $ri < $numrows; $ri++) {
            $r = new Repairer();
            $r->fill_from_array($result[$ri]);
            $rs[] = $r;
        }
        //print_r($rs);

        return $rs;
    }

    public static function get_ra($id) {
        $r = array();
        $r['result'] = 'Not ok';
        include('../../connect.php');
        $pdo = new PDO($connect_pdo, $dbuser, $dbpasswd);
        $sql = "select * from repairer where id = ?";
        $sth = $pdo->prepare($sql);
        $array = array($id);
        $sth->execute($array);
        //$sth->execute();
        $result = $sth->fetchAll();
        $numrows = $sth->rowCount();
        $stherr = $sth->errorInfo();

        if ($stherr[0] != '00000') {
            return "An  error occurred " . $stherr[0] . " " . $stherr[1] . "" . $stherr[2];
        }
        if ($numrows == 1) {
            $r = $result[0];
            $r['result'] = 'ok';
        }
        return $r;
    }
    function connect(){
        
    }

}
