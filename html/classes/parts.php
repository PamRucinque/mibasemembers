<?php

class part {

    public $id, $datepart, $idcat, $toyname, $description, $datepart_str,
            $typepart, $borid, $cost, $alertuser, $paid, $result, $numrows, $warning;

    public function part_from_table($id) {
        include('../connect.php');
        $this->result = false;

        $sql = "select parts.*, t.toyname as toyname, to_char(datepart, 'dd-mm-YYYY') as datepart_str,
            typepart.picture as warning
            from parts 
            left join toys t on t.idcat = parts.itemno 
            LEFT JOIN typepart on (parts.type = typepart.typepart) 
            where parts.id = ?;";

        $pdo = new PDO($connect_pdo, $dbuser, $dbpwd);
        $sth = $pdo->prepare($sql);
        $array = array($id);
        $sth->execute($array);

        $result = $sth->fetchAll();
        $numrows = $sth->rowCount();
        $stherr = $sth->errorInfo();
        if ($stherr[0] != '00000') {
            $status .= "An  error occurred " . $stherr[0] . " " . $stherr[1] . "" . $stherr[2];
        }
        for ($ri = 0; $ri < $numrows; $ri++) {
            $part = $result[$ri];
            $this->id = $part['id'];
            $this->datepart = $part['datepart'];
            $this->datepart_str = $part['datepart_str'];
            $this->description = $part['description'];
            $this->idcat = $part['itemno'];
            $this->toyname = $part['toyname'];
            $this->typepart = $part['type'];
            $this->borid = $part['borcode'];
            $this->cost = $part['cost'];
            $this->paid = $part['paid'];
            $this->alertuser = $part['alertuser'];
            $this->warning = $part['warning'];
            $this->result = true;
        }
        $this->numrows = $numrows;
    }

    public function fill_from_array($result) {
            $this->id = $result['id'];
            $this->datepart = $result['datepart'];
            $this->datepart_str = $result['datepart_str'];
            $this->description = $result['description'];
            $this->idcat = $result['itemno'];
            $this->toyname = $result['toyname'];
            $this->typepart = $result['type'];
            $this->borid = $result['borcode'];
            $this->cost = $result['cost'];
            $this->paid = $result['paid'];
            $this->alertuser = $result['alertuser'];
            $this->warning = $result['warning'];
     }

}
