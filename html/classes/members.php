<?php

class member {

    public $longname, $firstname, $id, $key_15, $coord, $result, $membertype, $state,
            $balance, $balance_str, $loans_table, $holds_table, $roster_table,
            $next_duty, $expired_str, $expired, $count_roster, $duties, $parts, $no_parts;

    public function __construct($id) {
        if (!session_id()) {
            session_start();
        }
        $connect_pdo = $_SESSION['connect_pdo'];
        $dbuser = $_SESSION['dbuser'];
        $dbpasswd = $_SESSION['dbpasswd'];
        $dbpwd = $_SESSION['dbpasswd'];
        $bor = array();
        $status = '';
        $expired_str = '';
        $this->result = false;
        $this->expired_flag = false;


        $sql = "select borwrs.*, to_char(expired,'dd-mm-yyyy') as format_expired, m.duties as duties,
                (select count(id) from event where borwrs.id = event.memberid and typeevent = 'Roster Coord') as coord,
                coalesce(dr.amount,0) as dr, coalesce(cr.amount,0) as cr, to_char((dr.amount - cr.amount),'$0.00') as balance_str, (dr.amount - cr.amount) as balance 
                 from borwrs 
                left join membertype m on m.membertype = borwrs.membertype 
                left join (select bcode, sum(amount) as amount from journal where type = 'DR' group by bcode) dr on dr.bcode = borwrs.id
                left join (select bcode, sum(amount) as amount from journal where type = 'CR' group by bcode) cr on cr.bcode = borwrs.id 
                where borwrs.id = ?;";

        //$connect_pdo = "pgsql:host=" . $dbhost . ";port=" . $dbport . ";dbname=" . $dbuser;
        try {
            $pdo = new PDO($connect_pdo, $dbuser, $dbpwd);
        } catch (PDOException $e) {
            print "Error! connecting to database - returns list: " . $e->getMessage() . "<br/>";
            die();
        }
        $sth = $pdo->prepare($sql);
        $array = array($id);
        $sth->execute($array);


        $result = $sth->fetchAll();
        $numrows = $sth->rowCount();
        $stherr = $sth->errorInfo();
        if ($stherr[0] != '00000') {
            $status .= "An  error occurred " . $stherr[0] . " " . $stherr[1] . "" . $stherr[2];
        }
        for ($ri = 0; $ri < $numrows; $ri++) {

            $bor = $result[$ri];
            $this->firstname = $bor['firstname'];
            $this->key_15 = substr($bor['key'], 0, 15);
            $this->coord = $bor['coord'];
            $this->result = true;
            $this->id = $bor['id'];
            $this->longname = $bor['firstname'] . ' ' . $bor['surname'];
            $this->membertype = $bor['membertype'];
            $this->balance = $bor['balance'];
            $this->balance_str = $bor['balance_str'];
            $this->expired = $bor['expired'];
            $this->duties = $bor['duties'];

            $now = time(); // or your date as well
            $exp = strtotime($bor['expired']);
            $diff = ($exp - $now) / (60 * 60 * 24);


            if ($diff > 0) {
                $expired_str = '<font color="darkgreen">Expires: ' . $bor['format_expired'] . '</font><br>';
            }
            if ($diff >= 0 && $diff <= 30) {
                $expired_str = '<font color="orange">Due for Renewal on: ' . $bor['format_expired'] . '</font><br>';
            }
            if ($diff < 0) {
                $expired_str = '<font color="red">Expired on: ' . $bor['format_expired'] . '</font><br>';
                $expired_str .= '<font color="red">This membership has Expired!</font><br>';
                $this->expired_flag = true;
            }
            $this->expired_str = $expired_str;
        }
    }

    public function fill_parts() {

        include('../connect.php');

        $sql = "select parts.*, typepart.picture as warning, to_char(datepart, 'dd-mm-YYYY') as datepart_str,
        typepart.picture as warning, toys.toyname as toyname 
        from parts 
        LEFT JOIN typepart on (parts.type = typepart.typepart) 
        LEFT JOIN toys on (parts.itemno = toys.idcat) 
        where borcode = ? 
        order by alertuser, datepart desc;";
        try {
            $pdo = new PDO($connect_pdo, $dbuser, $dbpwd);
        } catch (PDOException $e) {
            print "Error! connecting to database - returns list: " . $e->getMessage() . "<br/>";
            die();
        }

        $sth = $pdo->prepare($sql);
        $array = array($this->id);
        $sth->execute($array);

        $result = $sth->fetchAll();
        $numrows = $sth->rowCount();
        $stherr = $sth->errorInfo();
        if ($stherr[0] != '00000') {
            $status .= "An  error occurred " . $stherr[0] . " " . $stherr[1] . "" . $stherr[2];
        }

        $a = array();

        for ($ri = 0; $ri < $numrows; $ri++) {
            $p = new part();
            $p->fill_from_array($result[$ri]);
            $a[] = $p;
        }
        $this->parts = $a;
        $this->no_parts = $numrows;
        //echo $numrows;
    }

    public function check_key($m_key, $key) {
        $ok = false;
        if ($key == $m_key) {
            $ok = true;
        }
        return $ok;
    }

    public function loans($borid) {
        $str = '';
        include('../connect/connect_local.php');
        $sql = "select transaction.*, to_char(due,'dd-mm-yyyy') as due_f,to_char(date_loan,'dd-mm-yyyy') as loan_f  "
                . "from transaction where return is null and borid = ?;";

        $pdo = new PDO($connect_pdo, $dbuser, $dbpwd);
        $sth = $pdo->prepare($sql);
        $array = array($borid);
        $sth->execute($array);

        $result = $sth->fetchAll();
        $numrows = $sth->rowCount();
        $stherr = $sth->errorInfo();
        if ($stherr[0] != '00000') {
            $status .= "An  error occurred " . $stherr[0] . " " . $stherr[1] . "" . $stherr[2];
        }
        if ($numrows > 0) {
            $str = '<h3>Toy Loans</h3>';
            $str .= '<table id="members" class="table table-striped table-bordered table-sm table-hover table-responsive-md dataTable compact nowrap order-column" cellspacing="0" width="100%" role="grid">';
            $str .= '<thead>
        <tr>
            <th class="th-sm all">Loaned
            </th>
            <th class="th-sm">Due
            </th>
            <th class="th-sm all">Toy#
            </th>
            <th class="th-sm" data-priority="8">Toyname
            </th>
            <th class="th-sm" data-priority="8">
            </th>
        </tr></thead>';
        }
        for ($ri = 0; $ri < $numrows; $ri++) {
            $trans = $result[$ri];
            $return_str = 'buttons';
            $str .= '<tr>';
            $str .= '<td>' . $trans['loan_f'] . '</td>';
            $str .= '<td>' . $trans['due_f'] . '</td>';
            $str .= '<td>' . $trans['idcat'] . '</td>';
            $str .= '<td>' . $trans['item'] . '</td>';
            $str .= '<td>' . $return_str . '</td>';
            $str .= '</tr>';
        }
        if ($numrows > 0) {
            $str .= '</table>';
        }
        $this->loans_table = $str;
    }

    public function holds($borid) {

        include('../connect/connect_local.php');
        $str = '';
        $sql = "select toy_holds.*, to_char(date_start,'dd-mm-yyyy') as start, toys.toyname "
                . "from toy_holds "
                . "left join toys on toys.idcat = toy_holds.idcat "
                . "where toy_holds.borid = ? and (toy_holds.status = 'PENDING' or toy_holds.status = 'ACTIVE');";

        $pdo = new PDO($connect_pdo, $dbuser, $dbpwd);
        $sth = $pdo->prepare($sql);
        $array = array($borid);
        $sth->execute($array);

        $result = $sth->fetchAll();
        $numrows = $sth->rowCount();
        $stherr = $sth->errorInfo();
        if ($stherr[0] != '00000') {
            $status .= "An  error occurred " . $stherr[0] . " " . $stherr[1] . "" . $stherr[2];
        }
        if ($numrows > 0) {
            $str = '<h3>Toy Holds</h3>';
            $str .= '<table id="members" class="table table-striped table-bordered table-sm table-hover table-responsive-md dataTable compact nowrap order-column" cellspacing="0" width="100%" role="grid">';
            $str .= '<thead>
        <tr>
            <th class="th-sm all">start
            </th>
            <th class="th-sm">status
            </th>
            <th class="th-sm all">Toy#
            </th>
            <th class="th-sm" data-priority="8">Toyname
            </th>
            <th class="th-sm" data-priority="8">
            </th>
        </tr></thead>';
        }

        for ($ri = 0; $ri < $numrows; $ri++) {
            $trans = $result[$ri];
            $return_str = 'buttons';
            $str .= '<tr>';
            $str .= '<td>' . $trans['start'] . '</td>';
            $str .= '<td><b>' . $trans['status'] . '</b></td>';
            $str .= '<td>' . $trans['idcat'] . '</td>';
            $str .= '<td>' . $trans['toyname'] . '</td>';
            $str .= '<td>' . $return_str . '</td>';
            $str .= '</tr>';
        }
        if ($numrows > 0) {
            $str .= '</table>';
        }
        $this->holds_table = $str;
    }

    public function rosters($borid) {

        include('../connect/connect_local.php');
        $str = '';
        $count_roster = 0;
        $sql = "select roster.*, to_char(date_roster,'dd-mm-yyyy') as date_str, expired 
                    from roster 
                    left join borwrs b on b.id = roster.member_id
                    where date_roster >= (b.expired - interval '12 months')
                    and type_roster = 'Roster' 
                    and member_id = ? order by date_roster asc;";

        $pdo = new PDO($connect_pdo, $dbuser, $dbpwd);
        $sth = $pdo->prepare($sql);
        $array = array($borid);
        $sth->execute($array);

        $result = $sth->fetchAll();
        $numrows = $sth->rowCount();
        if ($numrows == 0) {
            $this->next_duty = '';
        }
        $stherr = $sth->errorInfo();
        if ($stherr[0] != '00000') {
            $status .= "An  error occurred " . $stherr[0] . " " . $stherr[1] . "" . $stherr[2];
        }
        if ($numrows > 0) {
            $str = '';
            $str .= '<table id="members" class="table table-striped table-bordered table-sm table-hover table-responsive-md dataTable compact nowrap order-column" cellspacing="0" width="100%" role="grid">';
            $str .= '<thead>
        <tr>
            <th class="th-sm all">Date
            </th>
            <th class="th-sm">Weekday
            </th>
            <th class="th-sm all">Session
            </th>
            <th class="th-sm" data-priority="8">role
            </th>
                        <th class="th-sm">role
            </th>
            
        </tr></thead>';
        }

        for ($ri = 0; $ri < $numrows; $ri++) {
            $roster = $result[$ri];
            $return_str = 'buttons';
            if ($ri == 0) {
                $this->next_duty = 'Next Duty is on: <font color="blue">' . $roster['weekday'] . ' ' . $roster['date_str'] . '</font>';
                $this->next_duty .= '  <input type="button" id="roster_btn" class="btn btn-lg btn-success btn-sm" onclick="roster();" value="Show Duties" />';
            }

            $str .= '<tr>';
            if ($roster['date_roster'] < date("Y-m-d H:i:s")) {
                $str .= '<td style="background-color: lightyellow;">' . $roster['date_str'] . '</td>';
            } else {
                $str .= '<td>' . $roster['date_str'] . '</td>';
            }

            $str .= '<td><b>' . $roster['weekday'] . '</b></td>';
            $str .= '<td>' . $roster['roster_session'] . '</td>';
            $str .= '<td>' . $roster['session_role'] . '</td>';

            if (($roster['status'] == 'completed') || ($roster['status'] == 'pending')) {
                if ($roster['status'] == 'completed') {
                    $str .= '<td align="center" style="background-color: lightgreen;">' . $roster['status'] . '</td>';
                } else {
                    $str .= '<td align="center" style="background-color: lightyellow;">' . $roster['status'] . '</td>';
                }
            } else {
                $str .= '<td align="center">' . $roster['status'] . '</td>';
            }
            if (trim($roster['status']) != 'no show') {
                $count_roster = $count_roster + 1;
            }

            $str .= '</tr>';
        }
        if ($numrows > 0) {
            $str .= '</table>';
        }
        $this->roster_table = $str;
        $this->count_roster = $count_roster;
    }

    public static function loan_history($mid) {
        $str = '';
        $count_gold_star = 0;
        if (!session_id()) {
            session_start();
        }
        $connect_pdo = $_SESSION['connect_pdo'];
        $dbuser = $_SESSION['dbuser'];
        $dbpasswd = $_SESSION['dbpasswd'];
        $dbpwd = $_SESSION['dbpasswd'];
        //$connect_pdo = "pgsql:host=" . $dbuser . ";port=" . $dbport . ";dbname=" . $dbuser;
        $sql = "select transaction.*, to_char(due,'dd-mm-yy') as due_f,to_char(date_loan,'dd-mm-yy') as loan_f, (due - return) as overdue,  to_char(return,'dd-mm-yy') as return_f    
            from transaction 
            where return is not null and borid = ? order by id desc;";
        //$connect_pdo = "pgsql:host=" . $dbhost . ";port=" . $dbport . ";dbname=" . $dbuser;

        $pdo = new PDO($connect_pdo, $dbuser, $dbpasswd);
        $sth = $pdo->prepare($sql);
        $array = array($mid);
        $sth->execute($array);

        $result = $sth->fetchAll();
        $numrows = $sth->rowCount();
        $stherr = $sth->errorInfo();
        if ($stherr[0] != '00000') {
            $status .= "An  error occurred " . $stherr[0] . " " . $stherr[1] . "" . $stherr[2];
        }
        $rows = array();

        for ($ri = 0; $ri < $numrows; $ri++) {
            $trans = $result[$ri];
            $row = array();
            $row[] = $trans["id"];
            $row[] = $trans["idcat"];
            $row[] = $trans["borid"];
            $row[] = $trans["item"]; //3
            $row[] = $trans["loan_f"]; //4
            $row[] = $trans["due_f"]; //5
            $row[] = $trans["due"]; //6
            $row[] = $trans['overdue']; //7
            $row[] = $trans['return']; //8
            $row[] = $trans['return_f']; //9
            $row[] = $trans['date_loan']; //10
            $rows[] = $row;
        }
        //print_r($rows);

        return $rows;
    }

}

function member_key($key) {
    include('../connect/connect_local.php');
    $status = '';
    $borid = 0;

    $sql = "select id, firstname, surname,   
                (select count(id) from event where borwrs.id = event.memberid and typeevent = 'Roster Coord') as coord
                 from borwrs "
            . "where substring(key,1,15) = ?;";

    $connect_pdo = "pgsql:host=" . $dbhost . ";port=" . $dbport . ";dbname=" . $dbuser;
    $pdo = new PDO($connect_pdo, $dbuser, $dbpwd);
    $sth = $pdo->prepare($sql);
    $array = array($key);
    $sth->execute($array);

    $result = $sth->fetchAll();
    $numrows = $sth->rowCount();
    $stherr = $sth->errorInfo();
    if ($stherr[1] != '00000') {
        $status .= "An  error occurred " . $stherr[0] . " " . $stherr[1] . "" . $stherr[2];
    }
    for ($ri = 0; $ri < $numrows; $ri++) {
        $bor = $result[$ri];
        $firstname = $bor['firstname'];
        $borid = $bor['id'];
    }
    //echo $status;

    return $borid;
}

function member_pin($borid, $pin) {
    $dbhost = 'localhost';
    $dbport = '5432';
    $status = '';
    $bor = array();
    $bor['result'] = 'error';
    $dbuser = $_SESSION['library_code'];
    $dbpwd = $_SESSION['db_pwd'];
    $sql = "select borwrs.*, "
            . "(select count(id) from event where borwrs.id = event.memberid and typeevent = 'Roster Coord') as coord "
            . "from borwrs where borwrs.id = ? and pin1 = ?;";
    //echo $sql;
    $connect_pdo = "pgsql:host=" . $dbhost . ";port=" . $dbport . ";dbname=" . $dbuser;
    $pdo = new PDO($connect_pdo, $dbuser, $dbpwd);
    $sth = $pdo->prepare($sql);
    $array = array($borid, $pin);
    $sth->execute($array);

    $result = $sth->fetchAll();
    $numrows = $sth->rowCount();
    $stherr = $sth->errorInfo();
    if ($stherr[0] != '00000') {
        $status .= "An  error occurred " . $stherr[0] . " " . $stherr[1] . "" . $stherr[2];
    }
    for ($ri = 0; $ri < $numrows; $ri++) {
        $bor = $result[$ri];
        $bor['result'] = 'ok';
    }

    return $bor;
}
