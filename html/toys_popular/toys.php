<!DOCTYPE html>
<?php include('../mibase_check_login.php'); ?>
<html>
    <head>
        <title>Mibase Search Toys</title>
        <?php include('../header/head.php'); ?>
    </head>
    <script>
        $(document).ready(function () {
            $('#members').DataTable({
                "lengthChange": true,
                "pageLength": 100,
                "pagingType": "full_numbers",
                "lengthChange": false,
                "bFilter": true,
                language: {
                    searchPlaceholder: "Search Popular Toys",
                    searchClass: "form-control",
                    search: "",
                },
                responsive: {
                    details: {
                        renderer: function (api, rowIdx, columns) {
                            var data = $.map(columns, function (col, i) {
                                return col.hidden ?
                                        '<tr data-dt-row="' + col.rowIndex + '" data-dt-column="' + col.columnIndex + '">' +
                                        '<td>' + col.title + ':' + '</td> ' +
                                        '<td>' + col.data + '</td>' +
                                        '</tr>' :
                                        '';
                            }).join('');
                            return data ?
                                    $('<table/>').append(data) :
                                    false;
                        }
                    }
                }
            });
            $('div.dataTables_filter input').addClass('form-control');
            $('div.dataTables_length select').addClass('recs');
        });
        function set_toyid(str) {
            document.getElementById('get_toy').value = str;
            document.getElementById('submit_toy').submit();
        }
        function pic() {
            if (document.getElementById("pictures").value === 'Pictures') {
                document.getElementById("pictures").value = 'List';
            } else {
                document.getElementById("pictures").value = 'Pictures';
            }
        }


    </script>


    <body>
        <?php include('../header/header.php'); ?>
        <div class="container-fluid">
            <div class="row">
                <div class="col-sm-11" style="background-color: white;padding-left: 20px;">
                    <h2>Most Popular Toys - Top 100 borrowed in the last 12 months</h2>
                </div>
                <div class="col-sm-1" style="padding-bottom: 5px; float: right;padding-top: 10px;">
                    <?php include('form_pictures.php'); ?>
                </div>
            </div>
            <div class="row">
                <div class="col-sm-12" style="background-color: white;padding-left: 20px;">
                    <?php
                    $multi_location = $_SESSION['settings']['multi_location'];
                    include('data.php');
                    // include('search_form.php');
                    ?>

                    <?php include('list.php'); ?>


                    <?php include('form.php'); ?>
                </div>
            </div>
        </div>
    </div>
    <script type="text/javascript" src="js/menu.js"></script>
</body>
</html>