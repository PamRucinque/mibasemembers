<?php

require('../mibase_check_login.php');
include('../connect.php');

$total = 0;
$x = 0;

$pdo = new PDO($_SESSION['connect_pdo'], $_SESSION['dbuser'], $_SESSION['dbpasswd']);

$query_toys = "Select toys.idcat as idcat, toyname, toys.id, toys.category, cat.description as category_description, age, toys.no_pieces as no_pieces, manufacturer, reservecode,
t.due as due, l.description as location, toys.block_image as block_image, COALESCE(cl.count_loans,0) as loans,
 to_char(t.due,'dd-mm-yyyy') as due_str, t.total as onloan, COALESCE(cl.days,0) as days,coalesce(h.total,0) as holds     
from toys 
left join category cat on cat.category = toys.category
left join location l on l.location = toys.location
left join (select count(id) as total, idcat from toy_holds where (status = 'ACTIVE'or status = 'READY' or status = 'PENDING' or status = 'LOCKED') group by idcat) h on h.idcat = toys.idcat  
 left join (select count(id) as total,idcat, due from transaction where return is null group by idcat,due) t on t.idcat = toys.idcat 
left join (select count(id) count_loans, sum(return-date_loan) as days, idcat from transaction where date_loan > current_date - INTERVAL '12 months' group by idcat) cl on cl.idcat = toys.idcat 
where toy_status = 'ACTIVE' 
order by days desc limit 100;";

$sth = $pdo->prepare($query_toys);
$array = array();
$sth->execute($array);

$result = $sth->fetchAll();


$stherr = $sth->errorInfo();

if ($stherr[0] != '00000') {
    echo "An INSERT query error occurred.\n";
    echo $query_edit;
    echo $connect_pdo;
    echo 'Error' . $stherr[0] . '<br>';
    echo 'Error' . $stherr[1] . '<br>';
    echo 'Error' . $stherr[2] . '<br>';
}

$XX = "No Record Found";

$result_txt = '';
$today = date("Y-m-d");

$total = sizeof($result);

$search_str = '';
if ($str_category != '') {
    $search_str .= ' Filtered on Category = ' . $str_category . ', ';
}
if ($age_str != '%%') {
    $search_str .= ' Filtered on Age = ' . $age . ', ';
}
if ($search != '') {
    $search_str .= ' Filtered on String = ' . $search . ', ';
}
if ($attribute != '') {
    $search_str .= ' Filtered on Attribute = ' . $attribute . ', ';
}
$toprint = sizeof($result);
if (($toprint > 0) && ($_SESSION['view'] == 'list')) {
    include('header.php');
} else {
    echo '<div class="row responsive" style="padding-bottom: 10px; max-height="200px;>';
}

//include('total_row.php');

for ($ri = 0; $ri < $toprint; $ri++) {
    $toy = $result[$ri];
    $bgcolor = 'white';
    $status = '';
    if ($toy['onloan'] > 0) {
        $bgcolor = 'lightgreen';
        $status = $toy['due_str'];
    }
    if ($toy['holds'] > 0) {
        $bgcolor = '#FFCCFF';
        $status = 'On Hold';
    }
    $loans = round(100 * ($toy['days'] / 365), 0) . '%';
    if ($multi_location == 'Yes') {
        $location_description = $toy['location_description'];
    } else {
        $location_description = '';
    }
    $subdomain = $_SESSION['library_code'];
    $file_pic = '/home/mibaselive/html/toy_images/' . $subdomain . '/' . strtolower($toy['idcat']) . '.jpg';
    if ((file_exists($file_pic) && ($row['block_image'] != 'Yes'))) {
        $pic = "'https://" . $subdomain . ".mibase.com.au/toy_images/" . $subdomain . "/" . strtolower($toy['idcat']) . ".jpg" . "'";
        $pic_grid = "https://" . $subdomain . ".mibase.com.au/toy_images/" . $subdomain . "/" . strtolower($toy['idcat']) . ".jpg";
    } else {
        $pic = "'https://" . $subdomain . ".mibase.com.au/toy_images/" . $subdomain . "/" . 'blank' . ".jpg" . "'";
        $pic_grid = "https://" . $subdomain . ".mibase.com.au/toy_images/demo/" . 'blank' . ".jpg";
    }
    $img_tag = '<img src="' . $pic . '" alt="' . strtolower($toy['idcat']) . '" height="42" width="42">';
    $img_grid = '<img src="' . $pic_grid . '" alt="' . strtolower($toy['idcat']) . '" width="180px" >';
    $link = '../toy/toy.php?v=p&&id=' . $toy['idcat'];
    $category_description = $toy['category_description'];
    //$link = '../toy/toy.php?v=p';
    $view_str = '<a style="color:white;" class="btn btn-primary btn-sm" id="button" '
            . 'onmouseover="showtrail(175,220, ' . $pic . ');" onmouseout="hidetrail();" '
            . ' >View Toy</a>';
    if ($_SESSION['view'] == 'list') {
        include('row.php');
    } else {
        include('grid.php');
    }
}
if (($toprint > 0) && ($_SESSION['view'] == 'list')) {
    echo '</tbody></table>';
} else {
    echo '</div>';
}




//print $result_txt;
?> 
