<!DOCTYPE html>
<?php include('../mibase_check_login.php'); ?>
<html>
    <head>
        <title>Mibase Search Toys</title>
        <?php include('../header/head.php'); ?>
    </head>
    <script>
        $(document).ready(function () {
            $('#toys').DataTable({
                "lengthChange": true,
                "pageLength": 100,
                "pagingType": "full_numbers",
                "lengthChange": false,
                "bFilter": true,
                language: {
                    searchPlaceholder: "Search New Toys",
                    searchClass: "form-control",
                    search: "",
                },
                responsive: {
                    details: {
                        renderer: function (api, rowIdx, columns) {
                            var data = $.map(columns, function (col, i) {
                                return col.hidden ?
                                        '<tr data-dt-row="' + col.rowIndex + '" data-dt-column="' + col.columnIndex + '">' +
                                        '<td>' + col.title + ':' + '</td> ' +
                                        '<td>' + col.data + '</td>' +
                                        '</tr>' :
                                        '';
                            }).join('');
                            return data ?
                                    $('<table/>').append(data) :
                                    false;
                        }
                    }
                }
            });

            $('#toys1').DataTable({
                "aLengthMenu": [[100, 200, 300, 1000, -1], [100, 200, 300, 1000, "All"]],
                "iDisplayLength": 200,
                "sDom": '<"top"ip>rt<"bottom"flp><"clear">',
                "orderClasses": false,
                "searching": true,
                "bFilter": true,
                responsive: {
                    details: {
                        type: 'column',
                        target: 'th'
                    }
                },
                "pageLength": 200,
                "paging": true,
                "binfo": true,
                "lengthChange": false,
                language: {
                    searchPlaceholder: "Search Toys",
                    searchClass: "form-control",
                    "lengthMenu": "Display _MENU_  records per page",
                }

            });
            $('div.dataTables_filter input').addClass('form-control');
            $('div.dataTables_length select').addClass('recs');
        });
        function pic() {
            if (document.getElementById("pictures").value === 'Pictures') {
                document.getElementById("pictures").value = 'List';
            } else {
                document.getElementById("pictures").value = 'Pictures';
            }
        }

    </script>
    <style>
        .dataTables_paginate>span>a {
            margin-bottom: 0px !important;
            padding: 1px 5px !important;
        }

        .dataTables_paginate>a {
            margin-bottom: 0px !important;
            padding: 1px 5px !important;
        }
    </style>


    <body>
        <?php
        include('../header/header.php');
        include('data.php');
        ?>
        <div class="container-fluid">
            <div class="row">
                <div class="col-sm-11" style="background-color: white;padding-left: 20px;">
                    <h2>New Toys - Newest 100 toys.</h2>
                </div>
                <div class="col-sm-1" style="padding-bottom: 5px; float: right;padding-top: 10px;">
                    <?php include('form_pictures.php'); ?>
                </div>

                <div class="col-sm-12" style="background-color: white;padding-left: 20px;">

                    <?php
                    $multi_location = $_SESSION['settings']['multi_location'];

                    include('list.php');
                    ?>

                    <?php include('form.php'); ?>
                </div>
            </div>
        </div>
        <script type="text/javascript" src="js/menu.js"></script>
    </body>
</html>