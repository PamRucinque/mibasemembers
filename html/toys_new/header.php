<table id="toys" class="table table-striped table-bordered table-sm table-hover display responsive compact" style="overflow: hidden;" cellspacing="0" width="100%" role="grid">
    <thead>
        <tr>
            <th class="th-sm all" data-priority="1">No</th>
            <th class="hidden-xs">id</th>
            <th class="hidden-xs" data-priority="2">Cat</th>
            <th data-priority="2" class="wrap">Toyname</th>
            <th data-priority="3" class="wrap">Purchased</th>
            <th class="hidden-xs" data-priority="5" class="wrap"></th>
            <th class="hidden-xs nowrap"  style="min-width: 150px;" data-priority="6">Manufacturer</th>
            <th class="hidden-xs nowrap"  style="min-width: 100px;" data-priority="7">Age</th>
            <th class="hidden-xs nowrap"  style="min-width: 30px;" data-priority="8">Pieces</th>
            <th class="hidden-xs" data-priority="4">Due</th>

        </tr>
    </thead><tbody>

