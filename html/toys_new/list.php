<script>
    function toy(idcat) {
        var ref = '../toy/toy.php?v=new&&idcat=' + idcat;
        window.location.href = ref;
    }
</script>

<?php
require('../mibase_check_login.php');
include('../connect.php');
//$block_image = $_SESSION['settings']['block_image'];

$total = 0;
$x = 0;

$pdo = new PDO($_SESSION['connect_pdo'], $_SESSION['dbuser'], $_SESSION['dbpasswd']);

$query_toys = "SELECT toys.idcat as idcat, toyname, toys.category,no_pieces, cat.description as category_description, user1, toys.id as id, age, rent,
 manufacturer, toy_status, twitter, sub_category, toys.location as location, t.due as due, 
 to_char(t.due,'dd-mm-yyyy') as due_str, location.description as location_description, t.total as onloan,
toys.date_purchase as purchased, to_char(date_purchase, 'dd-mm-yyyy') as purchase_format,coalesce(h.total,0) as holds  
 FROM toys 
 left join category cat on cat.category = toys.category
 left join location on location.location = toys.location
 left join (select count(id) as total,idcat, due from transaction where return is null group by idcat,due) t on t.idcat = toys.idcat 
 left join (select count(id) as total, idcat from toy_holds where (status = 'ACTIVE'or status = 'READY' or status = 'PENDING' or status = 'LOCKED') group by idcat) h on h.idcat = toys.idcat  
WHERE 
 (toy_status = 'ACTIVE') 
 AND (date_purchase != null or date_purchase != '1900-01-01') 
 order by toys.date_purchase DESC LIMIT 100;";

$sth = $pdo->prepare($query_toys);
$array = array();
$sth->execute($array);

$result = $sth->fetchAll();


$stherr = $sth->errorInfo();

if ($stherr[0] != '00000') {
    echo "An INSERT query error occurred.\n";
    echo $query_edit;
    echo $connect_pdo;
    echo 'Error' . $stherr[0] . '<br>';
    echo 'Error' . $stherr[1] . '<br>';
    echo 'Error' . $stherr[2] . '<br>';
}

$XX = "No Record Found";

$result_txt = '';
$today = date("Y-m-d");

$total = sizeof($result);

$search_str = '';
if ($str_category != '') {
    $search_str .= ' Filtered on Category = ' . $str_category . ', ';
}
if ($age_str != '%%') {
    $search_str .= ' Filtered on Age = ' . $age . ', ';
}
if ($search != '') {
    $search_str .= ' Filtered on String = ' . $search . ', ';
}
if ($attribute != '') {
    $search_str .= ' Filtered on Attribute = ' . $attribute . ', ';
}
$toprint = sizeof($result);
if (($toprint > 0) && ($_SESSION['view'] == 'list')) {
    include('header.php');
} else {
    echo '<div class="row responsive" style="padding-bottom: 10px; max-height="200px;">';
}

//include('total_row.php');

for ($ri = 0; $ri < $toprint; $ri++) {
    $toy = $result[$ri];
    $bgcolor = 'white';
    $status = '';
    if ($toy['onloan'] > 0) {
        $bgcolor = 'lightgreen';
        $status = $toy['due_str'];
    }
    if ($toy['holds'] > 0) {
        $bgcolor = '#FFCCFF';
        $status = 'On Hold';
    }
    if ($multi_location == 'Yes') {
        $location_description = $toy['location_description'];
    } else {
        $location_description = '';
    }
    $subdomain = $_SESSION['library_code'];
    //$file_pic = '/home/mibaselive/html/toy_images/' . $subdomain . '/' . strtolower($toy['idcat']) . '.jpg';
    $file_pic = '../..' . $toy_images . '/' . $subdomain . '/' . strtolower($toy['idcat']) . '.jpg';
    //echo $file_pic;
    if (file_exists($file_pic)) {
        $pic = "'https://" . $subdomain . ".mibase.com.au/toy_images/" . $subdomain . "/" . strtolower($toy['idcat']) . ".jpg" . "'";
        $pic_grid = "https://" . $subdomain . ".mibase.com.au/toy_images/" . $subdomain . "/" . strtolower($toy['idcat']) . ".jpg";
    } else {
        $pic = "'https://" . $subdomain . ".mibase.com.au/toy_images/" . $subdomain . "/" . 'blank' . ".jpg" . "'";
        $pic_grid = "https://" . $subdomain . ".mibase.com.au/toy_images/demo/" . 'blank' . ".jpg";
    }
    $view_str = "showtrail(175,220," . $pic . ");";
    $linkToy = '<a onmouseout="hidetrail();" on mouseover="' . $view_str . '" class="btn btn-primary btn-sm" style="color: white;" href="../toy/toy.php?v=new&&idcat=' . strtoupper($toy['idcat']) . '">View Toy</a>';
    $link = '../toy/toy.php?id=' . $toy['idcat'];
    $category_description = $toy['category_description'];
    $img_grid = '<img src="' . $pic_grid . '" alt="' . strtolower($toy['idcat']) . '" width="180px" >';
    //$link = '../toy/toy.php';

    if ($_SESSION['view'] == 'list') {
        include('row.php');
    } else {
        include('grid.php');
    }
}
if (($toprint > 0) && ($_SESSION['view'] == 'list')) {
    echo '</tbody></table>';
} else {
    echo '</div>';
}




//print $result_txt;
?> 
