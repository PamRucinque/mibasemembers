<?php
require(dirname(__FILE__) . '/../../mibase_check_login.php');

//connect to MySQL
//include( dirname(__FILE__) . '/../connect.php');
$query = "SELECT location, description  FROM location ORDER by location ASC;";
$result = pg_Exec($conn, $query);


echo '<select name="location" id="location" onchange="get_all();" class="form-control" placeholder="Location" style="padding-left: 2px;">\n';

$numrows = pg_numrows($result);

if ($_SESSION['loc_filter'] != '') {
    echo '<option value="" selected="selected"></option>';
} else {
    echo '<option value="" selected="selected">Location</option>';
}


for ($ri = 0; $ri < $numrows; $ri++) {
    $row = pg_fetch_array($result, $ri);
    $row['location'] = trim($row['location']);
    echo "<option value='{$row['location']}'>{$row['description']}
    </option>\n";
}

echo "</select>\n";
?>

