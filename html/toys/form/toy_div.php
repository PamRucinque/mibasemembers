<div class="modal fade" id="toy_div_popup" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title" id="myModalLabel"><div id="toy_title" name="toy_title"></div></h4>
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">X</button>


            </div>
            <div class="modal-body">
                <div class="row">
                    <div class="col-sm-8"  id="Toy"></div>
                    <div id="hold" class="col-sm-2"></div>
                    <div id="fav" class="col-sm-2" style="text-align: right;"></div>
                </div>
                <div class="row">
                    <div class="col-sm-4" id="picture" style="padding-top: 10px;padding-right: 10px;"></div> 
                    <div  class="col-sm-8" id="Toy_details"></div>
                </div>
                <div id="fav"  style="padding-left: 50px;padding-bottom: 10px;"></div>

            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>
            </div>
        </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
</div><!-- /.modal -->
<!-- Modal -->
