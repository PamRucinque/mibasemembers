var view = 'pic';
var g_idcat = '';
var pic_small;
function sleep(ms) {
    return new Promise(resolve => setTimeout(resolve, ms));
}

var pc = 0;
var idcat = '';

async function getToy(element) {

    $("#toy_div").hide();
    $("#Toys").show();
    $("#Parts").show();
    $("#TableHeading").show();
    if (event.keyCode === 13) {
        get_all();
    }
    var value = $(element).val();
    if (value.length < 1)
        return;
    pc++;
    var s = pc;
    //await sleep(1000);
    if (s !== pc)
        return;

    //var rows = $("#CustomerRows").val();
    var rows = 50;
    var url = "data/get_all_toys.php";


    $.ajax({
        type: "POST",
        cache: false,
        url: url,
        data: {
            search: value,
            rows: rows,
            view: view,
            filter: $("#toys_s1").serialize()
        },
        success: function (data) {
            if (view === 'list') {
                draw_table(data);
            } else {
                draw_grid(data);
            }
        },
        error: function (textStatus, errorThrown) {
            $("#Error").html("Error " + textStatus + " " + errorThrown);
        },
        dataType: "json"
    });
}

var pc = 0;
var borid = 0;

function get_all() {
    var img = ' <center><img src="images/ajax-loader.gif" alt="waiting" height="100" width="100"></center>';
    //document.getElementById('wait').innerHTML = img;
    document.getElementById('wait').innerHTML = '<h4><font color="red">Loading....</font></h4>';
    var rows = 50;
    $.ajax({
        type: "POST",
        url: "data/get_all_toys.php",
        data: {
            rows: rows,
            view: view,
            filter: $("#toys_s1").serialize()
        },
        success: function (data) {
            //console.log(data.size);
            if (view === 'list') {
                draw_table(data);
            } else {
                draw_grid(data);
            }
            //

            load_image();
            document.getElementById('wait').innerHTML = '';
            //$("#Error").html(data.age);

        },
        error: function (textStatus, errorThrown) {
            $("#Toys").html("Error " + textStatus + " " + errorThrown);
        },
        dataType: "json"
    });
}
function load_image() {
    $('[data-toggle="tooltip"]').tooltip({
        html: true
    });
}
function fav(str,idcat) {
    //alert('add to favourite' + str);
    $.ajax({
        type: "POST",
        url: "data/update_fav.php",
        data: {
            idcat: idcat,
            mode: str
        },
        success: function (data) {
            Toy_popup(data.idcat);
                      
        },
        error: function (textStatus, errorThrown) {
            $("#Error").html("Error " + textStatus + " " + errorThrown);
        },
        dataType: "json"
    });
}


function draw_table(data) {
    var pic = '';
    var filter = document.getElementById("category").value;

    if (data !== undefined && data !== null) {
        //we got some data back
        if (data.result !== undefined && data.result !== null) {
            if (filter === null) {
                if (filter === 'new') {
                    document.getElementById("due_heading").innerHTML = 'Purchased';
                }
            }
            //we retrived a data object with a result variable
            if (data.result === "ok") {
                len = data.toys.length;
                //idcat = row.idcat.toLowerCase();
                //we got ack an array, assume it is a valid array
                var table = "<table  id=\"toys_select\" class=\"table table-striped table-bordered table-sm table-hover table-responsive-sm dataTable compact order-column\" style=\"width: 100%\">"
                if (len > 0) {
                    table += '<thead><tr>';
                    table += "<th>idcat</th>";
                    table += "<th>Toyname</th>";
                    table += "<th>Cat</th>";
                    table += "<th>pic</th>";
                    table += "<th>Status</th>";
                    table += "<th>Manufacturer</th>";
                    table += "<th>Age</th>";
                    table += "<th>Parts</th>";
                    table += "<th>Attr</th>";
                    table += "<th></th>";
                    table += "</tr></thead><tbody>";

                } else {
                    table += "<thead><tr><th>No Results</th></tr></thead><tbody>";
                }
                var link = '';
                //var table = "<table  class=\"table-striped table-hover\" style=\"width: 100%\"><tbody><tr><th>Name</th><th>Company</th><th>Suburb</th><th>State</th></tr>";
                for (index = 0, len; index < len; ++index) {

                    var row = data.toys[index]
                    link = '../toy/toy.php?idcat=' + row.idcat;
                    if (index === 0) {
                        g_idcat = row.idcat;
                    }

                    if (row.pic === 'Yes') {
                        pic = '<a class="btn btn-primary btn-sm" style="color:white;" href="' + link + '" data-toggle=\"tooltip\" data-original-title=\"<img width=\'175px\' src=\'../../toy_images/' + data.subdomain + '/' + row.idcat.toLowerCase() + '.jpg?v=' + row.holds + '\' alt=\'toy_image\'>" \/>View</a>';
                    } else {
                        pic = '<a class="btn btn-primary btn-sm" style="color:white;" href="' + link + '" >View</a>';
                    }
                    table += "<tr class=\"item\">";
                    table += "<td class='idcat'>" + row.idcat + "</td>";
                    table += "<td onclick=\"Toy_popup('" + row.idcat + "');\">" + row.toyname + "</td>";
                    table += "<td onclick=\"Toy_popup('" + row.idcat + "');\" >" + row.category_description + "</td>";
                    table += "<td class='idcat'>" + pic + "</td>";
                    if (row.onloan === 0) {
                        if (row.holds > 0) {
                            table += "<td style='background-color:#FFCCFF;text-align:center;' onclick=\"Toy_popup('" + row.idcat + "');\"><font color='darkred'>On Hold</font></td>";

                        } else {
                            table += "<td style='text-align:center;' onclick=\"Toy_popup('" + row.idcat + "');\"><font color='blue'>LIBRARY</font></td>";
                        }
                    } else {
                        table += "<td sort-order='" + row.due + "' style='background-color:lightgreen;text-align:center;' onclick=\"Toy_popup('" + row.idcat + "');\">" + row.due_f + "</td>";
                    }
                    if (row.manufacturer === null) {
                        row.manufacturer = ''
                    }
                    ;
                    if (row.age === null) {
                        row.age = ''
                    }
                    ;

                    table += "<td onclick=\"Toy_popup('" + row.idcat + "');\">" + row.manufacturer + "</td>";
                    table += "<td style='min-width: 100px;' onclick=\"Toy_popup('" + row.idcat + "');\">" + row.age + "</td>";
                    table += "<td onclick=\"Toy_popup('" + row.idcat + "');\">" + row.no_pieces + "</td>";
                    if (row.attributes === null) {
                        row.attributes = '';
                    }
                    table += "<td onclick=\"Toy_popup('" + row.idcat + "');\">" + row.attributes + "</td>";
                    if (row.loc_desc === '') {
                        table += "<td onclick=\"Toy_popup('" + row.idcat + "');\">" + row.id + "</td>";
                    } else {
                        table += "<td onclick=\"Toy_popup('" + row.idcat + "');\">" + row.loc_desc + "</td>";
                    }

                    table += "</tr>";
                }

                table += "</tbody></table>";
                if (data.size > 20) {
                    //table += '<b><font color="red">Limited to 20 toys</font></b><br>';
                }
                $("#Toys").html(table);
                $('#toys_select').DataTable({
                    "aLengthMenu": [[100, 200, 300, 1000, -1], [100, 200, 300, 1000, "All"]],
                    "iDisplayLength": 200,
                    "sDom": '<"top"ip>rt<"bottom"flp><"clear">',
                    "orderClasses": false,
                    "searching": false,
                    responsive: {
                        details: {
                            type: 'column',
                            target: 'th'
                        }
                    },
                    "dom": "lifrtp",
                    "pageLength": 200,
                    "paging": true,
                    "binfo": true,
                    "lengthChange": false,
                    language: {
                        searchPlaceholder: "Search Toys",
                        searchClass: "form-control",
                        "lengthMenu": "Display _MENU_  records per page",
                    }

                });



            } else if (data.result === "error") {
                //we got back the error object
                $("#Toys").html("Error: " + data.error);
            } else {
                //we got back a json object but it's result variable is not ok or error.
                $("#Toys").html("Error: returned data result variable is not ok or error");
            }
        } else {
            $("#Toys").html("Error: Returned object with no result value");
        }

    } else {
        //no data was returned
        $("#Toys").html("Error: No Data Returned");
    }

}
function change_view() {
    $("#toy_div").hide();
    if (view === 'list') {
        view = 'pic';
        $('#pic_btn').removeClass('btn-success');
        $('#pic_btn').addClass('btn-primary');
        document.getElementById("pic_btn").innerHTML = "List";

        // $('#pic_btn').innerHTML('List');
    } else {
        view = 'list';
        $('#pic_btn').removeClass('btn-primary');
        $('#pic_btn').addClass('btn-success');
        // $('#pic_btn').innerHTML('Pictures');
        document.getElementById("pic_btn").innerHTML = "Pictures";
    }
    get_all();
    //alert(view);

}
function draw_grid(data) {
    if (data !== undefined && data !== null) {
        //we got some data back
        if (data.result !== undefined && data.result !== null) {

            //we retrived a data object with a result variable
            if (data.result === "ok") {
                len = data.toys.length;
                var table = "<div class='row responsive' style='padding-bottom: 10px;height:200px;padding-left:20px;padding-bottom:20px;'>";
                if (len > 0) {

                } else {
                    table += "<div class='col-sm-12'>No Results</div>";
                }
                var link = '';
                for (index = 0, len; index < len; ++index) {
                    var row = data.toys[index]
                    var img;
                    if (index === 0) {
                        g_idcat = row.idcat;
                    }
                    //row.toyname.replace('"', "′")
                    var h1 = '<b><font color=\'yellow\'>' + row.idcat + ': ' + row.toyname + '</font></b><br>';
                    var h2 = '<b><font color=\'blue\'>' + row.idcat + ': ' + row.toyname + '</font></b><br>';
                    var tooltip = '<b>Category: </b>' + row.category_description;
                    if (row.attributes !== '' && row.attributes !== null) {
                        tooltip += '<br><b>Attributes: </b>' + row.attributes;
                    }
                    if (row.age !== '' && row.age !== null) {
                        tooltip += '<br><b>Age: </b>' + row.age;
                    }
                    if (row.loc_desc !== '' && row.loc_desc !== null) {
                        tooltip += '<br><b></b>' + row.loc_desc + '</b>';
                    }
                    var status = '<br><b><font color=\'green\'>IN LIBRARY</font></b>';

                    if (row.onloan > 0) {
                        status = '<br><b><font color=\'red\'>ON LOAN</font></b>';
                    }
                    if (row.holds > 1) {
                        status = '<br><b><font color=\'darkred\'>ON HOLD</font></b>';
                    }
                    tooltip += status;



                    //link = '../toy/toy.php?idcat=' + row.idcat;
                    var div_click = "Toy_popup('" + row.idcat + "');";
                    //data-toggle=\"tooltip\" data-original-title=\"<img width=\'175px\' src=\'../../toy_images/' + data.subdomain + '/' + row.idcat.toLowerCase() + '.jpg?v=' + row.holds + '\'
                    if (row.pic === 'Yes') {
                        img = '<div onclick="' + div_click + '" >';
                        img += '<img data-toggle=\"tooltip\" data-original-title=\"' + h1 + tooltip + '\" ';
                        img += 'width=\"175px\" src=\"../../toy_images/' + data.subdomain + '/' + row.idcat.toLowerCase() + '.jpg?v=' + row.holds + '\" alt=\"toy_image\" \>';
                        img += '</div>';
                    } else {
                        //img = '<div onclick="' + div_click + '" >';
                        // img += '<img data-toggle=\"tooltip\" data-original-title=\"' + tooltip + '\" ';
                        //  img += 'width=\'175px\' src=\'../../toy_images/' + 'demo' + '/' + 'blank' + '.jpg?v=' + row.holds + '\' alt=\'toy_image\'>';
                        //  img += '</div>';
                        img = '<div onclick="' + div_click + '" >';
                        //img += '<div data-toggle=\"tooltip\" data-original-title=\"' + tooltip + '\" >' + tooltip + '</div>';
                        img += '<div style="background-color: whitesmoke;min-height: 175px;padding:5px;border-radius:5px;border:grey;">' + h2 + tooltip + '</div>';
                        img += '</div>';
                        // img = '<img width=\'175px\' src=\'../../toy_images/' + 'demo' + '/' + 'blank' + '.jpg?v=' + row.holds + '\' alt=\'toy_image\'>';

                    }
                    table += "<div id='" + row.idcat + "' class='col-xs-12 col-sm-2' style='padding: 5px;height: 200px;overflow: hidden;text-align:left;' >" + img + "</div>";
                }

                table += "</div>";

                $("#Toys").html(table);


            } else if (data.result === "error") {
                //we got back the error object
                $("#Toys").html("Error: " + data.error);
            } else {
                //we got back a json object but it's result variable is not ok or error.
                $("#Toys").html("Error: returned data result variable is not ok or error");
            }
        } else {
            $("#Toys").html("Error: Returned object with no result value");
        }

    } else {
        //no data was returned
        $("#Toys").html("Error: No Data Returned");
    }

}
function clear_toy() {
    $("#toy_div").hide();
    $("#picture").html('');
    //$("#toprint").html('');
    $("#Toys").html('');
    $("#Member").html('');
    $("#Toy_details").html('');
    $("#select_list").val('');
    $("#goto").html('');
    $("#ToyFilter").val('');
    $("#category").val('');
    $("#age").val('');
    $("#location").val('');
    $("#attribute").val('');
    $("#status").val('');
    $("#location").val('');
    $("#limit").val('100');
    document.querySelectorAll('.tooltip').forEach(function (el) {
        el.style.display = 'none';
    });
    get_all();
    $("#ToyFilter").focus();

    //var category = document.getElementById('category');
    // category.querySelector('[selected]').value = '';
    // category.querySelector('[selected]').text = 'Select Category';
    // category.selectedIndex = 0;
}


function add_to_print() {
    var idcats = "";
    var select_list = '';

    $("tr.item").each(function () {
        $this = $(this);
        var idcat = $this.find("td.idcat").html();
        var value = $this.find("td :checkbox").is(':checked');
        if (value) {
            idcats += "'" + idcat + "',";
        }
    });

    if (idcats.length > 0) {
        idcats = idcats.substring(0, idcats.length - 1);

        select_list = document.getElementById("select_list").value;
        if (select_list !== '') {
            select_list += "," + idcats;
        } else {
            select_list = idcats;
        }
        $("#idcats_list").val(idcats);
        $("#select_list").val(select_list);
        alert(idcats);
        //string.replace(searchvalue, newvalue)
        var toprint = '';
        if (select_list !== '') {
            toprint = 'To Print: ' + select_list.replace(/'/g, '');
            document.getElementById("toprint").innerHTML = toprint;
            $("#idcats_list").val(select_list);
        }

        //document.forms["reports"].submit();
        //document.reports.submit();
        return true;
    } else {
        alert('No toys selected!');
        return false;
    }

    //alert(idcats);
}
function go() {
    var idcats = "";

    $("tr.item").each(function () {
        $this = $(this);
        var idcat = $this.find("td.idcat").html();
        var value = $this.find("td :checkbox").is(':checked');
        if (value) {
            idcats += "'" + idcat + "',";
        }
    });

    if (idcats.length > 0) {
        idcats = idcats.substring(0, idcats.length - 1);
        $("#idcats_list").val(idcats);
        //$("#select_list").val(idcats);
        var select_list = document.getElementById("select_list").value;
        if (select_list === '') {
            $("#idcats_list").val(idcats);
        } else {
            $("#idcats_list").val(select_list);
        }
        //document.forms["reports"].submit();
        //document.reports.submit();
        return true;
    } else {
        alert('No toys selected!');
        return false;
    }


    //alert(idcats);
}
function print_div() {
    if ($("#print_div").is(":visible")) {
        $("#print_div").hide();
        document.getElementById("print_btn").innerHTML = 'Print';
    } else {
        document.getElementById("print_btn").innerHTML = 'Hide Print';
        $("#print_div").show();
    }
}

function select_all() {
    //var idcats = "";
    $("tr.item").each(function () {
        $this = $(this);
        $this.find("td :checkbox").prop("checked", true);
    });
}
function selectToy(idcat) {
    $("#toy_div").show();
    $("#print_div").hide();
    $.ajax({
        type: "POST",
        url: "data/get_toy.php",
        data: {
            idcat: idcat
        },
        success: function (data) {
            get_divs_toy(data, idcat);

            window.location.hash = '#top';
        },
        error: function (textStatus, errorThrown) {
            $("#Error").html("Error " + textStatus + " " + errorThrown);
        },
        dataType: "json"
    });
}
function Toy_popup(idcat) {
    $('#toy_div_popup').modal('show');
    //$("#toy_div_popup").show();
    $.ajax({
        type: "POST",
        url: "data/get_toy.php",
        data: {
            idcat: idcat
        },
        success: function (data) {
            get_divs_toy(data, idcat);
        },
        error: function (textStatus, errorThrown) {
            $("#Error").html("Error " + textStatus + " " + errorThrown);
        },
        dataType: "json"
    });
}
function selectToy_pic(idcat) {
    //$("#toy_div").show();
    $.ajax({
        type: "POST",
        url: "data/get_toy.php",
        data: {
            idcat: idcat
        },
        success: function (data) {
            if (g_idcat === '' || g_idcat === null) {
                g_idcat = idcat;
            }

            if (g_idcat !== '' || g_idcat === null) {
                //document.getElementById(g_idcat).style.backgroundColor = 'white';
                close_preview(g_idcat);
            }
            g_idcat = idcat;
            document.getElementById(g_idcat).style.backgroundColor = 'lightyellow';
            pic_small = document.getElementById(g_idcat).innerHTML;
            fill_div(data, idcat);
            //$(divID).css("display", "none");
            //document.getElementsByClassName('tooltip').style.display = 'none';
            document.querySelectorAll('.tooltip').forEach(function (el) {
                el.style.display = 'none';
            });

            window.location.hash = '#top_' + idcat;
        },
        error: function (textStatus, errorThrown) {
            $("#Error").html("Error " + textStatus + " " + errorThrown);
        },
        dataType: "json"
    });
}
function close_preview(idcat) {
    if (idcat !== null) {
        $("#" + idcat).html(pic_small);
        $("#" + idcat).removeClass('col-sm-12');
        $("#" + idcat).addClass('col-sm-2');
        document.getElementById(idcat).style.height = "200px";
        document.getElementById(idcat).style.backgroundColor = 'lightyellow';
        //document.getElementsByClassName('tooltip').style.display = 'block';
        //text-align
        document.getElementById(idcat).style.textalign = 'center';
    }

    //document.querySelectorAll('.tooltip').forEach(function (el) {
    // el.style.display = 'block';
    //});
}
function hold(idcat) {
    $("#toy_div").show();
    $("#print_div").hide();
    $.ajax({
        type: "POST",
        url: "data/new_hold.php",
        data: {
            idcat: idcat
        },
        success: function (data) {
            $('#toy_div_popup').modal('hide');
            alert(data.result);
            //$("#toy_div").hide();
           // $("#" + idcat).hide();
            //window.location.hash = '#top';
        },
        error: function (textStatus, errorThrown) {
            $("#Error").html("Error " + textStatus + " " + errorThrown);
        },
        dataType: "json"
    });
}
function imgError()
{
    var picture = "<img style=\"max-height:150px;\" src=\"images/blank.jpg\">";
    $("#picture").html(picture);
}
function get_divs_toy(data, idcat) {
    var t_alert = '';
    var link = '../toy/toy.php?idcat=' + idcat;
    var toy_btn = '<a class="btn btn-primary" style="color: white;" href="' + link + '">' + idcat + '</a>';

    //var r = "<h4>" + toy_btn + "  " + data.data.toyname + "</h4>";
    var s = '';
    var r = '';

    if (data.data.pic === 'Yes') {
        var picture = "<img style=\"max-width:200px;border-radius: 5px;\" src=\"../../toy_images/" + data.data.subdomain + "/" + idcat.toLowerCase() + ".jpg?v=" + data.data.pic_ver + "\" onerror=\"imgError();\">";
    } else {
        var picture = "<img style=\"max-width:200px;border-radius: 5px;\" src=\"../../toy_images/" + 'demo' + "/" + 'blank' + ".jpg?v=" + data.data.pic_ver + "\" onerror=\"imgError();\">";
    }
    if (data.data.alert_toy !== '' && data.data.alert_toy !== null) {
        t_alert += '<font color="red">' + data.data.alert_toy + '</font>';
    }

    var ok = 'Yes';

    if (data.data.loans > 0) {
        t_alert += '<h4 style="background-color:yellow;padding-left:10px;max-width: 130px;"><font color="red">ON LOAN</font></h4>';
        //    s += '<b>On loan to </b>' + data.data.onloan_borid + "  " + data.data.borname + '<br>';
        ok = 'No';
    }
    if (data.data.hold_borid > 0) {
        t_alert += '<h4 style="background-color:lightgreen;padding-left:10px;max-width: 130px;"><font color="red">ON HOLD</font></h4>';
        //      t_alert += '<br>This Toy is On Hold by <b>' + data.data.onloan_borid + '</b>: ' + data.data.holdname + '.';
        ok = 'No';
    }
    if (data.data.reserves > 0) {
        t_alert += '<font color="red"><h4 style="background-color:lightblue;padding-left:10px;max-width: 130px;">RESERVED</h4></font>';
        ok = 'No';
    }
    if (data.data.reservecode !== '' && data.data.reservecode !== null) {
        ok = 'No';
        t_alert += '<br>RESERVATION TOY';
    }
    if (data.data.loan_type === 'GOLD STAR') {
        ok = 'No';
        t_alert += '<br>GOLD STAR/DUTY TOY';
    }

    if ((data.data.toy_status !== 'ACTIVE')) {
        t_alert += '<h4><font color="red">This Toy is ' + data.data.toy_status + '.</font></h4>';
        ok = 'No';
    }

    if ((data.data.q_lock !== null)) {
        t_alert += '<h4><font color="red">This Toy is in Quarantine</font></h4>';
        ok = 'No';
    }
    if (data.data.stocktake_status === 'LOCKED') {
        ok = 'No';
        t_alert += '<h4><font color="red">This Toy is LOCKED.</font></h4>';
    }
    var c3 = data.data.desc2;
    c3 += '<br><a class="btn btn-danger btn-lg" style="color:white;" onclick="hide_toy();">Close</a>';

    var btn_hold = '<a class="btn btn-primary btn-lg" style="color:white;background-color:#F660AB;border-color:#F660AB;" onclick="hold(\'' + idcat + '\')">Hold ' + idcat + '</a>';

    if (data.data.due_f !== null) {
        t_alert += '<b>Due:</b> ' + data.data.due_f;
    }
    if (data.data.mem_toy_holds_off === 'Yes') {
        ok = 'No';
    }

    t_alert += '<b>Category:</b> ' + data.data.category_description;
    if (data.data.age !== null && data.data.age !== '') {
        t_alert += '<br><b>Age:</b> ' + data.data.age;
    }
    t_alert += '<br><b>No Pieces:</b> ' + data.data.no_pieces + '<br>';
    var hold_ok = '';
    if (ok === 'Yes') {
        hold_ok += btn_hold;
    }
    r += t_alert;


    //s += '<br><a class="btn btn-primary" style="color:white;" href="' + link + '">' + idcat + '</a>';

    if (data.data.desc1 !== '' && data.data.desc1 !== null) {
        s += '<b>Piece Description:</b><br> ' + data.data.desc1;
    }


    $("#Toy").html(r);
    $("#hold").html(hold_ok);
    $("#toy_status").html(s);
    $("#toyname").html(data.data.toyname);
    $("#toy_title").html(toy_btn + "  " + data.data.toyname);
    $("#goto").html(c3);
    $("#picture").html(picture);
    $("#fav").html(data.data.fav);
    $("#toy_alerts").html(t_alert);
    $("#Toy_details").html(s);
    $("ToyFilter").value = '';
}
function fill_div(data, idcat) {

    var link = '../toy/toy.php?idcat=' + idcat;
    var toy_btn = '<a class="btn btn-primary" style="color: white;" href="' + link + '">' + idcat + '</a>';

    var c1 = "<h4>" + toy_btn + "  " + data.data.toyname + "</h4>";
    if (data.data.pic === 'Yes') {
        var picture = "<img style=\"max-width:200px;border-radius: 5px;\" src=\"../../toy_images/" + data.data.subdomain + "/" + idcat.toLowerCase() + ".jpg?v=" + data.data.pic_ver + "\" onerror=\"imgError();\">";
    } else {
        var picture = "<img style=\"max-width:200px;border-radius: 5px;\" src=\"../../toy_images/" + 'demo' + "/" + 'blank' + ".jpg?v=" + data.data.pic_ver + "\" onerror=\"imgError();\">";
    }

    // var picture = "<img style=\"max-width:200px;border-radius: 5px;\" src=\"../../toy_images/" + data.data.subdomain + "/" + idcat.toLowerCase() + ".jpg?v=" + data.data.pic_ver + "\" onerror=\"imgError();\">";
    if (data.data.alert_toy !== '' && data.data.alert_toy !== null) {
        c1 += '<font color="red">' + data.data.alert_toy + '</font>';
    }

    var ok = 'Yes';

    if (data.data.loans > 0) {
        c1 += '<h4 style="background-color:yellow;padding-left:10px;max-width: 130px;"><font color="red">ON LOAN</font></h4>';
        ok = 'No';
    }
    if (data.data.hold_borid > 0) {
        c1 += '<h4 style="background-color:lightgreen;padding-left:10px;max-width: 130px;"><font color="red">ON HOLD</font></h4>';
        //      t_alert += '<br>This Toy is On Hold by <b>' + data.data.onloan_borid + '</b>: ' + data.data.holdname + '.';
        ok = 'No';
    }
    if (data.data.reserves > 0) {
        c1 += '<font color="red"><h4 style="background-color:lightblue;padding-left:10px;max-width: 130px;">RESERVED</h4></font>';
        ok = 'No';
    }
    if ((data.data.toy_status !== 'ACTIVE')) {
        c1 += '<h4><font color="red">This Toy is ' + data.data.toy_status + '.</font></h4>';
        ok = 'No';
    }
    if (data.data.stocktake_status === 'LOCKED') {
        ok = 'No';
        c1 += '<h4><font color="red">This Toy is LOCKED.</font></h4>';
    }
    if (data.data.loan_type === 'GOLD STAR') {
        ok = 'No';
        c1 += '<br>GOLD STAR/DUTY TOY<br>';
    }
    if (data.data.reservecode !== '' && data.data.reservecode !== null) {
        ok = 'No';
        c1 += '<br>RESERVATION TOY';
    }
    if (data.data.mem_toy_holds_off === 'Yes') {
        ok = 'No';
    }

    if ((data.data.q_lock !== null)) {
        c1 += '<h4><font color="red">This Toy is in Quarantine</font></h4>';
        ok = 'No';
    }
    var c3 = '';
    c3 += '<br><a class="btn btn-danger btn-lg" style="color:white;" onclick="close_preview(\'' + idcat + '\');">Close</a>';

    var btn_hold = '<a class="btn btn-primary btn-lg" style="color:white;background-color:#F660AB;border-color:#F660AB;" onclick="hold(\'' + idcat + '\')">Hold ' + idcat + '</a>';

    if (data.data.due_f !== null) {
        c1 += '<b>Due:</b> ' + data.data.due_f;
    }

    c1 += '<br><b>Category:</b> ' + data.data.category_description;
    if (data.data.age !== null && data.data.age !== '') {
        c1 += '<br><b>Age:</b> ' + data.data.age;
    }
    c1 += '<br><b>No Pieces:</b> ' + data.data.no_pieces + '<br>';
    if (ok === 'Yes') {
        c1 += '<br>' + btn_hold;
    }
    var c2 = '';
    if (data.data.desc1 !== '' && data.data.desc1 !== null) {
        c2 = '<b>Piece Description:</b><br> ' + data.data.desc1;
    }
    if (data.data.desc2 !== '' && data.data.desc2 !== null) {
        c2 += data.data.desc2;
    }


    var div = '<div class="row" style="width:100%;padding-top: 10px; padding-bottom: 10px; background-color: whitesmoke;">';
    //div += '<div style="col-sm-1"></div>';
    div += '<div id="top_' + idcat + '" class="col-sm-3 col-xs-12">' + c1 + '</div>';
    div += '<div class="col-sm-3 col-xs-12" style="padding-top:10px;text-align:center;">' + picture + '</div>';
    div += '<div class="col-sm-1 col-xs-12">' + data.data.fav + '</div>';
    div += '<div class="col-sm-3 col-xs-12">' + c2 + '</div>';
    div += '<div class="col-sm-2 col-xs-12">' + c3 + '</div>';
    // div += '<div style="col-sm-1"></div>';
    div += '</div>';

    $("#" + idcat).html('');
    $("#" + idcat).html(div);
    $("#" + idcat).removeClass('col-sm-2');
    $("#" + idcat).addClass('col-sm-12');
    document.getElementById(idcat).style.height = "auto";
    document.getElementsByClassName('tooltip-inner').innerHTML = '';
}

function toy(idcat) {
    window.open('../toys/update/toy_detail.php?idcat=' + idcat, 'Toy').focus();
}
function hide_toy() {
    $("#toy_div").hide();
    $("#print_div").hide();
}

function onclick_delete_toy(idcat) {
    // alert(borid);
    $('#p6').modal('show');
    var title = '<h4>Deleting ' + idcat + ': ' + document.getElementById("toyname").innerHTML + '</h4>';
    document.getElementById("p6_title").innerHTML = title;
    document.getElementById("idcat_p6").value = idcat;
    document.getElementById("pwd_p6").value = '';
    document.getElementById("pwd_p6").focus();
    $("#submit_p6_update").hide();
    $("#pwd_p6").focus();
    // $("#pwd_p6").focus();
}
function cancel_delete() {

    $("#delete_btn").hide();
    $('#p6').modal('hide');
}
function check_pwd(pwd) {
    if (event.keyCode === 13) {
        var new_pwd = document.getElementById("saved_pwd_p6").value;
        if (pwd === new_pwd) {
            //alert('match');
            $("#submit_p6_update").show();
        } else {
            document.getElementById("pwd_p6").value = '';
            alert('Incorrect Delete Password, please check settings and try again!');
            $("#submit_p6_update").hide();

        }
    }

}
function stop_submit()
{
    return false;
}
function delete_toy() {
    var url = "data/delete_toy.php"; // the script where you handle the form input.
    $.ajax({
        type: "POST",
        url: url,
        data: $("#p6_frm").serialize(), // serializes the form's elements.
        success: function (data)
        {
            alert(data.idcat + ' has been deleted!');
            $('#p6').modal('hide');
            $("#toy_div").hide();
            get_all();
            $("#ToyFilter").focus();
        }
    });

}
function imgError()
{
    var picture = "<img style=\"max-height:150px;\" src=\"images/blank.jpg\">";
    $("#picture").html(picture);
}
function rotate_toy(idcat) {
    $.ajax({
        type: "POST",
        url: "rotate_image.php",
        data: {
            idcat: idcat
        },
        success: function (data) {
            //alert(data.result);
            $("#picture").html(data.img);

        }
    });
    //location.reload(); 
}
function show_all() {
    $("#limit").val('All');
    get_all();

}





