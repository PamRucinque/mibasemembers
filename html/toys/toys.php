<!DOCTYPE html>
<?php include('../mibase_check_login.php'); ?>
<html>
    <head>
        <title>Mibase Search Toys</title>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <?php include('head/head.php'); ?>
    </head>


    <script>

        function page_load() {
            //myFunction();

            if ((navigator.userAgent.indexOf("MSIE") !== -1) || (!!document.documentMode === true)) //IF IE > 10
            {
                console.log('IE');
                $("#Error").html('<h4><font color="red">Please use Google Chrome or Firefox for the admin login ,<br>  Internet explorer doesn\'t support the code in the new version.(can use ie for member login) </font></h4>');
            } else {
                get_all();
                $("#ToyFilter").focus();
            }
            //get_all();

        }
        //change_view();
        $(document).ready(function () {
            $(window).keydown(function (event) {
                if (event.keyCode === 13) {
                    event.preventDefault();
                    return false;
                }
                $('.dropdown-toggle').dropdown();
            });

        });
        
        function myFunction() {
            if ((navigator.userAgent.indexOf("Opera") || navigator.userAgent.indexOf('OPR')) != -1)
            {
                console.log('Opera');
            } else if (navigator.userAgent.indexOf("Chrome") != -1)
            {
                console.log('Chrome');
            } else if (navigator.userAgent.indexOf("Safari") != -1)
            {
                console.log('Safari');
            } else if (navigator.userAgent.indexOf("Firefox") != -1)
            {
                console.log('Firefox');
            } else if ((navigator.userAgent.indexOf("MSIE") != -1) || (!!document.documentMode == true)) //IF IE > 10
            {
                console.log('IE');
            } else
            {
                console.log('unknown');
            }
        }
    </script>
    <style>
        .dataTables_paginate>span>a {
            margin-bottom: 0px !important;
            padding: 1px 5px !important;
        }

        .dataTables_paginate>a {
            margin-bottom: 0px !important;
            padding: 1px 5px !important;
        }
    </style>

    <body onload="page_load();" >

        <?php
        if (!session_id()) {
            session_start();
        }
        include('../header/header.php');
        include('data/functions.php');
        //include('../header/head_new.php');
        $s = $_SESSION['settings'];
        $multi_location = 'Yes';
        if (isset($s['multi_location'])) {
            $multi_location = $s['multi_location'];
        }
        ?>
        <div class="container-fluid" style="padding-left: 30px;padding-top:20px;">
            <form action="" id='toys_s1'>
                <div class="row" style="padding-left: 10px;">
                    <div id="Error" name="Error" class="col-sm-12"></div>
                    <div class="col-sm-2"  style="padding-top:10px;" >
                        <input  id="ToyFilter" name="ToyFilter" placeholder="Enter Keywords" type="text" onkeyup="getToy(this)" class="form-control" onfocus="this.value = ''" ons/>
                    </div>
                    <div class="col-sm-2  col-xs-6" style="padding-top:10px;"><?php include('form/get_category.php'); ?></div>
                    <div class="col-sm-2  col-xs-6" style="padding-top:10px;"><?php include('form/get_attribute.php'); ?></div>
                    <div class="col-sm-2  col-xs-6" style="padding-top:10px;"><?php include('form/get_age.php'); ?></div>
                    <div class="col-sm-2  col-xs-6" style="padding-top:10px;"><?php include('form/get_status.php'); ?></div>
                </div>
                <div class="row" style="padding-left: 10px;">

                    <div class="col-sm-1 col-xs-6"   style="padding-top: 10px;padding-right: 10px;text-align: right;">
                        <select name='limit' id='limit' style='max-width: 100px' class='form-control' onchange="get_all();">
                            <option selected='selected' value='100'>100</option>
                            <option value="200">200</option>
                            <option value="300">300</option>
                            <option value="1000">1000</option>
                            <option value="All">All</option>
                        </select> 
                    </div>
                    <div class="col-sm-5 col-xs-12"   style="padding-top: 10px;padding-right: 10px;text-align: left;">
                        <a id = "pic_btn" class="btn btn-primary" style="color:white;" onclick="change_view();">List</a>

                        <a class="btn btn-warning" style="color:black;" onclick="show_all();">Show All</a>
                        <a class="btn btn-danger" style="color:white;" onclick="clear_toy()">Reset</a> 

                    </div>
                    <div class="col-sm-1 col-xs-12" id="wait"></div>

                    <div class="col-sm-2"   style="padding-top: 10px;padding-right: 10px;text-align: right;">
                        <?php
                        if ($multi_location == 'Yes') {
                            include('form/get_location.php');
                        }
                        ?>

                    </div>
                    <div class="col-sm-3"   style="padding-top: 10px;padding-right: 10px;text-align: right;">
                        <?php include('data/get_location.php'); ?>
                    </div>



                </div>

        </div>
    </form>
    <div class="row" style="padding-top: 10px;">

        <div class="hidden-xs col-sm-12" style="float: right; padding-top: 10px;display: none;"   id='print_div'>

            <?php //include('report/baglabels_toys.php');  ?>
        </div>
    </div>


    <div class="row" style="padding-top: 10px;">
        <div class="col-sm-12 col-xs-12" id="Toys" style='padding-left:30px;padding-right:30px;'>

        </div>
    </div>
    <div style="height:100px;"></div>
    <?php include('form/toy_div.php'); ?>
</body>
</html>