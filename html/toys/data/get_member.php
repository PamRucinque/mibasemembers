<?php
include(dirname(__FILE__) . '/../../mibase_check_login.php');
include('../../class/member.php');
if (!session_id()) {
    session_start();
}
$s = $_SESSION['settings'];
try {
    if (isset($_POST['mid'])) {
        $mid = $_POST['mid'];
    }
    //$mid = 169;

    $m = new Member();
    $m->data_from_table($mid);
    $member = array();
    $member = $m->member_array($m);
    //print_r($member);
    //echo $t->numrows;

    if ($m->numrows > 0) {
        $member = $m->member_array($m);
        
        $member['member'] = '<h4>' . $m->id . ': ' . $m->longname . '</h4>';

        $today = date("Y-m-d");
        $expired = date($m->expired);
        $expired_soon = date($m->expired_soon);

        $renew_btn = '<a class="btn btn-primary btn-sm" style="color:white;" onclick="renew_membership(' . $m->id . ');">Renew</a>';
        $member['membership'] = '<h4>Member Type: <font color="blue">' . $m->membertype . '</font></h4>';
        if ($expired > $today) {
            if ($expired > $expired_soon) {
                $member['membership'] .= '<h4>Expires: <font color="green">' . $m->expired_str . '</font></h4>';
            } else {
                $member['membership'] .= '<h4>Expires Soon:<font color="darkorange"> ' . $m->expired_str . '</font></h4>';
            }
        } else {
            $member['membership'] .= '<h4>Expired:<font color="red"> ' . $m->expired_str . '</font></h4>';
        }
        //console_log($m->member_status);
        if ($m->member_status !== 'ACTIVE') {
            $member['membership'] .= '<h4><font color="red">' . $m->member_status . '</font></h4>';
        }else{
            $member['membership'] .= '<h4><font color="green">' . $m->member_status . '</font></h4>';
        }

        $payment_btn = ' <a class="btn btn-primary btn-sm" style="color:white;" onclick="payment(' . $m->id . ',' . -1 * $m->balance . ')">Pay Now</a>';
        $payment_btn .= ' <a class="btn btn-primary btn-sm" id="payments_btn" style="color:white;" onclick="payment_details(' . $m->id . ')">Payments List</a>';

        if (($m->alert == null) || (trim($m->alert) == '')) {
            $member['alert_member'] = '';
        } else {
            $member['alert_member'] = $m->alert;
        }

        $r["result"] = 'ok';
        $r["data"] = $member;    //fix
        header('Content-Type: application/json');
        //send the object as json 
        echo json_encode($r);
    } else {

        $r = array();
        $r["result"] = "error";
        $r["error"] = "No Member found ";
        header('Content-Type: application/json');
        echo json_encode($r);
    }
} catch (Exception $e) {
    //something went wrong, send an error object with the error test
    header('Content-Type: application/json');
    $r = array();
    $r["result"] = "error";
    $r["error"] = "exception: " + $e->getMessage();
    echo json_encode($r);
}










