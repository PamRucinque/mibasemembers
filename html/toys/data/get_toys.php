<?php

include(dirname(__FILE__) . '/../../mibase_check_login.php');

function parseFilter($filter) {
    $w = array();
    $w1 = explode('&', $filter);
    foreach ($w1 as $w2) {
        $w3 = explode("=", $w2);
        $w[$w3[0]] = $w3[1];
    }
    return $w;
}

try {

    $search = $_POST['search'];
    $filter = array();
    $filter = parseFilter($_POST['filter']);
    $category_str = $filter['category'] . '%';
    $att_str = '%' . $filter['attribute'] . '%';
    $cat_str = 'AND category LIKE ? ';
    
    if (isset($filter['category'])) {
        if ($filter['category'] == 'onloan') {
            $cat_str = 'AND category LIKE ? AND t.id > 0 ';
            $category_str = '%%';
        }
    }

    $location_str = '%%';
    if (isset($filter['location'])) {
        $location_str = '%' . $filter['location'] . '%';
    }
    if ($filter['limit'] == 'All') {
        $limit_str = '';
    } else {
        $limit_str = 'limit ' . $filter['limit'];
    }
    if ($filter['age'] == '') {
        $age_str = '%%';
        $age_filter = 'AND (lower(age) LIKE ? or age is null)  ';
    } else {
        $age_str = '%' . strtolower(urldecode($filter['age'])) . '%';
        $age_filter = 'AND lower(age) LIKE ?  ';
    }
    if ($filter['attribute'] == '') {
        $att_str = '%%';
        $att_filter = ' AND (lower(atta.atts) LIKE ? or atta.atts is null)  ';
    } else {
        $att_str = '%' . strtolower(urldecode($filter['attribute'])) . '%';
        $att_filter = ' AND lower(atta.atts) LIKE ?  ';
    }
    $catsort = $_SESSION['settings']['catsort'];
    $order_str = ' order by id;';
    if ($catsort == 'Yes') {
        $order_str = ' order by category, id;';
    }

//$filter = 'little';

    $rows = "5";
    if (!empty($_POST['rows'])) {
        $rowin = $_POST['rows'];
        if (preg_match("/\d+/", $rowin) === 1) {
            $rows = $rowin;
        }
    }

    include('../../connect/connect_local.php');
    $sql = "SELECT toys.idcat as idcat,toyname,category, toys.id as id, age, location,
            rent,manufacturer, age, atta.atts as attributes,t.due as due,t.borname as borname,no_pieces,coalesce(t.id,0) as onloan
            from toys 
            left join (select id, to_char(due, 'dd-mm-YYYY') as due,borname,idcat from transaction where return is null) t on t.idcat = toys.idcat 
            left join (SELECT att.idcat, string_agg(att.attribute, ' ') AS atts FROM toy_attributes att group by att.idcat) atta on atta.idcat = toys.idcat 
             where (toy_status = 'ACTIVE' or toy_status = 'PROCESSING')  "
             . $cat_str 
            . $age_filter . $att_filter .
            " AND ((toys.location LIKE ?) OR (toys.location IS NULL)) 
             AND (lower(toyname) LIKE ?
                OR lower(manufacturer) LIKE ?
                OR lower(storage) LIKE ?
                OR lower(user1) LIKE ?
                OR lower(toys.idcat) LIKE ?
                OR lower(twitter) LIKE ?)" . $order_str;

    $pdo = new PDO($connect_pdo, $dbuser, $dbpwd);
    $search1 = preg_replace('/\s+/', "%", $search);
    $search2 = strtolower($search1);
    $search3 = "%" . $search2 . "%";
    //  print $filter3;
    $sth = $pdo->prepare($sql);
    $array = array($category_str, $age_str, $att_str, $location_str, $search3, $search3, $search3, $search3, $search3, $search3);
    //$array = array();
    $sth->execute($array);

    $result = $sth->fetchAll();
    $stherr = $sth->errorInfo();
    $numrows = $sth->rowCount();



    if ($stherr[0] != '00000') {
        $error_msg .= "An  error occurred.\n";
        $error_msg .= 'Error' . $stherr[0] . '<br>';
        $error_msg .= 'Error' . $stherr[1] . '<br>';
        $error_msg .= 'Error' . $stherr[2] . '<br>';
        $r = array();
        $r["result"] = "error";
        $r["error"] = "exception: " . $error_msg;
        echo json_encode($r);
        exit;
    }
    $multi_location = 'No';
    if (isset($_SESSION['settings']['multi_location'])) {
        $multi_location = $_SESSION['settings']['multi_location'];
    }


    $rows = array();

    for ($ri = 0; $ri < $numrows; $ri++) {
        $card = $result[$ri];
        $row = array();

        $row[] = $card["idcat"];
        $row[] = $card["toyname"];
        $row[] = $card["category"];
        $row[] = $card["rent"];
        $row[] = $card["manufacturer"];
        $row[] = $card["age"];
        $row[] = $card["attributes"];
        $row[] = $card["due"];
        $row[] = $card["borname"];
        if ($multi_location == 'Yes') {
            $row[] = $card["location"];
        } else {
            $row[] = '';
        }
        $row[] = $card["id"];
        $row[] = $card["no_pieces"];
        $rows[] = $row;
    }

    //tell the client we are sending a json object
    header('Content-Type: application/json');
    //build the object to return, all object have the result variable that is either ok or error
    $r = array();
    $r["result"] = "ok";
    $r["data"] = $rows;
    $r['catsort'] = $_SESSION['settings']['catsort'];
    $r["size"] = $numrows;
    if (!session_id()) {
        session_start();
    }
    $r['subdomain'] = $_SESSION['library_code'];

    //send the object as json 
    echo json_encode($r);
} catch (Exception $e) {
    //something went wrong, send an error object with the error test
    header('Content-Type: application/json');
    $r = array();
    $r["result"] = "error";
    $r["error"] = "exception: " + $e->getMessage();
    echo json_encode($r);
}










