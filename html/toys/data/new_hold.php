<?php

include(dirname(__FILE__) . '/../../mibase_check_login.php');
if (!session_id()) {
    session_start();
}
if (isset($_SESSION['library_code'])) {
    $subdomain = $_SESSION['library_code'];
}
include('functions.php');
//To secure your hold please pay $5 per toy. Total amount due is shown on the My Library page and payment 
//methods are detailed on the Home page. We will email you once your toy is ready for collection. 

$s = $_SESSION['settings'];
$hold_fee = $s['toy_hold_fee'];
//include('../../class/toy.php');
try {
    if (isset($_POST['idcat'])) {
        $idcat = $_POST['idcat'];
    }
    $idcat = strtoupper($idcat);

    $check = array();
    $ok = 'Yes';

    if (!session_id()) {
        session_start();
    }
    $borname = $_SESSION['firstname'] . ' ' . $_SESSION['surname'];
    $toy_hold_fee = $_SESSION['settings']['toy_hold_fee'];
    $msg_str = '';

    $borid = $_SESSION['borid'];
    $date_start = date('Y-m-d');
    include('../../class/toy.php');
    $toy = new Toy();
    $toy->data_from_table($idcat);
    $check = check_max_holds($borid);

    if ($toy->reservecode !== '' && $toy->reservecode !== null) {
        $ok = 'No';
        $msg_str .= 'Cannot Hold a Reservation Toy.';
    }
    if ($toy->loan_type === 'GOLD STAR') {
        $ok = 'No';
        $msg_str .= 'Cannot Hold a Gold Star Toy or Duty Toy.';
    }
    if ($toy->stocktake_status === 'LOCKED') {
        $ok = 'No';
        $msg_str .= 'This toy is Locked.';
    }
    if ($toy->toy_status !== 'ACTIVE') {
        $ok = 'No';
        $msg_str .= 'This toy is ' . $toy->toy_status . '.';
    }
    $multi_location = $_SESSION['settings']['multi_location'];
    if ($multi_location === 'Yes' && $_SESSION['location'] === '') {
        $loc = get_location($borid);

        $_SESSION['location'] = $loc['location'];
        if ($loc['location'] !== '' && $loc['location'] !== null) {

            //echo '<h4>You have an appointment at <font color="red">' . $loc['loc_desc'] . '</font></h4>';
        } else {
            if ($subdomain !== 'melbourne') {
                $msg_str = 'Please book an appointment, before holding any toys.';
                //$ok = 'No';
            }
        }
    }
    if ($multi_location === 'Yes' && $subdomain === 'moreland' && $subdomain === 'bendigo') {
        $loc = get_location($borid);
        if ($toy->location !== $_SESSION['location']) {
            $msg_str = 'This toy is NOT located at the same branch as your appointment, please hold toys at ' . $loc['loc_desc'] . '.';
            $ok = 'No';
        }
    }
    if ($subdomain === 'bendigo' && ($check['holds']+1 === $check['max'])) {
        $msg_str .=  'Reminder: the extra toy must include at least one puzzle!' . "\n". "\n";
    }
    if ($check['holds'] >= $check['max']) {
        if ($_SESSION['settings']['mem_override_maxitems'] === 'Yes') {

            //$ok = 'Yes';
        } else {
            $ok = 'No';
            $msg_str .= 'Too many holds, max of ' . $check['max']  . ' is allowed for this membership.';
        }
    }


    $query_hold = "INSERT INTO toy_holds "
            . "(borid, idcat, date_start, status,created, paid, id, type_hold) VALUES (?,?,?,'PENDING',now(),'No',?,'M');";

    if ($ok == 'Yes') {

        delete_hold($idcat, $borid);
        $holdid = new_holdid();
        $connect_pdo = $_SESSION['connect_pdo'];
        $dbuser = $_SESSION['dbuser'];
        $dbpasswd = $_SESSION['dbpasswd'];

        $pdo = new PDO($connect_pdo, $dbuser, $dbpasswd);
        $sth = $pdo->prepare($query_hold);
        $array = array($borid, $idcat, $date_start, $holdid);
        $sth->execute($array);
//$holdid = $sth->fetchColumn();
        $stherr = $sth->errorInfo();
        if ($stherr[0] != '00000') {
            $return = "An INSERT query error occurred.\n";
            $return .= 'Error ' . $stherr[0] . '<br>';
            $return .= 'Error ' . $stherr[1] . '<br>';
            $return .= 'Error ' . $stherr[2] . '<br>';
//echo $return;
//echo $date_end;
        } else {

            if ($toy_hold_fee > 0) {

                $toy_hold_desc = 'Fee for Toy hold ' . $holdid;
                $pdo = new PDO($connect_pdo, $dbuser, $dbpasswd);
                $query_hold = "INSERT INTO journal (datepaid, bcode, icode, name, description, category, amount, type, typepayment, holdid)
                 VALUES (?,?,?,?,?,?,?,?,?,?);";
                $array = array($date_start, $borid, $idcat, $borname, 'Fee for Toy Hold, Toy # ' . $idcat, 'Hold Toy', $toy_hold_fee, 'DR', 'Debit', $holdid);
                $sth = $pdo->prepare($query_hold);
                $sth->execute($array);

                $stherr = $sth->errorInfo();
                if ($stherr[0] != '00000') {
                    $return = "An INSERT query error occurred.\n";
                    $return .= 'Error ' . $stherr[0] . '<br>';
                    $return .= 'Error ' . $stherr[1] . '<br>';
                    $return .= 'Error ' . $stherr[2] . '<br>';
                    $msg_str .= $return;
                } else {
                    $msg_str .= $holdid . ' : A Hold has been added on ' . strtoupper($idcat) . ' for ' . $borname . ' (' . $_SESSION['borid'] . ')';
                    $msg_str .= "\n" . 'To secure your hold please pay $' . $toy_hold_fee . ' per toy. ' . "\n" . 'Total amount due is shown on the My Library page and payment methods are detailed on the Home page.' . "\n" . ' We will email you once your toy is ready for collection. ';
                    $msg_str .= "\n\n" . 'Please ensure you book an Appointment to collect your toys.';
                }
            } else {
                $msg_str .= $holdid . ': A Hold has been added on ' . strtoupper($idcat) . ' for ' . $borname . ' (' . $_SESSION['borid'] . ').';
                $msg_str .= "\n\n" . 'Please ensure you book an Appointment to collect your toys.';
            }
            //$msg_str .= $ok;
        }
    }



    $r["result"] = $msg_str;
    header('Content-Type: application/json');
    //send the object as json 
    echo json_encode($r);
} catch (Exception $e) {
    //something went wrong, send an error object with the error test
    header('Content-Type: application/json');
    $r = array();
    $r["result"] = "error";
    $r["error"] = "exception: " + $e->getMessage();
    echo json_encode($r);
}

function new_holdid() {
    if (!session_id()) {
        session_start();
    }
    $holdid = 0;
    $connect_str = $_SESSION['connect_str'];
    $conn = pg_connect($connect_str);
    $sql = "SELECT MAX(id) as holdid FROM toy_holds;";
//echo $query_new;

    $nextval = pg_Exec($conn, $sql);
//echo $connection_str;
    $row = pg_fetch_array($nextval, 0);
    $holdid = $row['holdid'];
    $holdid = $holdid + 1;
    return $holdid;
}

function delete_hold($idcat, $borid) {
    if (!session_id()) {
        session_start();
    }
    $output = 'No';
    $connect_pdo = $_SESSION['connect_pdo'];
    $dbuser = $_SESSION['dbuser'];
    $dbpasswd = $_SESSION['dbpasswd'];
    $query = "delete from toy_holds where idcat = ?";
    $query .= " and borid = ?;";
    $pdo = new PDO($connect_pdo, $dbuser, $dbpasswd);
    $sth = $pdo->prepare($query);
    $array = array($idcat, $borid);
    $sth->execute($array);
}

function check_max_holds($borid) {
    if (!session_id()) {
        session_start();
    }
    $subdomain = $_SESSION['library_code'];
    $connect_pdo = $_SESSION['connect_pdo'];
    $dbuser = $_SESSION['dbuser'];
    $dbpasswd = $_SESSION['dbpasswd'];
    $query = "select borwrs.id as borid, m.maxnoitems as max, coalesce(holds,0) as holds
                from borwrs 
                left join  membertype m on m.membertype = borwrs.membertype
                left join (select count(id) as holds,borid from toy_holds 
                where trim(UPPER(status)) = 'READY' or trim(upper(status)) = 'ACTIVE' or trim(upper(status)) = 'PENDING'
                group by borid) h on h.borid = borwrs.id
                where borwrs.id = ?;";

    $pdo = new PDO($connect_pdo, $dbuser, $dbpasswd);
    $sth = $pdo->prepare($query);
    $array = array($borid);
    $sth->execute($array);
    $result = $sth->fetchAll();
    $stherr = $sth->errorInfo();
    $numrows = $sth->rowCount();


    $row = $result[0];
    if ($subdomain == 'bendigo') {
        $row['max'] = $row['max'] + 1;
    }

    return $row;
}

function addDayswithdate($date, $days) {

    $date = strtotime("+" . $days . " days", strtotime($date));
    return date("Y-m-d", $date);
}

function check_dup_hold($idcat, $borid) {
    if (!session_id()) {
        session_start();
    }
    $output = 'No';
    $connect_pdo = $_SESSION['connect_pdo'];
    $dbuser = $_SESSION['dbuser'];
    $dbpasswd = $_SESSION['dbpasswd'];
    $query = "select id from toy_holds where idcat = ?";
    $query .= " and borid = ? and status != 'EXPIRED' and status != 'LOANED';";
    $pdo = new PDO($connect_pdo, $dbuser, $dbpasswd);
    $sth = $pdo->prepare($query);
    $array = array($idcat, $borid);
    $sth->execute($array);
    $numrows = $sth->rowCount();
    if ($numrows > 0) {
        $output = 'Yes';
    }

    return $output;
}
