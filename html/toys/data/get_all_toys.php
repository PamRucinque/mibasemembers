<?php

include(dirname(__FILE__) . '/../../mibase_check_login.php');
if (!session_id()) {
    session_start();
}
$subdomain = $_SESSION['library_code'];
$view = 'list';

function parseFilter($filter) {
    $w = array();
    $w1 = explode('&', $filter);
    foreach ($w1 as $w2) {
        $w3 = explode("=", $w2);
        $w[$w3[0]] = $w3[1];
    }
    return $w;
}

try {

    //$search = $_POST['search'];
    $filter = array();
    $status_str = '';
    $filter = parseFilter($_POST['filter']);
    $view = $_POST['view'];
    $category_str = $filter['category'] . '%';
    if ($filter['category'] !== '') {
        $category_str = $filter['category'];
    }
    $att_str = '%' . $filter['attribute'] . '%';
    $age_str = '%' . $filter['age'] . '%';
    $nopic = 'No';
    $locked_str = " (toy_status = 'ACTIVE' ) ";
    $limit_date = '';
    if ($filter['age'] == '') {
        $age_str = '%%';
        $age_filter = 'AND (lower(age) LIKE ? or age is null)  ';
    } else {
        $age_str = '%' . strtolower(urldecode($filter['age'])) . '%';
        $age_filter = 'AND lower(age) LIKE ?  ';
    }

    if ($filter['attribute'] == '') {
        $att_str = '%%';
        $att_filter = ' AND (lower(atta.atts) LIKE ? or atta.atts is null)  ';
    } else {
        $att_str = '%' . strtolower(urldecode($filter['attribute'])) . '%';
        $att_filter = ' AND lower(atta.atts) LIKE ?  ';
    }

    $search = $filter['ToyFilter'];


    $search1 = preg_replace('/\s+/', "%", $search);
    $search1 = str_replace('%20', ' ', $search1);
    ///\s\s+/'
    //$search1 = preg_replace('/\s\s+/', " ", $search);
    $search2 = strtolower($search1);
    $search3 = "%" . $search2 . "%";
    if ($filter['limit'] == 'All') {
        $limit_str = '';
    } else {
        $limit_str = 'limit ' . $filter['limit'];
    }
    $location_str = '%%';
    if (isset($filter['location'])) {
        $location_str = '%' . trim($filter['location']) . '%';
    }

    //$multi_location = 'No';
    if (isset($_SESSION['settings']['multi_location'])) {
        $multi_location = $_SESSION['settings']['multi_location'];
    }
    $catsort = $_SESSION['settings']['catsort'];
    $order_str = ' order by id ';
    //$catsort = 'No';
    if ($catsort == 'Yes') {
        $order_str = ' order by toys.category, id ';
    }
    $cat_str = 'AND toys.category LIKE ? ';
    if (isset($filter['status'])) {
        if ($filter['status'] == 'onloan') {
            $status_str = ' AND t.id > 0 ';
            //$category_str = '%%';
        }
    }
    if (isset($filter['status'])) {
        if ($filter['status'] == 'inlibrary') {
            $status_str = ' and h.total is null and t.id is null and toys.stocktake_status != \'LOCKED\' ';
            //$category_str = '%%';
        }
    }
    if (isset($filter['status'])) {
        if ($filter['status'] == 'hold') {
            $status_str = ' and h.total > 0 ';
            //$category_str = '%%';
        }
    }


    if (isset($filter['status'])) {
        if ($filter['status'] == 'new') {
            //$category_str = '%%';
            if ($catsort == 'Yes') {
                $order_str = ' order by toys.date_purchase DESC, toys.category, id  ';
            } else {
                $order_str = ' order by toys.date_purchase DESC, toys.category, id  ';
            }
            $limit_date = " AND (date_purchase != null or date_purchase != '1900-01-01') ";
        }
    }


    include('../../connect.php');
    $sql = "WITH h AS (select coalesce(count(id),0) as total, idcat from toy_holds where (status = 'ACTIVE'or status = 'READY' or status = 'PENDING' or status = 'LOCKED') group by idcat),
                 t AS (select coalesce(id,0) as id, to_char(due, 'dd-mm-YYYY') as due_f,due, idcat from transaction where return is null),
                 atta AS (SELECT att.idcat, string_agg(att.attribute, ' ') AS atts FROM toy_attributes att group by att.idcat)
            SELECT toys.idcat as idcat,toyname, toys.category, cat.description as category_description, toys.id as id, age, toy_status,
            rent,manufacturer, age, atta.atts as attributes,t.due_f as due_f, t.due as due, no_pieces, pic_ver, coalesce(h.total,0) as holds,
            coalesce(t.id,0) as onloan, to_char(date_purchase, 'dd-mm-YYYY') as purchased, date_purchase, pic_ver,l.location as location, l.description as loc_desc      
            from toys 
            left join category cat on cat.category = toys.category
            left join location l on l.location = toys.location 
            left join h on h.idcat = toys.idcat 
            left join t on t.idcat = toys.idcat 
            left join  atta on atta.idcat = toys.idcat 
            WHERE "
            . $locked_str
            . $cat_str . $status_str
            . $age_filter . $att_filter . $limit_date .
            "AND ((toys.location LIKE ?) OR (toys.location IS NULL)) " .
            " AND (lower(toyname) LIKE ?
                OR lower(manufacturer) LIKE ?
                OR lower(storage) LIKE ?
                OR lower(user1) LIKE ?
                OR lower(toys.idcat) LIKE ?
                OR lower(twitter) LIKE ?)" . $order_str .
            $limit_str . ";";

    //echo $sql;

    $pdo = new PDO($connect_pdo, $dbuser, $dbpwd);

    $sth = $pdo->prepare($sql);
    $array = array($category_str, $age_str, $att_str, $location_str, $search3, $search3, $search3, $search3, $search3, $search3);
    //$array = array();
    $sth->execute($array);

    $result = $sth->fetchAll();
    $stherr = $sth->errorInfo();
    $numrows = $sth->rowCount();



    if ($stherr[0] != '00000') {
        $error_msg .= "An  error occurred.\n";
        $error_msg .= 'Error' . $stherr[0] . '<br>';
        $error_msg .= 'Error' . $stherr[1] . '<br>';
        $error_msg .= 'Error' . $stherr[2] . '<br>';
        $r = array();
        $r["result"] = "error";
        $r["error"] = "exception: " . $error_msg;
        echo json_encode($r);
        exit;
    }
    $toys = array();
    $toy = array();

    for ($ri = 0; $ri < $numrows; $ri++) {
        $card = $result[$ri];
        $toy = $card;
        $file_pic = '../../../toy_images/' . $subdomain . '/' . strtolower($card["idcat"]) . '.jpg';
        //if ($view === 'pic') {
            if (file_exists($file_pic)) {
                $toy['pic'] = 'Yes';
            } else {
                $toy['pic'] = 'No';
            }
       // }
        if ($multi_location == 'Yes') {
            $toy['loc_desc'] = $card["loc_desc"];
        } else {
            $toy['loc_desc'] = '';
        }
        $toys[] = $toy;
    }

    //tell the client we are sending a json object
    header('Content-Type: application/json');
    //build the object to return, all object have the result variable that is either ok or error
    $r = array();
    $r["result"] = "ok";
    $r["toys"] = $toys;
    $r['age'] = strtolower(urldecode($filter['age']));
    $r["size"] = $numrows;
    $r['catsort'] = $_SESSION['settings']['catsort'];
    $r['subdomain'] = $_SESSION['library_code'];

    //send the object as json 
    echo json_encode($r);
} catch (Exception $e) {
    //something went wrong, send an error object with the error test
    header('Content-Type: application/json');
    $r = array();
    $r["result"] = "error";
    $r["error"] = "exception: " + $e->getMessage();
    echo json_encode($r);
}











