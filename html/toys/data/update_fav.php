<?php

include(dirname(__FILE__) . '/../../mibase_check_login.php');
include('../../mibase_check_login.php');
include('../../connect.php');

if (!session_id()) {
    session_start();
}
if (isset($_POST['idcat'])) {
    $idcat = $_POST['idcat'];
    $borid = $_SESSION['borid'];
    $mode = $_POST['mode'];
}
$connect_pdo = $_SESSION['connect_pdo'];
$dbuser = $_SESSION['dbuser'];
$dbpasswd = $_SESSION['dbpasswd'];
$r['idcat'] = $idcat;
if ($mode === 'delete') {
    try {
        $sql = "DELETE from favs where borid = ? and idcat = ?;";
        $pdo = new PDO($connect_pdo, $dbuser, $dbpasswd);
        $sth = $pdo->prepare($sql);
        $array = array($borid, $idcat);
        $sth->execute($array);
    } catch (PDOException $e) {
        $error = "Error! toy detail: " . $e->getMessage() . "<br/>";
        $r['result'] = $error;
    }
}
if ($mode === 'add') {
    try {
        $sql = "INSERT INTO favs "
                . "(borid, idcat, date_added) VALUES (?,?,now())"
                . " RETURNING id;";
        $pdo = new PDO($connect_pdo, $dbuser, $dbpasswd);
        $sth = $pdo->prepare($sql);
        $array = array($borid, $idcat);
        $sth->execute($array);
    } catch (PDOException $e) {
        $error = "Error! toy detail: " . $e->getMessage() . "<br/>";
        $r['result'] = $error;
    }
}

header('Content-Type: application/json');
echo json_encode($r);


