<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<?php
$branch = substr(getcwd(), 22, strpos(getcwd() . '/', '/', 22 + 1) - 22);
include(get_include_path() . '/mibaselive/html/' . $branch . '/mibase_check_login.php');
include('get_settings.php');
include('../get_settings.php');
include('../home/get_index_info.php');

?>
<html xmlns="http://www.w3.org/1999/xhtml">
    <head>
        <title>Mibase Members</title>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
        <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0"/> 
        <link rel="apple-touch-icon" href="favicon-114.png" />
        <meta name="apple-mobile-web-app-capable" content="yes" /><!-- hide top bar in mobile safari-->
        <!--<meta name="apple-mobile-web-app-status-bar-style" content="black-translucent" /> translucent top bar -->
        <!--<link rel="stylesheet" type="text/css" media="screen" href="style.css" />-->
        <link rel="shortcut icon" href="/favicon.ico">
            <script type="text/javascript" src="../js/tooltip.js"></script>
            <script>
                function menu() {
                    var sidebar = document.getElementById('sidebar');
                    var main = document.getElementById('main');
                    var top = document.getElementById('heading');
                    if (sidebar.style.visibility === 'visible') {
                        sidebar.style.visibility = 'hidden';
                        //main.style.visibility = 'visible';
                        main.style.padding = '80px 20px 20px 10px';
                        top.style.padding = '0 0 0 10px';


                    } else {
                        sidebar.style.visibility = 'visible';
                        //main.style.visibility = 'hidden';
                        //top.style.visibility = 'visible';
                        main.style.padding = '80px 20px 20px 210px';
                        top.style.padding = '0 0 0 210px';
                        document.getElementById('menu').value = 'Hide Menu';
                    }
                }

            </script>


    </head>
    <?php
    if (!session_id()) {
        session_start();
    }

    function iphone() {
        if (!isset($_SERVER['HTTP_USER_AGENT'])) {
            return false;
        }
        if (strstr($_SERVER['HTTP_USER_AGENT'], 'iPod') || strstr($_SERVER['HTTP_USER_AGENT'], 'iPhone') || strstr($_SERVER['HTTP_USER_AGENT'], 'iPad') || strstr($_SERVER['HTTP_USER_AGENT'], 'Android')) {
            return true;
        }
        //return strstr($_SERVER['HTTP_USER_AGENT'], 'iPhone') !== false;
    }

    if (iphone()) {
        echo '<link rel="stylesheet" type="text/css" href="../toys/css/iphone.css" />';
    } else {
        echo '<link rel="stylesheet" type="text/css" href="../toys/css/ipad.css" />';
    }
    $view = 1;
    if ($_GET['v'] != '') {
        $view = trim($_GET['v']);
        $_SESSION['v'] = $view;
    }
    if ($_GET['idcat'] != '') {
        $_SESSION['idcat'] = $_GET['idcat'];
        $_SESSION['idcat'] = strtoupper($_SESSION['idcat']);
        $idcat = $_SESSION['idcat'];
    } else {
        $idcat = $_SESSION['idcat'];
    }
    include('../toys/get_toy.php');
    include('../toys/toy_detail.php');
    //include('data/side_bar.php');
    $img_home = '<img src="../css/img/home-icon.png" width="23px" style="padding-top: 2px; padding-left: 2px;">';
    $img_star = '<img src="../css/img/star-icon.png" width="23px" style="padding-top: 2px; padding-left: 2px;">';
    $img_select = '<img src="../css/img/stary-icon.png" width="23px" style="padding-top: 2px; padding-left: 2px;">';

    $str_menu_home = '<li><a href="../mylibrary/index.php?v=5"><span class="ico msg">' . $img_home . '</span>My Library</a></li>';
    $str_menu_toys = '<li><a href="../toys/index.php?v=1"><span class="ico msg">' . $img_star . '</span>Toy List</a></li>';
    $str_menu_details = '<li><a href="../toys/index.php?v=4"><span class="ico msg">' . $img_star . '</span>' . $_SESSION['idcat'] . ' Details</a></li>';
    $str_menu_party = '<li><a href="../toys/index.php?v=6"><span class="ico msg">' . $img_star . '</span>' . $hiretoys_label . '</a></li>';
    $str_menu_reserve = '<li><a href="" class="active"><span class="ico msg">' . $img_select . '</span>Reservations</a></li>';
    ?>
    <body>
        <div id="wrap">
            <div id="main" style="padding-top: 20px;">
                <div class="top"  id="heading">
                    <h2 class="title"><?php echo $str_heading; ?></h2>


                </div>
                <div class="search">
                    <h2><?php
                    if (($stype == 'JC')||($loan_type == 'JC')){
                        $str_heading_right = $castle;
                    }else{
                        $str_heading_right = '';
                    }
                     echo $str_heading_right;   
                        ?></h2>
                </div>
                <div class="header">

                    <a class="left" href="../password/index.php">Reset Password</a>
                    <a class="middle"  id="menu" onclick="menu()">Menu</a>
                    <a class="right" href="../Logout.php">Logout</a>

                </div><!--header-->

                <div class="content">
                    <table>
                        <tbody>
                            <div class="calendar">
                                <?php
                                include('reservation.php');
                                ?> 
                            </div>


                        </tbody>
                    </table>
                    <tbody>

                    </tbody>

                </div><!--content-->

            </div><!--main-->


            <div id="sidebar">

                <div class="content">
                    <ul class="nav">
                        <?php
                        echo $str_menu_home;
                        echo $str_menu_party;
                        echo $str_menu_toys;
                        if (isset($_SESSION['idcat'])) {
                            echo $str_menu_details;
                            echo $str_menu_reserves;
                            include('../toys/get_toy.php');
                            include('../toys/toy_detail.php');

                            $img = '<li style="background-color: white;"><img width="190px" style="padding-left: 10px;" src="../../toy_images/' . $subdomain . '/' . strtolower($idcat) . '.jpg" alt="toy image"></li>';
                            echo $img;
                            //$str_alert = $_SESSION['reset'];

                            if ($str_alert != '') {
                                echo '<script language="javascript">';
                                echo 'alert("' . $str_alert . '")';
                                echo '</script>';
                            }
                            $str_alert = '';
                        }
                        echo $str_menu_reserve;
                        echo $str_print_reserves;
                        ?>

                    </ul>
                    <p>
                        <?php ?>
                    </p>
                </div>
                <!--content-->


            </div><!--sidebar-->
        </div><!--wrap-->


<!--<script type="text/javascript" src="script.js">-->
    </body>
</html>