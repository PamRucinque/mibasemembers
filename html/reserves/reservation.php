<?php
include('../mibase_check_login.php');
?>
<!doctype html>
<html lang="en">
    <head>
        <?php
        include ('../header/head.php');
        ?>
    </head>

    <?php
    $success = '';
    $str_alert = '';
    $str_rent = '';
    $mem_hire_conditions = '';
    $reserve_period = $_SESSION['settings']['reserve_period'];
    $hiretoys_label = $_SESSION['settings']['hiretoys_label'];
    $mem_reserve_notification = $_SESSION['settings']['mem_reserve_notification'];
    $jc_reserve_period = $_SESSION['settings']['jc_reserve_period'];
    include('../home/get_index_info.php');
    $calendar_text = '';
    //include('data/get_toy.php');



    if (isset($_POST['submit'])) {

        //include('../home/get_settings.php');
        $end = $_POST['date_end'];
        $start = $_POST['date_start'];
        $idcat = $_SESSION['idcat'];

        $query = "INSERT INTO reserve_toy (member_id, approved, date_start, date_end, idcat, date_created, status)
                        VALUES (
                        {$_SESSION['borid']}, FALSE,
                        '{$_POST['date_start']}',
                        '{$_POST['date_end']}',
                        '{$idcat}',
                            NOW(), 'ACTIVE') RETURNING id;";

        include('data/check_dates.php');
        include('fill_array_closed.php');
        include('fill_array_open.php');

        //include('data/get_toy.php');
        //include('get_toy.php');
        //include('get_settings.php');
        $dw = trim(date('l', strtotime($end)));
        if (count($weekday_array) != 0) {
            if (!in_array($dw, $weekday_array)) {
                //$success = print_r($weekday_array);
                if ($dw == 'Wednesday' && $subdomain == 'rangiora') {
                    //$success .= 'Please change the end date, the Library is closed on a ' . $dw . '.'; 
                } else {
                    $success .= 'Please change the end date, the Library is closed on a ' . $dw . '.';
                }
            }
        }
        if (in_array($end, $closed)) {
            $success .= 'Please change the end date, the Library is closed on the ' . $end . '.';
        }

        $dw = trim(date('l', strtotime($start)));

        //check start date if the library is open
        if (count($weekday_array) != 0) {
            if (!in_array($dw, $weekday_array)) {
                //$success = print_r($weekday_array);
                if ($dw == 'Wednesday' && $subdomain == 'rangiora') {
                    //$success .= 'Please change the end date, the Library is closed on a ' . $dw . '.'; 
                } else {
                    $success .= 'Please change the start date, the Library is closed on a ' . $dw . '.';
                }
            }
        }
        if (in_array($start, $closed)) {
            $success .= 'Please change the start date, the Library is closed on the ' . $start . '.';
        }

        if ($success == '') {
            $success .= check_date($_POST['date_start'], $_POST['date_end'], $idcat);
        }
        if ($success == '') {
            $success .= check_date_loans($_POST['date_start'], $_POST['date_end'], $idcat);
        }

        $date1 = new DateTime($_POST['date_start']);
        $date2 = new DateTime($_POST['date_end']);
        if (($stype == 'JC') || ($loan_type == 'JC')) {
            if ($jc_reserve_period != '') {
                $reserve_period = $jc_reserve_period;
            }
        }

        $diff = $date2->diff($date1)->format("%a");
        //$success = 'reserve period: ' . $reserve_period . ' Difference: ' . $diff;
        if ($diff > $reserve_period) {
            //$button = '<br><br><a class="button1_blue" href="reservation.php?idcat="' . $_SESSION['idcat'] . '>OK</a>';
            $success .= 'Cannot Reserve this toy for more than ' . $reserve_period . ' days. ';
        }
        //  $success = check_date_loans($_POST['date_start'], $_POST['date_end'], $idcat);
        if ($success == '') {

            // echo $query;
            $connect_str = $_SESSION['connect_str'];
            $conn = pg_connect($connect_str);
            $result = pg_Exec($conn, $query);


            if (!$result) {
                echo "An INSERT query error occurred.\n";
                echo $query;
                //exit;
            } else {
                $row = pg_fetch_array($result);
                $rid = $row['id'];
                $str_alert = 'Success! your reservation has been saved. ID: ' . $rid;
                if (($stype == 'JC') || ($loan_type == 'JC')) {
                    //$str_alert .= '\n' . 'Please read and sign the Terms and Conditions, ';
                    //$str_alert .= 'the link is at the top of this page. Bouncy castles are to be returned before 10am and picked up after 11am on the day of hire.';
                    $str_alert .= $castle_link;
                    $button_str = 'I Agree';
                    $blinking = '<a id="blinkdiv"><br><strong><font color="red"><h4 class="blink_me">By clicking I agree, I agree to the conditions of Membership</h4></font></strong></a>';

                    //echo $str_alert;
                    //include('data/overlay.php');
                    //$str_alert = '';
                }
                if ($mem_reserve_notification == 'Yes') {
                    include('send_email_reservation.php');
                }
            }
        } else {
            $str_alert = $success;
            $button_str = 'OK';
            $blinking = '';
        }
        $_SESSION['alert_reserve'] = $str_alert;

        //pg_FreeResult($result);
// Close the connection
        //pg_Close($conn);
    }

    include('fill_array.php');
    $max_due = date('Y-m-d');
    $date_txt = date_create($max_due);
    //date_add($date_txt, date_interval_create_from_date_string('+0 days'));
    $max_due = date_format($date_txt, 'Y-m-d');
    include('fill_array_loan.php');
    //echo '<br>Due Back: ' . $max_due . '<br>';
    $reserve_from = get_reserve_dates($_SESSION['idcat']);
    //echo $reserve_from . '<br>';
    if ($reserve_from == 'no reserves') {
        $reserve_from = $max_due;
    }

    $date_txt = date_create($reserve_from);
    $buffer_txt = '+' . 0 . ' days';
    date_add($date_txt, date_interval_create_from_date_string($buffer_txt));
    $reserve_from = date_format($date_txt, 'Y-m-d');

    $date_txt = date_create($reserve_from);
    $date_string = '+' . $reserve_period . ' days';
    date_add($date_txt, date_interval_create_from_date_string($date_string));
    $reserve_to = date_format($date_txt, 'Y-m-d');
    //include('fill_array_reserve.php');

    include('data/get_toy.php');
    if (($_SESSION['idcat'] == '')) {
        if (($_SESSION['idcat'] == '')) {
            echo '<br>Please Select a Toy first!';
        }
    } else {
        //$img = '<img height = "100px" src = "../../toy_images/' . $subdomain . '/' . strtolower($idcat) . '.jpg" alt = "toy image">';
        //echo '<table width = "100%"><tr><td><h2><font color = "blue">' . $idcat . ': ' . $toyname . '</font></h2></td>';
        //echo '<td align = "right">' . $img . '</td><td width = "10%"></td></tr></table>';
        //echo '<font color = "red">' . $reservecode . '</font>';
        if (($reservecode != null) || ($reservecode != '')) {
            //include('../toys/get_toy.php');
            //include('get_settings.php');
            include('calendar.php');
            $legend_txt = '';
            $legend_txt .= '<table width = "100%"><tr>';
            //$legend_txt .= '<td align = "center" >Legend: </td>';
            $legend_txt .= '<td align = "center" bgcolor = "#fc7f8e">On Loan</td>';
            $legend_txt .= '<td align = "center" bgcolor = "#e6ec8b">Reserved</td>';
            $legend_txt .= '<td align = "center" bgcolor = "#CEF6F5">Selected</td>';
            $legend_txt .= '<td align = "center" bgcolor = "lightgrey">Closed</td>';
            $legend_txt .= '</table>';
            ?>
            <div class="row">
                <div class="col-md-6"><?php
                    include('reservation_edit_form.php');
                    echo $legend_txt;
                    ?></div>  
                <div class="col-md-6">
                    <div class='row'>
                        <div class='col-md-12'>
                            <?php
                            echo '<h4 align="left"><font color="#900C3F">  ' . $idcat . ': ' . $toyname . '</font></h4>';
                            ?>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-6"><?php echo $img; ?></div>
                        <div class="col-md-12"><?php
                            echo $str_rent;
                            echo $str_status;
                            if (($stype == 'JC') || ($loan_type == 'JC') || ($mem_hire_conditions == 'Yes')) {
                                echo $castle;
                            }
                            ?>
                        </div>
                    </div>
                    <?php
                } else {
                    echo '<br><h1><font color = "red">This Toy Cannot be Reserved!</font></h1>';
                }
            }
            ?>

        </div>
    </div>

    <div class="row">
        <div class="col-md-12">
            <?php
            echo  $calendar_text;
            ?>
        </div>

    </div>
    <?php

// include('partypack_form.php');

    function get_reserve_dates($idcat) {
        //$max_date = $max_due;
        $connect_str = $_SESSION['connect_str'];
        $conn = pg_connect($connect_str);

        $query = "select max(date_end) as max_date from reserve_toy where idcat = '" . $idcat . "' and date_end > current_date;";
        $a1 = array();

        $result1 = pg_Exec($conn, $query);
        $numrows = pg_numrows($result1);
        //echo $max_due;
        if ($numrows > 0) {
            $row = pg_fetch_array($result1);
            $max_date = $row['max_date'];
        }
        if ($max_date == '') {
            $max_date = 'no reserves';
        }


        pg_FreeResult($result1);

        pg_Close($conn);
        //echo $max_due . '<br>';
        return $max_date;
    }
    ?>