<?php

include('../mibase_check_login.php');

date_default_timezone_set('Australia/Melbourne');

function check_date($start_date, $end_date, $idcat) {
    $connect_str = $_SESSION['connect_str'];
    $conn = pg_connect($connect_str);
    //  $success = '';
    $query = "SELECT date_start, member_id, date_end FROM reserve_toy  
            WHERE status = 'ACTIVE' AND idcat = '" . $idcat . "' ORDER by date_start;";

 
    $result = pg_Exec($conn, $query);
    $success = '';
    $start = strtotime($start_date);  //the entered start date for the reservation
    $end = strtotime($end_date);  //the netered end date for the reservatiosn

    $today = mktime(0, 0, 0, date("m"), date("d"), date("Y"));
    if ($start >= $end) {
        $success .= "Start date cannot be after than End date. <br>";
    } elseif (($start < $today) || ($end < $today)) {
        $success .= 'Start and or End date cannot be before than today.' . $button . '<br>';
    }


    $numrows = pg_numrows($result);
    if ($numrows > 0) {

        for ($ri = 0; $ri < $numrows; $ri++) {
            $row = pg_fetch_array($result, $ri);

            $date_end = strtotime($row['date_end']);
            $date_start = strtotime($row['date_start']);

            if ($end > $date_start AND $end < $date_end) {
                $success .= '<br>Dates overlap, reservation failed. error code 1: ' .  '<br>';
                //$success = false;
            }
            if ($start >= $date_start AND $start < $date_end) {
                $success .= '<br>Dates overlap, reservation failed. error code 2. ' .  '<br>';
                //$success = false;
            }
            if ($start < $date_start AND $end >= $date_end) {
                $success .= '<br>Dates overlap, reservation failed. error code 3. ' . '<br>';
                //$success = false;
            }
            if (($start == $date_end)) {
                $success .= "Cannot loan and return a hire toy on the same day, reservation failed. ";
            }
            if (($start == $date_end)) {
                $success .= "Cannot loan a toy on the same day it is due to be returned, reservation failed. ";
            }
            if (($end == $date_start)) {
                $success .= "Cannot loan a toy on the same day it is due to be returned, reservation failed. ";
            }
        }
    }
    return $success;
}

function check_date_loans($start_date, $end_date, $idcat) {
    $success = '';
    $query = "SELECT date_loan as date_start, due as date_end FROM transaction  
            WHERE idcat = '" . $idcat . "' AND return is null ORDER by date_start;";
    $connect_str = $_SESSION['connect_str'];
    $conn = pg_connect($connect_str);

    //echo $query;
    $result = pg_Exec($conn, $query);


    $start = date_create_from_format('Y-m-d', $start_date);
    $end = date_create_from_format('Y-m-d', $end_date);
    //echo date_format($date, 'Y-m-d');
    //echo "Start Date: " . date_format($start, 'd/m/Y' ) . " and end date is: " . date_format($end, 'd/m/Y') . "<br>";
    $now = date('Y-m-d');
    $today = date_create_from_format('Y-m-d', $now);
    $numrows = pg_numrows($result);
    if ($numrows > 0) {

        for ($ri = 0; $ri < $numrows; $ri++) {
            $row = pg_fetch_array($result, $ri);
            //echo "Start: " . $row['date_start'] . " End Date: " . $row['date_end'] . "<br>";
            //echo "Input Start: " . $start_date . " End Date: " . $end_date . "<br>";
            $date_end = date_create_from_format('Y-m-d', $row['date_end']);
            $date_start = date_create_from_format('Y-m-d', $row['date_start']);
            if (($start < $today) || ($end < $today)) {
                $success = '<br>Start and or End date cannot be less than today. <br>';
            }
            if (($end < $start)) {
                $success = '<br>End date cannot be less than Start date. <br>';
            }

            If (($start < $date_start) AND ( $end < $date_end) AND ( $end > $date_start)) {
                //AND (date_format('Y-m-d', $start) < $row['date_end'])
                // AND (date_format('Y-m-d', $end) > $row['date_end']))
                $success = "<br>Dates overlap with current Loan, reservation failed <br>";
            }
            If (($start > $date_start) && ($start < $date_end) && ($end < $date_end)) {
                $success = "<br>Dates overlap with current Loan, reservation failed <br>";
                //$success = false;
            }

            If ($start > $date_start AND $start < $date_end AND $end > $date_end) {
                $success = "<br>Dates overlap with current Loan, reservation failed <br>";
                //$success = false;
            }
            if (($start == $date_end)) {
                $success = "Cannot loan and return a hire toy on the same day, reservation failed. ";
            }
        }
    }
    return $success;
}
?>


