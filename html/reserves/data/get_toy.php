<?php

include('../mibase_check_login.php');
if (isset($_GET['idcat'])){
    $_SESSION['idcat'] = $_GET['idcat'];
}
$idcat = $_SESSION['idcat'];

$connect_pdo = $_SESSION['connect_pdo'];
$dbuser = $_SESSION['dbuser'];
$dbpasswd = $_SESSION['dbpasswd'];
$library_code = $_SESSION['library_code'];
$reserve_period = $_SESSION['settings']['reserve_period'];
$mem_hide_duedate = $_SESSION['settings']['mem_hide_duedate'];

$status_txt = Null;

if (!isset($_SESSION['idcat'])) {
    $_SESSION['idcat'] = $_GET['idcat'];
    $idcat = $_SESSION['idcat'];
}
if ($_SESSION['idcat'] == '') {
    $_SESSION['idcat'] = $_GET['idcat'];
}
$_SESSION['idcat'] = strtoupper($_SESSION['idcat']);

$pdo = new PDO($connect_pdo, $dbuser, $dbpasswd);

$sql = "select toys.*, l.description as loc_description, t.due as due   
                from toys
                left join (select idcat, return, due from transaction where return is null) t on t.idcat = toys.idcat
                left join location l on l.location = toys.location
                where upper(toys.idcat) = ?;";

$sth = $pdo->prepare($sql);
$array = array($idcat);
$sth->execute($array);

$result = $sth->fetchAll();
$stherr = $sth->errorInfo();
$numrows = $sth->rowCount();

for ($ri = 0; $ri < $numrows; $ri++) {
    $row = $result[$ri];
    $toy_id = $row['id'];
    $desc1 = $row['desc1'];
    $desc2 = $row['desc2'];
    $loan_type = trim($row['loan_type']);
    $category = $row['category'];
    $reservecode = $row['reservecode'];
    $toyname = $row['toyname'];
    $location = $row['loc_description'];
    $rent = $row['rent'];
    $age = $row['age'];
    $datestocktake = $row['datestocktake'];
    $manufacturer = $row['manufacturer'];
    $discountcost = $row['discountcost'];
    $status = $row['status'];
    $cost = $row['cost'];
    $supplier = $row['supplier'];
    $comments = $row['comments'];
    $returndateperiod = $row['returndateperiod'];
    $returndate = $row['returndate'];
    $no_pieces = $row['no_pieces'];
    $date_purchase = $row['date_purchase'];
    $sponsorname = $row['sponsorname'];
    $warnings = $row['warnings'];
    $user1 = $row['user1'];
    //$date_entered = date_format($row['date_entered'], 'd/m/y');
    $alert = $row['alert'];
    $toy_status = $row['toy_status'];
    $stype = $row['stype'];
    $units = $row['units'];
    $borrower = $row['borrower'];
    $lockdate = $row['datestocktake'];
    $alerts = $row['alert'];
    $lockreason = $row['lockreason'];
    $created = $row['created'];
    $modified = $row['modified'];
    $condition = $row['reservedfor'];
    $storage = $row['storage'];
    $trans_due = $row['due'];
    $file_pic = '../..' . $toy_images . '/' . $library_code . '/' . strtolower($idcat) . '.jpg';
    $format_due = substr($row['due'], 8, 2) . '-' . substr($row['due'], 5, 2) . '-' . substr($row['due'], 0, 4);


    if (file_exists($file_pic)) {
        $img = '<img width="100px" src="../..' . $toy_images . '/' . $library_code . '/' . strtolower($idcat) . '.jpg" alt="toy image">';
    } else {
        //$img = '<img width="230px" src="../toy_images/demo/' . 'blank' . '.jpg" alt="toy image">';
        $img = '<img width="100px" src="../..' . $toy_images . '/' . $library_code . '/blank.jpg" alt="toy image">';
    }

    $format_purchase = substr($row['date_purchase'], 8, 2) . '-' . substr($row['date_purchase'], 5, 2) . '-' . substr($row['date_purchase'], 0, 4);
    $format_lock = substr($row['date_purchase'], 8, 2) . '-' . substr($row['date_purchase'], 5, 2) . '-' . substr($row['date_purchase'], 0, 4);
    if ($cost == Null) {
        $cost = 0;
    }
    if ($discountcost == Null) {
        $discountcost = 0;
    }
    if ($rent == Null) {
        $rent = 0;
    }
    if ($rent > 0) {
        $str_rent = '<strong><font="green">Hire Charge: </font>$</strong>' . $rent . ' per ' . $reserve_period . ' days.</font><br>';
        //$str_rent .= ' Hire Toys are for ' . $reserve_period . ' days </h2><br>';
    }
    if (($trans_due == null) || ($trans_due == '')) {
        if (($location == '') || ($location == '')) {
            $str_status = '<a style="background-color: lightgreen;"><font color="#330066">IN LIBRARY</font></a>';
        } else {
            $str_status = '<a style="background-color: lightgreen;"><font color="#330066">' . $location . '</font></a>';
        }
    } else {

        $str_status = '<a style="background-color: yellow;font-size: 20px;"><font color="#330066"></font>';
        if ($mem_hide_duedate == 'Yes') {
            $str_status = '</a>';
        } else {
            $str_status .= '<font color="#330066"> Due: ' . $format_due . '</font></a> ';
        }
    }




    $_SESSION['idcat'] = $idcat;
}

if ($stherr[0] != '00000') {
    $_SESSION['login_status'] .= "An  error occurred.\n";
    $_SESSION['login_status'] .= 'Error' . $stherr[0] . '<br>';
    $_SESSION['login_status'] .= 'Error' . $stherr[1] . '<br>';
    $_SESSION['login_status'] .= 'Error' . $stherr[2] . '<br>';
}

