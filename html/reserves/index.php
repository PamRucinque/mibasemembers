<!DOCTYPE html>
<!--[if lt IE 8 ]> <html class="no-js ie7 oldie" lang="en"> <![endif]-->
<!--[if IE 8]> <html class="no-js ie8 oldie" lang="en"> <![endif]-->
<!--[if IE 9]> <html class="no-js ie9" lang="en"> <![endif]-->
<!--[if (gt IE 9)|!(IE)]><!--> <html class="no-jQuery" lang="en"> <!--<![endif]-->
    <head>
        <?php
        include('../header/head.php');
        ?>
        <script>
            function setFocus()
            {
                var msg = document.getElementById("msg").innerText;
                //alert(msg);
                if (msg !== '') {
                    $('#myModal').modal('show');
                }

            }
        </script>


    </head>
    <body  onload="setFocus()">
        <?php include('../header/header.php'); ?>

        <section class="container fluid">
            <div class="row" style="padding-bottom: 20px;padding-top: 20px;">
                <div class="col-sm-3">
                    <a style="font-size: larger;" class="btn btn-success" href="../toys_hire/toys.php">Back to Toy Hire List</a>
                </div>
                <div class="col-sm-6"></div>
                <div class="col-sm-3">

                </div>
            </div>


            <?php
            $java_str = "$(location).attr('href', '../mylibrary/index.php')";
            $java_str_my_library = "$(location).attr('href', '../mylibrary/index.php')";
            $button_str = 'OK';
            include('data/get_toy.php');
            include('reservation.php');
            ?>
        </section>
        <section class="container fluid">
            <div id='msg' style='display: block;'><?php echo $str_alert; ?></div>
            <!-- Trigger the modal with a button -->
            <!-- Modal -->
            <div class="modal fade" id="myModal" role="dialog"  data-backdrop="static" data-keyboard="false">
                <div class="modal-dialog">

                    <!-- Modal content-->
                    <div class="modal-content">
                        <div class="modal-header">

                            <h4 class="modal-title">Mibase Alert</h4>
                        </div>
                        <div class="modal-body">
                            <p><?php echo $str_alert; ?></p>
                        </div>
                        <div class="modal-footer">
                             <button type="button" class="btn btn-danger" data-dismiss="modal" id="update_terms" name="update_terms" onclick="<?php echo $java_str; ?>"><?php echo $button_str; ?></button>
                        </div>
                    </div>

                </div>
            </div>

        </section>
    </body>
</html>
<?php
$str_alert = '';
$_SESSION['alert_reserve'] = '';

