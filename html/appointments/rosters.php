<?php

/*
 * Copyright (C) 2018 SqualaDesign ltd
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/*
 * This page should be included in the top of all pages.
 * so if a user is not authenticated they are redirected back to the login page
 */
$libraryname = $_SESSION['settings']['libraryname'];
$last = '';

//$query = "select * from holmes_files order by id;";
$query = "SELECT roster.*, borwrs.id as borid, borwrs.firstname as firstname, borwrs.surname as surname, borwrs.emailaddress , borwrs.phone2 as phone2
FROM Roster LEFT JOIN Borwrs ON Roster.member_id = Borwrs.ID 
WHERE 
  roster.date_roster  > (current_date - interval '1 days') 
 ORDER BY date_roster, roster_session, roster.id;";
$count = 1;
$conn = pg_connect($_SESSION['connection_str']);
$result = pg_exec($conn, $query);
$numrows = pg_numrows($result);
$total = 0;
for ($ri = 0; $ri < $numrows; $ri++) {
    $row = pg_fetch_array($result, $ri);
    $total = $total + 1;
    $weekday = date('l', strtotime($row['date_roster']));
    $month = date('M', strtotime($row['date_roster']));
    if ($row['session_role'] == 'Co-ordinator') {
        $str_role = $row['firstname'];
    } else {
        $str_role = $row['firstname'];
    }

    //$str_roster .= '<div class="row"><div class="col-md-12">';
    $format_date_roster = substr($row['date_roster'], 8, 2) . '-' . $month . '-' . substr($row['date_roster'], 0, 4);

    if ($last != ($format_date_roster . $row['weekday'] . $row['location'])) {
        $count = 1;
        If ($total != 1) {
             $str_roster .= '</ul>';
            $str_roster .= '</div></div>';
        }

        $str_roster .= '<div class="row"><div class="col-md-12">';
        $str_roster .= '<h3 class="text-uppercase"><strong><i class="fas fa-calendar-alt"></i> ' . $format_date_roster . '</strong></h3>';
        $str_roster .= '<ul class="list-inline">';
        $str_roster .= '<li class="list-inline-item"><i class="far fa-calendar-alt"></i> ' . $row['weekday'] . '</li>';
        $str_roster .= '<li class="list-inline-item"><i class="fas fa-clock"></i> ' . $row['roster_session'] . '</li>';
        //$str_roster .= '<li class="list-inline-item"><i class="fa fa-location-arrow"></i> ' . substr($row['location'], 0, 4) . '</li>';
        //$str_roster .= '</ul>';
    }
    //$str_roster .= '<ul>';
    if ($row['borid'] != 0) {
        $str_roster .= '<li>' . $count . ' ' . $str_role . '</li>';
    } else {
        $str_roster .= '<li>' . $count . '___________________' . $str_role . '</li>';
    }
   
    if ($last != ($format_date_roster . $row['weekday'] . $row['location'])) {
        //$str_roster .= '</div></div>';
    }
    $count = $count + 1;
    $last = $format_date_roster . $row['weekday'] . $row['location'];
}



$public_roster = $_SESSION['settings']['public_roster'];
if ($public_roster == 'Yes') {
    echo $str_roster;
} else {
    echo '<h4>Roster has been disabled</h4>';
}
?>