<?php
$branch = substr(getcwd(), 22, strpos(getcwd() . '/', '/', 22 + 1) - 22);
include(get_include_path() . '/mibaselive/html/' . $branch . '/mibase_check_login.php');
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8"></meta>
        <title>Mibase Roster - Admin Edit Roster</title>
        <link rel="stylesheet" type="text/css" href="view.css" media="all"></link>
        <link type="text/css" href="js/themes/base/jquery.ui.all.css" rel="stylesheet" />
        <script type="text/javascript" src="js/jquery-1.9.0.js"></script>
        <script type="text/javascript" src="js/ui/jquery.ui.core.js"></script>
        <script type="text/javascript" src="js/ui/jquery.ui.datepicker.js"></script>

        <script type="text/javascript">
            $(function () {
                var pickerOpts = {
                    dateFormat: "d MM yy",
                    showOtherMonths: true

                };
                $("#date_roster").datepicker(pickerOpts);
            });


        </script>
    </head>



    <body id="main_body" >

        <?php
        //include('../home/header.php');

        $membername = $_SESSION['membername'];
        include('../home/get_settings.php');
        ?>
        <div style="padding-left: 20px;">
            <?php
            if ($mem_roster != 'No') {

                include('rosters.php');
            } else {
                echo "<br><h2><font color='blue'>The Roster System has been temporarily disabled.</font></h2><br>";
            }
            ?>

        </div>
    </body>
</html>