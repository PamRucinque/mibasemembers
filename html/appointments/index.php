<!DOCTYPE html>
<!--[if lt IE 8 ]> <html class="no-js ie7 oldie" lang="en"> <![endif]-->
<!--[if IE 8]> <html class="no-js ie8 oldie" lang="en"> <![endif]-->
<!--[if IE 9]> <html class="no-js ie9" lang="en"> <![endif]-->
<!--[if (gt IE 9)|!(IE)]><!--> <html class="no-jQuery" lang="en"> <!--<![endif]-->
    <head>
        <?php
        include('../header/head.php');
        ?>
        <script>
            function setFocus()
            {
                var msg = document.getElementById("msg").innerText;
                //alert(msg);
                if (msg !== '') {
                    $('#myModal').modal('show');
                }

            }
            function cancel_msg() {
                $('#msg').hide();
            }
            function show_app(id) {
                if ($("#" + id).is(":visible")) {
                    $("#" + id).hide();
                    var c = document.getElementById("btn_" + id);
                    if (c.classList.contains('btn-danger')) {
                        document.getElementById("btn_" + id).innerHTML = 'No Appointments';
                    } else {
                        document.getElementById("btn_" + id).innerHTML = 'Show Appointments';
                    }

                } else {
                    document.getElementById("btn_" + id).innerHTML = 'Hide Appointments';
                    $("#" + id).show();
                }
            }


        </script>

    </head>

    <body onload="setFocus();">

        <?php include('../header/header.php'); ?>
        <div class="container-fluid">
            <?php
            if (!session_id()) {
                session_start();
            }
            $vacant = 0;

            $str_alert = '';
            if (isset($_SESSION['alert'])) {
                $str_alert = $_SESSION['alert'];
            }
            if (isset($_POST['submit_new_log'])) {
                include('update_app.php');
            }

            $mem_cancel_appointment = $_SESSION['settings']['mem_cancel_appointment'];
            $collect_box = $_SESSION['settings']['collect_box'];
            // $collect_box = 'Yes';
            include('get_member.php');
            //include('../header/get_settings.php');
            echo '<title>' . $libraryname . '</title>';
            echo '<h3>' . $_SESSION['borid'] . ': ' . $_SESSION['firstname'] . ' ' . $_SESSION['surname'] . '</h3>';
            if ($subdomain == 'maroondah') {
                //echo '<h4><font color="red">' . $_SESSION['settings']['libraryname'] . ' offer appointments one week at a time. <br>Then for each appointment add the person's first name in comments. Please thoroughly clean all toys, boxes and bags for return before visiting a session.</font></h4>';
                $str_maroondah = '<h4><font color="red">Members must book an appointment to attend a session.</font><br>';
                $str_maroondah .= '<font color="red">Please thoroughly clean all toys, boxes and bags for return before your visit.</font><br> ';
                $str_maroondah .= '<font color="red"> See <a href="https://www.toylibraries.org.au/cleaning-toys" target="_blank" >www.toylibraries.org.au/cleaning-toys</a>.</font></h4>';
                $str_maroondah .= '<h4><font color=blue">Toy drop off only, no toy collection<br> 10am - 12pm Thursday and 10am - 12pm Saturday.</font><h4>';
                echo $str_maroondah;
            }
            if ($subdomain == 'bayside') {
                $str_bayside = '<h4><font color="red">Holds on Toys MUST be made <u>48 hours</u> before your scheduled appointment. </font><br>';
                echo $str_bayside;
            }
            if ($mem_cancel_appointment > 0) {
                echo '<br>Note: Bookings Cannot be Cancelled <font color="red">' . $mem_cancel_appointment . '</font> day(s) before scheduled date.';
            } else {
                echo '<br>Note: Bookings can be cancelled at any time';
            }
            ?>
            <div class="row">
                <div class="col-sm-1"></div>
                <div class="col-sm-10">
                    <?php
                    include('edit_app.php');
                    include('rosters/list.php');
                    ?>
                </div>
                <div class="col-sm-3"></div>
            </div>
            <div class="row">

                <div class="col-sm-12">

                    <?php
// if ($numrows === 0) {

                    echo '<h2><font color="darkorange">Booking Calendar</font></h2>';
                    include('rosters_new.php');
// }
                    ?>
                </div>

            </div>

        </div>

        <script type="text/javascript" src="../js/menu.js"></script>
        <?php
        include('forms/msg_form.php');
        $_SESSION['alert'] = '';
        ?>
        <div style="height: 100px;"></div>
    </body>
</html>


