<?php
include('../mibase_check_login.php');
$term_dates = $_SESSION['settings']['term_dates'];
$membertype = $_SESSION['membertype'];
$next_term_alert = $_SESSION['settings']['next_term_alert'];


$str_month = date('m');
$str_year = date('Y');
$str_next_year = date('Y', strtotime('+1 year'));
$str_duties_completed = '';
$duties_complete = 0;
$remind = 'No';
//$term_dates = '04-04;06-27;09-19';
//echo $term_dates;
$end_term1 = $str_year . '-' . substr($term_dates, 0, 5);

$end_term1_next = $str_next_year . '-' . substr($term_dates, 0, 5);
$end_term2 = $str_year . '-' . substr($term_dates, 6, 5);
$end_term3 = $str_year . '-' . substr($term_dates, 12, 5);
$start_term1 = $str_year . '-' . '01-01';
$start_term1_next = $str_next_year . '-' . '01-01';
$end_term4 = $str_year . '-' . '12-31';

//echo $end_term1;

$todays_date = date("Y-m-d");
//$todays_date = '2014-10-22';

$today_ts = strtotime($todays_date); //unix timestamp value for today

if ($today_ts >= strtotime($start_term1) && $today_ts <= strtotime($end_term1)) { //condition to check which is greater
    $str_current = 'Member must sign up for Term 1 roster duty.';
    $str_next = 'Member must sign up for Term 2 roster duty.';
    $query_current = "SELECT 
        coalesce(count(duration),0) as duties_complete 
        FROM roster 
        WHERE 
        member_id = " . $_SESSION['borid'] .
            " AND (date_roster >= (date '" . $start_term1 . "'))"
            . " AND (date_roster <= (date '" . $end_term1 . "'));";
    $query_next = "SELECT 
        coalesce(count(duration),0) as duties_complete 
        FROM roster 
        WHERE 
        member_id = " . $_SESSION['borid'] .
            " AND (date_roster > (date '" . $end_term1 . "'))"
            . " AND (date_roster <= (date '" . $end_term2 . "'));";
}
if ($today_ts > strtotime($end_term1) && $today_ts <= strtotime($end_term2)) { //condition to check which is greater
    $str_current = 'Member must sign up for Term 2 roster duty.';
    $str_next = 'Member must sign up for Term 3 roster duty.';
    $query_current = "SELECT 
        coalesce(count(duration),0) as duties_complete 
        FROM roster 
        WHERE 
        member_id = " . $_SESSION['borid'] .
            " AND (date_roster > (date '" . $end_term1 . "'))"
            . " AND (date_roster <= (date '" . $end_term2 . "'));";
    $query_next = "SELECT 
        coalesce(count(duration),0) as duties_complete 
        FROM roster 
        WHERE 
        member_id = " . $_SESSION['borid'] .
            " AND (date_roster > (date '" . $end_term2 . "'))"
            . " AND (date_roster <= (date '" . $end_term3 . "'));";
}
if ($today_ts > strtotime($end_term2) && $today_ts <= strtotime($end_term3)) { //condition to check which is greater
    $str_current = 'Member must sign up for Term 3 roster duty.';
    $str_next = 'Member must sign up for Term 4 roster duty.';
    $query_current = "SELECT 
        coalesce(count(duration),0) as duties_complete 
        FROM roster 
        WHERE 
        member_id = " . $_SESSION['borid'] .
            " AND (date_roster > (date '" . $end_term2 . "'))"
            . " AND (date_roster <= (date '" . $end_term3 . "'));";
    $query_next = "SELECT 
        coalesce(count(duration),0) as duties_complete 
        FROM roster 
        WHERE 
        member_id = " . $_SESSION['borid'] .
            " AND (date_roster > (date '" . $end_term3 . "'))"
            . " AND (date_roster <= (date '" . $end_term4 . "'));";
}
if ($today_ts > strtotime($end_term3) && $today_ts <= strtotime($end_term4)) { //condition to check which is greater
    $str_current = 'Member must sign up for Term 4 roster duty.';
    $str_next = 'Member must sign up for Term 1 roster duty next year.';
    $query_current = "SELECT 
        coalesce(count(duration),0) as duties_complete 
        FROM roster 
        WHERE 
        member_id = " . $_SESSION['borid'] .
            " AND (date_roster > (date '" . $end_term3 . "'))"
            . " AND (date_roster <= (date '" . $end_term4 . "'));";
    $query_next = "SELECT 
        coalesce(count(duration),0) as duties_complete 
        FROM roster 
        WHERE 
        member_id = " . $_SESSION['borid'] .
            " AND (date_roster >= (date '" . $start_term1_next . "'))"
            . " AND (date_roster <= (date '" . $end_term1_next . "'));";
}

$connect_str = $_SESSION['connect_str'];
$conn = pg_connect($connect_str);
//$conn = pg_connect($conn);
$str_duties_completed = '';


$query_req = "SELECT 
  duties 
FROM 
  membertype
  WHERE membertype = '" . $membertype . "';";


$result = pg_exec($conn, $query_req);
$numrows = pg_num_rows($result);

for ($ri = 0; $ri < $numrows; $ri++) {
//echo "<tr>\n";
    $row = pg_fetch_array($result, $ri);
    $duties_req = $row['duties'];
}

//echo 'Duties Required: ' . $query_req . '<br>';
//echo $query_current;

if ($duties_req > 0) {


    $result = pg_exec($conn, $query_current);

    for ($ri = 0; $ri < $numrows; $ri++) {
//echo "<tr>\n";
        $row = pg_fetch_array($result, $ri);
        $duties_this_term = $row['duties_complete'];
    }

//echo $query_next;
    $result = pg_exec($conn, $query_next);

    for ($ri = 0; $ri < $numrows; $ri++) {
//echo "<tr>\n";
        $row = pg_fetch_array($result, $ri);
        $duties_next_term = $row['duties_complete'];
    }



    if ($duties_this_term == 0) {
        $str_duties_completed .= ' ' . $str_current . ' ';
    }
    if ($next_term_alert == 'Yes') {
        if ($duties_next_term == 0) {
            if ($duties_this_term == 0) {
                $str_duties_completed .= '\n ';
            }
            $str_duties_completed .= $str_next;
        }
    }
}
//echo 'Duties Completed: ' . $str_duties_completed;
//echo 'Duties Required: ' . $str_duties_completed;




