<?php

include('../mibase_check_login.php');
if (!session_id()) {
    session_start();
}
$connect_str = $_SESSION['connect_str'];
$conn = pg_connect($connect_str);
$auto_approve_roster = $_SESSION['settings']['auto_approve_roster'];

// confirm that the 'id' variable has been set
if (isset($_GET['id']) && is_numeric($_GET['id']) && ($_SESSION['mibase'] != '')) {
    $member_id = $_SESSION['borid'];
    $id = $_GET['id'];
    // get the 'id' variable from the URL
    if (isset($_GET['mem_id']) && is_numeric($_GET['mem_id'])) {

        if ($_GET['mem_id'] == $member_id) {
            $member_id = 0;
            $add_comments = " MEMBER #" . $_GET['mem_id'] . " DELETED";
            $query = "UPDATE roster SET member_id =" . $member_id . ", approved = TRUE, delete_comments = delete_comments || ' MEMBER #" . $_GET['mem_id'] . " DELETED', comments = '', m1 = '', m2 = ''  WHERE id = " . $_GET['id'] . ";";
            $sql = "UPDATE roster SET member_id = ?, approved = TRUE, delete_comments = delete_comments || ? WHERE id = ?;";
            $delete = 'Yes';
        } else {
            if ($auto_approve_roster == 'Yes') {
                $query = "UPDATE roster SET member_id =" . $member_id . ", approved = TRUE WHERE id = " . $_GET['id'] . ";";
            } else {
                $query = "UPDATE roster SET member_id =" . $member_id . ", approved = FALSE WHERE id = " . $_GET['id'] . ";";
            }
            //$_SESSION['alert'] = 'Thank you for your booking. <br>Members must ensure all toys, bags and boxes are thoroughly cleaned before return.';
            $_SESSION['alert'] = 'Thank you for your booking.<br><br>';
            $_SESSION['alert'] .= 'Please ensure all family members are well before returning toys.<br>';
            $_SESSION['alert'] .= 'Please ensure all toys, boxes and bags (including fabric toy bags and puzzle sleeves) are thoroughly cleaned before return.<br>';
            $_SESSION['alert'] .= ' See <a href="https://www.toylibraries.org.au/cleaning-toys" target="_blank" >www.toylibraries.org.au/cleaning-toys</a>.';
        }
    }

    //$query = "UPDATE roster SET member_id =" . $member_id . ", approved = FALSE WHERE id = " . $_GET['id'] . ";";
    $result = pg_exec($conn, $query);

    if (!$result) {
        $message = 'Invalid query: ' . "\n";
        $message = 'Whole query: ' . $query;
    } else {

        //include('send_email_roster.php');
    }
    //$_SESSION['alert'] = 'Members must ensure all toys, bags and boxes are thoroughly cleaned before return.';
    //$_SESSION['alert'] .= ' See <a href="https://www.toylibraries.org.au/cleaning-toys" target="_blank" >www.toylibraries.org.au/cleaning-toys</a>.';

    $redirect = 'Location: index.php';               //print $redirect;
    header($redirect);
} else {
    
}
?>
