<?php

include('../classes/toy.php');

$subs = Toy::get_all_sub_cats();
$subs_select_str = draw_select($subs, '', '', 'sub_category');


//print_r($loan_types);

function draw_select($t, $value, $description, $cname) {
    $count = 0;
    $str = '';
    if ($value == null) {
        $value = '';
    }
    $numrows = sizeof($t);
    $str .= '<b>Select ' . $cname . ':</b><br>';
    $str .= '<select id="' . $cname . '" name="' . $cname . '"  class="form-control">';
    $str .= '<option value="' . $value . '" selected="selected">' . $description . '</option>';

    for ($ri = 0; $ri < $numrows; $ri++) {
        $str .= '<option value="' . $t[$ri] . '" >' . $t[$ri] . '</option>';
    }
    $str .= '</select><br>';

    return $str;
}

function draw_select_desc($t, $value, $description, $cname) {
    $count = 0;
    $str = '';
    if ($value == null) {
        $value = '';
    }
    $numrows = sizeof($t);
    $str .= '<b>' . $cname . ':</b><br>';
    $str .= '<select id="' . $cname . '" name="' . $cname . '"  class="form-control">';
    $str .= '<option value="' . $value . '" selected="selected">' . $description . '</option>';

    for ($ri = 0; $ri < $numrows; $ri++) {
        $str .= '<option value="' . $t[$ri][1] . '" >' . $t[$ri][1] . ': ' . $t[$ri][2] . '</option>';
    }
    $str .= '</select><br>';

    return $str;
}
