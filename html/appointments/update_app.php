<?php

$branch = substr(getcwd(), 22, strpos(getcwd() . '/', '/', 22 + 1) - 22);
include(dirname(__FILE__) . '/../mibase_check_login.php');
$error = '';
if (!session_id()) {
    session_start();
}
$connect_pdo = $_SESSION['connect_pdo'];
$dbuser = $_SESSION['dbuser'];
$dbpasswd = $_SESSION['dbpasswd'];

try {
    $pdo = new PDO($connect_pdo, $dbuser, $dbpasswd);
} catch (PDOException $e) {
    print "Error! saving record : " . $e->getMessage() . "<br/>";
    die();
}
$query = "update roster set comments = ?, m1=?, m2=? where id = ?;";

//echo '<br>' . $query;

$sth = $pdo->prepare($query);
$array = array($_POST['notes1'], $_POST['m1'], $_POST['m2'],$_POST['id']);

$sth->execute($array);

$stherr = $sth->errorInfo();
if ($stherr[0] != '00000') {
    $error = "An  error occurred checking the login: " . $stherr[0] . " " . $stherr[1] . "" . $stherr[2];
} else {
    echo $error;
    $hold_str_save = "<font color='red'>Your notes have been successfully saved.</font>";
}
echo $error;

