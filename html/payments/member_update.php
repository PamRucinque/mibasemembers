<?php
require(dirname(__FILE__) . '/../mibase_check_login.php');
$subdomain = $_SESSION['library_code'];
$memberid = $_SESSION['borid'];
$str_summary = '';

$cancel_page = 'https://' . $subdomain . '.mibase.com.au/members/payments/cancel.php';
$success_page = 'https://' . $subdomain . '.mibase.com.au/members/payments/success.php';
$member = get_details($_SESSION['borid']);

//$fee = money_format('%i', $member['fee'] + 1.5);
$fee = number_format($member['fee'] + 1.5,2);

$button = '<form action="renew.php">
  <input id="button_red_login" name="button_red_login" type="submit" value="Email My Details" class="btn btn-danger btn-lg">
</form> ';
$str_summary .= '<font color="#4C536C">Your Membership Details need checking, please  ';
$str_summary .= 'press below button to email your details for checking.</font><br><br><br>';

    $str_summary .= $button;



echo $str_summary;
//echo $member['error'];
?>


</div>
</body>

<?php

function get_details($id) {
    $error_msg = '';
    include('../connect.php');
    $pdo = new PDO($connect_pdo, $dbuser, $dbpasswd);

    $sql = "SELECT borwrs.id as id, 
        emailaddress, 
        firstname, 
        surname, 
        expired,
        member_update,
        membertype.membertype as membertype,
        membertype.renewal_fee as fee,
        membertype.paypal as paypal
        from borwrs 
        LEFT JOIN membertype on (membertype.membertype = borwrs.membertype)
        where borwrs.id = ?";

    $sth = $pdo->prepare($sql);
    $array = array($id);
    $sth->execute($array);

    $result = $sth->fetchAll();
    $stherr = $sth->errorInfo();
    $numrows = $sth->rowCount();
    //$email = 'not working';
    for ($ri = 0; $ri < $numrows; $ri++) {
        $row = $result[$ri];
        $memberid = $row['id'];
        $email = $row['emailaddress'];
        $surname = $row['surname'];
        $firstname = $row['firstname'];
        $fee = $row['fee'];
        $member_update = $row['member_update'];
        $membertype = $row['membertype'];
        $exp = strtotime($row['expired']);
        ;
        $expired = substr($row['expired'], 8, 2) . '-' . substr($row['expired'], 5, 2) . '-' . substr($row['expired'], 0, 4);
        $paypal = $row['paypal'];
        $now = time(); // or your date as well

        $diff = ($exp - $now) / (60 * 60 * 24);


        if ($diff > 0) {
            $warning = '<h3><font color="darkgreen">Due: ' . $expired . '</font></h3>';
        }
        if ($diff >= 0 && $diff <= 30) {
            $warning = '<h3><font color="orange">Due for Renewal on: ' . $expired . '</font></h3>';
        }
        if ($diff < 0) {
            $warning = '<h3><font color="red">Expired on: ' . $expired . '</font></h3>';
        }
    }


    if ($stherr[0] != '00000') {
        $error_msg .= "An  error occurred.\n";
        $error_msg .= 'Error' . $stherr[0] . '<br>';
        $error_msg .= 'Error' . $stherr[1] . '<br>';
        $error_msg .= 'Error' . $stherr[2] . '<br>';
    }

    return array('email' => $email, 'id' => $memberid, 'surname' => $surname, 'fee' => $fee, 'paypal' => $paypal, 'warning' => $warning,
        'membertype' => $membertype, 'expired' => $expired, 'error' => $error_msg, 'firstname' => $firstname, 'member_update' => $member_update);
}
?>



