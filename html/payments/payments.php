<?php
$branch = substr(getcwd(), 22, strpos(getcwd() . '/', '/', 22 + 1) - 22);
include(get_include_path() . '/mibaselive/html/' . $branch . '/mibase_check_login.php');

$cancel_page = 'https://' . $subdomain . '.mibase.com.au/members/payments/cancel.php';
$success_page = 'https://' . $subdomain . '.mibase.com.au/members/payments/success.php';
$member = get_details($_SESSION['memberid']);

$fee = money_format('%i', $member['fee'] + 1.5);
$str_summary = '<br><h2><font color="blue">Membership Renewal Payments:</font></h2>';
$str_summary .= '<p>Paypal add $1.50 surcharge to each transaction. We have added this fee onto each membership, to reduce our costs.</p><br>';
$str_summary .= '<strong>Membership Fee + Paypal surcharge: </strong> $' . $fee . '<br>';

$str_summary .= $member['warning'];
$str_summary .= '<strong>Membership Type:</strong> ' . $member['membertype'] . '<br><br>';
//$str_summary .= '<strong>Paypal ID: </strong>' . $hosted_button_id . '<br>';
echo $str_summary;
echo $member['error'];
if ($member['paypal'] != '') {
    include('paypal.php');
}
?>


</div>
</body>

<?php

function get_details($id) {
    include('../connect.php');
    $pdo = new PDO($connect_pdo, $dbuser, $dbpasswd);

    $sql = "SELECT borwrs.id as id, 
        emailaddress, 
        firstname, 
        surname, 
        expired,
        membertype.membertype as membertype,
        membertype.renewal_fee as fee,
        membertype.paypal as paypal
        from borwrs 
        LEFT JOIN membertype on (membertype.membertype = borwrs.membertype)
        where borwrs.id = ?";

    $sth = $pdo->prepare($sql);
    $array = array($id);
    $sth->execute($array);

    $result = $sth->fetchAll();
    $stherr = $sth->errorInfo();
    $numrows = $sth->rowCount();
    //$email = 'not working';
    for ($ri = 0; $ri < $numrows; $ri++) {
        $row = $result[$ri];
        $memberid = $row['id'];
        $email = $row['emailaddress'];
        $surname = $row['surname'];
        $firstname = $row['firstname'];
        $fee = $row['fee'];
        $membertype = $row['membertype'];
        $exp = strtotime($row['expired']);
        ;
        $expired = substr($row['expired'], 8, 2) . '-' . substr($row['expired'], 5, 2) . '-' . substr($row['expired'], 0, 4);
        $paypal = $row['paypal'];
        $now = time(); // or your date as well

        $diff = ($exp - $now) / (60 * 60 * 24);


        if ($diff > 0) {
            $warning = '<h3><font color="darkgreen">Due: ' . $expired . '</font></h3>';
        }
        if ($diff >= 0 && $diff <= 30) {
            $warning = '<h3><font color="orange">Due for Renewal on: ' . $expired . '</font></h3>';
        }
        if ($diff < 0) {
            $warning = '<h3><font color="red">Expired on: ' . $expired . '</font></h3>';
        }
    }


    if ($stherr[0] != '00000') {
        $error_msg .= "An  error occurred.\n";
        $error_msg .= 'Error' . $stherr[0] . '<br>';
        $error_msg .= 'Error' . $stherr[1] . '<br>';
        $error_msg .= 'Error' . $stherr[2] . '<br>';
    }

    return array('email' => $email, 'id' => $memberid, 'surname' => $surname, 'fee' => $fee, 'paypal' => $paypal, 'warning' => $warning,
        'membertype' => $membertype, 'expired' => $expired, 'error' => $error_msg, 'firstname' => $firstname);
}
?>



