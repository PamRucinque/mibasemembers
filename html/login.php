<meta name="viewport" content="width=device-width">
<link href="css/bootstrap.min.css" rel="stylesheet" id="bootstrap-css">
<script src="js/bootstrap.min.js"></script>
<link rel="stylesheet" type="text/css" href="css/login.css">

<?php
/*
 * Copyright (C) 2018 Michelle Baird
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

//include the configuration so the system knows where the database is etc
//echo 'hello';
include( dirname(__FILE__) . '/config.php');
//echo $_SESSION['library_code'];

if (!session_id()) {
    session_start();
}
$member = array();
$member['status'] = '';


//get the post variables if the user has clicked the login putton
if (isset($_POST['username'])) {
    $username = $_POST['username'];
//limit length
    if ((strlen($username)) > 25) {
        echo 'Login error: Username exceeds minimum length of 25 charaters';
        exit;
    }
//limit allowed charaters   
    if (preg_match('/^[\w@]*$/', $username) == 0) {
        echo 'Login error: Username can only contain word charaters and numbers and -_ charaters';
        exit;
    }
}

if (isset($_POST['password'])) {
    $password = $_POST['password'];
//limit length
    if ((strlen($password)) > 31) {
        echo 'Login Error: Password exceeds minimum length of 30 charaters';
        exit;
    }
//limit allowed charaters   
    if (preg_match('/^[\w@]*$/', $password) == 0) {
        echo 'Login error: Password can only contain word charaters and numbers and -_ charaters';
        exit;
    }
}


$host = $_SERVER["SERVER_NAME"];
$library_code = get_host_name($host, $shared_domain);
include('redirect.php');
//validate the input
//limit length
if ((strlen($host)) > 35) {
    echo 'Login Error: Password exceeds minimum length of 30 charaters';
    exit;
}
//limit allowed charaters  
if (preg_match('/^[\w\.\-]*$/', $host) == 0) {
    echo 'Host Name Error: Host name can only contain word charaters and numbers _ - and . charaters';
    exit;
}


$error = '';


if (isset($username)) {

//the user has attempted to log in
    if ($shared_server) {
        //echo 'hello';
//get the library code from the host name requested by the client taking of the $shared_domain of this server
//eg demo.mibase.com.au  $shared_domain mibase.com.au would give demo as the library code.
        $library_code = get_host_name($host, $shared_domain);

        //echo $library_code;
        $library_postgres_password = getLibraryPostgresPassword($library_code, $mibase_server);
        $dbpasswd = $library_postgres_password;
        $dbuser = $library_code;
        $dbname = $library_code;
        //echo $library_postgres_password;
//validate this user in this library database
        $member = mibase_validate_login_shared($library_code, $library_postgres_password, $username, $password);
        //echo $member['firstname'];

        if ($member['status'] == 'ok') {
//the login is ok set the session variable


            $_SESSION['password'] = $password;
            $_SESSION['username'] = $username;
            $_SESSION['borid'] = $member['borid'];
            $_SESSION['firstname'] = $member['firstname'];
            $_SESSION['surname'] = $member['surname'];
            $_SESSION['membername'] = $member['firstname'] . ' ' . $member['surname'];
            $_SESSION['membertype'] = $member['membertype'];
            $_SESSION['mem_alert'] = $member['alert'];
            $_SESSION['app'] = 'members';
            $connect_str = "host='" . $dbhost . "' port=" . $dbport . " dbname='" . $dbname . "' user='" . $dbuser . "' password='" . $dbpasswd . "'";
            //echo $connect_str;
            $_SESSION['connect_str'] = $connect_str;

            $connect_pdo = "pgsql:host=" . $dbhost . ";port=" . $dbport . ";dbname=" . $dbname;
            $_SESSION['connect_pdo'] = $connect_pdo;
            $_SESSION['dbuser'] = $dbuser;
            $_SESSION['dbpasswd'] = $dbpasswd;
            $_SESSION['connection_str'] = $connect_str;
            $_SESSION['library_code'] = $dbname;
            $_SESSION['subdomain'] = $dbname;
            $_SESSION['toy_images'] = $toy_images;
            $_SESSION['location'] = '';

            //set the host name for this session
            $_SESSION['host'] = $library_code . '.' . $shared_domain;

            $_SESSION['settings'] = fillSettingSessionVariables($connect_str);
            //fill other session variables that are need by the application
            fillOtherSessionVariables();

            //and forward the user to the admin home pages.
            $headerStr = 'location:' . $app_root . '/home/index.php';
            //echo $headerStr;
            header($headerStr);
            exit();
        }
    } else {

        //its a single server
        $member = mibase_validate_login_single($username, $password);


        if ($member['status'] == 'ok') {
            //the login is ok set the session variable
            //echo 'hello';
            $_SESSION['username'] = $username;
            $_SESSION['password'] = $password;
            $_SESSION['mibase'] = 'member';
            $_SESSION['borid'] = $member['borid'];
            $_SESSION['firstname'] = $member['firstname'];
            $_SESSION['surname'] = $member['surname'];
            $_SESSION['membername'] = $member['firstname'] . ' ' . $member['surname'];
            $_SESSION['membertype'] = $member['membertype'];
            $_SESSION['mem_alert'] = $member['alert'];
            $_SESSION['app'] = 'members';
            $connect_str = "host='" . $dbhost . "' port=" . $dbport . " dbname='" . $dbname . "' user='" . $dbuser . "' password='" . $dbpasswd . "'";
            //echo $connect_str;
            $_SESSION['connect_str'] = $connect_str;

            $connect_pdo = "pgsql:host=" . $dbhost . ";port=" . $dbport . ";dbname=" . $dbname;
            $_SESSION['connect_pdo'] = $connect_pdo;
            $_SESSION['dbuser'] = $dbuser;
            $_SESSION['dbpasswd'] = $dbpasswd;
            $_SESSION['connection_str'] = $connect_str;
            $_SESSION['library_code'] = $dbname;

            //save the library code in the session for later use
            //$_SESSION['library_code'] = $library_code;
            //set the host name for this session
            $_SESSION['host'] = $host;

            $_SESSION['settings'] = fillSettingSessionVariables($connect_str);

            //fill other session variables that are need by the application
            fillOtherSessionVariables();
            $_SESSION['toy_images'] = $toy_images;

            //echo $app_root;
            //and forward the user to the admin home pages.
            $headerStr = 'location:' . $app_root . '/home/index.php';
            header($headerStr);
            exit();
        }
    }
}

if ($shared_server) {

//get the server that the client requested
//note we will check it for hacker payload below
    $fqn = $_SERVER["SERVER_NAME"];

//do some checks on this variable
    if (check_server_fqn($fqn)) {
        if (ends_with($fqn, $shared_domain)) {
            $host = get_host_name($fqn, $shared_domain);
        } else {
            exit_script('requested host is not on this server: ' . $host . ':' . $shared_domain);
        }
    } else {
        exit_script('incorrect server name');
    }
} else {
//don't have to do anything
}
?>

<body style="background-image:url('css/images/bg.jpg');background-repeat: no-repeat;center: fixed;background: clear;
      -webkit-background-size: cover;
      -moz-background-size: cover;
      -o-background-size: cover;
      background-size: cover;" >


    <div class="container">
        <div class="row">
            <div class="card card-container">
                <?php
                //echo $toy_images;
                if ($shared_server) {
                    echo '<img src="' . $login_image_folder . '/' . $host . '.jpg' . '" alt="Library Logo" class="profile-img-card">';
                } else {
                    echo '<img src="logo.jpg" alt="Library Logo" class="profile-img-card">';
                }
                $home = '../' . $home_location . '/index.php';
                $email = 'email_password.php';
                ?>
                <h1>Mibase Member Login</h1>
                <p><a href="<?php echo $email; ?>">Forget your password?</a></p>
                <form name="form1" method="post" action="login.php" class="form-signin">
                    <span id="reauth-email" class="reauth-email"><?php echo $member['status']; ?></span>
                    <input name="username" type="text" id="username" class="form-control" placeholder="username">
                    <input name="password" type="password" id="password" class="form-control" placeholder="Password">

                    <div class="col-xs-12" style='padding-top: 10px;'>
                        <a href="<?php echo $home; ?>" class="btn btn-lg btn-info btn-block btn-signin">Home</a>
                    </div>
                    <div class="col-xs-12" style='padding-top: 10px;'>
                        <input type="submit" name="Submit" value="Login" class="btn btn-lg btn-primary btn-block btn-signin">  
                    </div>



                </form>
            </div><!-- /form -->   
        </div><!-- /card-container -->
    </div><!-- /row -->
</div><!-- /container -->
</body>




<?php

/**
 * checks that the $fqn provided by the client is OK
 * @param type $fqn
 * @return boolean
 */
function check_server_fqn($fqn) {  //@todo  we should also check that this fqn only contains letters numbers . - _
    if (strlen($fqn) > 100) {
        return false;
    } else {
        return true;
    }
}

/**
 * 
 * @param type $fqn
 * @param type $shared_domain
 * @return type
 */
function ends_with($fqn, $shared_domain) {
    return ( substr($fqn, strlen($fqn) - strlen($shared_domain)) == $shared_domain );
}

/**
 * gets the host name give the hosts fqn and the $shared_domain
 * eg $fqn = demo.mibase.com.au $shared_domain = mibase.com.au
 * @param type $fqn
 * @param type $shared_domain  
 * @return type string
 */
function get_host_name($fqn, $shared_domain) {
    return substr($fqn, 0, strlen($fqn) - strlen($shared_domain) - 1);
}

/**
 * simple finction to output an error page and exit
 * @param $message the message to display
 */
function exit_script($message) {
    ?>
    <!DOCTYPE html>
    <html>
        <head>
            <title>Mibase Error</title>
            <meta charset="windows-1250">
            <meta name="viewport" content="width=device-width, initial-scale=1.0">
            <link rel="stylesheet" type="text/css" href="css/view.css">
        </head>
        <body>
        <center>
            <div><h2><font color="blue">Mibase Error: <?php echo $message; ?></font></h2>
            </div>
        </center>
    </body>
    </html>
    <?php
    exit();
}

/**
 * 
 * @param type $username
 * @param type $password
 * @return string
 */
function mibase_validate_login_single($username, $password) {

    global $dbhost, $dbport, $dbname, $dbuser, $dbpasswd;
    $status = '';
    $borid = 0;
    $firstname = '';
    $surname = '';
    $membertype = '';
    $alert = '';


//now check the username and password in the users table
    $connect_pdo = "pgsql:dbname=" . $dbname . ";host=$dbhost;port=" . $dbport . ";";
    //echo $connect_pdo;

    try {
        $pdo = new PDO($connect_pdo, $dbuser, $dbpasswd);
    } catch (PDOException $e) {
        print "Error! member login : " . $e->getMessage() . "<br/>";
        die();
    }


    $query_login = "select rostertype5 as username, pwd, id, firstname, surname, membertype, alert   
                    FROM borwrs 
                    WHERE rostertype5 = ? 
                    AND pwd = ? 
                    AND (member_status = 'ACTIVE' or member_status = 'SUSPEND');";
    //echo $query_login;
    $sth = $pdo->prepare($query_login);
    $array = array($username, $password);
    $sth->execute($array);

    $result = $sth->fetchAll();
    $numrows = $sth->rowCount();

    $stherr = $sth->errorInfo();
    if ($stherr[0] != '00000') {
        $error = "An  error occurred checking the login: " . $stherr[0] . " " . $stherr[1] . "" . $stherr[2];
    }
// 0    SQLSTATE error code (a five characters alphanumeric identifier defined in the ANSI SQL standard).   
// 1    Driver-specific error code.
// 2    Driver-specific error message.
    //echo $numrows;

    if ($numrows > 0) {
        for ($ri = 0; $ri < $numrows; $ri++) {
            $row = $result[$ri];
            $borid = $row['id'];
            $surname = $row['surname'];
            $firstname = $row['firstname'];
            $membertype = $row['membertype'];
            $username = $row['username'];
            $password = $row['pwd'];
            $alert = $row['alert'];
        }
//the user is valid
        $status = 'ok';
    } else {
        $status .= "Invalid User and Password";
    }
    return array('status' => $status, 'borid' => $borid, 'firstname' => $firstname, 'surname' => $surname, 'membertype' => $membertype, 'alert' => $alert);
}

/**
 * 
 * @param string $library_code
 * @param string $username
 * @param string $password
 * @return string
 */
function mibase_validate_login_shared($library_code, $library_postgres_password, $username, $password) {
    global $dbport;
    $connect_pdo = "pgsql:dbname=" . $library_code . ";host=localhost;port=" . $dbport . ";";
    //echo $connect_pdo;
    $status = '';
    $borid = 0;
    $firstname = '';
    $surname = '';
    $membertype = '';
    $alert = '';
    global $dbhost, $dbport;
    $pattern = '/^[\w@]*$/';
    if (preg_match($pattern, $username) == 0) {
        return 'Login error: Username can only contain word charaters and numbers and -_ charaters';
    }
    if (preg_match($pattern, $password) == 0) {
        return 'Login error: Password can only contain word charaters and numbers and -_ charaters';
    }
    if ((strlen($username)) > 25) {
        return 'Login error: Username exceeds minimum length of 25 charaters';
    }
    if ((strlen($password)) > 31) {
        return 'Login Error: Password exceeds minimum length of 30 charaters';
    }




//the user is trying to log into a valid toy library
//now check the username and password in the users table of that toylibrary
    //now check the username and password in the users table
    //$connect_pdo = "pgsql:dbname=" . $library_code . ";host=localhost;port=" . $dbport . ";";
    //echo $connect_pdo;
    //echo $library_postgres_password;

    try {
        $pdo = new PDO($connect_pdo, $library_code, $library_postgres_password);
    } catch (PDOException $e) {

        print "Error! member login shared : " . $e->getMessage() . "<br/>";
        if ($library_code == 'taranaki') {
            $redirect = "Location: https://www.taranakitoylibrary.org.nz/";               //print $redirect;
            header($redirect);
        }

        die();
    }

    $query_login = "select rostertype5 as username, pwd, id, firstname, surname, membertype, alert   
                    FROM borwrs 
                    WHERE rostertype5 = ? 
                    AND pwd = ? 
                    AND member_status = 'ACTIVE';";
    $sth = $pdo->prepare($query_login);
    $array = array($username, $password);
    $sth->execute($array);

    $numrows = $sth->rowCount();
    $result = $sth->fetchAll();

    $stherr = $sth->errorInfo();
    if ($stherr[0] != '00000') {
        $status = "An  error occurred checking the login: " . $stherr[0] . " " . $stherr[1] . "" . $stherr[2];
    }
    //echo $username . ' ' . $password;

    if ($numrows > 0) {
        for ($ri = 0; $ri < $numrows; $ri++) {
            $row = $result[$ri];
            $borid = $row['id'];
            $surname = $row['surname'];
            $firstname = $row['firstname'];
            $membertype = $row['membertype'];
            $username = $row['username'];
            $password = $row['pwd'];
            $alert = $row['alert'];
        }
//the user is valid
        $status = 'ok';
    } else {
        $status .= "Invalid User and Password";
    }
    //echo $status;

    return array('status' => $status, 'borid' => $borid, 'firstname' => $firstname, 'surname' => $surname, 'membertype' => $membertype, 'alert' => $alert);
}

/**
 * gets the postgres password for a librarys database in a shared system
 * the database user is the library_code
 * the database name is the library_code
 * the library databases are on the same server, port as the toybase database
 * @param type $library_code
 * @return type
 */
function getLibraryPostgresPassword($library_code, $mibaseserver) {
    global $dbhost, $toybasedbname, $toybasedbuser, $toybasedbpasswd, $dbport;
//now get the database password for the library database
    $toybase_connect_pdo = "pgsql:host=" . $dbhost . ";port=" . $dbport . ";dbname=" . $toybasedbname;
    $toybase_pdo = new PDO($toybase_connect_pdo, $toybasedbuser, $toybasedbpasswd);
    if ($mibaseserver == 'Yes') {
        $toybase_query_login = "select postgres as dbpassword
                    FROM libraries
                    WHERE subdomain = ? ;";
    } else {
        $toybase_query_login = "select dbpassword
                    FROM libraries
                    WHERE library_code = ? ;";
    }


    $toybase_sth = $toybase_pdo->prepare($toybase_query_login);
    $toybase_array = array($library_code);
    $toybase_sth->execute($toybase_array);

    $toybase_result = $toybase_sth->fetchAll();
    $toybase_numrows = $toybase_sth->rowCount();
    $toybase_stherr = $toybase_sth->errorInfo();
    if ($toybase_stherr[0] != '00000') {
        return "An  error occurred checking the login: " . $toybase_stherr[0] . " " . $toybase_stherr[1] . "" . $toybase_stherr[2];
    }

    if ($toybase_numrows > 0) {
        $row = $toybase_result[0];
        return $row['dbpassword'];
    } else {
        return 'Login Error: This library not on this server: ' . $library_code;
    }
}

function getLoginType($username, $password, $connect_pdo, $dbuser, $dbpasswd) {

    $pdo = new PDO($connect_pdo, $dbuser, $dbpasswd);
    $query_login = "select rostertype5 as username, pwd, location 
                    FROM borwrs 
                    WHERE rostertype5 = ? 
                    AND password = ? AND member_status = 'ACTIVE';";
    $sth = $pdo->prepare($query_login);
    $array = array($username, $password);
    $sth->execute($array);

    $result = $sth->fetchAll();
    $numrows = $sth->rowCount();

    $stherr = $sth->errorInfo();
    if ($stherr[0] != '00000') {
        return "An  error occurred checking the login type: " . $stherr[0] . " " . $stherr[1] . "" . $stherr[2];
    }
// 0    SQLSTATE error code (a five characters alphanumeric identifier defined in the ANSI SQL standard).   
// 1    Driver-specific error code.
// 2    Driver-specific error message.

    if ($numrows > 0) {
        $row = $result[0];
        return array('level' => $row['level'], 'location' => $row['location']);
    } else {
        return array('level' => '', 'location' => '');
    }
}

/**
 * fills all the settings as session variables
 * @param string $connect_str
 */
function fillSettingSessionVariables($connect_str) {

    $array = array();

    //set default values
    $array['mem_payments_homepage'] = 'No';
    $array['overdue_txt'] = 'OVERDUE';
    $array['storage_label'] = 'No';
    $array['toy_holds'] = 'No';
    $array['hide_balance'] = 'No';
    $array['display_overdues'] = 'Yes';
    $array['hide_value'] = 'Yes';
    $array['show_desc'] = 'No';
    $array['display_overdues'] = 'Yes';
    $array['user_toys'] = 'User1';
    $array['format_toyid'] = 'No';
    $array['timezone'] = 'Australia/ACT';
    $array['roster'] = 'Yes';
    $array['nationality_label'] = '';
    $array['reservations'] = 'No';
    $array['rentfactor'] = 1;
    $array['extra_rent'] = 'No';
    $array['charge_rent'] = 'No';
    $array['hide_city'] = 'No';
    $array['global_rent'] = 0;
    $array['grace'] = 0;
    $array['rentasfine'] = 'No';
    $array['fine_factor'] = 1;
    $array['email_from'] = 'info@mibase.com.au';
    $array['loan_default_missing_paid'] = 'No';
    $array['auto_debit_missing'] = 'No';
    $array['loan_default_missing_alert'] = 'Yes';
    $array['admin_renewal_list'] = 'No';
    $array['toy_holds'] = 'Yes';
    $array['reservations'] = 'No';
    $array['simple_new_member'] = 'Yes';
    $array['vol_no_toy_edits'] = 'Yes';
    $array['suburb_title'] = '';
    $array['city_title'] = '';
    $arary['renew_button'] = 'Yes';
    $array['hide_city'] = 'Yes';
    $array['selectbox'] = 'No';
    $array['user1_borwrs'] = '';
    $array['nationality_label'] = '';
    $array['override_date_loan'] = 'No';
    $array['storage_label'] = 'Storage: ';
    $array['donated_label'] = 'Donated by:';
    $array['format_toyid'] = 'No';
    $array['idcat_noedit'] = 'Yes';
    $array['catsort'] = 'Yes';
    $array['automatic_debit_off'] = 'Yes';
    $array['renew_member_email'] = 'No';
    $array['logo_height'] = '150px';
    $array['library_heading_off'] = 'No';
    $array['mem_hide_duedate'] = 'No';
    $array['term_dates'] = '04-15;07-15;10-02;12-16;';
    $array['next_term_alert'] = 'Yes';
    $array['mem_cancel_roster'] = 14;
    $array['mem_cancel_appointment'] = 1;
    $array['collect_box'] = 'No';
    $array['rosters_annual'] = 'No';
    $array['mem_private'] = 'Yes';
    $array['multi_location'] = 'No';
    $array['mem_roster_lock'] = 'No';
    $array['mem_coord_off'] = 'No';
    $array['reserve_period'] = 8;
    $array['holiday_hire'] = 'No';
    $array['jc_reserve_period'] = 8;
    $array['hiretoys_label'] = 'Party Packs';
    $array['mem_reserve_notification'] = 'No';
    $array['menu_member_options'] = 'No';
    $array['menu_faq'] = 'No';
    $array['block_image'] = 'No';
    $array['reserve_period'] = 8;
    $array['reserve_fee'] = 0;
    $array['toy_hold_period'] = 7;
    $array['public_roster'] = 'Yes';
    $array['$mem_roster_show_all'] = 'No';
    $array['menu_color'] = 'darkblue';
    $array['menu_font_color'] = 'yellow';
    $array['online_hire'] = 'Yes';
    $array['online_rego'] = 'No';
    $array['libraryname'] = '';
    $array['online_renew'] = 'Yes';
    $logo_height = $_SESSION['settings']['logo_height'];
    $array['mem_edit'] = 'No';
    $array['auto_approve_roster'] = 'No';
    $array['mem_levy_alert'] = 'No';
    $array['mem_hire_conditions'] = 'No';
    $array['mem_roster_show_all'] = 'No';
    $array['mem_renew_overdue'] = 'No';
    $array['mem_alerts'] = 'No';
    $array['mem_show_alert'] = 'No';
    $array['overdue_renew'] = 'No';
    $array['roster_alerts'] = 'No';
    $array['mem_override_maxitems'] = 'No';
    $array['mem_toy_holds_off'] = 'Yes';
    $array['mem_menu_my_holds_off'] = 'No';
//mem_hold_on_loan
    $array['mem_hold_on_loan'] = 'No';


    $query_settings = "SELECT setting_name, setting_value FROM settings;";

    $dbconn = pg_connect($connect_str);

    $result_settings = pg_Exec($dbconn, $query_settings);
    $numrows = pg_numrows($result_settings);

    for ($ri = 0; $ri < $numrows; $ri++) {
        $row = pg_fetch_array($result_settings, $ri);
        $array[$row['setting_name']] = $row['setting_value'];
    }

    pg_FreeResult($result_settings);
    pg_Close($dbconn);
    return $array;
}

/**
 * fills  other session variables
 * @param string $connect_str
 */
function fillOtherSessionVariables() {
    $_SESSION['category'] = '';
    $_SESSION['toyiderror'] = '';
    $_SESSION['partid'] = '';
    $_SESSION['override'] = 'No';
    $_SESSION['logo'] = '';
    $_SESSION['toyid'] = '';
    $_SESSION['location'] = '';
    $_SESSION['age'] = '';
    $_SESSION['weekday'] = '';
    $_SESSION['roster_type'] = '';
    $_SESSION['template'] = '';
    $_SESSION['error'] = '';
    $_SESSION['search'] = '';
    $_SESSION['idcat'] = '';
    $_SESSION['idcat_select'] = '';
    $_SESSION['limit'] = '';
    $_SESSION['status'] = '';
    $_SESSION['idcat'] = '';
    $_SESSION['mibase'] = 'members';
    $_SESSION['alert'] = '';
    $_SESSION['reset'] = '';
    $_SESSION['renew'] = '';
    $_SESSION['alert_reserve'] = '';
}

function mibase_log_login($username, $login_type) {
    $ip = $_SERVER["REMOTE_ADDR"];


    $pdo = new PDO($connect_pdo_toybase, $dbuser_toybase, $dbpasswd_toybase);
//$pdo = new PDO($connect_pdo, $dbuser, $dbpasswd);
    $query_toybase = "INSERT INTO log (ip, subdomain, username, password, logintype, status, email
        ) VALUES (?,?,?,?,?,?,?)";


    $sth = $pdo->prepare($query_toybase);
    $array = array($ip, $library_code_url, $username, $password, $login_type, $myemail, $status);
//execute the preparedstament
    $sth->execute($array);
    $stherr = $sth->errorInfo();
    if ($stherr[0] != '00000') {
        $return = "An INSERT query error occurred.\n";
        $return .= 'Error ' . $stherr[0] . '<br>';
        $return .= 'Error ' . $stherr[1] . '<br>';
        $return .= 'Error ' . $stherr[2] . '<br>';
    } else {
        $return = '';
    }
    return $return;
}
