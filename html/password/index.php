<!DOCTYPE html>
<!--[if lt IE 8 ]> <html class="no-js ie7 oldie" lang="en"> <![endif]-->
<!--[if IE 8]> <html class="no-js ie8 oldie" lang="en"> <![endif]-->
<!--[if IE 9]> <html class="no-js ie9" lang="en"> <![endif]-->
<!--[if (gt IE 9)|!(IE)]><!--> <html class="no-jQuery" lang="en"> <!--<![endif]-->
    <head>
        <?php
        include('../header/head.php');
        ?>

    </head>

    <body>
        <?php include('../header/header.php'); ?>

        <section class="container">
            <div class="row">

                <div class="col-sm-12">
                    <?php
                    $memberid = $_SESSION['borid'];
                    $membername = $_SESSION['firstname'] . ' ' . $_SESSION['surname'];
                    echo '<h3>Reset Password: ' . $_SESSION['borid'] . ': ' . $_SESSION['firstname'] . ' ' . $_SESSION['surname'] . '</h3>';


                    $heading = '<h2>Reset Password<br> <font color="blue">' . $memberid . '</font> : ' . $membername . '</h2>';
                    include('reset.php');
                    echo '<title>' . $libraryname . '</title>';
                    ?>
                </div>
            </div>
        </section>

        <script type="text/javascript" src="../js/menu.js"></script>

    </body>
</html>


