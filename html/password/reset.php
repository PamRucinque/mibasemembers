<?php

include('../mibase_check_login.php');

$connect_str = $_SESSION['connect_str'];
$conn = pg_connect($connect_str);


$total = 0;
//session_start();

$_SESSION['reset'] = '';

// username and password sent from form
if (isset($_POST['mypassword'])) {
    $mypassword = $_POST['mypassword'];
    $mypasswordagain = $_POST['mypasswordagain'];
}

//$_SESSION['status'] = '';

$ok = 'Yes';
if (isset($_POST['generate'])) {
    $_SESSION['password'] = generateStrongPassword();
}

if (isset($_POST['submit'])) {
    if ($mypassword != $mypasswordagain) {
        $_SESSION['reset'] .= 'Passwords do not match, please re-enter password';
        $ok = 'No';
    }
    $check_password = check_password($mypassword);
    //$_SESSION['reset'] .= $check_password;
    if ($check_password == '') {
        $result = set_pwd($mypassword, $_SESSION['username']);
        if ($result == 'ok') {
            $ok = 'Yes';
        } else {
            $_SESSION['reset'] .= $result;
            $ok = 'No';
        }
    } else {
        $ok = 'No';
        $_SESSION['reset'] .= $check_password;
    }
    if ($ok == 'Yes') {
        $_SESSION['reset'] .= 'Password has been succesfully Changed to ' . $mypassword;
        $_SESSION['password'] = $mypassword;
    }
}
if ($_SESSION['reset'] != '') {
    echo '<script language="javascript">';
    echo 'alert("' . $_SESSION['reset'] . '")';
    echo '</script>';
}
//$_SESSION['reset'] = '';
include('reset_password.php');

function check_password($password) {

    $pattern = '/^\w+$/';
    $output = '';
    if (preg_match($pattern, $password) == 0) {
        //$error .= 'Login error: Password fails the regular expression test: ' . $password . ': ';
        $output .= 'Validation Error: Please use numbers and letters Uppercase and Lowercase.';
    }
    if ((strlen($password)) > 20) {
        $length = strlen($password);
        //$username = substr($username, 0, 30);
        $output .= 'Validation Error: Password exceeds maximum length of 20 characters. ';
    }
    if ((strlen($password)) < 10) {
        $length = strlen($password);
        //$username = substr($username, 0, 30);
        $output .= 'Validation Error: Password must have a string length of at least 10 characters';
    }
    if (preg_match('([a-zA-Z].*[0-9]|[0-9].*[a-zA-Z])', $password)) {
        
    } else {
        $output .= 'Validation Error: string should contain a lower case character as well as an upper case containing a number.';
    }

    return $output;
}

function set_pwd($password, $username) {
    $connect_pdo = $_SESSION['connect_pdo'];
    $dbuser = $_SESSION['dbuser'];
    $dbpasswd = $_SESSION['dbpasswd'];
    $output = 'ok';
    $pdo = new PDO($connect_pdo, $dbuser, $dbpasswd);
    $query = "UPDATE borwrs SET pwd = ? WHERE rostertype5=?;";
//create the array of data to pass into the prepared stament
    $sth = $pdo->prepare($query);
    $array = array($password, $username);
    $sth->execute($array);
    $stherr = $sth->errorInfo();


    if ($stherr[0] != '00000') {
        $_SESSION['password_status'] = "An Update query error occurred.\n";
        //echo $connect_pdo;
        $_SESSION['reset'] .= 'Error: ' . $stherr[0] . '<br>';
        $_SESSION['reset'] .= 'Error: ' . $stherr[1] . '<br>';
        $_SESSION['reset'] .= 'Error: ' . $stherr[2] . '<br>';
        $output = 'Update Error Occurred.';
        //exit;
    } else {
        $output = 'ok';
    }
    return $output;
}

function generateStrongPassword($length = 10, $add_dashes = false, $available_sets = 'luds') {
    $sets = array();
    if (strpos($available_sets, 'l') !== false)
        $sets[] = 'abcdefghjkmnpqrstuvwxyz';
    if (strpos($available_sets, 'u') !== false)
        $sets[] = 'ABCDEFGHJKMNPQRSTUVWXYZ';
    if (strpos($available_sets, 'd') !== false)
        $sets[] = '23456789';
    if (strpos($available_sets, 's') == false)
        $sets[] = '!@#$%&*?';
    $all = '';
    $password = '';
    foreach ($sets as $set) {
        $password .= $set[array_rand(str_split($set))];
        $all .= $set;
    }
    $all = str_split($all);
    for ($i = 0; $i < $length - count($sets); $i++)
        $password .= $all[array_rand($all)];
    $password = str_shuffle($password);
    if (!$add_dashes)
        return $password;
    $dash_len = floor(sqrt($length));
    $dash_str = '';
    while (strlen($password) > $dash_len) {
        $dash_str .= substr($password, 0, $dash_len) . '-';
        $password = substr($password, $dash_len);
    }
    $dash_str .= $password;
    return $dash_str;
}
