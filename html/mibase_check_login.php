<?php

include(dirname(__FILE__) . '/config.php');
$branch = 'members';

if (isset($_SESSION['timezone'])) {
    date_default_timezone_set($_SESSION['timezone']);
} else {
    date_default_timezone_set('Australia/ACT');
}
if (!session_id()) {
    session_start();
}
//$branch = substr(getcwd(), 22, strpos(getcwd() . '/', '/', 22 + 1) - 22);


if (isset($_SESSION['LAST_ACTIVITY']) && (time() - $_SESSION['LAST_ACTIVITY'] > 36000)) {
    // last request was more than 30 minutes ago
    session_unset();     // unset $_SESSION variable for the run-time 
    session_destroy();
    header('location:../login.php');
    exit();
    // destroy session data in storage
}

$_SESSION['LAST_ACTIVITY'] = time();


//echo 'Branch: ' . $branch;
//echo 'Session: ' . $_SESSION['mibase'];

