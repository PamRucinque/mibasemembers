<!DOCTYPE html>
<?php include('../mibase_check_login.php'); ?>
<html>
    <head>
        <title>Mibase Search Toys</title>
        <?php include('../header/head.php'); ?>
    </head>
    <script>
        $(document).ready(function () {
            $('#members').DataTable({
                "lengthChange": true,
                "pageLength": 50,
                "pagingType": "full_numbers",
                "lengthChange": false,
                "bFilter": true,
                language: {
                    searchPlaceholder: "Search Favourites",
                    searchClass: "form-control",
                    search: "",
                }
            });
            $('div.dataTables_filter input').addClass('form-control');
            $('div.dataTables_length select').addClass('recs');
        });
        function set_toyid(str) {
            document.getElementById('get_toy').value = str;
            document.getElementById('submit_toy').submit();
        }
        function delete_fav(str){
            //document.getElementById('fav_idcat').value = str;
            alert('hello');
        }
        function hello(str){
            document.getElementById('fav_idcat').value = str;
            document.getElementById('delete_fav').submit();
        }
            
    </script>


    <body>
        <?php include('../header/header.php'); ?>
        <div class="container-fluid">
            <div class="row">
                <div class="col-sm-12" style="background-color: white;padding-left: 20px;">

                    <?php
                    $heart_img = '<img src="heart.png" alt="Smiley face" height="42" width="42">';
                    echo '<h2>' . $heart_img . ' My Favourite Toys ' . $heart_img . '</h2>'; 
                    $multi_location = $_SESSION['settings']['multi_location'];
                    include('data.php');
                    // include('search_form.php');
                    ?>

                    <table id="members" class="table table-striped table-bordered table-sm table-hover display responsive compact" style="overflow: hidden;" cellspacing="0" width="100%" role="grid">
                        <thead>
                            <tr>
                                <th  data-priority="1">No</th>
                                <th class="hidden-xs">id</th>
                                <th class="hidden-xs">Cat</th>
                                <th data-priority="3" class="wrap">Toyname</th>
                                <th data-priority="9" class="wrap">Purchased</th>
                                <th class="hidden-xs" data-priority="8" class="wrap"></th>
                                <th class="hidden-xs nowrap"  style="min-width: 150px;" data-priority="6">Manufacturer</th>
                                <th class="hidden-xs nowrap"  style="min-width: 100px;" data-priority="7">Age</th>
                                <th class="hidden-xs nowrap"  style="min-width: 30px;" data-priority="4">#</th>
                                <th class="hidden-xs" data-priority="5">Due</th>
                                <th class="hidden-xs" data-priority="2"></th>
                           </tr>
                        </thead>
                        <tbody>
                            <?php include('list.php'); ?>
                        </tbody>
                    </table>
                    <?php include('form.php'); ?>
                </div>
            </div>
        </div>
        <?php include('form_fav.php'); ?>
        <script type="text/javascript" src="js/menu.js"></script>
    </body>
</html>