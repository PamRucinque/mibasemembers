<?php

require('../mibase_check_login.php');
include('../connect.php');

$total = 0;
$x = 0;

$pdo = new PDO($_SESSION['connect_pdo'], $_SESSION['dbuser'], $_SESSION['dbpasswd']);

$query_toys = "SELECT toys.idcat as idcat, toyname, category,no_pieces, user1, toys.id as id, age, rent,
 manufacturer, toy_status, twitter, sub_category, toys.location as location, t.due as due, 
 to_char(t.due,'dd-mm-yyyy') as due_str, location.description as location_description, t.total as onloan,
toys.date_purchase as purchased, to_char(date_purchase, 'dd-mm-yyyy') as purchase_format, coalesce(f.c1,0) as favs   
 FROM toys 
 left join location on location.location = toys.location
 left join (select count(id) as total,idcat, due from transaction where return is null group by idcat,due) t on t.idcat = toys.idcat 
 left join (select count(id) as c1,idcat,borid from favs group by borid,idcat) f on f.idcat = toys.idcat
 WHERE 
 (toy_status = 'ACTIVE' or toy_status = 'PROCESSING') 
 AND (f.c1 > 0) and f.borid = ?  
 order by category, id;";

$sth = $pdo->prepare($query_toys);
$array = array($_SESSION['borid']);
$sth->execute($array);

$result = $sth->fetchAll();


$stherr = $sth->errorInfo();

if ($stherr[0] != '00000') {
    echo "An INSERT query error occurred.\n";
    echo $query_edit;
    echo $connect_pdo;
    echo 'Error' . $stherr[0] . '<br>';
    echo 'Error' . $stherr[1] . '<br>';
    echo 'Error' . $stherr[2] . '<br>';
}

$XX = "No Record Found";

$result_txt = '';
$today = date("Y-m-d");

$total = sizeof($result);

$search_str = '';
if ($str_category != '') {
    $search_str .= ' Filtered on Category = ' . $str_category . ', ';
}
if ($age_str != '%%') {
    $search_str .= ' Filtered on Age = ' . $age . ', ';
}
if ($search != '') {
    $search_str .= ' Filtered on String = ' . $search . ', ';
}
if ($attribute != '') {
    $search_str .= ' Filtered on Attribute = ' . $attribute . ', ';
}
$toprint = sizeof($result);

//include('total_row.php');

for ($ri = 0; $ri < $toprint; $ri++) {
    $toy = $result[$ri];
    $bgcolor = 'white';
    $status = '';
    if ($toy['onloan'] > 0) {
        $bgcolor = 'lightgreen';
        $status = $toy['due_str'];
    }
    if ($multi_location == 'Yes') {
        $location_description = $toy['location_description'];
    } else {
        $location_description = '';
    }
    $btn_delete = '<a style="color:white;" class="btn btn-danger btn-sm" id="button" onclick="hello(\'' . $toy['idcat'] . '\');">Delete</a>';
    $subdomain = $_SESSION['library_code'];
    $file_pic = '/home/mibaselive/html/toy_images/' . $subdomain . '/' . strtolower($toy['idcat']) . '.jpg';
    if ((file_exists($file_pic) && ($toy['block_image'] != 'Yes'))) {
        $pic = "'https://" . $subdomain . ".mibase.com.au/toy_images/" . $subdomain . "/" . strtolower($toy['idcat']) . ".jpg" . "'";
    } else {
        $pic = "'https://" . $subdomain . ".mibase.com.au/toy_images/" . $subdomain . "/" . 'blank' . ".jpg" . "'";
    }
    $img_tag = '<img src="' . $pic . '" alt="' . strtolower($toy['idcat']) . '" height="42" width="42">';
    $link = '../toy/toy.php?id=' . $toy['idcat'];
    $view_str = '<a style="color:white;" class="btn btn-primary btn-sm" id="button" '
            . 'onmouseover="showtrail(175,220, ' . $pic . ');" onmouseout="hidetrail();" '
            . ' >View Toy</a>';
    include('row.php');
}




//print $result_txt;
?> 
