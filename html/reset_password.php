<head>
    <meta name="viewport" content="width=device-width">
    <link href="css/bootstrap.min.css" rel="stylesheet" id="bootstrap-css">
    <script src='js/jquery-3.0.0.js' type='text/javascript'></script> 
    <script src="email_password/js/email_password.js?v=3"></script>
    <link rel="stylesheet" type="text/css" href="css/login.css">
</head>

<?php
include('config.php');
$librarycode = $dbname;

$m = array();

$img = '<img src="https://' . $librarycode . '.mibase.com.au/login_image/' . $librarycode . '.jpg" alt="Library Logo" width="150px">';
$login = "login.php";
$key = '';

if (isset($_GET['key'])) {
    $key = $_GET['key'];
}
$error = '';
if ($shared_server) {
    $fqn = $_SERVER["SERVER_NAME"];
    if (check_server_fqn($fqn)) {
        if (ends_with($fqn, $shared_domain)) {
            $librarycode = get_host_name($fqn, $shared_domain);
        } else {
            exit_script('requested host is not on this server: ' . $host . ':' . $shared_domain);
        }
    } else {
        exit_script('incorrect server name');
    }
    $dbpwd = getLibraryPostgresPassword($librarycode, $mibase_server);
}



$m = get_member($key, $librarycode, $dbpwd);
?>
<script>
    $(document).ready(function () {
        $(window).keydown(function (event) {
            if (event.keyCode === 13) {
                event.preventDefault();
                verify();
                return false;
            }
        });
    });
    function onload() {
        document.getElementById('pwd1').value = '';
    }
</script>

<body onload="onload();" style="background-image:url('email_password/img/beach.jpg');background-repeat: no-repeat;center: fixed;background: clear;
      -webkit-background-size: cover;
      -moz-background-size: cover;
      -o-background-size: cover;
      background-size: cover;">
    <div class="container">
        <div class="row">
            <div class="card card-container" style="min-width: 400px;">
                <h1>Hello <?php echo $m['firstname']; ?></h1>
                <h1>Reset your password.</h1>

                <div style="text-align: center;"><?php echo $img; ?></div>
                <?php
                if ($m['status'] === 'OK') {
                    include('email_password/forms/reset_form.php');
                }
                ?>
            </div><!-- /form -->   
        </div><!-- /card-container -->
    </div><!-- /row -->
</div><!-- /container -->
</body>
<?php

function get_host_name($fqn, $shared_domain) {
    return substr($fqn, 0, strlen($fqn) - strlen($shared_domain) - 1);
}

function getLibraryPostgresPassword($library_code, $mibaseserver) {
    global $dbhost, $toybasedbname, $toybasedbuser, $toybasedbpasswd, $dbport;

//now get the database password for the library database
    $toybase_connect_pdo = "pgsql:host=" . $dbhost . ";port=" . $dbport . ";dbname=" . $toybasedbname;
    $toybase_pdo = new PDO($toybase_connect_pdo, $toybasedbuser, $toybasedbpasswd);
    if ($mibaseserver == 'Yes') {
        $toybase_query_login = "select postgres as dbpassword
                    FROM libraries
                    WHERE subdomain = ? ;";
    } else {
        $toybase_query_login = "select dbpassword
                    FROM libraries
                    WHERE library_code = ? ;";
    }


    $toybase_sth = $toybase_pdo->prepare($toybase_query_login);
    $toybase_array = array($library_code);
    $toybase_sth->execute($toybase_array);

    $toybase_result = $toybase_sth->fetchAll();
    $toybase_numrows = $toybase_sth->rowCount();
    $toybase_stherr = $toybase_sth->errorInfo();
    if ($toybase_stherr[0] != '00000') {
        return "An  error occurred checking the login: " . $toybase_stherr[0] . " " . $toybase_stherr[1] . "" . $toybase_stherr[2];
    }

    if ($toybase_numrows > 0) {
        $row = $toybase_result[0];
        return $row['dbpassword'];
    } else {
        return 'Login Error: This library not on this server: ' . $library_code;
    }
}

function get_member($key, $dbname, $dbpwd) {
    $dbuser = $dbname;
    $member = array();
    $status = 'OK';
    $connect_pdo = "pgsql:host=localhost;port=5432;dbname=" . $dbname;

    $pdo = new PDO($connect_pdo, $dbuser, $dbpwd);
    $sql = "SELECT emailaddress, firstname,surname,id, rostertype5 as username, pwd,
	(select setting_value from settings where setting_name = 'libraryname') as libraryname
            from borwrs 
            where trim(key) = ? and member_status = 'ACTIVE';";
    $sth = $pdo->prepare($sql);
    $array = array($key);
    $sth->execute($array);

    $result = $sth->fetchAll();
    $stherr = $sth->errorInfo();
    $numrows = $sth->rowCount();
    //$email = 'not working';
    for ($ri = 0; $ri < $numrows; $ri++) {
        $member = $result[$ri];
    }
    if ($numrows != 1) {
        $status = 'error';
    }

    if ($stherr[0] != '00000') {
        $error_msg .= "An  error occurred.\n";
        $error_msg .= 'Error' . $stherr[0] . '<br>';
        $error_msg .= 'Error' . $stherr[1] . '<br>';
        $error_msg .= 'Error' . $stherr[2] . '<br>';
        $status = 'error';
    }
    $member['status'] = $status;
    return $member;
}
