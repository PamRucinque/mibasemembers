<!DOCTYPE html>
<?php include('../mibase_check_login.php'); ?>
<head>
    <?php
    include('../header/head.php');
    include('head/head.php');
    ?>
    <script>
        $(document).ready(function () {
            const title = get_categories_title_to_display();
            $("#sectionTitle").html('<h2>' + title + '</h2>');
            
            const categories = get_categories_to_display();
            get_all('inlibrary', categories, 100);

            $('.dropdown-toggle').dropdown();
        });
    </script>
</head>
<body>
    <?php include('../header/header.php'); ?>

    <div class="container-fluid">
        <div class="container-fluid">
            <div class="row" style="padding-left: 10px;">
                <div id="sectionTitle"></div>
                <div id="Error" name="Error" class="col-sm-12"></div>
            </div>
            <div class="row">
                <div class="col-sm-1 col-xs-12"   style="padding-top: 10px;">
                    <a style="color:white; margin-left:15px;"  class="btn btn-success" href="../findmytoys/index.php">Go Back</a>
                </div>
                <div class="col-sm-10 col-xs-12" style="padding-top: 10px; padding-right:0;">
                    <div class="col-sm-2 col-xs-12"   style="max-width: 210px; float:right; margin-left:10px; padding-right:0;">
                        <select name='status' id='status' class='form-control' onchange="get_all(this.value, get_categories_to_display(), $('#limit').val());">
                            <option value="null">Status</option>
                            <option value="null">Available and Unavailable</option>
                            <option selected='selected' value="inlibrary">Available Only</option>
                        </select> 
                    </div>
                    <div class="col-sm-10 col-xs-12" style='padding-left: 0px; max-width: 120px; float:right;'>
                        <select name='limit' id='limit'  class='form-control' onchange="get_all($('#status').val(), get_categories_to_display(), this.value);">
                            <option selected='selected' value='100'>100</option>
                            <option value="200">200</option>
                            <option value="300">300</option>
                            <option value="1000">1000</option>
                            <option value="All">All</option>
                        </select> 
                    </div>
                </div>
                <div class="col-sm-1 col-xs-12"   style="padding-top: 10px;text-align: right; padding-right:0;">
                    <a id = "pic_btn" class="btn btn-primary" style="color:white;" onclick="change_view();">List</a>
                </div>
            </div>
            <div class="row">
                <div class="col-sm-12 col-xs-12" id="wait"></div>
            </div>
            <div class="row" style="padding-top: 10px;">
                <div class="hidden-xs col-sm-12" style="float: right; padding-top: 10px;display: none;"   id='print_div'>
                </div>
            </div>

            <div class="row" style="padding-top: 10px;">
                <div class="col-sm-12 col-xs-12" id="Toys">
                </div>
            </div>
        </div>
    </div>
    <?php include('form/toy_div.php'); ?>
</body>
</html>


