<?php
$server = $_SERVER["SERVER_NAME"];
if (substr($server, -13, 13) == 'mibase.com.au') {
    $length = strlen($server) - 14;
    $subdomain = substr($server, 0, $length);
    $url = 'https://' . $subdomain . '.mibase.com.au/mem/';
} else {
    $url = 'http://' . $shared_domain . $app_root . '/';
}
?>
<meta charset="UTF-8">
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<script src="js/toy.js?v=4" type="text/javascript"></script>
<link rel="stylesheet" href="css/style.css" />
<title>Toys</title>
<link rel="stylesheet" href="<?php echo $url; ?>/css/all.css" />
<link rel="stylesheet" href="<?php echo $url; ?>/css/bootstrap.min.css" />
<link rel="stylesheet" href="<?php echo $url; ?>/css/style.css" />
<link href="<?php echo $url; ?>/css/datatables.min.css" rel="stylesheet">
<script src='<?php echo $url; ?>/js/jquery-3.0.0.js' type='text/javascript'></script> 

<script src="<?php echo $url; ?>/js/popper.min.js" type='text/javascript'></script>
<script src="<?php echo $url; ?>/js/jquery.dataTables.min.js"></script>
<script src="<?php echo $url; ?>/js/dataTables.responsive.min.js"></script>
<script src="<?php echo $url; ?>/js/bootstrap.min.js" type='text/javascript'></script>
