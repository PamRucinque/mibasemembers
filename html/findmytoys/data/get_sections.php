<?php
include(dirname(__FILE__) . '/../../mibase_check_login.php');
if (!session_id()) {
    session_start();
}
$subdomain = $_SESSION['library_code'];

try {
    include('../../connect.php');
    $sql = "SELECT id, categories, title FROM toys_section;";

    $pdo = new PDO($connect_pdo, $dbuser, $dbpwd);
    $sth = $pdo->prepare($sql);
    $sth->execute([]);

    $result = $sth->fetchAll();
    $stherr = $sth->errorInfo();
    $numrows = $sth->rowCount();

    if ($stherr[0] != '00000') {
        $error_msg .= "An  error occurred.\n";
        $error_msg .= 'Error' . $stherr[0] . '<br>';
        $error_msg .= 'Error' . $stherr[1] . '<br>';
        $error_msg .= 'Error' . $stherr[2] . '<br>';
        $r = array();
        $r["result"] = "error";
        $r["error"] = "exception: " . $error_msg;
        echo json_encode($r);
        exit;
    }

    $sections = [];
    foreach ($result as $item) {
        $section["id"] = $item["id"];
        $section["image"] = '../../toy_images/' . $subdomain . '/toys_section_images/' . strtolower($section["id"]) . '.jpg';
        $section["categories"] = array_map('trim', explode(",", $item["categories"]));
        //<img ../../toy_images/stonnington/1.jpg?v=0" alt="toy_image" width="175px">
        $section["title"] = $item["title"];
        $sections[] = $section;
    }

    header('Content-Type: application/json');

    $response = array();
    $response["result"] = "ok";
    $response["sections"] = $sections;
    $response["size"] = $numrows;
    $response['subdomain'] = $_SESSION['library_code'];
    echo json_encode($response);
} catch (Exception $e) {
    header('Content-Type: application/json');
    $response = array();
    $response["result"] = "error";
    $response["error"] = "exception: " + $e->getMessage();
    echo json_encode($r);
}