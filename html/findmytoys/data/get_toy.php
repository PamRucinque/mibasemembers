<?php

include(dirname(__FILE__) . '/../../mibase_check_login.php');
if (!session_id()) {
    session_start();
}
if (isset($_SESSION['library_code'])) {
    $subdomain = $_SESSION['library_code'];
}
$idcat = 'a060';
$borid = 758;
$mem_toy_holds_off = $_SESSION['settings']['mem_toy_holds_off'];
include('../../class/toy.php');
try {
    if (isset($_POST['idcat'])) {
        $idcat = $_POST['idcat'];
    }
    $idcat = strtoupper($idcat);

    $t = new Toy();
    $t->data_from_table($idcat);
    $toy = array();
    $fav = Toy::get_fav($idcat, $_SESSION['borid']);
    if ($fav > 0) {
        $fav_pic = '<img src="images/heart.png" alt="favourite" width="50px"  onclick="fav(\'delete\', \'' . $idcat . '\');">';
    } else {
        $fav_pic = '<img src="images/empty_heart.png" alt="favourite" width="50px" onclick="fav(\'add\',\'' . $idcat . '\');">';
    }

    $btn_hold = '';
    $s = '';

    $borid = intval($borid);
    if ($t->hold_borid === 0) {

        $btn_hold = '<a class="btn btn-primary btn-sm" style="color:white;background-color:#F660AB;border-color:#F660AB;" onclick="hold(' . $idcat . '\')">Hold ' . $idcat . '</a>';
        $btn_hold_delete = '<br><a class="btn btn-danger btn-sm" style="color:white;" onclick="delete_hold(' . $t->holdid . ');">Delete hold</a><br>';


        if ($borid == $t->hold_borid) {
            $s .= $btn_hold_delete;
        } else {
            $s .= '<b>Held by </b><br>' . $btn_hold . "  " . $t->holdname . '<br>';
        }
    } else {
        if ($t->toy_status === 'ACTIVE') {
            $btn_hold = '<a class="btn btn-primary btn-sm" style="color:white;background-color:#F660AB;border-color:#F660AB;" onclick="hold_toy(' . $borid . ',\'' . $idcat . '\')">Hold ' . $idcat . '</a>';
            $s .= '<b></b><br>' . $btn_hold . '<br>';
        }
    }
    $toy = $t->toy_array($t);
    $file_pic = '../../../toy_images/' . $subdomain . '/' . strtolower($idcat) . '.jpg';

    if (file_exists($file_pic)) {
        $pic = 'Yes';
    } else {
        $pic = 'No';
    }
    $toy['pic'] = $pic;
    $toy['s'] = $s;
    $toy['fav'] = $fav_pic;
    $toy['mem_toy_holds_off'] = $mem_toy_holds_off;
    $toy['subdomain'] = $subdomain;
    $r["result"] = $t->result;
    $r["data"] = $toy;    //fix
    header('Content-Type: application/json');
    //send the object as json 
    echo json_encode($r);
} catch (Exception $e) {
    //something went wrong, send an error object with the error test
    header('Content-Type: application/json');
    $r = array();
    $r["result"] = "error";
    $r["error"] = "exception: " + $e->getMessage();
    echo json_encode($r);
}










