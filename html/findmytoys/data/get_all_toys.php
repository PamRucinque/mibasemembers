<?php
include(dirname(__FILE__) . '/../../mibase_check_login.php');
if (!session_id()) {
    session_start();
}
$subdomain = $_SESSION['library_code'];
$view = 'list';

try {
    $filter = array();
    $status_str = '';
    $_POST = json_decode(file_get_contents('php://input'), true);
    $filter = $_POST['filter'];
    $view = $_POST['view'];
    $nopic = 'No';
    $locked_str = " (toy_status = 'ACTIVE' ) ";
    $categories = $filter['categories'];

    if ($filter['limit'] == 'All') {
        $limit_str = '';
    } else {
        $limit_str = 'limit ' . $filter['limit'];
    }
    $location_str = '%%';
    if (isset($filter['location'])) {
        $location_str = '%' . trim($filter['location']) . '%';
    }

    if (isset($_SESSION['settings']['multi_location'])) {
        $multi_location = $_SESSION['settings']['multi_location'];
    }
    $order_str = ' order by toys.date_purchase DESC  ';

    $limit_date = '';
    if (isset($filter['status'])) {
        if ($filter['status'] == 'inlibrary') {
            $status_str = ' and held_toys.total is null and transactions.id is null and toys.stocktake_status != \'LOCKED\' ';

        } elseif ($filter['status'] == 'onloan') {
            $status_str = ' AND transactions.id > 0 ';

        } elseif ($filter['status'] == 'hold') {
            $status_str = ' and held_toys.total > 0 ';

        } else if ($filter['status'] == 'new') {
            if ($catsort == 'Yes') {
                $order_str = ' order by toys.date_purchase DESC, toys.category, id  ';
            } else {
                $order_str = ' order by toys.date_purchase DESC, toys.category, id  ';
            }
            $limit_date = " AND (date_purchase != null or date_purchase != '1900-01-01') ";
        }
    }

    $categories_filter = "AND toys.category LIKE '%%'";
    if(sizeof($categories) > 0) {
        $categories_filter = "AND (";
        foreach ($categories as &$category) {
            $categories_filter .= " toys.category LIKE " . "'" . $category . "' OR";
        }
        $categories_filter= substr($categories_filter, 0, -2) . ")";
    }

    include('../../connect.php');
    $sql = "WITH held_toys AS (select coalesce(count(id),0) as total, idcat from toy_holds where (status = 'ACTIVE'or status = 'READY' or status = 'PENDING' or status = 'LOCKED') group by idcat),
                 transactions AS (select coalesce(id,0) as id, to_char(due, 'dd-mm-YYYY') as due_f,due, idcat from transaction where return is null)
            SELECT toys.idcat as idcat,toyname, toys.category, cat.description as category_description, toys.id as id, age, toy_status,
            rent,manufacturer, age, transactions.due_f as due_f, transactions.due as due, no_pieces, pic_ver, coalesce(held_toys.total,0) as holds,
            coalesce(transactions.id,0) as onloan, to_char(date_purchase, 'dd-mm-YYYY') as purchased, date_purchase, pic_ver,l.location as location, l.description as loc_desc
            from toys 
            left join category cat on cat.category = toys.category
            left join location l on l.location = toys.location 
            left join held_toys on held_toys.idcat = toys.idcat 
            left join transactions on transactions.idcat = toys.idcat 
            WHERE "
            . $locked_str
            . $status_str
            . $limit_date .
            "AND ((toys.location LIKE ?) OR (toys.location IS NULL)) " .
            $categories_filter .
            $order_str .
            $limit_str . ";";

    $pdo = new PDO($connect_pdo, $dbuser, $dbpwd);
    $sth = $pdo->prepare($sql);
    $array = array($location_str);
    $sth->execute($array);

    $result = $sth->fetchAll();
    $stherr = $sth->errorInfo();
    $numrows = $sth->rowCount();

    if ($stherr[0] != '00000') {
        $error_msg .= "An  error occurred.\n";
        $error_msg .= 'Error' . $stherr[0] . '<br>';
        $error_msg .= 'Error' . $stherr[1] . '<br>';
        $error_msg .= 'Error' . $stherr[2] . '<br>';
        $r = array();
        $r["result"] = "error";
        $r["error"] = "exception: " . $error_msg;
        echo json_encode($r);
        exit;
    }
    $toys = array();
    $toy = array();

    for ($ri = 0; $ri < $numrows; $ri++) {
        $card = $result[$ri];
        $toy = $card;
        $file_pic = '../../../toy_images/' . $subdomain . '/' . strtolower($card["idcat"]) . '.jpg';
        if (file_exists($file_pic)) {
            $toy['pic'] = true;
        } else {
            $toy['pic'] = false;
        }
        if ($multi_location == 'Yes') {
            $toy['loc_desc'] = $card["loc_desc"];
        } else {
            $toy['loc_desc'] = '';
        }
        $toys[] = $toy;
    }

    header('Content-Type: application/json');

    $r = array();
    $r["result"] = "ok";
    $r["toys"] = $toys;
    $r["size"] = $numrows;
    $r['subdomain'] = $_SESSION['library_code'];
    echo json_encode($r);
} catch (Exception $e) {
    header('Content-Type: application/json');
    $r = array();
    $r["result"] = "error";
    $r["error"] = "exception: " + $e->getMessage();
    echo json_encode($r);
}











