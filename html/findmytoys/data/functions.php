<?php

function get_location($borid) {
    if (!session_id()) {
        session_start();
    }
    $location['location'] = '';
    $connect_pdo = $_SESSION['connect_pdo'];
    $dbuser = $_SESSION['dbuser'];
    $dbpasswd = $_SESSION['dbpasswd'];
    $sql = "select roster.location as location, l.description as loc_desc from roster 
            left join location l on l.location = roster.location
            where date_roster >= current_date and member_id = ?;";
    $pdo = new PDO($connect_pdo, $dbuser, $dbpasswd);
    $sth = $pdo->prepare($sql);
    $array = array($borid);
    $sth->execute($array);
    $result = $sth->fetchAll();
    if ($sth->rowCount() > 0) {
        $location = $result[0];
    }

    return $location;
}
