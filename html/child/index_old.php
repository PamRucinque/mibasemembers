<!DOCTYPE html>
<head>
    <?php
    include('../header/head.php');
    ?>
    <script>
        function setFocus()
        {
            var msg = document.getElementById("msg").innerText;
            //alert(msg);
            if (msg !== '') {
                $('#myModal').modal('show');
            }

        }
        
    </script>

</head>

<body  onload="setFocus()">
    <section class="container fluid">

        <div class="col-sm-12">
            <?php
            if (!session_id()) {
                session_start();
            }

            include('../header/menu.php');
            $button_str = 'OK';
            $blinking = '';
            $java_str = 'Close';
            echo '<h3>Add A Child: ' . $_SESSION['borid'] . ': ' . $_SESSION['firstname'] . ' ' . $_SESSION['surname'] . '</h3>';

            if (isset($_POST['submit_child'])) {
                $result = add_child($_POST['dob'], $_POST['childname'], $gender, $_SESSION['surname'], $_SESSION['borid']);
                echo $result;
            } else {
                include('new_form_child.php');
            }
            ?>

        </div>
    </section>
    <section class="container fluid">
        <?php
        ?>
    </section>
    <section class="container fluid">
        <div id='msg' style='display: block;'><?php echo $str_alert; ?></div>
        <!-- Trigger the modal with a button -->
        <!-- Modal -->
        <div class="modal fade" id="myModal" role="dialog"  data-backdrop="static" data-keyboard="false">
            <div class="modal-dialog">

                <!-- Modal content-->
                <div class="modal-content">
                    <div class="modal-header">

                        <h4 class="modal-title">Mibase Alert</h4>
                    </div>
                    <div class="modal-body">
                        <p><?php echo $str_alert; ?></p>
                    </div>
                    <div class="modal-footer">
                        <?php echo $blinking; ?>
                        <button type="button" class="btn btn-danger" data-dismiss="modal" id="update_terms" name="update_terms" onclick="<?php echo $java_str; ?>"><?php echo $button_str; ?></button>
                    </div>
                </div>

            </div>
        </div>

    </section>
<script type="text/javascript" src="js/jquery-1.8.3.min.js" charset="UTF-8"></script>
<script type="text/javascript" src="./bootstrap/js/bootstrap.min.js"></script>
<script type="text/javascript" src="js/bootstrap-datetimepicker.js" charset="UTF-8"></script>
<script type="text/javascript">
    $('.form_date').datetimepicker({
        //language:  'fr',
        weekStart: 1,
        todayBtn: 1,
        autoclose: 1,
        todayHighlight: 1,
        startView: 2,
        forceParse: 0,
        showMeridian: 1,
        pickTime: false,
        minView: 2
    });


</script>



</body>
</html>
<?php
$str_alert = '';
$_SESSION['renew'] = '';


