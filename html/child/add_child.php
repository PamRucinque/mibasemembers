<?php


function add_child($dob, $childname, $gender, $surname, $borid) {
    include('../connect.php');
    $sql = "INSERT INTO children (id, child_name, surname, sex, d_o_b)
         VALUES (?,?,?,?,?) returning id;";
    $array = array($borid, $childname, $gender, $surname, $dob);
    $pdo = new PDO($connect_pdo, $dbuser, $dbpasswd);
    $sth = $pdo->prepare($sql);
    $sth->execute($array);
    $id = $sth->fetchColumn();
    $stherr = $sth->errorInfo();
    if ($stherr[0] != '00000') {
        $return = "An Update error occurred.\n";
        $return .= 'Error ' . $stherr[0] . '<br>';
        $return .= 'Error ' . $stherr[1] . '<br>';
        $return .= 'Error ' . $stherr[2] . '<br>';
        echo $return;
    } else {
        //echo $_POST['id'];
        $return = 'Thank you have successfully added a Child to your membership ' . $id . '!';
    }
    return $return;
}
