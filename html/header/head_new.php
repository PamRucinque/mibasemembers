<?php
if (!session_id()) {
    session_start();
}
$server = $_SERVER["SERVER_NAME"];
if (substr($server, -13, 13) == 'mibase.com.au') {
    $length = strlen($server) - 14;
    $subdomain = substr($server, 0, $length);
    $url = 'https://' . $subdomain . '.mibase.com.au/admin';
    $url_menu = 'https://' . $_SESSION['subdomain'] . $domain . $branch . '/';
} else {
    $url = 'http://localhost/mibaselive/html/admin';
    $subdomain = $_SESSION['library_code'];
    $url_menu = 'http://localhost/mibaselive/html/admin/';
}

?>
<meta charset="UTF-8">
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<link rel="stylesheet" href="<?php echo $url; ?>/css1/all.css" />
<link rel="stylesheet" href="<?php echo $url; ?>/css1/bootstrap.min.css" />
<link href="<?php echo $url; ?>/css1/datatables.min.css" rel="stylesheet">
<link rel="stylesheet" href="<?php echo $url; ?>/css1/style.css" />
<link rel="stylesheet" href="<?php echo $url; ?>/css1/print.css" />
<link rel="stylesheet" href="<?php echo $url; ?>/css1/jquery-ui.min.css" />
<link rel="stylesheet" href="<?php echo $url; ?>/css1/responsive.dataTables.min.css" />
<link rel="stylesheet" href="<?php echo $url; ?>/css/bootstrap-datetimepicker.min.css" />
<script src='<?php echo $url; ?>/js1/jquery-3.0.0.js' type='text/javascript'></script> 
<script src="<?php echo $url; ?>/js1/popper.min.js" type='text/javascript'></script>
<script src="<?php echo $url; ?>/js1/bootstrap.min.js" type='text/javascript'></script>
<script src="<?php echo $url; ?>/js1/jquery.dataTables.min.js"></script>
<script src="<?php echo $url; ?>/js1/dataTables.responsive.min.js"></script>
<script src="<?php echo $url; ?>/js1/bootstrap-datetimepicker.js"></script>


