<?php
if (!session_id()) {
    session_start();
}
$online_hire = $_SESSION['settings']['online_hire'];
$online_rego = $_SESSION['settings']['online_rego'];
$libraryname = $_SESSION['settings']['libraryname'];
$logo_height = $_SESSION['settings']['logo_height'];
$hiretoys_label = $_SESSION['settings']['hiretoys_label'];
$subdomain = $_SESSION['library_code'];
$library_heading_off = $_SESSION['settings']['library_heading_off'];
$reservations = $_SESSION['settings']['reservations'];
$library_code = $_SESSION['library_code'];
$subdomain = $_SESSION['library_code'];
$mem_reserves = $_SESSION['settings']['mem_reserves'];
$menu_member_options = $_SESSION['settings']['menu_member_options'];
$menu_faq = $_SESSION['settings']['menu_faq'];
$mem_payments = $_SESSION['settings']['mem_payments'];
//$url = 'https://' . $subdomain . '.mibase.com.au/mem/';
?>
<div style="width:100%">
    <nav class="navbar navbar-expand-lg navbar navbar-dark bg-primary">
        <a class="nav-link" href="<?php echo $url . 'home/index.php'; ?>"><span class="sr-only"></span></a>

        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
            <span class="navbar-toggler-icon"></span>
        </button>

        <div class="collapse navbar-collapse" id="navbarSupportedContent">
            <ul class="navbar-nav mr-auto">
                <li class="nav-item active">
                    <a class="nav-link" href="<?php echo $url . 'home/index.php'; ?>">Home <span class="sr-only">(current)</span></a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="<?php echo $url . 'mylibrary/index.php'; ?>">My Library</a>
                </li>
                <li class="nav-item dropdown">
                    <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                        Toys
                    </a>
                    <div class="dropdown-menu" aria-labelledby="navbarDropdown">
                        <a class="dropdown-item" href="<?php echo $url . 'toys/toys.php'; ?>">Search Toys</a>
                        <div class="dropdown-divider"></div>
                        <?php
                        if ($subdomain === 'stonnington') {
                            echo '<a class="dropdown-item" href="' . $url . 'findmytoys/index.php' . '">Help Me Choose</a>';
                            echo '<div class="dropdown-divider"></div>';
                        }
                        ?>
                        <a class="dropdown-item" href="<?php echo $url . 'toys_new/toys.php'; ?>">New Toys</a>
                        <a class="dropdown-item" href="<?php echo $url . 'toys_popular/toys.php'; ?>">Popular Toys</a>
                        <div class="dropdown-divider"></div>
                        <a class="dropdown-item" href="<?php echo $url . 'toys_favs/toys.php'; ?>">Favourites</a>
                    </div>
                </li>
                <?php
                if ($mem_reserves == 'Yes') {
                    echo '<li class="nav-item right"><a class="nav-link" href="' . $url . 'toys_hire/toys.php">' . $hiretoys_label . '</a></li>';
                }
                ?>                
                <li class="nav-item right">
                    <a class="nav-link" href="<?php echo $url . 'roster/index.php'; ?>">Roster</a>
                </li>
                <?php
                echo '<li class="nav-item right"><a class="nav-link" href="' . $url . 'appointments/index.php">Appointments</a></li>';
                ?>


                <?php
                if ($_SESSION['settings']['mem_menu_my_holds_off'] === 'No') {
                    echo '<li class="nav-item right"><a class="nav-link" href="' . $url . 'holds/index.php">My Holds</a></li>';
                }

                if ($mem_payments == 'Yes') {
                    echo '<li class="nav-item right"><a class="nav-link" href="' . $url . 'paypal/index.php">Payments</a></li>';
                }
                ?>
                <li class="nav-item right">
                    <a class="nav-link" href="<?php echo $url . 'password/index.php'; ?>">Reset Password</a>
                </li>


            </ul>
            <ul class="navbar-nav  right">
                <li class="nav-item">
                    <a class="btn btn-danger" href="<?php echo $url . 'Logout.php'; ?>">Logout</a>
                </li>
            </ul>

        </div>
    </nav>
</div>



