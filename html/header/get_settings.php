<?php

if (!session_id()) {
    session_start();
}
$connection_str = $_SESSION['connection_str'];

if (!isset($_SESSION['settings']['libraryname'])) {
    $_SESSION['settings'] = fillSettingSessionVariables($connection_str);
    //fill other session variables that are need by the application
    fillOtherSessionVariables();
}


function fillSettingSessionVariables($connect_str) {
    $array = array();
    $array['selectbox'] = 'No';
    $array['overdue_txt'] = 'OVERDUE';
    $array['admin_login_btn'] = 'No';
    $array['menu_member_options'] = 'No';
    $array['menu_faq'] = 'No';
    $array['volunteer_login_off'] = 'Yes';
    $array['gtmcode'] = '';
    $array['library_heading_off'] = 'No';
    $array['menu_color'] = 'Blue';
    $array['menu_font_color'] = 'whitesmoke';
    $array['online_hire'] = 'No';
    $array['online_rego'] = 'No';
    $array['catsort'] = 'Yes';
    $array['public_roster'] = 'No';
    $array['library_address'] = '';
    $array['toy_reserve'] = 'No';
    $array['loanperiod'] = 14;
    $array['format_toyid'] = 'No';
    $array['reserve_period'] = 8;
    $array['inlibrary'] = 'IN LIBRARY';
    $array['show_phone'] = 'No';
    $array['libraryname'] = 'Training Toy Library';
    $array['menu_color'] = 'darkblue';
    $array['logo_height'] = '100px';
    $array['mem_hide_duedate'] = 'No';
    $array['storage_label'] = 'Storage: ';
    $array['suburb_title'] = 'Suburb';
    $array['city_title'] = 'City';
    $array['exclude_mem_lote'] = 'Yes';
    $array['nationality_label'] = 'Language other than English';
    $array['online_second_contact'] = 'Yes';
    $array['online_membertype'] = 'Yes';
    $array['online_children'] = 'Yes';
    $array['online_address'] = 'Yes';
    $array['online_id'] = 'Yes';
    $array['online_roster_pref'] = 'Yes';
    $array['online_help_off'] = 'No';
    $array['state'] = 'VIC';
    $array['online_findus'] = 'Yes';
    $array['mem_levy'] = 'Yes';
    $array['online_helmet_waiver'] = 'Yes';
    $array['mem_payments'] = 'No';
    $array['mem_menu_my_holds_off'] = 'No';
     

    $query_settings = "SELECT setting_name, setting_value FROM settings;";
    $connect_str = $_SESSION['connection_str'];
    $dbconn = pg_connect($connect_str);

    $result_settings = pg_Exec($dbconn, $query_settings);
    $numrows = pg_numrows($result_settings);

    for ($ri = 0; $ri < $numrows; $ri++) {
        $row = pg_fetch_array($result_settings, $ri);
        $array[$row['setting_name']] = $row['setting_value'];
    }

    pg_FreeResult($result_settings);
    pg_Close($dbconn);
    return $array;
}

function fillOtherSessionVariables() {
    $_SESSION['category'] = '';
    $_SESSION['toyiderror'] = '';
    $_SESSION['borid'] = '';
    $_SESSION['logo'] = '';
    $_SESSION['toyid'] = '';
    $_SESSION['template'] = '';
    $_SESSION['error'] = '';
    $_SESSION['search'] = '';
    $_SESSION['idcat'] = '';
    $_SESSION['status'] = '';
    $_SESSION['limit'] = '';
    $_SESSION['connect_pdo'] = '';
    $_SESSION['memberid'] = 0;
    $_SESSION['mem_reserves'] = 'No';
}
?>

