<head>
    <meta name="viewport" content="width=device-width">
    <link href="css/bootstrap.min.css" rel="stylesheet" id="bootstrap-css">
    <script src='js/jquery-3.0.0.js' type='text/javascript'></script> 
    <script src="email_password/js/email_password.js?v=3"></script>
    <link rel="stylesheet" type="text/css" href="css/login.css">
</head>

<?php
include('config.php');
$host = $_SERVER["SERVER_NAME"];
$library_code = get_host_name($host, $shared_domain);
$img = '<img src="https://' .$library_code . '.mibase.com.au/login_image/' . $library_code .'.jpg" alt="Library Logo" width="150px">';
$home = "../../home/index.php";


if ((strlen($host)) > 31) {
    echo 'Login Error: Password exceeds minimum length of 30 charaters';
    exit;
}
//limit allowed charaters  
if (preg_match('/^[\w\.\-]*$/', $host) == 0) {
    echo 'Host Name Error: Host name can only contain word charaters and numbers _ - and . charaters';
    exit;
}


$error = '';
?>
<script>
    $(document).on('submit', 'email_password', function (e) {
        e.preventDefault();
        //your code goes here
        //100% works
        return;
    });
</script>

<body style="background-image:url('email_password/img/boat.jpg');background-repeat: no-repeat;center: fixed;background: clear;
      -webkit-background-size: cover;
      -moz-background-size: cover;
      -o-background-size: cover;
      background-size: cover;">
    <div class="container">
        <div class="row">
            <div class="card card-container" style="min-width: 400px;">
                <h1>Forgot your password?</h1>
                <div style="text-align: center;"><?php echo $img; ?></div>
                <form name="email_password" method="post" id="email_password" class="form-signin" onsubmit="return false">
                    <div id="reauth-email" class="reauth-email"></div>
                    <input required name="email" type="email" id="email" class="form-control" placeholder="Enter your Email address">
                    <div class="row">
                        <div class="col-sm-6">
                            <a href="<?php echo $home; ?>" class="btn btn-lg btn-info btn-block btn-signin">Home</a>
                        </div>
                        <div class="col-sm-6">
                            <input type="submit" name="submit" value="Email my password" class="btn btn-lg btn-primary btn-block btn-signin" onclick="email_pwd();">  
                        </div>  
                    </div>

                </form>
            </div><!-- /form -->   
        </div><!-- /card-container -->
    </div><!-- /row -->
</div><!-- /container -->
</body>
<?php

function get_host_name($fqn, $shared_domain) {
    return substr($fqn, 0, strlen($fqn) - strlen($shared_domain) - 1);
}
