
<?php

if (!session_id()) {
    session_start();
}
include('../mibase_check_login.php');

$memberid = $_SESSION['borid'];
$membername = $_SESSION['firstname'];
$subdomain = $_SESSION['library_code'];
$rosters_annual = $_SESSION['settings']['rosters_annual'];
$mem_private = $_SESSION['settings']['mem_private'];
$multi_location = $_SESSION['settings']['multi_location'];
$mem_roster_lock = $_SESSION['settings']['mem_roster_lock'];
$mem_coord_off = $_SESSION['settings']['mem_coord_off'];
$mem_cancel_roster = $_SESSION['settings']['mem_cancel_roster'];
$mem_roster_show_all = $_SESSION['settings']['mem_roster_show_all'];
$str_print = '';

$connect_pdo = $_SESSION['connect_pdo'];
$dbuser = $_SESSION['dbuser'];
$dbpasswd = $_SESSION['dbpasswd'];

$day = array();
$session = array();
$member = array();
$weekday = array();
$month = array();


$sql = "SELECT roster.id as id,to_char(date_roster,'dd-Mon-YYYY') as date_r, to_char(date_roster,'Month') as month, date_roster, member_id,
status,comments, roster.location as location, session_role, roster_session, approved, type_roster, to_char(date_roster,'Day') as weekday,
borwrs.firstname as firstname, (date_roster - current_date) as days_cancel,
borwrs.surname as surname, 
borwrs.phone2 as phone2,
location.description as location_long 
FROM Roster 
LEFT JOIN Borwrs ON Roster.member_id = Borwrs.ID 
left join location on location.location = roster.location
WHERE
date_roster >= current_date AND roster.complete = FALSE  AND type_vol = 'Vol' 
ORDER BY date_roster asc,  roster_session asc, location, roster.id asc;";

$pdo = new PDO($connect_pdo, $dbuser, $dbpasswd);
$sth = $pdo->prepare($sql);
$array = array();
$sth->execute($array);

$result = $sth->fetchAll();
$stherr = $sth->errorInfo();
$numrows = $sth->rowCount();

if ($stherr[0] != '00000') {
    $error_msg .= "An  error occurred.\n";
    $error_msg .= 'Error' . $stherr[0] . '<br>';
    $error_msg .= 'Error' . $stherr[1] . '<br>';
    $error_msg .= 'Error' . $stherr[2] . '<br>';
}

if ($numrows > 0) {

    for ($ri = 0; $ri < $numrows; $ri++) {
        $member = $result[$ri];

        if (!in_array($member['weekday'], $weekday)) {
            array_push($weekday, $member['weekday']);
        }
        if (!in_array($member['month'], $month)) {
            array_push($month, $member['month']);
        }
        //$date_roster = date("Y-m-d", $roster['date_roster']);
        if (!in_array($member['date_roster']. $member['location'], $day)) {
            array_push($day, $member['date_roster']. $member['location']);
            if (!in_array($member['roster_session'], $session)) {
                array_push($session, $member['roster_session']);
            }
        }
    }
}

for ($ri = 0; $ri < $numrows; $ri++) {
    $member = $result[$ri];
    //echo $member['member_id'] . '<br>';
}

//echo '<h3>' . date_format(date_create($day_r), "F") . '</h3>';
echo '<div class ="row" style="background-color: white;">';
$n = 0;
$total = 0;
//echo sizeof($day);
for ($di = 0; $di < sizeof($day); $di++) {
    $n = $n + 1;
 
    if (is_numeric($n)) {
        if ($total % 2 == 0) {
            $bg_color = 'background-color: whitesmoke;';
        } else {
            $bg_color = 'background-color: white;';
        }
    }

    $day_r = $day[$di];
    //$day_r = substr($day_r, 0,-1);
    $week_str = date_format(date_create(substr($day_r, 0,-1)), "l");
    $location_str = '<font color="red"> Hall ' . substr($day_r, -1) . '</font>';
    $day_str = date_format(date_create(substr($day_r, 0,-1)), "d-M-Y");
    
    include('roster_list_qld.php');
    include('roster_design_qld.php');
    if ($n == sizeof($weekday)) {
        $total = $total + 1;
        echo '</div>';
        echo '<div class ="row" style="' . $bg_color . '">';
        $n = 0;
    }
}
?>







