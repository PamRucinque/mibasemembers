<script>
    $(document).ready(function () {
        $('#rosters').DataTable({
            "paging": false,
            "lengthChange": false,
            "bFilter": false,
            "bInfo": false,
            language: {
                searchPlaceholder: "Search Toys",
                searchClass: "form-control",
                search: "",
            },
            responsive: {
                details: {
                    renderer: function (api, rowIdx, columns) {
                        var data = $.map(columns, function (col, i) {
                            return col.hidden ?
                                    '<tr data-dt-row="' + col.rowIndex + '" data-dt-column="' + col.columnIndex + '">' +
                                    '<td>' + col.title + ':' + '</td> ' +
                                    '<td>' + col.data + '</td>' +
                                    '</tr>' :
                                    '';
                        }).join('');
                        return data ?
                                $('<table/>').append(data) :
                                false;
                    }
                }
            }
        });

    });
</script>

<?php
require(dirname(__FILE__) . '/../../mibase_check_login.php');
$subdomain = $_SESSION['library_code'];



include('../connect.php');

if (isset($_SESSION['borid'])) {
    $memberid = $_SESSION['borid'];
} else {
    $memberid = 0;
}
$x = 0;
$total = 0;
$hrs = 0;
$required = 0;
$tocomplete = 0;
$mem_cancel_roster = $_SESSION['settings']['mem_cancel_roster'];

$sql = "select roster.id as id, to_char(date_roster,'dd/mm/yyyy') as date_roster, type_roster, weekday, roster_session, duration, status, roster.comments as comments,
session_role, to_char(expired,'dd/mm/yyyy') as expired, renewed, date_roster as roster_sort, m1,m2,
m.expiryperiod, m.duties as required, b.id as borid, (date_roster - current_date) as days_cancel,

to_char(CASE
    WHEN renewed is null THEN (expired - m.expiryperiod * '1 month'::INTERVAL) 
    ELSE  renewed
  END,'dd/mm/yyyy') as start


from roster
left join borwrs b on b.id = roster.member_id
left join membertype m on m.membertype = b.membertype
where 
date_roster >= current_date 
and b.id = ? 
and type_roster = 'Roster' 
order by roster_sort, roster_session;";

$pdo = new PDO($_SESSION['connect_pdo'], $_SESSION['dbuser'], $_SESSION['dbpasswd']);
$sth = $pdo->prepare($sql);
$array = array($memberid);
$sth->execute($array);
$result = $sth->fetchAll();
$stherr = $sth->errorInfo();
if ($stherr[0] != '00000') {
    $_SESSION['error'] = "An  error occurred.\n";
    $_SESSION['error'] .= 'Error' . $stherr[0] . '<br>';
    $_SESSION['error'] .= 'Error' . $stherr[1] . '<br>';
    $_SESSION['error'] .= 'Error' . $stherr[2] . '<br>';
}
$numrows = $sth->rowCount();
if ($numrows > 0) {
    echo '<h2><font color="darkorange">Booked Rosters</font></h2>';
    include('heading.php');
}
$complete_str = '';
//include('heading.php');

for ($ri = 0; $ri < $numrows; $ri++) {
    $roster = $result[$ri];
    $hrs = $hrs + $roster['duration'];
    if ($ri == 0) {
        //include('heading.php');
    }
    $required = $roster['required'];
    $tocomplete = $required - $hrs;
    $link = 'member_detail.php?borid=' . $roster['id'];
    $edit_btn = ' <div style="padding-top:5px;"><a class="btn btn-primary btn-sm" style="color:white;" onclick="edit_app(' . $roster['id'] . ');">Edit Comments</a></div>';

    $onclick = 'javascript:location.href="' . $link . '"';
    $ref_edit = '../../roster/edit_roster.php?id=' . $roster['id'];
    $str_edit = " <a class ='btn btn-primary btn-sm' href='" . $ref_edit . "'>Edit</a>";
    $link_delete = '  <a href="select.php?id=' . $roster['id'] . '&&mem_id=' . $roster['borid'] . '" class="btn btn-danger btn-sm">Delete</a>';
    if ($roster['status'] == 'completed') {
        $complete_str = 'Yes';
    }
    if (($roster['days_cancel'] > $mem_cancel_roster) && ($mem_cancel_roster != 0)) {
        $link_delete = '  <a href="select.php?id=' . $roster['id'] . '&&mem_id=' . $roster['borid'] . '" class="btn btn-danger btn-sm">Delete</a>';
    } else {
        $link_delete = '';
    }

    include('row.php');


    $total = $total + 1;
}
echo '</table>';



//$required = number_format($roster['required'],0);
if ($numrows > 0) {
    include ('footer.php');
}
?>
<script>
    function toggle_hist() {
        if ($("#roster_history").is(":visible")) {
            $("#roster_history").hide();
            document.getElementById("show_history").value = 'Show More Roster Duties';
        } else {
            $("#roster_history").show();
            document.getElementById("show_history").value = 'Show Less Roster Duties';
        }
    }
</script>





