<script>
    $(document).ready(function () {
        $('#rosters').DataTable({
            "paging": false,
            "lengthChange": false,
            "bFilter": false,
            "bInfo": false,
            language: {
                searchPlaceholder: "Search Toys",
                searchClass: "form-control",
                search: "",
            },
            responsive: {
                details: {
                    renderer: function (api, rowIdx, columns) {
                        var data = $.map(columns, function (col, i) {
                            return col.hidden ?
                                    '<tr data-dt-row="' + col.rowIndex + '" data-dt-column="' + col.columnIndex + '">' +
                                    '<td>' + col.title + ':' + '</td> ' +
                                    '<td>' + col.data + '</td>' +
                                    '</tr>' :
                                    '';
                        }).join('');
                        return data ?
                                $('<table/>').append(data) :
                                false;
                    }
                }
            }
        });

    });
</script>

<?php
require(dirname(__FILE__) . '/../../mibase_check_login.php');
$subdomain = $_SESSION['library_code'];
if ($subdomain != 'qld') {
    echo '<h2><font color="darkorange">Roster Duties</font></h2>';
} else {
    if ($membertype == 'Referee') {
        $str_comments = '<font color="red">NOTE:  </font> 1. There may not be time to travel from one hall to the other in between games, or one Hall may be running on a different timetable due to delays. Make sure you are in the SAME hall for the whole morning or afternoon (between scheduled breaks).<br> ';
        $str_comments .= '2. Select ONE game from each time slot (Hall 1 and Hall 2), and choose equal Table and Off Table.<br><br>';
        echo $str_comments;
    }
}
//echo 'hello';

include('../connect.php');

if (isset($_SESSION['borid'])) {
    $memberid = $_SESSION['borid'];
} else {
    $memberid = 0;
}
$x = 0;
$total = 0;
$hrs = 0;
$required = 0;
$tocomplete = 0;

$sql = "select roster.id as id, to_char(date_roster,'dd/mm/yyyy') as date_roster, type_roster, weekday, roster_session, duration, status, roster.comments as comments,
session_role, to_char(expired,'dd/mm/yyyy') as expired, renewed, date_roster as roster_sort, 
m.expiryperiod, m.duties as required,

to_char(CASE
    WHEN renewed is null THEN (expired - m.expiryperiod * '1 month'::INTERVAL) 
    ELSE  renewed
  END,'dd/mm/yyyy') as start


from roster
left join borwrs b on b.id = roster.member_id
left join membertype m on m.membertype = b.membertype
where 
date_roster >= (CASE
    WHEN renewed is null THEN (expired - m.expiryperiod * '1 month'::INTERVAL) 
    ELSE  renewed
  END)
and b.id = ? and type_roster = 'Roster' order by roster_sort, roster_session;";

$pdo = new PDO($_SESSION['connect_pdo'], $_SESSION['dbuser'], $_SESSION['dbpasswd']);
$sth = $pdo->prepare($sql);
$array = array($memberid);
$sth->execute($array);
$result = $sth->fetchAll();
$stherr = $sth->errorInfo();
if ($stherr[0] != '00000') {
    $_SESSION['error'] = "An  error occurred.\n";
    $_SESSION['error'] .= 'Error' . $stherr[0] . '<br>';
    $_SESSION['error'] .= 'Error' . $stherr[1] . '<br>';
    $_SESSION['error'] .= 'Error' . $stherr[2] . '<br>';
}
$numrows = $sth->rowCount();
if ($numrows > 0) {
    if ($subdomain == 'qld') {
        include('heading_qld.php');
    } else {
        include('heading.php');
    }
}
$complete_str = '';
//include('heading.php');

for ($ri = 0; $ri < $numrows; $ri++) {
    $roster = $result[$ri];
    $hrs = $hrs + $roster['duration'];
    if ($ri == 0) {
        //include('heading.php');
    }
    $required = $roster['required'];
    $tocomplete = $required - $hrs;
    $link = 'member_detail.php?borid=' . $roster['id'];
    $onclick = 'javascript:location.href="' . $link . '"';
    $ref_edit = '../../roster/edit_roster.php?id=' . $roster['id'];
    $str_edit = " <a class ='btn btn-primary btn-sm' href='" . $ref_edit . "'>Edit</a>";
    if ($roster['status'] == 'completed') {
        $complete_str = 'Yes';
    }
    if ($subdomain == 'qld') {
        include('row_qld.php');
    } else {
        include('row.php');
    }

    $total = $total + 1;
}
echo '</table>';



//$required = number_format($roster['required'],0);
if ($numrows > 0) {
    include ('footer.php');
} else {
    if ($duties > 0) {
        echo '<h2><font color="red">Duties Required: ' . $duties . '</font></h2>';
        echo '<h2>No Current Rosters Scheduled</h2>';
    } else {
        echo '<h2>This member is not required to do Roster duties.</h2>';
    }
}
?>
<script>
    function toggle_hist() {
        if ($("#roster_history").is(":visible")) {
            $("#roster_history").hide();
            document.getElementById("show_history").value = 'Show More Roster Duties';
        } else {
            $("#roster_history").show();
            document.getElementById("show_history").value = 'Show Less Roster Duties';
        }
    }
</script>





