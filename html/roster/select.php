<?php
include('../mibase_check_login.php');
if (!session_id()) {
    session_start();
}
$connect_str = $_SESSION['connect_str'];
$conn = pg_connect($connect_str);
$auto_approve_roster = $_SESSION['settings']['auto_approve_roster'];

// confirm that the 'id' variable has been set
if (isset($_GET['id']) && is_numeric($_GET['id']) && ($_SESSION['mibase'] != '')) {
    $member_id = $_SESSION['borid'];
    $id = $_GET['id'];
    // get the 'id' variable from the URL
    if (isset($_GET['mem_id']) && is_numeric($_GET['mem_id'])) {

        if ($_GET['mem_id'] == $member_id) {
            $member_id = 0;
            $add_comments = " MEMBER #" . $_GET['mem_id'] . " DELETED";
            $query = "UPDATE roster SET member_id =" . $member_id . ", approved = TRUE, comments = null  WHERE id = " . $_GET['id'] . ";";
            $sql = "UPDATE roster SET member_id = ?, approved = TRUE, delete_comments = delete_comments || ? WHERE id = ?;";
            $delete = 'Yes';
        } else {
            if ($auto_approve_roster == 'Yes') {
                $query = "UPDATE roster SET member_id =" . $member_id . ", approved = TRUE, comments = null WHERE id = " . $_GET['id'] . ";";
            } else {
                $query = "UPDATE roster SET member_id =" . $member_id . ", approved = FALSE, comments = null WHERE id = " . $_GET['id'] . ";";
            }
        }
    }

    //$query = "UPDATE roster SET member_id =" . $member_id . ", approved = FALSE WHERE id = " . $_GET['id'] . ";";
    $result = pg_exec($conn, $query);

    if (!$result) {
        $message = 'Invalid query: ' . "\n";
        $message = 'Whole query: ' . $query;
    } else {

        //include('send_email_roster.php');
    }

    $redirect = 'Location: index.php';               //print $redirect;
    header($redirect);
} else {
    
}
?>
