<script type="text/javascript">
    function edit_app(str) {

        $("#apps").show();
        document.forms['app_form']['id'].value = str;
        var notes = document.getElementById("notes_" + str).innerHTML;
        //alert(notes);
        var m1 = document.getElementById("m1_" + str).innerHTML;
        var m2 = document.getElementById("m2_" + str).innerHTML;
        SelectElement('m1', m1);
        SelectElement('m2', m2);
        SelectElement('notes1', notes);

        //document.forms['app_form']['notes1'].value = notes;
        //document.getElementById("notes1").focus();
        document.getElementById("notes1").value = notes;
    }
    function SelectElement(selectElementId, valueToSelect)
    {
        var element = document.getElementById(selectElementId);
        element.value = valueToSelect;
    }
    function new_log() {
        $("#apps").show();
        $("#new_log").hide();
        document.forms['log_form']['id'].value = 0;
        document.getElementById("notes").focus();
    }
    function cancel() {
        $("#apps").hide();
        $("#new_log").show();
    }
    function del_log(str) {
        //alert(str);
        document.getElementById("id_delete").value = str;
        document.getElementById("form_delete").submit();
    }
</script>
<?php
include('../classes/toy.php');

$subs = Toy::get_all_sub_cats();
$subs_select_m1 = draw_select_desc($subs, '', '', 'Box Type 1', 'm1');
$subs_select_m1_hidden = '<input type=hidden id="m1" name="m1" value="">';
$subs_select_m2_hidden = '<input type=hidden id="m2" name="m2" value="">';
$subs_select_m2 = draw_select_desc($subs, '', '', 'Box Type 2', 'm2');
?>

<div id="apps" style="display: none;background-color: whitesmoke;padding:10px;">
    <form id="app_form" name="app_form" method="post" action="index.php">
        <div class="row">
            <div class="col-sm-6">
                <label for="notes"><b>Comments:</b></label>
                <input type="text" class="form-control" id="notes1" placeholder="Add Comments, details of holds or firstname" name="notes1" value="">   

            </div>
            <div class="col-sm-2">

            </div>

            <div class="col-sm-1"  style="padding-top: 30px">
                <input type=submit id="submit_new_log" name="submit_new_log" class="btn btn-success" value="Save">
            </div>
            <div class="col-sm-1" style="padding-top: 30px">
                <a type=button id="cancel" name="cancel" style="color: white;" class="btn btn-danger" onclick="cancel();">Cancel</a>
            </div>

        </div>
        <input type="hidden" class="form-control" id="id" name="id" value="">
    </form>
</div>

<form id="form_delete" action="detail.php" method="post">
    <input type="hidden" class="form-control" id="id_delete" placeholder="" name="id_delete" value="">
</form>

<?php

function draw_select_desc($t, $value, $description, $cname, $id) {
    $count = 0;
    $str = '';
    if ($value == null) {
        $value = '';
    }
    $numrows = sizeof($t);
    $str .= '<b>' . $cname . ':</b><br>';
    $str .= '<select id="' . $id . '" name="' . $id . '"  class="form-control">';
    $str .= '<option value="' . $value . '" selected="selected">' . $description . '</option>';

    for ($ri = 0; $ri < $numrows; $ri++) {
        $str .= '<option value="' . $t[$ri][1] . '" >' . $t[$ri][1] . ': ' . $t[$ri][2] . '</option>';
    }
    $str .= '</select><br>';

    return $str;
}
?>


