<!DOCTYPE html>
<!--[if lt IE 8 ]> <html class="no-js ie7 oldie" lang="en"> <![endif]-->
<!--[if IE 8]> <html class="no-js ie8 oldie" lang="en"> <![endif]-->
<!--[if IE 9]> <html class="no-js ie9" lang="en"> <![endif]-->
<!--[if (gt IE 9)|!(IE)]><!--> <html class="no-jQuery" lang="en"> <!--<![endif]-->
    <head>
        <?php
        include('../header/head.php');
        ?>
        <script>
            function setFocus()
            {
                var msg = document.getElementById("msg").innerText;
                //alert(msg);
                if (msg !== '') {
                    $('#myModal').modal('show');
                }

            }
            function cancel_msg() {
                $('#msg').hide();
            }
            function show_app(id) {
                if ($("#" + id).is(":visible")) {
                    $("#" + id).hide();
                    document.getElementById("btn_" + id).innerHTML = 'Show Appointments';
                } else {
                    document.getElementById("btn_" + id).innerHTML = 'Hide Appointments';
                    $("#" + id).show();
                }
            }


        </script>

    </head>

    <body>
        <?php include('../header/header.php'); ?>
        <div class="container-fluid">
            <?php
            if (!session_id()) {
                session_start();
            }
            $vacant = 0;

            $str_alert = '';
            if (isset($_SESSION['alert'])) {
                $str_alert = $_SESSION['alert'];
            }
            if (isset($_POST['submit_new_log'])) {
                include('update_app.php');
            }
            include('get_member.php');
            //include('../header/get_settings.php');
            echo '<title>' . $libraryname . '</title>';
            echo '<h3>Roster: ' . $_SESSION['borid'] . ': ' . $_SESSION['firstname'] . ' ' . $_SESSION['surname'] . '</h3>';
            ?>
            <div class="row">
                <div class="col-sm-1"></div>
                <div class="col-sm-8">
                    <?php
                    include('edit_roster.php');
                    include('rosters/list.php');
                    ?>
                </div>
                <div class="col-sm-3"></div>
            </div>
            <div class="row">
                <div class="col-sm-8">
                    <?php
                    include('rosters_new.php');
                    ?>
                </div>
            </div>

        </div>

        <script type="text/javascript" src="../js/menu.js"></script>

    </body>
</html>


