
<?php

include('../mibase_check_login.php');

$memberid = $_SESSION['borid'];
$membername = $_SESSION['firstname'];
$subdomain = $_SESSION['library_code'];
$rosters_annual = $_SESSION['settings']['rosters_annual'];
$mem_private = $_SESSION['settings']['mem_private'];
$multi_location = $_SESSION['settings']['multi_location'];
$mem_roster_lock = $_SESSION['settings']['mem_roster_lock'];
$mem_coord_off = $_SESSION['settings']['mem_coord_off'];
$mem_cancel_roster = $_SESSION['settings']['mem_cancel_roster'];
$str_print = '';
$last_date = '';


include('data/duties.php');
include('data/duties_reminder.php');
include('get_member.php');
include('events.php');



if ($str_duties_completed != '') {
    $roster_alerts = '<br><font color="red">' . $str_duties_completed . '</font><br>';
}

$roster_alerts .= $reminder_txt;
//$roster_alerts .= $str_event;
echo '<font color="red">' . $roster_alerts . '</font>';
if ($rosters_annual == 'Yes') {
    include('rosters_summary_annual.php');
} else {
    include('rosters_summary.php');
}
echo $str_event;
$mem_type = strtolower($membertype);


//echo $multi_location;
if (isset($_GET['id'])) {
    $id = $_GET["id"];
}

//include('forms/db.php');




$query = "SELECT roster.*, 
borwrs.firstname as firstname, 
borwrs.surname as surname, 
borwrs.emailaddress , 
borwrs.phone2 as phone2,
location.description as location_long 
FROM Roster 
LEFT JOIN Borwrs ON Roster.member_id = Borwrs.ID 
left join location on location.location = roster.location
WHERE
date_roster >= current_date AND roster.complete = FALSE  
ORDER BY date_roster asc, roster_session, roster.id asc;";
//echo $query;
//$numrows = pg_numrows($result);
$count = 1;
$result = pg_exec($conn, $query);
$numrows = pg_numrows($result);
$print_str = '';


if ($numrows > 0) {
    //$print_str .= $heading;

    $print_str .= '<section class="container fluid" style="padding-left: 0px;">';
    //$print_str .= '<tr><td>#</td><td width="10%" align="center"></td><td width="40%">Name</td><td>Units</td></tr>';
}


for ($ri = 0; $ri < $numrows; $ri++) {
    //echo "<tr>\n";
    $row = pg_fetch_array($result, $ri);
    $weekday = date('l', strtotime($row['date_roster']));
    $month = date('M', strtotime($row['date_roster']));
    $session = $row['type_roster'];
    $arr = explode(" ", $row['type_roster'], 2);
    $roster_time = $row['roster_session'];
    $location = substr($row['location'], 0, 3);
    $comments = $row['comments'];
    $status = trim($row['status']);
//class="col-sm-3"


    $format_date_roster = substr($row['date_roster'], 8, 2) . '-' . $month . '-' . substr($row['date_roster'], 0, 4);

    if ($last_date != $format_date_roster) {
        $print_str .= '<div class="row">';
        $weekday_row = '<div class="col-sm-4" style="background-color:lightblue;"><h2>' . $format_date_roster . ' - ' . $weekday . '</h2></div>';
        $print_str .= $weekday_row;
        $print_str .= '</div>';
        $count = 1;
    } else {
        $weekday_row = '';
        $count = $count + 1;
    }


    $last_weekday = $weekday;
    $last_date = $format_date_roster;


    $link = 'select.php?id=' . $row['id'] . '&mem_id=' . $row['member_id'];
    $onclick = 'javascript:location.href="' . $link . '"';

    if ($row['member_id'] == $memberid) {
        $str_btn .= '<button type="button"  onclick="' . $onclick . '" class="btn btn-primary">Add My Duty</button>';
    } else {
        $str_btn = "";
    }
    //$str_print .= $str_btn;

    if ($row['approved'] == 't') {
        $approved = "Yes";
       // $print_str .= "<tr id = " . $row['id'] . " style='background-color:#E0F8E6;'>";
    } else {
        $approved = "No";
       // $print_str .= "<tr id = " . $row['id'] . " style='background-color:lightpink;'>";
    }

    $now = time(); // or your date as well
    $roster_date = strtotime($row['date_roster']);
    $diff = round(($roster_date - $now) / (60 * 60 * 24));
    //echo $diff;
    $print_str .= '<div class="row">';
    if (($status == 'Closed') || ($status == 'Note')) {
        //$print_str .= '<td align="center" style="background-color:yellow;" colspan="4"><h4>' . $status . ': ' . $row['comments'] . '</h4></td>';
        $print_str .= '<div class="col-sm-4" align="center" style="background-color:yellow;"><h4>' . $status . ': ' . $row['comments'] . '</h4></div>';
        $count = $count - 1;
    } else {
        $print_str .= '<div class="col-sm-2">' . $count . ': ' . $roster_time . '</div>';
        $print_str .= '<div class="col-sm-2">' . $row['comments'] . '</div>';
    }

    if ($memberid == $row['member_id']) {
        //echo $diff;
        if (($diff > $mem_cancel_roster) && ($mem_cancel_roster != 0)) {

            $print_str .= "<div id='duty' class='col-sm-2>" . $row['firstname'] . "  " . $row['surname'] . " (" . $row['member_id'] . ")"; //$print_str .= "<td onclick='" . $onclick . "' style='background-color:lightgreen;'>" . $row['surname'] . "</td>";
            //$print_str .= "<td onclick='" . $onclick . "'>" . $row['firstname'] . "  " . $row['surname'] . " (" . $row['member_id'] . ")"; //$print_str .= "<td onclick='" . $onclick . "' style='background-color:lightgreen;'>" . $row['surname'] . "</td>";
            $print_str .= "<div class='col-sm-2'><a id='button_red' style='display:block;width:100px;background-color: red;'>Delete</a></div>";
        } else {

            $print_str .= "<div class='col-sm-2'>" . $row['firstname'] . "  " . $row['surname'] . " (" . $row['member_id'] . ")"; //$print_str .= "<td onclick='" . $onclick . "' style='background-color:lightgreen;'>" . $row['surname'] . "</td>";
            $print_str .= "</div>";
        }
    } else {
        if ($row['member_id'] == 0) {
            if ($row['session_role'] != 'Member') {
                if ($mem_coord_off != 'Yes') {
                    if ($mem_roster_lock == 'Yes') {
                        $print_str .= "<div class='col-sm-2' style='background-color:white;'></div>";
                    } else {
                        if (($status != 'Closed') || ($status != 'Note')) {
                            $print_str .= "<div class='col-sm-2'  onclick='" . $onclick . "' style='background-color:yellow;'><div id='duty'><a id='button' style='display:block;width:100px;'>Add My Duty</a></div>";
                        } else {
                            //$print_str .= "<td   style='background-color:yellow;'></td>";
                        }
                    }
                } else {

                    if ($committee == 'Yes') {
                        $print_str .= "<td  onclick='" . $onclick . "' style='background-color:yellow;'><div id='duty'><a id='button' style='display:block;width:100px'>Add My Duty</a><div id='duty'></td>";
                    } else {
                        $print_str .= "<td style='background-color:#E0F8E6;'>LOCKED</td>";
                    }
                }
                //$print_str .= '<td> ' . $row['firstname'] . ' ' . $row['surname'] . '</td>';
            } else {
                if ($mem_roster_lock == 'Yes') {
                    $print_str .= "<td style='background-color:white;'></td>";
                } else {
                    if (($status == 'Closed') || ($status == 'Note')) {
                        
                    } else {
                        $print_str .= "<td  onclick='" . $onclick . "' style='background-color:#E0F8E6;'><div id='duty'><a id='button' style='display:block;width:100px'>Add My Duty</a><div id='duty'></td>";
                    }
                }
            }
        } else {
            if ($status != 'Closed') {
                if ($mem_private == 'No') {
                    if ($mem_roster_show_name == 'Yes') {
                        //$print_str .= "<td>" . $row['firstname'] . "  " . $row['surname'] . " (" . $row['member_id'] . ")"; //$print_str .= "<td onclick='" . $onclick . "' style='background-color:lightgreen;'>" . $row['surname'] . "</td>";
                        if ($mem_roster_show_all == 'Yes') {
                            $print_str .= "<td>" . $row['firstname'] . "  " . $row['surname'] . " (" . $row['member_id'] . ") <br>Moblie: " . $row['phone2'] . "</td>"; //$print_str .= "<td onclick='" . $onclick . "' style='background-color:lightgreen;'>" . $row['surname'] . "</td>";
                        } else {
                            $print_str .= "<td>" . $row['firstname'] . "  " . substr($row['surname'], 0, 2); //$print_str .= "<td onclick='" . $onclick . "' style='background-color:lightgreen;'>" . $row['surname'] . "</td>";
                        }
                    }
                } else {
                    $print_str .= "<td>#" . $row['member_id'] . '</td>'; //$print_str .= "<td onclick='" . $onclick . "' style='background-color:lightgreen;'>" . $row['surname'] . "</td>";
                }
            } else {
                //$print_str .= '<td align="center" style="background-color:yellow;"><h4>CLOSED  ' . $row['comments'] . '</h4></td>'; //$print_str .= "<td onclick='" . $onclick . "' style='background-color:lightgreen;'>" . $row['surname'] . "</td>";
            }

            //$print_str .= '<td> ' . $row['firstname'] . ' ' . $row['surname'] . ' (' . $row['member_id'] . ')</td>';
        }
    }

    //$print_str .= "<td width='50'><a class='button_small' href='approve_roster.php?id=" . $row['id'] . "'>Approve</a></td>";
    //$print_str .= '<td width="30" align="center"> ' . $row['duration'] . '</td>';
    if (($status == 'Closed') || ($status == 'Note')) {
        
    } else {
        $print_str .= '<td width="30" align="center"> ' . substr($row['session_role'], 0, 7) . '</td>';
    }


    if ($row['type_roster'] != 'Roster') {
        $print_str .= '<td  align="left">' . $row['type_roster'] . '<br>' . $row['comments'] . '</td>';
    } else {

        if ($multi_location == 'Yes') {
            $print_str .= '<td width="20%" style="padding-left:10px;">' . $location . '</td>';
        } else {
            //$print_str .= '<td width="5%"></td>';
        }
    }
    //$print_str .= '<td>' . $row['id'] . '</td>';
    //$print_str .= '<td>' . $button_click . '</td>';
    //$print_str .= '<td width="90" align="left">' . $row['emailaddress'] . '</td>';

    $print_str .= '</tr>';
}
$print_str .= '<tr height="50"></tr>';
$print_str .= '</section>';
$link_home = 'https://' . $subdomain . '.mibase.com.au/mobile/home/index.php';



pg_close($conn);

//echo $heading;
//echo '<br>Roster Coord setting: ' . $mem_coord_off;


echo $print_str;
//echo '<font color="red">To Add your duty beyond the ' . $format_date_roster . ', please renew your membership.</font>';
?>




