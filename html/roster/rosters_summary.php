<?php

include('../mibase_check_login.php');

$connect_str = $_SESSION['connect_str'];
$conn = pg_connect($connect_str);

include('get_member.php');
include('get_type.php');

$start_member = date('Y-m-d', strtotime($expired));
$expiry_str = $expired . ' +' . $expiryperiod . ' months';
$expired_roster = date('Y-m-d', strtotime($expiry_str));
$format_start_member = substr($start_member, 8, 2) . '-' . substr($start_member, 5, 2) . '-' . substr($start_member, 0, 4);

$format_expired_roster = substr($expired_roster, 8, 2) . '-' . substr($expired_roster, 5, 2) . '-' . substr($expired_roster, 0, 4);

$memberid = $_SESSION['borid'];
$expiry_str = $expired . ' -' . $expiryperiod . ' months';
$start_member = date('Y-m-d', strtotime($expiry_str));
$format_start_member = substr($start_member, 8, 2) . '-' . substr($start_member, 5, 2) . '-' . substr($start_member, 0, 4);


$query_trans = "SELECT * FROM roster WHERE member_id = " . $_SESSION['borid'] . " ORDER BY date_roster;";
$total = 0;
$hours = 0;
$done = 0;
$pending = 0;
$to_allocate = 0;
$to_complete = 0;
$result_trans = pg_exec($conn, $query_trans);
$numrows = pg_numrows($result_trans);
$str_roster = '';


if ($numrows > 0) {
    $str_roster .= '<table border="1" width="50%" style="border-collapse:collapse; border-color:grey">';
    $str_roster .= '<tr><td>id</td><td>Date</td><td>Type</td><td>Weekday</td><td align="center">Session</td><td>Duration</td><td>completed</td></tr>';
}

for ($ri = 0; $ri < $numrows; $ri++) {
    //echo "<tr>\n";
    $total = $total + 1;

    $row = pg_fetch_array($result_trans, $ri);
    //$weekday = date('l', strtotime($row['date_roster']));
    $id = $row['id'];
    $date_roster = $row['date_roster'];
    $member_id = $row['member_id'];
    $weekday = $row['weekday'];
    $type_roster = $row['type_roster'];
    $roster_session = $row['roster_session'];
    $duration = $row['duration'];
    $status = trim($row['status']);

    if ($status == 'completed') {
        $completed = 'Yes';
        $str_roster .= '<tr>';
    } else {
        $completed = 'No';
        $str_roster .= '<tr style="background-color:#F3F781;">';
        //$tobedone = $tobedone + $hours;
        if ($row['approved'] == 'f') {
            $str_roster .= '<tr style="background-color:lightpink;">';
        }
    }

    if ($date_roster >= $start_member && $date_roster <= $expired_roster) {
        $hours = $hours + $duration;
        if ($status == 'completed') {
            $done = $done + $duration;
        } else {
            if ($status == 'pending') {
                $pending = $pending + $duration;
            }
        }
    }
    $date_roster = substr($row['date_roster'], 8, 2) . '-' . substr($row['date_roster'], 5, 2) . '-' . substr($row['date_roster'], 0, 4);

    $str_roster .= '<td width="30">' . $id . '</td>';
    $str_roster .= '<td width="60" align="left">' . $date_roster . '</td>';
    $str_roster .= '<td width="150">' . $type_roster . '</td>';
    $str_roster .= '<td width="50">' . $weekday . '</td>';
    $str_roster .= '<td width="100" align="center">' . $roster_session . '</td>';
    $str_roster .= '<td width="30" align="center">' . $duration . '</td>';
    //$str_roster .= '<td width="30" align="center">' . $completed . '</td>';

    if ($status == 'completed') {
        $str_roster .= "<td align='center'>Yes</td>";
    } else {
        $str_roster .= "<td align='center'>" . $status . "</td>";
    }

    $str_roster .= '</tr>';
}

if ($numrows > 0) {
    $str_roster .= '</table>';
}

//echo $expiry_str;
//$heading = '<h2>Rosters: ' . $membername . ' (' . $_SESSION['mborid'] . ') </h2>';
//echo $heading;
echo '<strong>Member Type: <font color="blue">' . $membertype . '.</strong></font>';
echo '<br>Start Roster Date: <font color="blue">' . $format_start_member . '</font> End Roster Date: <font color="blue">' . $format_expired . '</font><br>';
//echo $roster_alerts;
$str_legend = '<div id="legend" style="background-color:whitesmoke;">';
//$str_legend .= 'ALERTS: <br>' .
//$str_legend .= $roster_alerts;
$str_legend .= '</div><div id="legend"<br><table><tr><td><strong>Legend:</strong></td></tr>';
$str_legend .= '<tr style="background-color:lightblue;"><td  style="padding-left: 5px;">Blue - Date of Roster Duty</td></tr>';
$str_legend .= '<tr style="background-color:lightpink;"><td style="padding-left: 5px;">Pink - Awaiting Approval</td></tr>';
$str_legend .= '<tr style="background-color:#F3F781;"><td style="padding-left: 5px;">Yellow - Closed or Note</td></tr>';
$str_legend .= '<tr style="background-color:#E0F8E6;"><td style="padding-left: 5px;">Green - Approved</td></tr>';
$str_legend .= '</table>';

$str_legend .= '</div>';
//echo '<strong>Rostered Duties  </strong>';
//echo $str_roster;
echo '<strong><font color="blue">Total: ' . $total . '</font>';
if ($total != $hours) {
    echo '<font color="green">  Hours: ' . $hours . '</font>';
}

//echo '<font color="purple">  Required: ' . $duties . '</font></strong>';
if (($duties - $done) > 0) {
    echo '<font color="#C73F17">  To complete: ' . ($duties - $done) . '</font>';
} else {
    echo '<font color="#C73F17"> No Duties to complete.</font>';
}
if (($duties - $pending - $done) > 0) {
    echo '<font color="red">  To Allocate: ' . ($duties - $pending - $done) . '</strong></font>';
} else {
    //echo '<font color="red">  No Duties to allocate.</strong></font><br><br>';
}
echo '<br><br><font color="green">Click on the Add my duty button to request a Roster duty for approval.</font><br>';
//echo $str_roster;
if ($coord > 0) {
    // echo 'Role: Roster Co-ordinator.<br>';
}


?>


