<?php

$str_detail = '';
$last = '';

for ($ri = 0; $ri < $numrows; $ri++) {
    $member = $result[$ri];
    if ($member['location'] == '') {
        $member['location'] = 'Toy Library';
    }
    //echo $member['date_roster'] . ' ' . $day_r . '<br>';
    if ($member['date_roster'] . $member['location'] == $day_r) {
        if (($member['roster_session'] . $member['location']) !== $last) {
            if ($ri != 0) {
                $str_detail .= '</ul>';
            }

            $str_detail .= '<ul class="list-inline">';
            $str_detail .= '<li class="list-inline-item"><i class="fas fa-clock"></i> ' . $member['roster_session'] . '</li>';
            $str_detail .= '<li class="list-inline-item"><i class="fa fa-location-arrow"></i><font color="red"> ' . $member['location_long'] . '</font></li>';
            $str_detail .= '<li class="list-inline-item"> <b>' . $member['comments'] . '</b></li>';
            $str_detail .= '</ul>';
            $str_detail .= '<ul>';
        }
        $bg_color_name = 'background-color: lightgrey;min-height: 65px;';
        $link = 'select.php?id=' . $member['id'] . '&mem_id=' . $member['member_id'];
        //$onclick = 'javascript:location.href="' . $link . '"';
        //<a href="edit.php" class="btn btn-primary">Edit</a>

        $btn_cancel = '<a href="' . $link . '" class="btn btn-danger">Delete</a>';
        $btn = '<a href="' . $link . '" class="btn btn-primary">Add My Duty</a>';
        if ($subdomain == 'qld') {
            $btn = '<a href="' . $link . '" class="btn btn-primary">Select</a>';
        }

        $membername = $member['firstname'] . ' ' . $member['surname'];
        if ($mem_roster_show_all == 'Yes') {
            $membername = $member['firstname'] . ' ' . $member['surname'] . ' (' . $member['member_id'] . ')<br> Mob: ' . $member['phone2'];
        }
        if ($mem_private == 'Yes') {
            $membername = '# ' . $member['member_id'];
        }
        if ($member['session_role'] != 'Member') {
            $membername .= '<br><font color="red"><strong> ' . $member['session_role'] . '</strong></font>';
        }
        if ($member['member_id'] == 0) {
            if ((trim($member['status']) == 'Note') || ((trim($member['status']) == 'Closed'))) {
                if ($member['comments'] == '') {
                    $member['comments'] = 'Closed';
                }
                $membername = $member['comments'];
            } else {

                $membername = $btn;
                if ($member['session_role'] != 'Member') {
                    $membername .= ' <font color="red"><strong> ' . $member['session_role'] . '</strong></font>';
                }
            }




            $bg_color_name = 'background-color: whitesmoke; min-height: 65px;';
            if (($member['session_role'] != 'Member') && ($mem_coord_off == 'Yes')) {
                $membername = 'Locked';
                $bg_color_name = 'background-color: lightpink;min-height: 65px;';
                if ($committee == 'Yes') {
                    $membername = $btn . '<br>' . $member['comments'];
                }
            }
            if (($member['session_role'] == 'Member') && $member['comments'] != '') {
                $bg_color_name = 'background-color: lightyellow; min-height: 65px;';
            }
        } else {
            if ($member['member_id'] == $_SESSION['borid']) {
                if (($member['days_cancel'] > $mem_cancel_roster) && ($mem_cancel_roster != 0)) {
                    $membername = $btn_cancel . ' <font color="red"><b>' . $member['session_role'] . '</b></font>' . ' ' . $member['firstname'] . ' ' . $member['surname'];
                } else {
                    $membername = '<font color="blue">' . $member['firstname'] . ' ' . $member['surname'] . '</font>';
                    if ($member['session_role'] != 'Member') {
                        $membername .= '<br><font color="red"><strong> ' . $member['session_role'] . '</strong></font><br>';
                    }
                }
            }
        }

        $str_detail .= '<li class="list-group-item" style="' . $bg_color_name . '" >' . $membername . '</li>';

        $last = $member['roster_session'] . $member['location'];
    }
}




