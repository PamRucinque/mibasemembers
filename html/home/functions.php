<?php

function get_roster_msg($mem_levy, $duties, $completed, $expired, $subdomain) {

    $format_expired = substr($expired, 8, 2) . '/' . substr($expired, 5, 2) . '/' . substr($expired, 0, 4);

    $expire_soon = 'No';
    if (($expired <= date('Y-m-d', strtotime('+6 months'))) && ($expired >= date('Y-m-d'))) {
        $expire_soon = 'Yes';
    }
    $levy = round($mem_levy / 4, 2);
    $out = '';
    $str_covid = '';
    if ($subdomain === 'maroondah') {
        //$out .='Yes, maroondah';
        $roster = count_roster_maroondah($_SESSION['borid']);
        if ($roster['closed'] === 98 && $roster['expired'] !== null) {
            $completed = $roster['completed_covid'];
            $str_covid = ' (Expiry date was adjusted for covid)';
        }
    }

    $num = $duties - $completed;
  
    if (($num > 0)) {
        $out .= 'Please complete <b>' . $num . '</b> duty(s) by <b>' . $format_expired . '</b> or pay  $' . $mem_levy . ' non-duty levy per duty.';
        $out .= $str_covid;
    } else {
        //$out = '';
    }


    return array('out' => $out);
}

function draw_table($parts, $numrows) {
    $warnings = '<font color="red">No Warnings</font><br>';
    $count = 0;
    $str = '';
    if ($numrows > 0) {

        $str = '<table id="parts" class="table table-striped table-bordered table-sm table-hover table-responsive-md dataTable compact nowrap order-column" cellspacing="0" width="100%" role="grid">';
        $str .= '<thead>
        <tr>
            <th class="th-sm all">id
            </th>
            <th class="th-sm all">date
            </th>
            <th class="th-sm">toyname
            </th>
              <th class="th-sm">details
            </th>
              <th class="th-sm">type
            </th>

        </tr></thead>';
    }

    foreach ($parts as $p) {
        if ($p->warning == '') {
            $str .= '<tr onclick = "get_part(' . $p->id . ');">';
            $str .= '<td>' . $p->id . '</td>';
            $str .= '<td>' . $p->datepart_str . '</td>';
            $str .= '<td>' . $p->toyname . '</td>';
            $str .= '<td>' . $p->description . '</td>';
            $str .= '<td>' . $p->typepart . '</td>';
            $str .= '</tr>';
        } else {
            if ($count == 0) {
                $warnings = '<font color="red">' . $p->warning . '</font><br>';
            } else {
                $warnings .= '<font color="red">' . $p->warning . '</font><br>';
            }

            $count = $count + 1;
        }
    }
    $str .= '</table>';
    if ($numrows > 0) {
        $str = '<br><h4>Missing Parts: (total: ' . $numrows . ')</h4>' . $str;
    } else {
        $str = '<h4>You have no Missing Pieces.</h4>';
    }


    return $str;
}

function count_roster_maroondah($id) {
    include('../connect.php');

    $sql = "select borwrs.id, covid_expired,expired, renewed,(expired - covid_expired) as closed,
        (select count(id) from roster where (type_roster = 'Roster' or type_roster = 'Exemption') 
        and date_roster >= (borwrs.expired - (m.expiryperiod * '1 month'::INTERVAL))  and member_id = borwrs.id and status != 'no show') as completed,
        (select count(id) from roster where (type_roster = 'Roster' or type_roster = 'Exemption') 
        and date_roster >= (borwrs.covid_expired - (m.expiryperiod * '1 month'::INTERVAL))  and member_id = borwrs.id and status != 'no show') as completed_covid
        from borwrs
        left join membertype m on m.membertype = borwrs.membertype
	where borwrs.id = ?;";
    $array = array($id);
    $pdo = new PDO($connect_pdo, $dbuser, $dbpwd);
    $sth = $pdo->prepare($sql);
    $sth->execute($array);

    $result = $sth->fetchAll();

    $stherr = $sth->errorInfo();
    if ($stherr[0] != '00000') {
        $status .= "An  error occurred " . $stherr[0] . " " . $stherr[1] . "" . $stherr[2];
    }

    return $result[0];
}
