<script type="text/javascript">
    $(function () {
        $('#dob').datetimepicker({
            format: 'yyyy-mm-dd',
            startView: 'month',
            minView: 'month',
            autoclose: true
        });
    });
</script>

<div class="modal fade" id="p1" role="dialog"  data-backdrop="static" data-keyboard="false">
    <div class="modal-dialog">

        <!-- Modal content-->
        <div class="modal-content" style="padding:20px;">
            <div class="row modal-header">
                <div class="col-sm-10">
                    <div id="p1_title"></div>
                </div>
                <div class="col-sm-2">
                    <a class="btn btn-danger" style="color:white;text-align: right;" onclick="cancel_p1();">Cancel</a>
                </div>

            </div>
            <div class="modal-body">
                <form id="p1_frm" method="post">
                    <div class="row">
                        <div class="col-sm-12">
                            Child's Given Name:<input type="Text" name="child_name" align="LEFT"   id="child_name" value="" class="form-control"/>
                            <br>Surname: <input type="Text" name="surname" id="surname" align="LEFT" class="form-control" value="" />
                        </div>
                        <div class="col-sm-6">

                            <br>Gender: <select id="gender" name="gender" class="form-control" required>
                                <option value='' selected="selected"></option>
                                <option value="M" >M</option>
                                <option value="F" >F</option>
                            </select>
                        </div>
                        <div class="col-sm-6">
                            <br>Date of Birth:<input type="Text" name="dob" id="dob" align="LEFT"   value="" class="form-control" required />
                              </div>

                        <div class="col-sm-6">
                            <br><a id="submit_p1_update" class="btn btn-primary" style="color:white;text-align: left;" onclick="update_child();">Save</a>

                        </div>
                        <div class="col-sm-6" style="text-align: right;">
                            <input class="form-control" type='hidden' size="60" id='borid' name='borid' value='0' />
                            <input class="form-control" type='hidden' size="60" id='id' name='id' value='0' />
                        </div>
                        <div class="col-sm-8" id="result_ra">

                        </div>

                    </div>
                </form>
            </div>
            <div class="modal-footer">
            </div>

        </div>
    </div>
</div>
