<?php

include('get_events.php');
if (!session_id()) {
    session_start();
}
$btn_renew = '';
//echo $online_renew;
$mem_edit = $_SESSION['settings']['mem_edit'];
$mem_alerts = $_SESSION['settings']['mem_alerts'];
$mem_levy_alert = $_SESSION['settings']['mem_levy_alert'];
$contact_txt = '';
//$contact_txt .= 'payment info: ' . $_SESSION['settings']['mem_payments_homepage'];
$btn_edit = '  <a class="btn btn-primary" href="../edit/index.php">Edit</a><br><br>';
$contact_txt .= '<h4><div id="title"><font color="blue">' . $_SESSION['firstname'] . ' ' . $_SESSION['surname'] . '</font></div></h4>';
$contact_txt .= '<div id="h1_surname" style="display: none;">' . $_SESSION['surname'] . '</div>';

$now = time(); // or your date as well
$exp = strtotime($expired);
$diff = ($exp - $now) / (60 * 60 * 24);
if ($row['holds'] != '') {
    $contact_txt .= '<h4><font color="red">HOLD TOYS: <font color="blue">' . $row['holds'] . '</font>  are ready for collection</font></h4>';

    if ($row['booking'] !== '' && $row['booking'] !== null) {
        $contact_txt .= '<h4><font color="red"> Appointment(s) </font><br><font color="black">' . $row['booking'] . '</font>.</h4><br>';
    } else {
        if ($row['holds'] != '') {
            $contact_txt .= '<h4><font color="red">Please make an appointment</font></h4>';
        }
    }
}
if ($online_renew == 'Yes') {
    $btn_renew = '<br><a class="btn btn-danger" href="../paypal/index.php">Renew Now</a> ';
}
if ($diff < 0) {
    $contact_txt .= '<strong>Your Membership Expired On: <font color="red">' . $format_expired . '</font></strong><br>';
    // if ($online_renew == 'Yes') {
    //   }
    //echo $payment_info . '<br>';
} else {
    if ($diff >= 0 && $diff <= 30) {
        $contact_txt .= '<strong><font color="red">Your Membership is Due for Renewal On: ' . $format_expired . '</font></strong><br>';
        if ($online_renew == 'Yes') {
            $btn_renew = '<br><a class="btn btn-danger" href="../paypal/index.php">Renew Now</a> ';
        }

        //echo $payment_info . '<br>';
    } else {
        $contact_txt .= '<font color="blue">Start Membership Period: </font>' . $format_start . '<br><font color="blue"> End Membership Period: </font>' . $format_expired . '<br>';
    }
}
$date_6_months = date('Y-m-d', strtotime('+6 months'));

if ($mem_levy > 0 && $date_6_months > $expired) {
    $contact_txt .= '<br><font color="red">' . $roster_msg['out'] . '</font>';
}
if (($mem_alerts == 'Yes') && ($row['alert'] != '')) {
    $contact_txt .= '<font color="red">' . get_text('', $row['alert']) . '</font>';
}
if ($_SESSION['settings']['roster_alerts'] == 'Yes') {
    if ($roster_message != '') {
        //$contact_txt .= $roster_message;
    }
}


if ($_SESSION['settings']['hide_value'] == 'No') {
    $contact_txt .= '<h4>Total Value of Toys Loaned: <font color="blue"> $' . number_format($row['dollar_to_date'], 2) . '</font></h4>';
}
if ($row['days_checked'] < 30) {
    $contact_txt .= '<b>Last Checked:</b> ' . $row['days_checked'] . ' days ago.<br>';
} else {
    if ($mem_edit == 'Yes') {
        $contact_txt .= '<h4><font color="red">Please update your contact and child details below or press the green button if all correct.</font></h4>   <a class="btn btn-success btn-sm" href="update.php">Yes, my details are Correct</a><br>';
    }
}

$contact_txt .= get_text('Member Category: ', $row['membertype']);

$contact_txt .= get_text('Mobile: ', $row['phone2']);
$contact_txt .= get_text('Email: ', $row['emailaddress']);
//$contact_txt .= '<strong><font color="blue">Second Contact</font></strong><br>';
$contact_name = $row['partnersname'] . ' ' . $row['partnerssurname'];
$contact_name = trim($contact_name);
$contact_txt .= get_text('Second Contact: ', $contact_name);
$contact_txt .= get_text('Surname: ', $row['partnerssurname']);
$contact_txt .= get_text('Mobile: ', $row['mobile1']);
$contact_txt .= get_text('Email: ', $row['email2']);

$contact_txt .= get_text('Address: ', $row['address']);
$contact_txt .= get_text('City: ', $row['city']);
$contact_txt .= get_text('Suburb: ', $row['suburb']);
$contact_txt .= get_text('Postcode: ', $row['postcode']);

$contact_txt .= get_text('Username: ', $row['rostertype5']);
$contact_txt .= get_text("Date Joined: ", $row['datejoined']);
//$contact_txt .= get_text("Next Roster Day: ", $row['next_open']);
//$contact_txt .= get_text("Duties Required: ", $row['duties']);

$contact_txt .= $btn_renew;
if ($mem_edit == 'Yes') {
    $contact_txt .= $btn_edit;
} else {
    //$contact_txt .= '<font color="red"><br><br>The Edit function has been turned off for this Library</font>';
}


echo $contact_txt;

function get_text($label, $field) {
    $out = '';
    if ($field != '') {
        $out = '<strong>' . $label . '</strong>' . $field . '<br>';
    }

    return $out;
}
