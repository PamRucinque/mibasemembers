function cancel_p1() {
    $("#p1_title").html('');
    $('#p1').modal('hide');

}

function update_child(id) {


    var url = "data/update_child.php"; // the script where you handle the form input.

    $.ajax({
        type: "POST",
        url: url,
        data: $("#p1_frm").serialize(), // serializes the form's elements.
        success: function (data)
        {

            //alert(data.result); // show response from the php script.
            console.log("Update Child result: " + data.result);
            $('#p1').modal('hide');
            window.location.href = "index.php";

        }
    });
    return false;
}


function delete_p1() {

    var url = "data/delete_child.php"; // the script where you handle the form input.

    $.ajax({
        type: "POST",
        url: url,
        data: $("#p1_frm").serialize(), // serializes the form's elements.
        success: function (data)
        {

            //alert(data.result); // show response from the php script.
            console.log("Delete Child result: " + data.result);
            $('#p1').modal('hide');
            window.location.href = "index.php";

        }
    });

    return false;
}


function on_click_p1(id) {

    $('p1').find('input, textarea').val('');
    $('p1').find('input, text').val('');
    $('p1').find('input, number').val(0);
    var title = '<h4>Child Details for <b>' + document.getElementById('title').innerHTML + '</b></h4>';
    document.getElementById('p1_title').innerHTML = title;
    $('#p1').modal('show');

    $.ajax({
        type: "POST",
        url: "data/get_child.php",
        data: {
            id: id
        },
        success: function (data) {
            for (key in data)
            {
                if (data.hasOwnProperty(key))
                    tc = $('#' + key).prop('type');
                if (tc === 'text' || tc === 'hidden') {
                    $('input[name=' + key + ']').val(data[key]);
                    //alert(key + ' : ' + tc);
                }
                if (tc === 'number') {
                    $('input[name=' + key + ']').val(data[key]);
                    //alert(key + ' : ' + tc);
                }
                if (tc === 'textarea') {
                    $('textarea[name=' + key + ']').val(data[key]);
                    //alert(key + ' : ' + tc);
                }
                var gender = document.getElementById('gender');
                gender.querySelector('[selected]').value = data.gender;
                gender.querySelector('[selected]').text = data.gender;
                gender.selectedIndex = 0;
            }
        },
        error: function (textStatus, errorThrown) {
            $("#Error").html("Error " + textStatus + " " + errorThrown);
        },
        dataType: "json"
    });
}
function on_click_new_child(borid) {
    var surname = document.getElementById('h1_surname').innerHTML;
    $('p1').find('input, textarea').val('');
    $('p1').find('input, text').val('');
    $('p1').find('input, number').val(0);
    $('input[name=borid]').val(borid);
    $('input[name=surname]').val(surname);
    $('input[name=id]').val(0);
    var title = '<h4>Child Details for <b>' + document.getElementById('title').innerHTML + '</b></h4>';
    document.getElementById('p1_title').innerHTML = title;
    //h1_surname

    $('#p1').modal('show');

}
function oc_p2(as, id) {

    $('p2').find('input, textarea').val('');
    $('p2').find('input, text').val('');
    $('p2').find('input, number').val(0);
    var title = '<h4>Assessor Payment for <b>' + document.getElementById('cid_' + id).innerHTML + ": " + document.getElementById('title_' + id).innerHTML + '</b></h4>';
    document.getElementById('p2_title').innerHTML = title;
    $('#p2').modal('show');

    $.ajax({
        type: "POST",
        url: "data/get_p2.php",
        data: {
            id: id,
            as: as
        },
        success: function (data) {
            for (key in data)
            {
                if (data.hasOwnProperty(key))
                    tc = $('#' + key).prop('type');
                if (tc === 'text' || tc === 'hidden') {
                    $('input[name=' + key + ']').val(data[key]);
                    //alert(key + ' : ' + tc);
                }
                if (tc === 'number') {
                    $('input[name=' + key + ']').val(data[key]);
                    //alert(key + ' : ' + tc);
                }
                if (tc === 'textarea') {
                    $('textarea[name=' + key + ']').val(data[key]);
                    //alert(key + ' : ' + tc);
                }
            }
        },
        error: function (textStatus, errorThrown) {
            $("#Error").html("Error " + textStatus + " " + errorThrown);
        },
        dataType: "json"
    });
}

function new_p1() {
    var url = "data/new_ra.php"; // the script where you handle the form input.

    $.ajax({
        type: "POST",
        url: url,
        data: $("#sr1_frm").serialize(), // serializes the form's elements.
        success: function (data)
        {

            //alert(data.result); // show response from the php script.
            console.log("Update Repairer Authority result: " + data.result);
            $('#sr_modal').modal('hide');
            window.location.href = "index.php";

        }
    });

    return false;
}




