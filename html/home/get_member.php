<?php
include('../mibase_check_login.php');
$connect_str = $_SESSION['connect_str'];
$conn = pg_connect($connect_str);

$committee = 'No';
$query = "SELECT borwrs.*, membertype.maxnoitems as max, 
        membertype.returnperiod as member_returnperiod, 
        membertype.expiryperiod as member_expiryperiod, 
        membertype.renewal_fee as renewal,
        membertype.duties as duties,
        borwrs.membertype as membertype,
        borwrs.alert as alert,borwrs.modified as modified, extract(day from current_date - modified)  as days_checked,
        array_to_string(
 array(  SELECT c.child_name
  FROM children c
  WHERE c.id = borwrs.id) ,
 ', '
 ) AS children,
 array_to_string(
 array(  SELECT t.idcat
  FROM toy_holds t
  WHERE t.borid = borwrs.id and status = 'READY'), ',') as holds,value_ytd,
 (select min(date_roster) from roster where date_roster > current_date  and type_roster = 'Roster' and  status != 'Closed' group by type_roster) as next_open,
        (select count(id) from event where typeevent = 'Roster Coord' and memberid = " . $_SESSION['borid'] . ") as coord,
        (select count(id) from event where typeevent = 'Student' and memberid = " . $_SESSION['borid'] . ") as student,        
(borwrs.expired - (membertype.expiryperiod * '1 month'::INTERVAL)) as start,
        (SELECT coalesce(sum(roster.duration),0) FROM roster WHERE roster.member_id = borwrs.id AND (roster.date_roster >= (borwrs.expired - (membertype.expiryperiod * '1 month'::INTERVAL))) and trim(roster.status) != 'no show')  as completed, 
   (SELECT COALESCE(sum(amount),0)  FROM journal WHERE type='CR' and journal.bcode = borwrs.id) as cr,  
    (SELECT COALESCE(sum(amount),0)  FROM journal WHERE type='DR' and journal.bcode = borwrs.id) as dr,
    (select string_agg(roster_session || ' ' || to_char(date_roster, 'dd-Mon-YYYY'),'<br>') as roster  from roster 
where member_id =  " . $_SESSION['borid'] . " and type_roster = 'Appointment' and date_roster >= current_date) as booking 
    FROM membertype RIGHT JOIN borwrs ON membertype.membertype = borwrs.membertype
    WHERE borwrs.id = " . $_SESSION['borid'] . ";";
//$query = "SELECT * from borwrs WHERE id = " . $_SESSION['memberid'] . ";";
//echo $query;
$result_member = pg_Exec($conn, $query);
$numrows = pg_numrows($result_member);


for ($ri = 0; $ri < $numrows; $ri++) {
    $row = pg_fetch_array($result_member, $ri);
    $children = $row['children'];
    $joined = $row['datejoined'];
    $expired = $row['expired'];
    $start_member = $row['start'];
    $alert = $row['alert'];
    $amount = $row['cr'] - $row['dr'];
    if ($amount > 0) {
        $balance = '$ ' . $amount;
    } else {
        $balance = '<font color="red"> $ ' . $amount . '</font>';
    }

    $format_start_member = substr($start_member, 8, 2) . '-' . substr($start_member, 5, 2) . '-' . substr($start_member, 0, 4);
    $member_expiryperiod = $row['member_expiryperiod'];

    $renewed = $row['renewed'];
    $format_expired = substr($row['expired'], 8, 2) . '-' . substr($row['expired'], 5, 2) . '-' . substr($row['expired'], 0, 4);
    $format_joined = substr($row['datejoined'], 8, 2) . '-' . substr($row['datejoined'], 5, 2) . '-' . substr($row['datejoined'], 0, 4);
    $format_start = substr($row['start'], 8, 2) . '-' . substr($row['start'], 5, 2) . '-' . substr($row['start'], 0, 4);
    $format_renewed = substr($row['renewed'], 8, 2) . '-' . substr($row['renewed'], 5, 2) . '-' . substr($row['renewed'], 0, 4);
    $next_open = substr($row['next_open'], 8, 2) . '-' . substr($row['next_open'], 5, 2) . '-' . substr($row['next_open'], 0, 4);
    $borid = $row["id"];
    $firstname = $row["firstname"];
    $membertype = $row["membertype"];
    $surname = $row["surname"];
    $partnersname = $row["partnersname"];
    $partnerssurname = $row['partnerssurname'];
    $email = $row["emailaddress"];
    $email2 = $row["email2"];
    $phone = $row["phone"];
    $postcode = $row['postcode'];
    $mobile1 = $row["phone2"];
    $mobile2 = $row['mobile1'];
    $address = $row['address'];
    $address2 = $row['address2'];
    $suburb = $row['suburb'];
    $city = $row['city'];
    $state = $row['state'];
    $discovery = $row['discoverytype'];
    $rostertype = $row['rostertype'];
    $rostertype2 = $row['rostertype2'];
    $rostertype3 = $row['rostertype3'];
    $user1 = $row['location'];
    $skills = $row['skills'];
    $skills2 = $row['skills2'];
    $member_status = $row['member_status'];
    $pwd = $row['pwd'];
    $coord = $row['coord'];
    $student = $row['student'];
    if ($partnersname == null || $partnerssurname == null) {
        $longname = $firstname . ' ' . $surname;
    } else {
        $longname = $firstname . ' ' . $surname . ' & ' . $partnersname . ' ' . $partnerssurname;
    }


    $email_link = '<a class="button_small_red" href="mailto:' . $email . '">send email</a>';
    $email_link2 = '<a class="button_small_red" href="mailto:' . $email2 . '">send email</a>';
    if ($coord > 0) {

        $committee = 'Yes';
    }
    if ($student > 0) {
        $committee = 'Yes';
    }
    //echo $committee;
}


?>
