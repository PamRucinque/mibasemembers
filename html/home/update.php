<!DOCTYPE html>
<!--[if lt IE 8 ]> <html class="no-js ie7 oldie" lang="en"> <![endif]-->
<!--[if IE 8]> <html class="no-js ie8 oldie" lang="en"> <![endif]-->
<!--[if IE 9]> <html class="no-js ie9" lang="en"> <![endif]-->
<!--[if (gt IE 9)|!(IE)]><!--> <html class="no-jQuery" lang="en"> <!--<![endif]-->
    <head>
        <?php
        include('../mibase_check_login.php');
        include('../header/head.php');
        ?>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
        <script src="../js/bootstrap.min.js"></script>

    </head>

    <body width="300px">
        <section class="container fluid">

            <div class="col-sm-12">
                <?php
                if (!session_id()) {
                    session_start();
                }

                include('../header/menu.php');
                ?>
            </div>
        </section>
        <section class="container" style="width: 100%;">
            <?php
            if (!session_id()) {
                session_start();
            }
            $connect_pdo = $_SESSION['connect_pdo'];
            $dbuser = $_SESSION['dbuser'];
            $dbpasswd = $_SESSION['dbpasswd'];

            try {
                $pdo = new PDO($connect_pdo, $dbuser, $dbpasswd);
            } catch (PDOException $e) {
                print "Error! saving record : " . $e->getMessage() . "<br/>";
                die();
            }
           // $id = $_get['id'];

            $query = "update borwrs  set modified = now() where id = ?;";

            //echo '<br>' . $query;

            $sth = $pdo->prepare($query);
            $array = array($_SESSION['borid']);

            $sth->execute($array);

            $stherr = $sth->errorInfo();
            if ($stherr[0] != '00000') {
                $error = "An  error occurred checking the login: " . $stherr[0] . " " . $stherr[1] . "" . $stherr[2];
               
            } else {
                 echo $error;
                echo "<br>Your information has been successfully update.  " . '<a class="btn btn-primary" href="index.php" >OK</a>' . "<br><br>";
            }
            echo $error;
            ?>
        </section>

        <script type="text/javascript" src="../js/menu.js"></script>

    </body>
</html>












