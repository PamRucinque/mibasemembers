<!DOCTYPE html>
<!--[if lt IE 8 ]> <html class="no-js ie7 oldie" lang="en"> <![endif]-->
<!--[if IE 8]> <html class="no-js ie8 oldie" lang="en"> <![endif]-->
<!--[if IE 9]> <html class="no-js ie9" lang="en"> <![endif]-->
<!--[if (gt IE 9)|!(IE)]><!--> <html class="no-jQuery" lang="en"> <!--<![endif]-->
    <head>
        <?php
        include('../header/head.php');
        ?>

    </head>

    <body width="300px">
        <section class="container fluid">
            <?php
            include('../header/menu.php');
            $member_update = $_SESSION['settings']['member_update'];
            ?>
        </section>
        <section class="container fluid">

            <div class="col-sm-9">
                <?php
                if (!session_id()) {
                    session_start();
                }


                include('get_index_info.php');
                include('get_member.php');
                include('merge_fields.php');
                $member_update = $_SESSION['settings']['member_update'];
                ?>

            </div>
        </section>
        <section class="container fluid">
            <div class="row">
                <div class="col-sm-1"></div>
                <div class="col-sm-8">
                    <?php
                    echo '<u><h2 style="font-size:24px;color:darkslateblue;">Payment Details</h2></u><br>';

                    echo $payment_info . '<br>';
                    ?>
                </div>


                <?php
                //echo $mem_homepage;
                $fee = '$' . number_format((float)$row['renewal'], 2, '.', '');
                $member_str = '<strong><font color="blue">Membership Details</strong></font><br>';
                $member_str .= '<strong>Member Type: </strong>' . $membertype . '<br>';
                $member_str .= '<strong>Renewal Fee: </strong>' . $fee . '</strong><br>';

//echo $payment_info;
                $now = time(); // or your date as well
                $exp = strtotime($expired);
                $diff = ($exp - $now) / (60 * 60 * 24);
                if ($diff < 0) {
                    $member_str .= '<strong>Expired On: </strong><font color="red">' . $format_expired . '</font><br>';
                } else {
                    if ($diff >= 0 && $diff <= 30) {
                        $member_str .= '<h4><strong><font color="red">Due for Renewal On: ' . $format_expired . '</font></strong></h4>';
                        //echo $payment_info . '<br>';
                    } else {
                        $member_str .= '<font color="blue">Start Membership Period: </font>' . $format_start . '<font color="blue"> End Membership Period: </font>' . $format_expired . '<br>';
                    }
                }
                if ($member_update == 'Yes') {
                    $member_str .= '<u><h2 style="font-size:24px;color:darkslateblue;">Update Member Information</h2></u><br>';
                    include('../payments/member_update.php');
                }
                ?> 
                <div class="col-sm-3"style="background-color:lightgoldenrodyellow;">
                    <?php echo $member_str; ?>
                </div>
            </div>

        </div>
    </section>


    <?php include('../header/footer.php'); ?>


</body>
</html>


