<head>
    <?php
    include('../header/head.php');
    ?>

</head>


<section class="container fluid">
    <?php
    if (!session_id()) {
        session_start();
    }
    $timezone = $_SESSION['settings']['timezone'];

    include('../header/header.php');
    ?>
    <div class="row">

        <div class="col-sm-12">
            <?php
            include('../classes/members.php');
            include('../classes/parts.php');
            include('functions.php');
            echo $_SESSION['borid'];
            $m = new member($_SESSION['borid'],$timezone);
            echo $m->firstname;
            $m->fill_parts();
            $missing = draw_table($m->parts, $m->no_parts);
            if ($missing > 0){
                $str_btn = '<a class="btn btn-warning btn-sm" id="button" href="../mylibrary/index.php#missing">click here</a>';
                echo 'You have '. $m->parts . ' missing parts ' . ' for details ' . $str_btn;
            }
            
            //$part = new part();
            ?>

        </div>
    </div>
</section>

