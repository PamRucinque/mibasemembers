<!DOCTYPE html>
<!--[if lt IE 8 ]> <html class="no-js ie7 oldie" lang="en"> <![endif]-->
<!--[if IE 8]> <html class="no-js ie8 oldie" lang="en"> <![endif]-->
<!--[if IE 9]> <html class="no-js ie9" lang="en"> <![endif]-->
<!--[if (gt IE 9)|!(IE)]><!--> <html class="no-jQuery" lang="en"> <!--<![endif]-->
    <head>
        <?php
        include('../header/head.php');
        ?>
        <script src='js/child.js' type='text/javascript'></script> 
        <script src="js/bootstrap-datetimepicker.js"></script>
    </head>

    <body >
        <?php include('../header/header.php'); ?>

        <div class="container-fluid">
            <div class="row">

            </div>
            <?php
            $roster_message = '';
            //echo $_SESSION['connect_pdo'];
            $online_renew = $_SESSION['settings']['online_renew'];
            $multi_location = $_SESSION['settings']['multi_location'];
            //echo $online_renew;
            $mem_alert = $_SESSION['mem_alert'];
            $mem_payments_homepage = $_SESSION['settings']['mem_payments_homepage'];
            $mem_levy = $_SESSION['settings']['mem_levy'];
            $timezone = $_SESSION['settings']['timezone'];
            $mem_show_alert = $_SESSION['settings']['mem_show_alert'];
            $mem_levy_alert = $_SESSION['settings']['mem_levy_alert'];

            include('get_index_info.php');
            include('get_member.php');
            include('merge_fields.php');
            include('functions.php');
            //echo $subdomain;
            $roster_msg = get_roster_msg($mem_levy, $row['duties'], $row['completed'], $expired,$subdomain);
            include('../classes/members.php');
            include('../classes/parts.php');
            $m = new member($_SESSION['borid'], $timezone);
            $m->fill_parts();
            ?>
            <div class ="row" style='padding: 20px;'>
                <div class="col-sm-6">
                    <?php
                    include('detail.php');
                    $str_btn_new_child = '  <a class="btn btn-warning btn-sm" id="button" onclick="on_click_new_child(' . $_SESSION['borid'] . ');">Add Child</a>';
                    if ($mem_edit == 'Yes') {
                        echo '<h4>Children ' . $str_btn_new_child . '</h4>';
                        include('children.php');
                    }

                    $m = new member($_SESSION['borid'], $timezone);
                    echo $m->parts;
                    //echo draw_table($m->parts, $m->no_parts);
                    $m->fill_parts();
                    if ($m->no_parts > 0) {
                        $str_btn = '<a class="btn btn-warning btn-sm" id="button" href="../mylibrary/index.php#missing_bm">click here</a>';
                        echo 'You have ' . $m->no_parts . ' missing parts ' . ' for details ' . $str_btn;
                    }
                    ?>
                </div>
                <div class="col-sm-6">
                    <?php
                    if (($alert != '') && ($mem_show_alert == 'Yes')) {
                        echo '<h4><font color="red">' . $alert . '</font></h4>';
                    }

                    echo $mem_homepage;

                    if ($mem_payments_homepage == 'Yes') {
                        echo '<font color="blue"><br><h4>Payment Details for ' . $libraryname . '</h4></font>';
                        echo $payment_info;
                    }
                    ?>

                </div>

            </div>
        </div>


        <?php
        //include('../header/footer.php'); 
        include('forms/child.php');
        ?>

    </body>
</html>


