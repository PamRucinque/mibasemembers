<?php

include('../../classes/child.php');
try {
    if (isset($_POST['id'])) {
        $id = $_POST['id'];
    }
    //$mid = 380;

    $r = array();
    $r2 = new Child();
    $r2->data_from_table($id);
    $output = '';

    $r['output'] = $output;
    $r['child_name'] = $r2->child_name;
    $r['surname'] = $r2->surname;
    $r['dob'] = $r2->dob;
    $r['notes'] = $r2->notes;
    $r['alert'] = $r2->alert;
    $r['borid'] = $r2->borid;
    $r['gender'] = $r2->gender;
    $r['id'] = $r2->id;

    if ($id > 0) {
        $r["data"] = $r;
        header('Content-Type: application/json');
        //send the object as json 
        echo json_encode($r);
    }
} catch (Exception $e) {
    //something went wrong, send an error object with the error test
    header('Content-Type: application/json');
    $r["result"] = "error";
    $r["error"] = "exception: " + $e->getMessage();
    echo json_encode($r);
}
