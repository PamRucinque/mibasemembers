<?php

$r = array();
try {
    if (isset($_POST['id'])) {
        $id = $_POST['id'];
        $p = array();
        $p = $_POST;
    }
    //$mid = 380;
    if (!session_id()) {
        session_start();
    }
    $connect_pdo = $_SESSION['connect_pdo'];
    $dbuser = $_SESSION['dbuser'];
    $dbpasswd = $_SESSION['dbpasswd'];
    $dbpwd = $_SESSION['dbpasswd'];
    $sql = "update children "
            . "set child_name=?,  "
            . "  surname=?, d_o_b=?, sex=?, id=?  "
            . "WHERE childid = ?;";

    if ($p['id'] == 0) {
        $sql = "INSERT INTO children (child_name,surname, d_o_b, sex, id, childid) "
                . " VALUES (?,?,?,?,?,?);";
        $id = get_new_id();
    }
    if ($p['dob'] === ''){
        $p['dob'] = null;
    }

    $pdo = new PDO($connect_pdo, $dbuser, $dbpwd);
    $sth = $pdo->prepare($sql);
    $array = array($p['child_name'], $p['surname'], $p['dob'], $p['gender'], $p['borid'], $id);
    $sth->execute($array);

    $stherr = $sth->errorInfo();
    if ($stherr[0] != '00000') {
        $r["result"] = 'Error' . $stherr[0] . ' ' . $stherr[1] . ' ' . $stherr[2] . '<br>';
        header('Content-Type: application/json');
        echo json_encode($r);
    } else {
        $r["result"] = 'ok';
        $r['borid'] = $p['borid'];
        header('Content-Type: application/json');
        echo json_encode($r);
    }
} catch (Exception $e) {
    //something went wrong, send an error object with the error test
    header('Content-Type: application/json');

    $r["result"] = "error";
    $r["error"] = "exception: " + $e->getMessage();
    echo json_encode($r);
}

function get_new_id() {

    if (!session_id()) {
        session_start();
    }
    //$_SESSION['connection_str']
    $connection_str = $_SESSION['connection_str'];
    $max = 0;
    $sql = "select max(childid) as max from children;";
    $conn = pg_connect($connection_str);
    $nextval = pg_Exec($conn, $sql);
    $row = pg_fetch_array($nextval);
    $max = $row['max'];
    $max = $max + 1;
    return $max;
}
