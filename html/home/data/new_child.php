<?php

$r = array();
try {
    //$mid = 380;
    $id = get_newid();
    include('../../connect.php');
    $sql = "INSERT INTO children (child_name,surname, d_o_b, sex, id, childid) "
            . " VALUES (?,?,?,?,?,?);";

    $pdo = new PDO($connect_pdo, $dbuser, $dbpwd);
    $sth = $pdo->prepare($sql);
    $array = array($p['child_name'], $p['surname'], $p['dob'], $p['gender'], $p['borid'], $id);
    $sth->execute($array);

    $stherr = $sth->errorInfo();
    if ($stherr[0] != '00000') {
        $r["result"] = 'Error' . $stherr[0] . ' ' . $stherr[1] . ' ' . $stherr[2] . '<br>';
        header('Content-Type: application/json');
        echo json_encode($r);
    } else {
        $r["result"] = 'ok';
        $r['borid'] = $p['borid'];
        header('Content-Type: application/json');
        echo json_encode($r);
    }
} catch (Exception $e) {
    //something went wrong, send an error object with the error test
    header('Content-Type: application/json');

    $r["result"] = "error";
    $r["error"] = "exception: " + $e->getMessage();
    echo json_encode($r);
}

function get_newid() {
    include('../../connect.php');
    $max = 0;
    $sql = "select max(id) as max from children;";
    $conn = pg_connect($connection_str);
    $nextval = pg_Exec($conn, $sql);
    $row = pg_fetch_array($nextval);
    $max = $row['max'];
    $max = $max + 1;
    return $max;
}
