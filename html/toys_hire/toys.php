<!DOCTYPE html>
<?php include('../mibase_check_login.php'); ?>
<html>
    <head>
        <title>Mibase Search Toys</title>
        <?php include('../header/head.php'); ?>
    </head>
    <script>
        $(document).ready(function () {
            $('#members').DataTable({
                "lengthChange": true,
                "pageLength": 50,
                "pagingType": "full_numbers",
                "order": [[2, "asc"], [1, "asc"]],
                "lengthChange": false,
                "bFilter": true,
                language: {
                    searchPlaceholder: "Search Hire Toys",
                    searchClass: "form-control",
                    search: "",
                },
                responsive: {
                    details: {
                        renderer: function (api, rowIdx, columns) {
                            var data = $.map(columns, function (col, i) {
                                return col.hidden ?
                                        '<tr data-dt-row="' + col.rowIndex + '" data-dt-column="' + col.columnIndex + '">' +
                                        '<td>' + col.title + ':' + '</td> ' +
                                        '<td>' + col.data + '</td>' +
                                        '</tr>' :
                                        '';
                            }).join('');
                            return data ?
                                    $('<table/>').append(data) :
                                    false;
                        }
                    }
                }
            });
            $('div.dataTables_filter input').addClass('form-control');
            $('div.dataTables_length select').addClass('recs');
        });
        function set_toyid(str) {
            document.getElementById('get_toy').value = str;
            document.getElementById('submit_toy').submit();
        }


    </script>


    <body>
        <?php include('../header/header.php'); ?>
        <section class="container fluid">
        </section>
        <div class="container-fluid">
            <div class="row">
                <div class="col-sm-12" style="background-color: white;padding-left: 20px;">
                    <h2>Hire Toys</h2>
                    <?php
                    $multi_location = $_SESSION['settings']['multi_location'];
                    include('data.php');
                    if (!session_id()) {
                        session_start();
                    }
                    ?>

                    <table id="members" class="table table-striped table-bordered table-sm table-hover display responsive compact" style="overflow: hidden;" cellspacing="0" width="100%" role="grid">
                        <thead>
                            <tr>
                                <th class="th-sm all" data-priority="1">No</th>
                                <th>id</th>
                                <th >Cat</th>
                                <th data-priority="2" class="wrap">Toyname</th>
                                <th data-priority="3" class="wrap"></th>
                                <th data-priority="5" class="wrap"></th>
                                <th style="min-width: 150px;" data-priority="6">Manufacturer</th>
                                <th class="hidden-xs nowrap"  style="min-width: 100px;" data-priority="7">Age</th>
                                <th class="hidden-xs nowrap"  style="min-width: 30px;" data-priority="8">Pieces</th>
                                <th class="hidden-xs" data-priority="4">Due</th>

                            </tr>
                        </thead>
                        <tbody>
                            <?php include('list.php'); ?>
                        </tbody>
                    </table>
                    <?php include('form.php'); ?>
                </div>
            </div>
        </div>
    </body>
</html>