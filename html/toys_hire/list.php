<?php

require('../mibase_check_login.php');
include('../connect.php');

$total = 0;
$x = 0;
$hiretoys_label_button = 'Hire Toy';
if (isset($_SESSION['settings']['hiretoys_label_button'])){
    $hiretoys_label_button = $_SESSION['settings']['hiretoys_label_button'];
}
$pdo = new PDO($_SESSION['connect_pdo'], $_SESSION['dbuser'], $_SESSION['dbpasswd']);

$query_toys = "SELECT toys.idcat as idcat, toyname, category,no_pieces, user1, toys.id as id, age, rent,
 manufacturer, toy_status, twitter, sub_category, toys.location as location, t.due as due, 
 to_char(t.due,'dd-mm-yyyy') as due_str, location.description as location_description, t.total as onloan,
toys.date_purchase as purchased, to_char(date_purchase, 'dd-mm-yyyy') as purchase_format  
 FROM toys 
 left join location on location.location = toys.location
 left join (select count(id) as total,idcat, due from transaction where return is null group by idcat,due) t on t.idcat = toys.idcat 
 WHERE 
 (toy_status = 'ACTIVE') 
 AND (reservecode != null or reservecode != '') 
 order by purchased DESC LIMIT 100;";

$sth = $pdo->prepare($query_toys);
$array = array();
$sth->execute($array);

$result = $sth->fetchAll();


$stherr = $sth->errorInfo();

if ($stherr[0] != '00000') {
    echo "An INSERT query error occurred.\n";
    echo $query_edit;
    echo $connect_pdo;
    echo 'Error' . $stherr[0] . '<br>';
    echo 'Error' . $stherr[1] . '<br>';
    echo 'Error' . $stherr[2] . '<br>';
}

$XX = "No Record Found";

$result_txt = '';
$today = date("Y-m-d");

$total = sizeof($result);

$search_str = '';
if ($str_category != '') {
    $search_str .= ' Filtered on Category = ' . $str_category . ', ';
}
if ($age_str != '%%') {
    $search_str .= ' Filtered on Age = ' . $age . ', ';
}
if ($search != '') {
    $search_str .= ' Filtered on String = ' . $search . ', ';
}
if ($attribute != '') {
    $search_str .= ' Filtered on Attribute = ' . $attribute . ', ';
}
$toprint = sizeof($result);

//include('total_row.php');

for ($ri = 0; $ri < $toprint; $ri++) {
    $toy = $result[$ri];
    $bgcolor = 'white';
    $status = '';
    if ($toy['onloan'] > 0) {
        $bgcolor = 'lightgreen';
        $status = $toy['due_str'];
    }
    if ($multi_location == 'Yes') {
        $location_description = $toy['location_description'];
    } else {
        $location_description = '';
    }
    $subdomain = $_SESSION['library_code'];
    $file_pic = '/home/mibaselive/html/toy_images/' . $subdomain . '/' . strtolower($toy['idcat']) . '.jpg';
    if ((file_exists($file_pic) && ($row['block_image'] != 'Yes'))) {
        $pic = "'https://" . $subdomain . ".mibase.com.au/toy_images/" . $subdomain . "/" . strtolower($toy['idcat']) . ".jpg" . "'";
    } else {
        $pic = "'https://" . $subdomain . ".mibase.com.au/toy_images/" . $subdomain . "/" . 'blank' . ".jpg" . "'";
    }
    $link = '../toy/toy.php?v=hire&&id=' . $toy['idcat'];
    //$link = '../toy/toy.php?v=hire';
    $view_str = '<a style="color:white;" class="btn btn-primary btn-sm" id="button" '
            . 'onmouseover="showtrail(175,220, ' . $pic . ');" onmouseout="hidetrail();" '
            . ' >View Toy</a>';
    $hire_link = '../reserves/index.php?idcat=' . $toy['idcat'] . '&&v=hire';
    $hire_btn = '<a class="btn btn-warning btn-sm" id="button" href="' . $hire_link . '"'
            . ' >' . $hiretoys_label_button . '</a>';
    include('row.php');
}




//print $result_txt;
?> 
