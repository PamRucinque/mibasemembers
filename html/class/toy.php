<?php

include(dirname(__FILE__) . '/../mibase_check_login.php');

class Toy {

    public $idcat, $id, $toyname, $alert, $category, $no_loans, $alert_toy, $loans, $toy_status, $reservecode,
            $hold_date, $hold_status, $hold_borid, $borname, $onloan_borid, $holdname, $stocktake_status, $loanperiod, $missing,
            $loan_type, $weight, $rent, $transid, $reserves, $holdid, $user1, $warnings, $comments,
            $desc1, $desc2, $category_description, $no_pieces, $purchased, $sub_category, $condition, $hold_days,
            $manufacturer, $supplier, $cost, $replace_cost, $lockdate, $lockreason, $age, $freight, $qholdid,
            $sponsorname, $created, $modified, $location, $packaging_cost, $process_time, $color, $changedby, $twitter, $link, $q_lock, $q_lockf,
            $sql_error, $result, $numrows, $due_f, $pic_ver, $loc_desc;

    public function data_from_table($idcat) {
        include('../../connect.php');

        $this->result = false;
        $idcat = strtoupper($idcat);

        $sql = "SELECT toys.idcat as idcat, no_pieces, age, toyname,toys.id as id, toys.category as category, c.description as category_description, toy_status,alert as alert_toy ,coalesce(t.loans,0) as loans, desc1, desc2, l.description as loc_desc,toys.location as location,
            h.date_start as hold_date, h.status as hold_status, q.id as qholdid, coalesce(h.borid,0) as hold_borid, h.id as holdid,h.holdname, coalesce(t.borid,0) as onloan_borid, borname,coalesce((q.date_end - current_date),0) as hold_days,
            stocktake_status,returndateperiod as loanperiod, reservecode,toys.loan_type as loan_type,coalesce(c.weight, 1) as weight, rent, t.transid as transid, coalesce(r.reserves,0) as reserves, t.due as due_f, pic_ver,
             array_to_string(
             array(  SELECT (p.type || ': ' || p.description)
              FROM parts p
              WHERE p.itemno = toys.idcat and (p.type = 'Missing' or p.type = 'Found' or p.type = 'Found') and alertuser = TRUE) ,
             ','
             ) AS missing, q.date_end as q_lock, to_char(q.date_end, 'dd-mm-YYYY') as q_lockf    
            from toys
            left join (select borid, idcat, date_start, status,toy_holds.id, b.firstname || ' ' || b.surname as holdname, (date_end - current_date) as days 
                from toy_holds 
                left join borwrs b on toy_holds.borid = b.id
                where (status = 'ACTIVE'or status = 'READY' or status = 'PENDING') and idcat = ? order by id limit 1) h on toys.idcat = h.idcat
            left join (SELECT count(id) as loans,to_char(due,'dd-mm-YYYY') as due, idcat,borid,borname,max(id) as transid from transaction where return is null group by idcat,borid,borname,due) t on t.idcat = toys.idcat 
	    left join (select  count(id) as reserves,idcat from reserve_toy where date_end >= current_date and status = 'ACTIVE' and idcat = ? group by idcat) r on r.idcat = toys.idcat      
            left join category c on (toys.category = c.category) 
            left join location l on l.location = toys.location 
            left join (select max(id) as id, max(date_end) as date_end,idcat from toy_holds where status = 'LOCKED' group by idcat) q on q.idcat = toys.idcat           
            where upper(toys.idcat) = ?    
            order by category, toys.id;";

        $pdo = new PDO($connect_pdo, $dbuser, $dbpwd);
        $sth = $pdo->prepare($sql);
        $array = array($idcat, $idcat, $idcat);
        $sth->execute($array);

        $result = $sth->fetchAll();
        $numrows = $sth->rowCount();
        $stherr = $sth->errorInfo();
        if ($stherr[0] != '00000') {
            $status .= "An  error occurred " . $stherr[0] . " " . $stherr[1] . "" . $stherr[2];
        }
        for ($ri = 0; $ri < $numrows; $ri++) {
            $user = $result[$ri];
            if ($user['loan_type'] == null) {
                $user['loan_type'] = '';
            }
            $this->idcat = $user['idcat'];
            $this->reservecode = $user['reservecode'];
            $this->toyname = $user['toyname'];
            $this->id = $user['id'];
            $this->category = $user['category'];
            $this->loans = $user['loans'];
            $this->hold_borid = $user['hold_borid'];
            $this->holdid = $user['holdid'];
            $this->qholdid = $user['qholdid'];
            $this->holdname = $user['holdname'];
            $this->hold_days = $user['hold_days'];
            $this->borname = $user['borname'];
            $this->onloan_borid = $user['onloan_borid'];
            $this->hold_status = $user['hold_status'];
            $this->hold_date = $user['hold_date'];
            $this->alert_toy = $user['alert_toy'];
            $this->toy_status = $user['toy_status'];
            $this->stocktake_status = $user['stocktake_status'];
            $this->loanperiod = $user['loanperiod'];
            $this->loan_type = $user['loan_type'];
            $this->weight = $user['weight'];
            $this->rent = $user['rent'];
            $this->missing = $user['missing'];
            $this->q_lock = $user['q_lock'];
            $this->q_lockf = $user['q_lockf'];
            $this->transid = $user['transid'];
            $this->due_f = $user['due_f'];
            $this->pic_ver = $user['pic_ver'];
            $this->no_pieces = $user['no_pieces'];
            $this->age = $user['age'];
            $this->desc1 = $user['desc1'];
            $this->desc2 = $user['desc2'];
            $this->loc_desc = $user['loc_desc'];
            $this->location = $user['location'];
            $this->category_description = $user['category_description'];
            if ($this->transid === null) {
                $this->transid = 0;
            }
            $this->reserves = $user['reserves'];

            $this->result = 'ok';
        }
        $this->numrows = $numrows;
    }

    public function toy_detail($idcat) {
        include('../connect.php');
        $this->result = false;

        $sql = "SELECT toys.*, c.description as category_description, to_char(date_purchase, 'dd-mm-YYYY') as purchased, 
            reservedfor as condition, cost as replace_cost, discountcost as cost, storage   
            from toys 
                left join category c on c.category = toys.category             
                where upper(toys.idcat) = ?;";

        $pdo = new PDO($connect_pdo, $dbuser, $dbpwd);
        $sth = $pdo->prepare($sql);
        $array = array($idcat);
        $sth->execute($array);

        $result = $sth->fetchAll();
        $numrows = $sth->rowCount();
        $stherr = $sth->errorInfo();
        if ($stherr[0] != '00000') {
            $status .= "An  error occurred " . $stherr[0] . " " . $stherr[1] . "" . $stherr[2];
        }
        for ($ri = 0; $ri < $numrows; $ri++) {
            $user = $result[$ri];
            $user['desc1'] = str_replace("\r", "", $user['desc1']);
            $user['desc1'] = str_replace("\n", "</br>", $user['desc1']);
            $user['desc2'] = str_replace("\r", "", $user['desc2']);
            $user['desc2'] = str_replace("\n", "</br>", $user['desc2']);
            $this->desc1 = $user['desc1'];
            $this->desc2 = $user['desc2'];
            $this->no_pieces = $user['no_pieces'];
            $this->purchased = $user['purchased'];
            $this->category_description = $user['category_description'];
            $this->sub_category = $user['sub_category'];
            $this->condition = $user['condition'];
            $this->manufacturer = $user['manufacturer'];
            $this->supplier = $user['supplier'];
            $this->cost = $user['cost'];
            $this->replace_cost = $user['replace_cost'];
            $this->lockdate = $user['lockdate'];
            $this->lockreason = $user['lockreason'];
            $this->age = $user['age'];
            $this->storage = $user['storage'];
            $this->sponsorname = $user['sponsorname'];
            $this->freight = $user['freight'];
            $this->created = $user['created'];
            $this->modified = $user['modified'];
            $this->location = $user['location'];
            $this->packaging_cost = $user['packaging_cost'];
            $this->process_time = $user['process_time'];
            $this->color = $user['color'];
            $this->changedby = $user['changedby'];
            $this->twitter = $user['twitter'];
            $this->link = $user['link'];
            $this->user1 = $user['user1'];
            $this->comments = $user['comments'];
            $this->warnings = $user['warnings'];
            $this->result = 'ok';
        }
        $this->numrows = $numrows;
    }

    public function fill_from_array($r) {
        $this->idcat = $r['idcat'];
        $this->reservecode = $r['reservecode'];
        $this->toyname = $r['toyname'];
        $this->id = $r['id'];
        $this->category = $r['category'];
        $this->loans = $r['loans'];
        $this->hold_borid = $r['hold_borid'];
        $this->holdname = $r['holdname'];
        $this->borname = $r['borname'];
        $this->onloan_borid = $r['onloan_borid'];
        $this->hold_status = $r['hold_status'];
        $this->hold_date = $r['hold_date'];
        $this->alert_toy = $r['alert_toy'];
        $this->toy_status = $r['toy_status'];
        $this->stocktake_status = $r['stocktake_status'];
        $this->loanperiod = $r['loanperiod'];
        $this->loan_type = $r['loan_type'];
        $this->weight = $r['weight'];
        $this->rent = $r['rent'];
        $this->transid = $r['transid'];
        $this->reserves = $r['reserves'];
        $this->q_lock = $r['q_lock'];
        $this->q_lockf = $r['q_lockf'];
    }

    public function toy_array($toy) {

        $r['idcat'] = $toy->idcat;
        $r['reservecode'] = $toy->reservecode;
        $r['toyname'] = $toy->toyname;
        $r['id'] = $toy->id;
        $r['category'] = $toy->category;
        $r['loans'] = $toy->loans;
        $r['age'] = $toy->age;
        $r['no_pieces'] = $toy->no_pieces;
        $r['due_f'] = $toy->due_f;
        $r['hold_borid'] = $toy->hold_borid;
        $r['holdname'] = $toy->holdname;
        $r['onloan_borid'] = $toy->onloan_borid;
        $r['borname'] = $toy->borname;
        $r['alert_toy'] = $toy->alert_toy;
        $r['result'] = $toy->result;
        $r['toy_status'] = $toy->toy_status;
        $r['stocktake_status'] = $toy->stocktake_status;
        $r['loanperiod'] = $toy->loanperiod;
        $r['loan_type'] = $toy->loan_type;
        $r['weight'] = $toy->weight;
        $r['rent'] = $toy->rent;
        $r['transid'] = $toy->transid;
        $r['reserves'] = $toy->reserves;
        $r['q_lock'] = $toy->q_lock;
        $r['q_lockf'] = $toy->q_lockf;
        $r['desc1'] = str_replace("\n", "<br>", $toy->desc1);
        $r['desc2'] = str_replace("\n", "<br>", $toy->desc2);
        $r['category_description'] = $toy->category_description;
        $r['loc_desc'] = $toy->loc_desc;
        $r['pic_ver'] = $toy->pic_ver;
        return $r;
    }

    public function parts($idcat) {
        $str = '';
        include('../connect.php');
        $sql = "select parts.id as id ,description,type as type_part,to_char(datepart, 'dd-mm-YYYY') as datepart,
                alertuser,itemno, b.surname || ', ' || b.firstname as borname, parts.borcode as borid 
                from parts 
                left join borwrs b on b.id = parts.borcode 
                where upper(itemno) = ? order by type;";

        $pdo = new PDO($connect_pdo, $dbuser, $dbpwd);
        $sth = $pdo->prepare($sql);
        $array = array($idcat);
        $sth->execute($array);

        $result = $sth->fetchAll();
        $numrows = $sth->rowCount();
        $stherr = $sth->errorInfo();
        if ($stherr[0] != '00000') {
            $status .= "An  error occurred " . $stherr[0] . " " . $stherr[1] . "" . $stherr[2];
        }
        $rows = array();

        for ($ri = 0; $ri < $numrows; $ri++) {
            $parts = $result[$ri];
            $row = array();
            $row[] = $parts["id"];
            $row[] = $parts["description"];
            $row[] = $parts["type_part"];
            $row[] = $parts["alertuser"];
            $row[] = $parts["itemno"];
            $row[] = $parts['borname'];
            $row[] = $parts['borid'];
            $row[] = $parts['datepart'];

            $rows[] = $row;
        }
        //print_r($rows);

        return $rows;
    }

    public function loan_history($idcat) {
        $str = '';
        $count_gold_star = 0;
        include('../connect.php');
        $sql = "select transaction.*, to_char(due,'dd-mm-yy') as due_f,to_char(date_loan,'dd-mm-yy') as loan_f, (due - return) as overdue,  to_char(return,'dd-mm-yy') as return_f    
            from transaction 
            where return is not null and idcat = ? order by id desc;";

        $pdo = new PDO($connect_pdo, $dbuser, $dbpwd);
        $sth = $pdo->prepare($sql);
        $array = array($idcat);
        $sth->execute($array);

        $result = $sth->fetchAll();
        $numrows = $sth->rowCount();
        $this->toys_onloan = $numrows;
        $stherr = $sth->errorInfo();
        if ($stherr[0] != '00000') {
            $status .= "An  error occurred " . $stherr[0] . " " . $stherr[1] . "" . $stherr[2];
        }
        $rows = array();

        for ($ri = 0; $ri < $numrows; $ri++) {
            $trans = $result[$ri];
            $row = array();
            $row[] = $trans["id"];
            $row[] = $trans["idcat"];
            $row[] = $trans["borid"];
            $row[] = $trans["borname"]; //3
            $row[] = $trans["loan_f"]; //4
            $row[] = $trans["due_f"]; //5
            $row[] = $trans["due"]; //6
            $row[] = $trans['overdue']; //7
            $row[] = $trans['return']; //8
            $row[] = $trans['return_f']; //9
            $row[] = $trans['date_loan']; //10
            $rows[] = $row;
        }
        //print_r($rows);

        return $rows;
    }

    public function reserves($tid) {
        $str = '';
        include('../connect.php');
        $sql = "select reserve_toy.id, to_char(date_start, 'dd-mm-YYYY') as date_start, 
            to_char(date_end, 'dd-mm-YYYY') as date_end, member_id as borid, reserve_toy.status as status,paid,member_id as borid,
                to_char(t.due, 'dd-mm-YYYY') as due,reserve_toy.idcat as idcat 
                from reserve_toy 
                left join toys on toys.idcat = reserve_toy.idcat
                left join (select due, idcat from transaction where return is null) t on t.idcat = reserve_toy.idcat
                where (trim(reserve_toy.status) ='ACTIVE') 
                and reserve_toy.idcat = ? order by date_start asc;";

        $pdo = new PDO($connect_pdo, $dbuser, $dbpwd);
        $sth = $pdo->prepare($sql);
        $array = array($tid);
        $sth->execute($array);

        $result = $sth->fetchAll();
        $numrows = $sth->rowCount();
        $stherr = $sth->errorInfo();
        if ($stherr[0] != '00000') {
            $status .= "An  error occurred " . $stherr[0] . " " . $stherr[1] . "" . $stherr[2];
        }
        $rows = array();

        for ($ri = 0; $ri < $numrows; $ri++) {
            $trans = $result[$ri];
            $row = array();
            $row[] = $trans["id"];
            $row[] = $trans["date_start"];
            $row[] = $trans["date_end"];
            $row[] = $trans["status"];
            if ($trans["due"] === null) {
                $row[] = 'LIBRARY';
            } else {
                $row[] = $trans["due"];
            }
            $row[] = $trans["borid"];
            $row[] = $trans["paid"];
            $row[] = $trans["idcat"];
            $rows[] = $row;
        }
        //print_r($rows);

        return $rows;
    }

    public function calendar($tid) {
        $str = '';
        include('../connect.php');
        $sql = "select reserve_toy.id as id,date_start,  date_end, member_id as borid, (b.firstname || ' ' || b.surname) as mem,
                reserve_toy.idcat as idcat 
                from reserve_toy 
                left join borwrs b on b.id = reserve_toy.member_id
                where (trim(reserve_toy.status) !='ACTIVE') 
                and reserve_toy.idcat = ? order by date_start asc;";

        $pdo = new PDO($connect_pdo, $dbuser, $dbpwd);
        $sth = $pdo->prepare($sql);
        $array = array($tid);
        $sth->execute($array);

        $result = $sth->fetchAll();
        $numrows = $sth->rowCount();
        $stherr = $sth->errorInfo();
        if ($stherr[0] != '00000') {
            $status .= "An  error occurred " . $stherr[0] . " " . $stherr[1] . "" . $stherr[2];
        }
        $rows = array();

        for ($ri = 0; $ri < $numrows; $ri++) {
            $trans = $result[$ri];
            $row = array();
            $row[] = $trans["id"];
            $row[] = $trans["date_start"];
            $row[] = $trans["date_end"];
            $row[] = $trans["borid"];
            $row[] = $trans["mem"];
            $row[] = $trans["idcat"];
            $rows[] = $row;
        }
        //print_r($rows);

        return $rows;
    }

    public function attributes($tid) {
        $str = '';
        include('../connect.php');
        $sql = "select toy_attributes.id as id, toy_attributes.attribute as attribute, a.description 
            from toy_attributes 
            left join attributes a on a.attribute = toy_attributes.attribute 
            where idcat = ?  
            order by attribute;";

        $pdo = new PDO($connect_pdo, $dbuser, $dbpwd);
        $sth = $pdo->prepare($sql);
        $array = array($tid);
        $sth->execute($array);

        $result = $sth->fetchAll();
        $numrows = $sth->rowCount();
        $stherr = $sth->errorInfo();
        if ($stherr[0] != '00000') {
            $status .= "An  error occurred " . $stherr[0] . " " . $stherr[1] . "" . $stherr[2];
        }
        $rows = array();

        for ($ri = 0; $ri < $numrows; $ri++) {
            $trans = $result[$ri];
            $row = array();
            $row[] = $trans["id"];
            $row[] = $trans["attribute"];
            $row[] = $trans["description"];
            $rows[] = $row;
        }
        //print_r($rows);

        return $rows;
    }

    public function holds($idcat) {
        $str = '';
        include('../connect.php');
        $sql = "SELECT toy_holds.id as id,toy_holds.idcat as idcat, toys.toyname as toyname, to_char(t.due, 'dd-mm-YYYY') as due, toy_holds.status as status,paid,
                to_char(toy_holds.date_start, 'dd-mm-YYYY') as date_start, borid, (b.firstname || ' ' || b.surname) as borname, (current_date - date_end) as days, paid    
               from toy_holds 
               LEFT JOIN (select idcat, due, borname from transaction where return is null) t on t.idcat = toy_holds.idcat 
               LEFT JOIN toys on toys.idcat = toy_holds.idcat
               LEFT JOIN borwrs b on b.id = toy_holds.borid
               where toy_holds.idcat = ?;";

        $pdo = new PDO($connect_pdo, $dbuser, $dbpwd);
        $sth = $pdo->prepare($sql);
        $array = array($idcat);
        $sth->execute($array);

        $result = $sth->fetchAll();
        $numrows = $sth->rowCount();
        $stherr = $sth->errorInfo();
        if ($stherr[0] != '00000') {
            $status .= "An  error occurred " . $stherr[0] . " " . $stherr[1] . "" . $stherr[2];
        }
        $rows = array();

        for ($ri = 0; $ri < $numrows; $ri++) {
            $trans = $result[$ri];
            $row = array();
            $row[] = $trans["id"];
            $row[] = $trans["idcat"];
            $row[] = $trans["toyname"];
            $row[] = $trans["due"];
            $row[] = $trans["status"];
            if ($trans['paid'] === null) {
                $trans['paid'] = '';
            }
            $row[] = $trans["paid"];
            $row[] = $trans["date_start"];
            if ($trans["due"] === null) {
                $row[] = 'LIBRARY';
            } else {
                $row[] = 'ON LOAN';
            }
            $row[] = $trans["borid"];
            $row[] = $trans["borname"];
            $row[] = $trans["days"];
            $rows[] = $row;
        }
        //print_r($rows);

        return $rows;
    }

    public static function part($partid) {
        $part = array();
        include('../connect.php');
        $sql = "select parts.*,  (b.firstname || ' ' || b.surname) as borname, t.toyname as toyname 
            from parts 
            left join borwrs b  on parts.borcode = b.id
            left join toys t on parts.itemno = t.idcat
            where parts.id = ?;";

        $pdo = new PDO($connect_pdo, $dbuser, $dbpwd);
        $sth = $pdo->prepare($sql);
        $array = array($partid);
        $sth->execute($array);
        $result = $sth->fetchAll();
        $numrows = $sth->rowCount();
        $stherr = $sth->errorInfo();
        if ($stherr[0] != '00000') {
            $status .= "An  error occurred " . $stherr[0] . " " . $stherr[1] . "" . $stherr[2];
        }
        for ($ri = 0; $ri < $numrows; $ri++) {
            $part = $result[$ri];
        }
        return $part;
    }

    public static function reservation($rid) {
        $r = array();
        include('../../connect/connect_local.php');
        $sql = "select id, date_start,date_end,idcat, member_id as borid  
            from reserve_toy  
            where id = ?;";

        $pdo = new PDO($connect_pdo, $dbuser, $dbpwd);
        $sth = $pdo->prepare($sql);
        $array = array($rid);
        $sth->execute($array);
        $result = $sth->fetchAll();
        $numrows = $sth->rowCount();
        $stherr = $sth->errorInfo();
        if ($stherr[0] != '00000') {
            $status .= "An  error occurred " . $stherr[0] . " " . $stherr[1] . "" . $stherr[2];
        }
        for ($ri = 0; $ri < $numrows; $ri++) {
            $r = $result[$ri];
        }
        return $r;
    }

    public static function part_type_select($part_type) {

        include('../../connect/connect_local.php');
        $sql = "SELECT typepart FROM typepart where picture is null ORDER by typepart;";
        $str = '';
        $pdo = new PDO($connect_pdo, $dbuser, $dbpwd);
        $sth = $pdo->prepare($sql);
        $array = array();
        $sth->execute($array);
        $result = $sth->fetchAll();
        $numrows = $sth->rowCount();
        $stherr = $sth->errorInfo();
        if ($stherr[0] != '00000') {
            $str .= "An  error occurred " . $stherr[0] . " " . $stherr[1] . "" . $stherr[2];
        }
        $str = "<b>Select Part Type:</b><br>";
        $str .= "<select required='required' name='up1_type' id='up1_type' style='min-width: 170px;' class='form-control'  onchange='up1_alert(this.value);'>\n";
        for ($ri = 0; $ri < $numrows; $ri++) {
            $pt = $result[$ri];
            $str .= "<option value='" . $pt['typepart'] . "'>" . $pt['typepart'] . "</option>\n";
        }
        $str .= "<option value='" . $part_type . "' selected='selected'>" . $part_type . "</option>\n";
        $str .= "</select>\n";
        return $str;
    }

    public static function fetch_all($idcat) {
        include('../../connect/connect_local.php');
        $m = array();
        $m['error'] = '';

        $sql = "SELECT * from toys 
            where toys.idcat = ? limit 1;";

        $pdo = new PDO($connect_pdo, $dbuser, $dbpwd);
        $sth = $pdo->prepare($sql);
        $array = array($idcat);
        $sth->execute($array);


        $result = $sth->fetchAll();
        $numrows = $sth->rowCount();
        $stherr = $sth->errorInfo();
        if ($stherr[0] != '00000') {
            $m['error'] = "An  error occurred " . $stherr[0] . " " . $stherr[1] . "" . $stherr[2];
        }
        if ($numrows == 1) {

            $r = $result[0];
            $m['toyname1'] = $r['toyname'];
            $m['idcat'] = $r['idcat'];
            $m['no_pieces'] = $r['no_pieces'];
            $m['date_purchase'] = $r['date_purchase'];
            $m['desc1'] = $r['desc1'];
            $m['desc2'] = $r['desc2'];
            $m['cost'] = $r['discountcost'];
            $m['discountcost'] = $r['cost'];
            $m['rent'] = $r['rent'];
            $m['condition'] = $r['reservedfor'];
            $m['sponsorname'] = $r['sponsorname'];
            $m['keywords'] = $r['twitter'];
            $m['link'] = $r['link'];
            $m['alert'] = $r['alert'];
            $m['reserve'] = 'Yes';
            $m['toy_status'] = $r['toy_status'];
            $m['returndateperiod'] = $r['returndateperiod'];
            $m['manufacturer'] = $r['manufacturer'];
            $m['supplier'] = $r['supplier'];
            $m['stocktake_status'] = $r['stocktake_status'];
            $m['comments1'] = $r['comments'];
            $m['user1'] = $r['user1'];
            $m['warnings1'] = $r['warnings'];
            $m['age'] = $r['age'];
            $m['loan_type'] = $r['loan_type'];
            $m['storage'] = $r['storage'];
            $m['datestocktake'] = $r['datestocktake'];
            $m['lockreason'] = $r['lockreason'];
            $m['block_image'] = $r['block_image'];
            $m['toyname'] = $r['toyname'];
            $m['freight'] = $r['freight'];
            $m['process_time'] = $r['process_time'];
            $m['packaging_cost'] = $r['packaging_cost'];
            $m['sub_category'] = $r['sub_category'];
            $m['category1'] = $r['category'];
            $m['location'] = $r['location'];
            $m['result'] = 'ok';
        } else {
            $m['result'] = 'error';
            $m['error'] = 'No Toy Found';
        }

        return $m;
    }

    public static function get_all_ages() {
        include('../connect/connect_local.php');

        $sql = "select * from age order by age;";
        $pdo = new PDO($connect_pdo, $dbuser, $dbpwd);
        $sth = $pdo->prepare($sql);
        $array = array();
        $sth->execute($array);

        $result = $sth->fetchAll();
        $numrows = $sth->rowCount();
        $stherr = $sth->errorInfo();
        if ($stherr[0] != '00000') {
            $status .= "An  error occurred " . $stherr[0] . " " . $stherr[1] . "" . $stherr[2];
        }
        $ages = array();
        for ($ri = 0; $ri < $numrows; $ri++) {
            $age = $result[$ri];
            $ages[] = $age[1];
        }

        return $ages;
    }

    public static function get_fav($idcat, $borid) {
        include('../../connect.php');
        $fav = 0;
        $sql = 'select count(id) as total from favs where borid = ?  and idcat = ? group by idcat;';
        $pdo = new PDO($connect_pdo, $dbuser, $dbpwd);
        $sth = $pdo->prepare($sql);
        $array = array($borid, $idcat);
        $sth->execute($array);
        $result = $sth->fetchAll();
        $numrows = $sth->rowCount();
        $stherr = $sth->errorInfo();
        if ($stherr[0] != '00000') {
            $status .= "An  error occurred " . $stherr[0] . " " . $stherr[1] . "" . $stherr[2];
        }
        for ($ri = 0; $ri < $numrows; $ri++) {
            $r = $result[$ri];
            $fav = $r['total'];
        }
        return $fav;
    }

    public static function get_all_locations() {
        include('../connect/connect_local.php');

        $sql = "select id, location,description from location order by location;";
        $pdo = new PDO($connect_pdo, $dbuser, $dbpwd);
        $sth = $pdo->prepare($sql);
        $array = array();
        $sth->execute($array);

        $result = $sth->fetchAll();
        $numrows = $sth->rowCount();
        $stherr = $sth->errorInfo();
        if ($stherr[0] != '00000') {
            $status .= "An  error occurred " . $stherr[0] . " " . $stherr[1] . "" . $stherr[2];
        }
        $ages = array();
        for ($ri = 0; $ri < $numrows; $ri++) {
            $age = $result[$ri];
            $ages[] = $age[1];
        }

        return $ages;
    }

    public static function get_all_storages() {
        include('../connect/connect_local.php');

        $sql = "SELECT id, storage FROM storage ORDER by storage ASC;";
        $pdo = new PDO($connect_pdo, $dbuser, $dbpwd);
        $sth = $pdo->prepare($sql);
        $array = array();
        $sth->execute($array);

        $result = $sth->fetchAll();
        $numrows = $sth->rowCount();
        $stherr = $sth->errorInfo();
        if ($stherr[0] != '00000') {
            $status .= "An  error occurred " . $stherr[0] . " " . $stherr[1] . "" . $stherr[2];
        }
        $ages = array();
        for ($ri = 0; $ri < $numrows; $ri++) {
            $age = $result[$ri];
            $ages[] = $age[1];
        }

        return $ages;
    }

    public static function get_all_conditions() {
        include('../connect/connect_local.php');

        $sql = "SELECT id, condition FROM condition ORDER by condition ASC;";
        $pdo = new PDO($connect_pdo, $dbuser, $dbpwd);
        $sth = $pdo->prepare($sql);
        $array = array();
        $sth->execute($array);

        $result = $sth->fetchAll();
        $numrows = $sth->rowCount();
        $stherr = $sth->errorInfo();
        if ($stherr[0] != '00000') {
            $status .= "An  error occurred " . $stherr[0] . " " . $stherr[1] . "" . $stherr[2];
        }
        $ages = array();
        for ($ri = 0; $ri < $numrows; $ri++) {
            $age = $result[$ri];
            $ages[] = $age[1];
        }

        return $ages;
    }

    public static function get_all_sub_cats() {
        include('../connect/connect_local.php');

        $sql = "SELECT s.id as id, sub_category, s.description as description, c.category, c.description as cat_desc
                FROM sub_category s
                left join category c on c.category = s.category
                ORDER by category, sub_category ASC;";
        $pdo = new PDO($connect_pdo, $dbuser, $dbpwd);
        $sth = $pdo->prepare($sql);
        $array = array();
        $sth->execute($array);

        $result = $sth->fetchAll();
        $numrows = $sth->rowCount();
        $stherr = $sth->errorInfo();
        if ($stherr[0] != '00000') {
            $status .= "An  error occurred " . $stherr[0] . " " . $stherr[1] . "" . $stherr[2];
        }
        $ages = array();
        for ($ri = 0; $ri < $numrows; $ri++) {
            $age = $result[$ri];
            $ages[] = $age[1];
        }

        return $ages;
    }

    public static function get_all_loan_types() {
        include('../connect/connect_local.php');

        $sql = "SELECT id, stype, description  FROM stype ORDER by stype ASC;";
        $pdo = new PDO($connect_pdo, $dbuser, $dbpwd);
        $sth = $pdo->prepare($sql);
        $array = array();
        $sth->execute($array);

        $result = $sth->fetchAll();
        $numrows = $sth->rowCount();
        $stherr = $sth->errorInfo();
        if ($stherr[0] != '00000') {
            $status .= "An  error occurred " . $stherr[0] . " " . $stherr[1] . "" . $stherr[2];
        }
        $types = array();
        for ($ri = 0; $ri < $numrows; $ri++) {
            $type = $result[$ri];
            $types[] = $type;
        }

        return $types;
    }

    public static function get_all_categories() {
        include('../connect/connect_local.php');

        $sql = "SELECT id, category, description  FROM category ORDER by category ASC;";
        $pdo = new PDO($connect_pdo, $dbuser, $dbpwd);
        $sth = $pdo->prepare($sql);
        $array = array();
        $sth->execute($array);

        $result = $sth->fetchAll();
        $numrows = $sth->rowCount();
        $stherr = $sth->errorInfo();
        if ($stherr[0] != '00000') {
            $status .= "An  error occurred " . $stherr[0] . " " . $stherr[1] . "" . $stherr[2];
        }
        $types = array();
        for ($ri = 0; $ri < $numrows; $ri++) {
            $type = $result[$ri];
            $types[] = $type;
        }

        return $types;
    }

}
