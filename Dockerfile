FROM php:7.2-apache

RUN apt-get update && apt-get install -y libpq-dev iputils-ping netcat vim
RUN docker-php-ext-install pdo pdo_pgsql pgsql

COPY php.ini-sample /usr/local/etc/php/php.ini